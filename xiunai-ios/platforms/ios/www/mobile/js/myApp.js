/*
    Author : Han-sik Choi
    Blog   : http://hans.or.kr
    E-Mail : 74hans@gmail.com
    Date   : 2014-07-22
*/

var app = angular.module('ngTemplate', []);

app.controller('MainCtrl', function($scope) {
    $scope.templates = [
        { name: 'template1.html', url: 'template1.html'},
        { name: 'template2.html', url: 'template2.html'},
        { name: 'template3.html', url: 'template3.html'},
        { name: 'template4.html', url: 'template4.html'}
    ];
});