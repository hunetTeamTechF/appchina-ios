// 실제 도메인
var hunetSiteUrl = "http://www.xiunaichina.com";
var LmsUrl = "http://lms.xiunaichina.com";

// 개발용 도메인
//var hunetSiteUrl = "http://172.20.80.100:8089";
//var LmsUrl = "http://172.20.80.100:8088";
var AppVersion = "1.0.0.0";
var AppType = "I";  // I ios , A android
var AppCode = "001";
var user_id = "";
var user_pw = "";

function nl2br(str, is_xhtml) {
  var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>'; // Adjust comment to avoid issue on phpjs.org display
  return (str + '')
    .replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function mobileLoading() {
	$.mobile.loading( 'show', {
		text: '加载中...',
		textVisible: true,
		theme: 'b',
		html: ""
	});
}

function GetVersion() {
	$.getJSON(LmsUrl+"/MobileBm/GetVersion?AppType="+AppType+"&AppCode="+AppCode+"&callback=?", function (data) {
		if (AppVersion!=data)
		{
			//alert("有新的版本，请先更新后使用!");
		}
	});
}

function makeView(obj) {
  try {
	var data = '';
	for (var attr in obj) {
		if (attr=="visit_person_no" || attr=="cust_person_no")
		{
			$("#hdvl1").val(obj[attr]);
		}

		var n2brString = nl2br(obj[attr]).replace("null","");
		
        if (attr=="start_date_string" || attr=="end_date_string") {
            $("."+attr).html(obj[attr].replace("T"," "));
        } else {
            $("."+attr).html(n2brString);
        }
        
        $("#"+attr).val(obj[attr]);
		
	}
  } catch (e) {
	alert(e.message);
  }
}



function makeNView(obj) {
    try {
        var data = '';
        for (var attr in obj) {
            if (attr=="visit_person_no" || attr=="cust_person_no")
            {
                $("#hdvl1").val(obj[attr]);
            }
            
            var n2brString = nl2br(obj[attr]).replace("null","");
            
            if (attr=="start_date_string" || attr=="end_date_string") {
                $("."+attr).html(obj[attr].replace("T"," "));
            } else {
                if (attr!="ckbx_string") {
                    $("."+attr).html(n2brString);
                }
            }
            
            if (attr!="ckbx_string") {
                $("#"+attr).val(obj[attr]);
            }
            
            
        }
    } catch (e) {
        alert(e.message);
    }
}






function GetCompanySelect() {
	mobileLoading();
	$.getJSON(LmsUrl+"/MobileBm/GetCompanySelect?&callback=?", function (data) {
		$('#bm_company_seq').html(data);
		$.mobile.loading('hide');
	});
}

function GetMemberList(valueId) {
	mobileLoading();
	$.getJSON(LmsUrl+"/MobileBm/GetMemberSelect?&callback=?", function (data) {
		$('#'+valueId).append(data);
		$.mobile.loading('hide');
	});
}

function GetMemberList_byCompany(valueId,visit_person_no,bm_company_seq) {
	mobileLoading();

	$.getJSON(LmsUrl+"/MobileBm/GetMemberSelectByCompany?visit_person_no="+visit_person_no+"&bm_company_seq="+bm_company_seq+"&callback=?", function (data) {
		$('#'+valueId).html("<option value=''>选择对方相关人</option>");
		$('#'+valueId).append(data);
		$.mobile.loading('hide');
		$("select").selectmenu("refresh",true);
	});
}

function GetBusiness_typeSelect(valueId) {
	mobileLoading();
	$.getJSON(LmsUrl+"/MobileBm/GetBusiness_typeSelect?&callback=?", function (data) {
		$('#'+valueId).append(data);
		$("#"+valueId).selectmenu("refresh");
		$.mobile.loading('hide');
	});
}

// tcn_code 기반의 selectBox만들기
function GetMakeSelectBox(valueId,parent_cd) {
	mobileLoading();
	$.getJSON(LmsUrl+"/MobileBm/GetMakeSelectOptions?parent_cd="+parent_cd+"&callback=?", function (data) {
		$('#'+valueId).append(data);
		$("#"+valueId).selectmenu("refresh");
		$.mobile.loading('hide');
	});
}

function alertp(message)
{
	$("#errMsg").html(message);
	$.mobile.changePage($("#errAlert"),{transition:"pop",role:"dialog"});
	return;
}

function checkPreAuth() {
	var username = window.localStorage["username"];
	var userpw = window.localStorage["password"];
	user_id = username;
	user_pw = userpw;
	if (window.localStorage["username"] == undefined || window.localStorage["username"] == "") {
		location.href="login.html";
	} else {
	//	alert(username);
	}
}


function logout() {
	window.localStorage.removeItem("username");
	window.localStorage.removeItem("password");
	location.href="login.html";
}

function back() {
	history.go(-1);
}

$( document ).on( "pagecreate", function() {
	$( "body > [data-role='panel']" ).panel();
	$( "body > [data-role='panel'] [data-role='listview']" ).listview();
	$("#inside-a").load('./layout/LeftMenu.html', function(){$(this).trigger("create")});
});

$( document ).one( "pageshow", function() {
	$( "body > [data-role='header']" ).toolbar();
	$( "body > [data-role='header'] [data-role='navbar']" ).navbar();
});

