//
//  NoticeViewController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 6. 14..
//  Copyright 2011 hunet. All rights reserved.
//

#import "NoticeViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "ModalAlert.h"
#import "ViewerViewController.h"
#import "CounselViewController.h"
#import "GraphicsUtile.h"

@implementation NoticeViewController
@synthesize url;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        self.title = NSLocalizedString(@"公告", @"公告");
        self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_lecturelist"];
    }
    return self;
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    if ([viewWeb.subviews count] > 0) {
        UIView *scrollView = viewWeb.subviews[0];
        
        for (UIView *childView in scrollView.subviews) {
            if ([childView isKindOfClass:[UIWebDocumentView class]]) {
                UIWebDocumentView *documentView = (UIWebDocumentView *)childView;
                WebScriptObject *wso = documentView.webView.windowScriptObject;
                
                [wso setValue:[WebScriptBridge getWebScriptBridge] forKey:@"ScriptBridge"];
            }
        }
    }
	[indicator stopAnimating];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
	[indicator startAnimating];
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
//	[indicator stopAnimating];
//    NSLog(@"error : %i", error.code);
//	[ModalAlert notify:@"네트워크 연결에 문제가 생겼습니다."];	
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	
	
	NSURL *attachUrl = request.URL;
	NSString *urlString = [attachUrl.absoluteString uppercaseString];

	if ([urlString rangeOfString:@".PDF"].location != NSNotFound
		|| [urlString rangeOfString:@".PNG"].location != NSNotFound
		|| [urlString rangeOfString:@".JPG"].location != NSNotFound 
		|| [urlString rangeOfString:@".GIF"].location != NSNotFound 
		|| [urlString rangeOfString:@".JPEG"].location != NSNotFound){
		
		ViewerViewController *viewToPush = [[ViewerViewController alloc] initWithNibName:@"ViewerViewController" bundle:nil];
		if (viewToPush) {
			viewToPush.url = urlString;
			[self.navigationController pushViewController:viewToPush animated:YES];
		}
		
		return NO;
	}

	return YES;
}

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        // Custom initialization.
    }
    return self;
}
*/


- (void)viewDidLoad {
	/*
	MLCenterForBusinessAppDelegate* app = [[UIApplication sharedApplication] delegate];	
	NSString *urlText = [NSString stringWithFormat:@"%@?type=13&uid=%@&pw=%@", app.urlBase, [app.profileUser objectForKey:@"userid"], [app.profileUser objectForKey:@"password"]];
	[viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlText]]];
    [super viewDidLoad];
	 */
    if ([[self.navigationController.viewControllers objectAtIndex:0] isKindOfClass:[CounselViewController class]])
    {
        CounselViewController *view = [self.navigationController.viewControllers objectAtIndex:0];
        view.linkUrl = @"";
    }
	
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    NSString *urlText = [NSString stringWithFormat:@"%@?type=13&uid=%@&pw=%@", app.urlBase, [app.profileUser objectForKey:@"userid"], [app.profileUser objectForKey:@"password"]];
    [viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];
    
    [super viewDidLoad];
    
	
//	[viewWeb loadRequest:[NSURLRequest requestWithURL:
//						  [NSURL URLWithString:self.url] cachePolicy: NSURLRequestReloadIgnoringCacheData
//                                      timeoutInterval:60.0]];
//    
//    
//    [super viewDidLoad];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (NSUInteger)supportedInterfaceOrientations
{
    indicator.center = viewWeb.center;
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskAllButUpsideDown;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    
	viewWeb = nil;
	indicator = nil;
	self.url = nil;
    
    [super viewDidUnload];
}


- (void)dealloc {
	viewWeb.delegate = nil;
}


@end
