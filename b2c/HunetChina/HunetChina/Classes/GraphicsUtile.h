//
//  GraphicsUtile.h
//  MLCenterForBusiness
//
//  Created by Park SeungKyun on 2015. 7. 24..
//
//

#import <Foundation/Foundation.h>

@interface GraphicsUtile : NSObject

+ (void)setNavigationBarColor:(UIColor *)barColor;
+ (void)setTabBarColor:(UITabBar *)tabBar barColor:(UIColor *)barColor selectedIconColor:(UIColor *)selectedIconColor unselectedIconColor:(UIColor *)unselectedIconColor;
+ (UIImage *)imageWithColor:(UIImage *)image color:(UIColor *)color whiteSpaceHeight:(CGFloat)whiteSpaceHeight;
+ (UIImage *)imageWithScaling:(NSString*)imageName;
+ (UIImage *)imageWithScaling:(NSString*)imageName scale:(float)scale;

@end
