
#import "DownloadIndexViewController.h"
#import "DownloadCourseCell.h"
#import "DownloadStudyCell.h"
#import "ModalAlert.h"
#import "Util.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "MoviePlayerController.h"

@implementation DownloadIndexViewController

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (void)coredata
{
    self.courseArray = [Util getCourse:self.coursecd andTakeCourseSeq:self.takecourseseq];
    self.studyArray = [Util getStudy:self.coursecd andTakeCourseSeq:self.takecourseseq];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = NSLocalizedString(@"下载中心", @"下载中心");

    
    self.tableview.dataSource = self;
    self.tableview.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self coredata];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.studyArray count] > 0) {
        return [self.studyArray count] + 1;;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 84;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        static NSString *CellIdentifier = @"DownloadCourseCell";
        DownloadCourseCell *cell = (DownloadCourseCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            NSArray *arr = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil];
            cell = [arr objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSManagedObject *obj = [self.courseArray objectAtIndex:indexPath.row];
        cell.courseNm.text = [obj valueForKey:@"course_nm"];
        cell.studyEndDate.text = [NSString stringWithFormat:@"%@", [obj valueForKey:@"study_end_date"]];
        if ([Util expirationDay:[obj valueForKey:@"study_end_date"]] >= 0) {
            //cell.expirationDay.text = [NSString stringWithFormat:@"(%d일 남음)", [Util expirationDay:[obj valueForKey:@"study_end_date"]]];
            cell.expirationDay.text = [NSString stringWithFormat:@"(剩余%li天)", [Util calculatePeroidOfDay:nil to:[obj valueForKey:@"study_end_date"]]];
        }
        
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"DownloadStudyCell";
        DownloadStudyCell *cell = (DownloadStudyCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            NSArray *arr = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil];
            cell = [arr objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSManagedObject *obj = [self.studyArray objectAtIndex:indexPath.row - 1];
        cell.displayNoLabel.text = [NSString stringWithFormat:@"%@课时", [obj valueForKey:@"display_no"]];
        cell.indexNmLabel.text = [NSString stringWithFormat:@"%@\n", [obj valueForKey:@"index_nm"]];
        
        cell.synButton.tag = indexPath.row - 1;
        cell.studyButton.tag = indexPath.row - 1;
        cell.delButton.tag = indexPath.row - 1;
        [cell.synButton addTarget:self action:@selector(ClickSync:) forControlEvents:UIControlEventTouchUpInside];
        [cell.studyButton addTarget:self action:@selector(ClickStudy:) forControlEvents:UIControlEventTouchUpInside];
        [cell.delButton addTarget:self action:@selector(ClickDel:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    
    return nil;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}


- (void)ClickSync:(id)sender
{
    if (self.expirationDay < 0) {
        [ModalAlert notify:@"因学习期间结束不能同步学习进度！"];
        return;
    }
#ifdef nhlife_14384
    BOOL syncContinue = [ModalAlert ask:@"在下线（无wi-fi或没有链接3G/4G网络）学习的内容不能自动存储进度内容。要进行进度同步吗？"];
    if (syncContinue == NO)
        return;
#endif
    
    UIButton *button = (UIButton*)sender;
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] maskLoading];
    [NSThread detachNewThreadSelector:@selector(sync:) toTarget:self withObject:[NSArray arrayWithObjects:
                                                                                 [NSString stringWithFormat:@"%d", button.tag], @"sync", nil]];
}

- (void)ClickStudy:(id)sender
{
    if (self.expirationDay < 0) {
        [ModalAlert notify:@"因学习期间结束不能进行学习！"];
        return;
    }
    
    UIButton *button = (UIButton*)sender;
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] maskLoading];
    [NSThread detachNewThreadSelector:@selector(sync:) toTarget:self withObject:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%d", button.tag], @"play", nil] ];
}

- (void)ClickDel:(id)sender
{
    UIButton *button = (UIButton*)sender;
    
    if ([ModalAlert ask:@"真的删除内容?"]) {
        NSManagedObject *obj = [self.studyArray objectAtIndex:button.tag];
        NSString *takeCourseSeq = [obj valueForKey:@"take_course_seq"];
        NSString *courseCd = [obj valueForKey:@"course_cd"];
        NSString *chapterNo = [obj valueForKey:@"chapter_no"];
        NSString *fileNm = [obj valueForKey:@"file_nm"];
        
        [Util deleteDocrootFile:fileNm];
        [Util deleteStudy:courseCd andTakeCourseSeq:takeCourseSeq andChapterNo:chapterNo];
        
        [self coredata];
        [self.tableview reloadData];
    }
}


- (void)sync:(NSArray*)array {
    
    @autoreleasepool {
    
        NSManagedObject *obj = [self.studyArray objectAtIndex:[[array objectAtIndex:0] intValue]];
        NSString *courseCd = [obj valueForKey:@"course_cd"];
        NSString *getTakeCourseSeq = [obj valueForKey:@"take_course_seq"];
        NSString *chapterNo = [obj valueForKey:@"chapter_no"];
        NSString *maxSec2 = [obj valueForKey:@"max_sec"];
        NSString *totalSec = [obj valueForKey:@"total_sec"];
        NSString *userId = [obj valueForKey:@"user_id"];
        NSString *fileNm = [obj valueForKey:@"file_nm"];
        NSString *indexNm = [obj valueForKey:@"index_nm"];
        
        if (![[MLCenterForBusinessAppDelegate sharedAppDelegate] isNotInternet])
        {
            //NSDictionary *result = [Util syn:courseCd andTakeCourseSeq:takeCourseSeq andChapterNo:chapterNo andMaxSec:maxSec andTotalSec:totalSec andUserId:userId];
            NSDictionary *result = [Util synV2:courseCd andTakeCourseSeq:getTakeCourseSeq andChapterNo:chapterNo andStudySec:totalSec andMarkValue:maxSec2 andUserId:userId];
            if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
                
                if ([[result objectForKey:@"max_study_sec"] intValue]>0) {
                    maxSec2 = [result objectForKey:@"max_study_sec"];
                }
                
                NSLog(@"%maxSec : %@", maxSec2);
                self.synMessage = @"学习进度同步成功！";
            } else {
                self.synMessage = @"在进行同步时出现错误。请稍后从心尝试！";
            }
        }
        else {
            self.synMessage = @"请先确认网络状态.";
        }
        
        [self performSelectorOnMainThread:@selector(downloadPlay:) withObject:[NSArray arrayWithObjects:courseCd,
                                                                               getTakeCourseSeq,
                                                                               chapterNo,
                                                                               maxSec2,
                                                                               fileNm,
                                                                               indexNm,
                                                                               [array objectAtIndex:1], nil] waitUntilDone:NO];
    }
}

- (void)downloadPlay:(NSArray*)array
{
    NSString *courseCd = [array objectAtIndex:0];
    NSString *takeCourseSeq = [array objectAtIndex:1];
    NSString *chapterNo = [array objectAtIndex:2];
    NSString *maxSec = [array objectAtIndex:3];
    NSString *fileNm = [array objectAtIndex:4];
    NSString *indexNm = [array objectAtIndex:5];
    NSString *act = [array objectAtIndex:6];
    
    
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] removemaskLoading];
    if ([act isEqualToString:@"play"]) {
        NSUserDefaults *userdeDefaults = [NSUserDefaults standardUserDefaults];
        [userdeDefaults setObject:fileNm forKey:FILENAME];
        NSLog(@"download filename = %@", [userdeDefaults objectForKey:FILENAME]);
        MoviePlayerController * movieCon = [[MoviePlayerController alloc] initWithNibName:@"MoviePlayerController" bundle:nil];
        movieCon.isStreamingPlay = FALSE;
        movieCon.currentSecond = [maxSec intValue];
        movieCon.scrollType = 0;
        movieCon.courseCd = courseCd;
        movieCon.takecourseSeq = [takeCourseSeq intValue];
        movieCon.chapterNo = chapterNo;
        movieCon.movieTitle = indexNm;
        [MLCenterForBusinessAppDelegate sharedAppDelegate].moviePlayerController = movieCon;
        [self presentViewController:movieCon animated:YES completion:nil];
    } else { //sync
        [ModalAlert notify:self.synMessage];
    }
}


@end
