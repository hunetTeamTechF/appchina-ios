    //
//  CBNavigationBar.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 5. 9..
//  Copyright 2011 hunet. All rights reserved.
//

#import "CBNavigationBar.h"


@implementation CBNavigationBar

- (void)drawRect:(CGRect)rect {
	
	self.backgroundColor = [UIColor orangeColor];
	UIImage *image = [UIImage imageNamed:@"bar_bg"];
	[image drawInRect:rect];
}

#pragma mark -
#pragma mark UINavigationDelegate Methods

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{

	NSString *title = [viewController title];
	UILabel *myTitleView = [[UILabel alloc] init];
	[myTitleView setFont:[UIFont boldSystemFontOfSize:18]];
	[myTitleView setTextColor:[UIColor yellowColor]];
	myTitleView.text = title;
	myTitleView.backgroundColor = [UIColor orangeColor];
	[myTitleView sizeToFit];
	viewController.navigationItem.titleView = myTitleView;
	
	viewController.navigationController.navigationBar.tintColor = [UIColor orangeColor];
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
}

@end
