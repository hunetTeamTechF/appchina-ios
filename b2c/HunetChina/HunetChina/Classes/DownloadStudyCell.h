//
//  DownloadStudyCell.h
//  MLCenterForBusiness
//
//  Created by 박 기복 on 13. 4. 5..
//
//

#import <UIKit/UIKit.h>

@interface DownloadStudyCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *displayNoLabel;
@property (strong, nonatomic) IBOutlet UILabel *indexNmLabel;
@property (strong, nonatomic) IBOutlet UIButton *synButton;
@property (strong, nonatomic) IBOutlet UIButton *studyButton;
@property (strong, nonatomic) IBOutlet UIButton *delButton;


@end
