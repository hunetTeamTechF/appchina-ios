//
//  BarDecorator.m
//  MLCenterForBusiness
//
//  Created by Park SeungKyun on 2015. 7. 20..
//
//

#import "BarDecorator.h"
#import "BaseViewController.h"
#import "MyViewController.h"
#import "LectureViewController.h"
#import "MoreWebViewController.h"
#import "DownloadViewController.h"
#import "Util.h"
#import "MoreViewController.h"
#import "HomeViewController.h"
#import "SelfStudyViewController.h"
#import "StudyStateViewController.h"
#import "NoticeViewController.h"
#import "QnaViewController.h"
//#import "AllianzLectureViewController.h"
#import "Header.h"
#import "GraphicsUtile.h"
#import "WebViewTestController.h"
@implementation BarDecorator

/*** 메뉴타이틀바 & 탭바색상 커스텀 예제 : /Classes/Custom/kangwonland_3856/BarDecorator.m ***/
+ (void)setting:(MLCenterForBusinessAppDelegate *) app
{
  
    
    [app.window.rootViewController removeFromParentViewController];
    if (app.tabBarController != nil) {
        app.tabBarController = nil;
    }
    app.tabBarController = [[UITabBarController alloc] init];
    app.tabBarController.delegate = app;
    app.window.rootViewController = app.tabBarController;
    
    UINavigationController *navDownload = [Util getNavigationController:[DownloadViewController class] initWithStyle:UITableViewStylePlain];
    UINavigationController *navMore = [Util getNavigationController:[MoreViewController class] withNibName:@"MoreViewController"];
    
    ////////////////////////////////////////////////
    // tabBar 버튼 갯수와 종류 셋팅
    // 2015-01-29, magic7k Refactoring
    //기본연수원 조건 분기
    
    if ([app.eduType isEqualToString:@"OnlyImagine"])
    {
        app.tabBarController.viewControllers = @[
                                                  [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                                  [[SelfStudyViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                                  [[StudyStateViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                                  navDownload,
                                                  navMore
                                                  ];
    }
    else if ([app.eduType isEqualToString:@"LearningAndImagine"])
    {
        app.tabBarController.viewControllers = @[
                                                 [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                                 //[[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                                  [[NoticeViewController alloc] initWithNibName:@"NoticeViewController" bundle:nil],
                                                  [[QnaViewController alloc] initWithNibName:@"QnaViewController" bundle:nil],
                                                  navDownload,
                                                  navMore
                                                  ];
    }
    else if ([app.eduType isEqualToString:@"OnlySangsang"])
    {
        app.tabBarController.viewControllers = @[
                                                 [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                                 [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                                 navDownload,
                                                 navMore
                                                 ];
    }
    else
    {
        app.tabBarController.viewControllers = @[
                                                  [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                                  [[LectureViewController alloc] initWithNibName:@"LectureViewController" bundle:nil],
                                                  [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                                  navDownload,
                                                  navMore
                                                  ];
    }

    //
    ////////////////////////////////////////////////
    

    /* 탭바 배경 설정 */
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
//        app.tabBarController.tabBar.tintColor = [UIColor colorWithRed:97.0/255.0 green:169.0/255.0 blue:132.0/255.0 alpha:1.0];
    }
//    [GraphicsUtile setTabBarColor:barColor selectedIconColor:selectedIconColor unselectedIconColor:unselectedIconColor];
    
    // 디폴트배경(탭바 & 네비게이션바)
    UIColor *barColor = [UIColor colorWithRed:48.0/255.0 green:70.0/255.0 blue:143.0/255.0 alpha:1.0];
    /* 네비게이션바 배경 설정 */
    [GraphicsUtile setNavigationBarColor:barColor];
    
    [app performSelector:@selector(pushServiceURL) withObject:nil afterDelay:0.5];
}

@end
