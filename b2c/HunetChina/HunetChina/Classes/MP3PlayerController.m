//
//  MP3PlayerController.m
//  Learning
//
//  Created by Joey on 2013. 12. 24..
//
//

#import "MP3PlayerController.h"
#import "Util.h"

@interface MP3PlayerController ()

@end

@implementation MP3PlayerController

NSURLConnection *theConnection;
NSURLRequest *theRequest;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        // Custom initialization
    }
    return self;
}


-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	[_indicator startAnimating];
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
	[_songData appendData:data];
	//NSLog([NSString stringWithFormat:@"Appending data - %d"], [_songData length]);
    
	//NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    if(_audioPlayer.isPlaying == NO && [_songData length] > 100000 && !isPause)
    {
        
        NSLog(@"Starting the song");
        [self startPlaying];
    }
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSLog(@"Succeeded! Received %d bytes of data",[_songData length]);
	//[connection release];
	//[songData release];
    
}

-(void) startPlaying
{
	NSError *error;
    
    AVAudioPlayer *av = [[AVAudioPlayer alloc] initWithData:_songData error:&error];
    _audioPlayer.enableRate=YES;
    [_audioPlayer setVolume:_volumeSlider.value];
	_audioPlayer = av;
    self.timer  = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                   target:self
                                                 selector:@selector(updateProgress)
                                                 userInfo:nil
                                                  repeats:YES];
    
    [_audioPlayer play];
    
    //[[AVAudioSession sharedInstance] setDelegate: self];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleInterruption:)
                                                 name: AVAudioSessionInterruptionNotification
                                               object: [AVAudioSession sharedInstance]];

    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive: YES error: nil];
    [_indicator stopAnimating];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    isPause =false;
    isSpeedPushed = false;
    isVolumePushed = false;
    
    [self SetInitSpeedControls];
    [self SetInitVolumeControls];
    [_indicator startAnimating];
    _audioPlayer.enableRate=YES;

   // NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:_mp3Url]];
    
    theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:_mp3Url]];
    
    theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];

    
    //_audioPlayer = [[AVAudioPlayer alloc] initWithData:data error:nil];
    //_audioPlayer.enableRate=YES;
    
//    self.timer  = [NSTimer scheduledTimerWithTimeInterval:1.0
//                                                   target:self
//                                                 selector:@selector(updateProgress)
//                                                 userInfo:nil
//                                                  repeats:YES];
    
    
    [_audioPlayer setVolume:_volumeSlider.value];
    
    
    // 현재 볼륨 표시
    [self updateVolume];
    // 현재 스피드 표시
    [_btnSpeed setTitle:@"倍数1.0" forState:UIControlStateNormal];
    
    
    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI * -0.5);
    _volumeSlider.transform = trans;
    
    //[_audioPlayer play];
    if (theConnection)
    {
		_songData=[NSMutableData data];
        
    }
    
    //[[AVAudioSession sharedInstance] setDelegate: self];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleInterruption:)
                                                 name: AVAudioSessionInterruptionNotification
                                               object: [AVAudioSession sharedInstance]];
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive: YES error: nil];
    
}

-(void)SetInitSpeedControls{
    if (isSpeedPushed) {
        [_imgSpeedbg setHidden:false];
        [_btnSpeedUp setHidden:false];
        [_btnSpeedDown setHidden:false];
    }
    else{
        [_imgSpeedbg setHidden:true];
        [_btnSpeedUp setHidden:true];
        [_btnSpeedDown setHidden:true];
    }
}

-(void)SetInitVolumeControls{
    if (isVolumePushed) {
        [_lblVolume setHidden:false];
        [_volumeSlider setHidden:false];
    }
    else{
        [_lblVolume setHidden:true];
        [_volumeSlider setHidden:true];
    }
}

- (IBAction)PlaynPause:(id)sender {
    if(!isPause){
        isPause = true;
        UIImage * play = [UIImage imageNamed:@"play.png"];
        [_btnPlayPause setImage:play];
        [_audioPlayer pause];
    }else{
        isPause = false;
        UIImage * pause = [UIImage imageNamed:@"pause.png"];
        [_btnPlayPause setImage:pause];
        [_audioPlayer play];
    }
}

- (IBAction)next:(id)sender {
    [_audioPlayer stop];
    //NSLog(@"%f", _progressSlider.value);
    double seekTime = (_progressSlider.value*_audioPlayer.duration+10) > (_progressSlider.value*_audioPlayer.duration+10) ? (_progressSlider.value)*_audioPlayer.duration : (_progressSlider.value*_audioPlayer.duration+10);
    [_audioPlayer setCurrentTime:seekTime];
    [_audioPlayer prepareToPlay];
    [_audioPlayer play];
}

- (IBAction)rewind:(id)sender {
    [_audioPlayer stop];
    double seekTime = (_progressSlider.value*_audioPlayer.duration-10) < 0.0 ? 0.0 : (_progressSlider.value*_audioPlayer.duration-10);
    [_audioPlayer setCurrentTime:seekTime];
    [_audioPlayer prepareToPlay];
    [_audioPlayer play];
}

- (IBAction)adjustVolume:(id)sender {
    [self updateVolumeProgress];
}


- (IBAction)volumeSpeakerTouch:(id)sender {
    if (isSpeedPushed) {
        isSpeedPushed = false;
        [self SetInitSpeedControls];
    }
    
    if (!isVolumePushed) {
        isVolumePushed = true;
    }
    else{
        isVolumePushed = false;
    }
    [self SetInitVolumeControls];
}


- (IBAction)btnSpeed:(id)sender {
    if (isVolumePushed) {
        isVolumePushed = false;
        [self SetInitVolumeControls];
    }
    
    if (!isSpeedPushed) {
        isSpeedPushed = true;
    }
    else{
        isSpeedPushed = false;
    }
    [self SetInitSpeedControls];
}


- (IBAction)speedUp:(id)sender {
    _audioPlayer.enableRate=YES;
    [_audioPlayer stop];
    [_audioPlayer setCurrentTime:(_progressSlider.value*_audioPlayer.duration)];
    [_audioPlayer prepareToPlay];
    [_audioPlayer play];
    
	if (_audioPlayer.rate > 0.1 && _audioPlayer.rate < 2.1) {
        realSpeed = _audioPlayer.rate + 0.1;
        NSString *speed= [[NSString stringWithFormat:@"倍数%f", realSpeed] substringToIndex:5];
        [_btnSpeed setTitle:speed forState:UIControlStateNormal];
        _audioPlayer.rate = realSpeed;
        
    }
    NSLog(@"%f", realSpeed);
}

- (IBAction)speedDown:(id)sender {
    _audioPlayer.enableRate=YES;
    [_audioPlayer stop];
    [_audioPlayer setCurrentTime:(_progressSlider.value*_audioPlayer.duration)];
    [_audioPlayer prepareToPlay];
    [_audioPlayer play];
    
    if (_audioPlayer.rate > 0.3) {
        realSpeed = _audioPlayer.rate - 0.1;
        NSString *speed= [[NSString stringWithFormat:@"倍数%f", realSpeed] substringToIndex:5];
        [_btnSpeed setTitle:speed forState:UIControlStateNormal];
        _audioPlayer.rate = realSpeed;
       
    }
    NSLog(@"%f", realSpeed);
}


- (void)updateVolume {
    NSString *volumStr = [[[NSString stringWithFormat:@"%f",(_audioPlayer.volume/0.0625)] componentsSeparatedByString:@"."] objectAtIndex:0];
    _lblVolume.text =  volumStr;
}

- (void)updateVolumeProgress {
    float volumeProgress = _volumeSlider.value;
    NSString *volumStr = [[[NSString stringWithFormat:@"%f",(_audioPlayer.volume/0.0625)] componentsSeparatedByString:@"."] objectAtIndex:0];
    _lblVolume.text =  volumStr;
    _audioPlayer.volume = volumeProgress;
}

-(void)updateProgress{
    float progress = [_audioPlayer currentTime]/[_audioPlayer duration];
    _lblPast.text = [Util timeMmddss:(int)[_audioPlayer currentTime]];
    _lblLeft.text = [Util timeMmddss:(int)([_audioPlayer duration]-[_audioPlayer currentTime])];
    _progressSlider.value = progress;
    
}

- (IBAction)adjustProgress:(id)sender {
    [_audioPlayer stop];
    [_audioPlayer setCurrentTime:(_progressSlider.value*_audioPlayer.duration)];
    [_audioPlayer prepareToPlay];
    [_audioPlayer play];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return NO;
}

// New Autorotation support for iOS 6.
- (BOOL)shouldAutorotate NS_AVAILABLE_IOS(6_0);
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    _indicator.center = _bgView.center;
    return UIInterfaceOrientationMaskPortrait;
}



- (void)didReceiveMemoryWarning
{
    [self setBtnClose:nil];
    [self setLblPast:nil];
    [self setLblLeft:nil];
    [self setBtnPlayPause:nil];
    [self setVolumeSlider:nil];
    [self setProgressSlider:nil];
    [self setNext:nil];
    [self setLblVolume:nil];
    [self setBtnSpeed:nil];
    [self setImgSpeedbg:nil];
    [self setBtnSpeedDown:nil];
    [self setBtnSpeedUp:nil];
    [self setIndicator:nil];
    [self setBgView:nil];
    [self setAudioPlayer:nil];
    [self setSongData:nil];
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setBtnClose:nil];
    [self setLblPast:nil];
    [self setLblLeft:nil];
    [self setBtnPlayPause:nil];
    [self setVolumeSlider:nil];
    [self setProgressSlider:nil];
    [self setNext:nil];
    [self setLblVolume:nil];
    [self setBtnSpeed:nil];
    [self setImgSpeedbg:nil];
    [self setBtnSpeedDown:nil];
    [self setBtnSpeedUp:nil];
    [self setIndicator:nil];
    [self setBgView:nil];
    [self setAudioPlayer:nil];
    [self setSongData:nil];
    
    [super viewDidUnload];
}


- (IBAction)closePlayer:(id)sender {
    [_audioPlayer stop];
    [self dismissViewControllerAnimated:YES completion:nil];
    //[self viewDidUnload];
}
@end
