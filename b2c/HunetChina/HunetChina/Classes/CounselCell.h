//
//  CounselCell.h
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 5. 16..
//  Copyright 2011 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CounselCell : UITableViewCell {
	IBOutlet UIImageView *viewImage;
}

@property (nonatomic, strong) IBOutlet UIImageView *viewImage;
@end
