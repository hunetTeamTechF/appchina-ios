//
//  DrmManager.h
//  MLCenterForBusiness
//
//  Created by Park SeungKyun on 2015. 9. 11..
//
//

#import <Foundation/Foundation.h>
#import <HDRM/HDRM.h>

#define kDrmPrefix @"HunetDRM"
#define kDrmFolderName @"hunet_docroot"
#define kDrmEnctyptBytesSize @"1000"

#define FILENAME                @"filename"
#define DOCSFOLDER				[NSHomeDirectory() stringByAppendingPathComponent: @"Documents"]
#define PREFERENCE				[NSHomeDirectory() stringByAppendingPathComponent: @"Library/Preferences"]
#define TEMP					[NSHomeDirectory() stringByAppendingPathComponent: @"tmp"]
#define DOWNLOAD				[NSHomeDirectory() stringByAppendingPathComponent: @"Documents/Download"]
#define CACHESFOLDER			[NSHomeDirectory() stringByAppendingPathComponent: @"Documents/ImgCaches"]
#define BASERATE                1
#define AVFILENM				@"playAv"
#define PLAYER_JUMP_TIME        10

@interface DrmManager : NSObject

@property (nonatomic, strong) HdrmServer *hdrmServer;

// 암호화된 Bytes를 반환합니다.
+ (NSData*)encrypt:(NSData*)firstData totalBytes:(UInt64)totalBytes;

// HTTP서버를 시작합니다.
// 로컬파일경로를 인자로 받아 http주소를 반환합니다.
- (NSURL*)startHttpServer:(NSString*)fileName;

// HTTP서버를 종료합니다.
- (void)stopHttpServer;

@end
