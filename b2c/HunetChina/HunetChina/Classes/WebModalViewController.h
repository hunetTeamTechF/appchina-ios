
#import <UIKit/UIKit.h>
#import "PlayerCallFun.h"
#import "DownloadingView.h"

@interface WebModalViewController : UIViewController <UIWebViewDelegate, DownloadingViewDelegate>
{
    PlayerCallFun *playerCallFun;
}
@property (nonatomic, strong) PlayerCallFun *playerCallFun;
@property (strong, nonatomic) IBOutlet UILabel *LabelNaviTitle;
@property (strong, nonatomic) IBOutlet UIImageView *ImageViewNavi;
@property (strong, nonatomic) IBOutlet UIWebView *WebView;
@property (strong, nonatomic) IBOutlet UIButton *ButtonClose;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *Indicator;
//@property (nonatomic, strong) NSURLRequest *webViewRequest;
@property (retain, strong) NSString *webViewUrl;
@property (strong, nonatomic) IBOutlet UIView *ViewNavi;
@property BOOL IsHiddenNaviBar;

- (IBAction)ButtonCloseClicked:(id)sender;
- (void)processAction;
- (void)setHiddenNaviBar:(BOOL) hidden;

@end
