//
//  LectureViewController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 5. 31..
//  Copyright 2011 hunet. All rights reserved.
//

#import "LectureViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "ModalAlert.h"
#import "SSViewController.h"
#import "Header.h"
#import "PlayerCallFun.h"
#import "GraphicsUtile.h"

@implementation LectureViewController
@synthesize _currentUrl, _param;
@synthesize playerCallFun = _playerCallFun;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        self.title = NSLocalizedString(@"教育课程", @"教育课程");
        self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_lecturelist"];
    }
    return self;
}

//- (id)setTitle:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil title:(NSString*)title
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self != nil) {
//        self.title = NSLocalizedString(title, title);
//        self.tabBarItem.image = [UIImage imageNamed:@"ti_lecturelist"];
//    }
//    return self;
//}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    [indicator stopAnimating];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
    if ([viewWeb.subviews count] > 0) {
        UIView *scrollView = viewWeb.subviews[0];
        
        for (UIView *childView in scrollView.subviews) {
            if ([childView isKindOfClass:[UIWebDocumentView class]]) {
                UIWebDocumentView *documentView = (UIWebDocumentView *)childView;
                WebScriptObject *wso = documentView.webView.windowScriptObject;
                
                [wso setValue:[WebScriptBridge getWebScriptBridge] forKey:@"ScriptBridge"];
            }
        }
    }
	[indicator startAnimating];
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
	[indicator stopAnimating];	
	//[ModalAlert notify:@"네트워크 연결에 문제가 생겼습니다."];	
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
	NSURL *url = request.URL;
	NSString *urlString = url.absoluteString.lowercaseString;

    /*
	if ([urlString rangeOfString:@"/Lecture/"].location != NSNotFound) {
        self._currentUrl = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
        if ([self._currentUrl isEqualToString:@"about:blank"]) {
            self._currentUrl = @"";
        }
	}
	*/
    
	//Home
	if ([urlString.lowercaseString rangeOfString:@"/home/home.aspx"].location != NSNotFound) {
		[app setSelectedTabIndex:0];
		return NO;
	}	
		
    if ([urlString.lowercaseString hasPrefix:@"sangsangdrmplayer://"] ||
        [urlString.lowercaseString hasPrefix:@"sangsangdownload://"] ||
        [urlString.lowercaseString hasPrefix:@"sangsangdownloadplay://"]) {
        
        NSLog(@"sangsangdrmp");
        
        NSArray *tmpArray = [urlString componentsSeparatedByString:@"://"];
        NSString *act = [tmpArray objectAtIndex:0];
        NSString *param = [tmpArray objectAtIndex:1];
        
        if ([app isWiFi]) {
            
            _playerCallFun = [[PlayerCallFun alloc] init];
            _playerCallFun.delegateViewController = self;
            
            //self.urlMark = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
            [_playerCallFun sangsangmaruInit:[NSArray arrayWithObjects:act, param, nil]];
        }
        
        return NO;
    }
    
	//상상마루
	if ([urlString.lowercaseString rangeOfString:@"/submain.aspx?type=app"].location != NSNotFound) {

//		MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
//		NSString *ssUrl = [NSString stringWithFormat:@"%@/submain.aspx?type=app",
//						   [app.urlCenter stringByReplacingOccurrencesOfString:@"http://" withString:@"http://m."]];
//		
//		if ([urlString.lowercaseString isEqualToString:ssUrl.lowercaseString]) {
//			SSViewController *viewToPush = [[[SSViewController alloc] initWithNibName:@"SSViewController" bundle:nil] autorelease];
//			if (viewToPush) {
//				viewToPush.url = urlString;
//				[self presentModalViewController:viewToPush animated:YES];
//			}
//			return NO;
//		}
		return YES;
	}
    
    //apptoapp://?schemeurl=&storeurl=
    //apptoapp://?schemeurl=hunetceo%3A%2F%2F%3Fuserid%3Daaa%26userpw%3Dbbb&storeurl=http%3A%2F%2Fapps.hunet.co.kr%2Fdown%2Fhunetceo.htm
    if ([urlString hasPrefix:@"apptoapp://?"]) {
        NSDictionary *protocalParams = [MLCenterForBusinessAppDelegate getProtocalParams:request.URL];
        NSString *schemeurl = [protocalParams objectForKey:@"schemeurl"];
        NSString *storeurl = [protocalParams objectForKey:@"storeurl"];
        BOOL isInstalled = [[UIApplication sharedApplication] openURL:[NSURL URLWithString:schemeurl]];
        if (!isInstalled) {
            // 설치 되어 있지 않습니다! 앱스토어로 안내...
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:storeurl]];
        }
        return NO;
    }
    
    //웹 로그아웃 제어
    if([urlString hasPrefix:@"logout://"])
    {
        app.flagLogin = NO;
        [app loginViewControllerDisplay];
        return NO;
    }
    
	return YES;
}

- (void)viewWillAppear:(BOOL)animated {
	
	if (animated) {
		[self viewLoad];
	}
    
	[super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];	
	[self viewLoad];
    
    NSArray *vComp = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[vComp objectAtIndex:0] intValue] >= 7) {
        [self setNeedsStatusBarAppearanceUpdate];
        self.edgesForExtendedLayout = UIRectEdgeNone;
        // iOS 7 or above
        CGRect oldBounds = [self.view bounds];
        CGRect newViewBounds = CGRectMake(0, -10, oldBounds.size.width, oldBounds.size.height - 20);
        CGRect newWebViewBounds = CGRectMake(0, -20, oldBounds.size.width, oldBounds.size.height - 40);
        if (self.view.bounds.origin.y != newViewBounds.origin.y) {
            [self.view setBounds:newViewBounds];
            [viewWeb setBounds:newWebViewBounds];
        }
    }
}


- (void)viewLoad {
	
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
	NSString *type = @"5";
	if (app.lectureUrlType != nil) {
		type = app.lectureUrlType;
	}
	
	NSString *urlText = [[NSString stringWithFormat:@"%@?type=%@&uid=%@&pw=%@", app.urlBase, type, [app.profileUser objectForKey:@"userid"], [app.profileUser objectForKey:@"password"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
    /*
	if ([self._currentUrl length] > 0) {
		urlText = self._currentUrl;
	}
    */
    
    if ([self._param length] > 0) {
        urlText = [NSString stringWithFormat:@"%@&param=%@", urlText, self._param];
    }
    
    [viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlText]]];
	viewWeb.scalesPageToFit = YES;	
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (NSUInteger)supportedInterfaceOrientations
{
    indicator.center = viewWeb.center;
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)prefersStatusBarHidden
{
    //return UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]);
    return NO;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {

	viewWeb = nil;
	indicator = nil;
	self._currentUrl = nil;
    self._param = nil;
    [super viewDidUnload];
}


- (void)dealloc {
	viewWeb.delegate = nil;
}

- (void)webViewpushUrl:(NSString *)webViewpushUrl {
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    [viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:webViewpushUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0]];
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    app.pushService = nil;
    //self.appDelegate.pushService = nil;
}

- (void)openerCallFun {
    NSLog(@"player close");
}

@end
