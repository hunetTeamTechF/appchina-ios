//
//  VersionChecher.m
//  MLCenterForBusiness
//
//  Created by Park SeungKyun on 2015. 7. 27..
//
//

#import "VersionChecher.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "Util.h"
#import "CJSONDeserializer.h"
#import "ModalAlert.h"

@implementation VersionChecher

+ (NSDictionary *)requestCurrentVersionInfo {
    
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
#ifdef PRO
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=tcenter", app.urlBase];
#elif ekdp_5034
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=ekdp", app.urlBase];
#elif lemon
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=LLemon", app.urlBase];
#endif
    
    NSDictionary *dic = nil;
    NSError *errorReq = nil;
    NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:url]
                                          andQuery:nil
                                          andError:&errorReq];
    if (jsonData)
    {
        NSError *errorJSON = nil;
        dic = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
    }
    
    return dic;
}




+ (void)checkWithUpdate {
    NSDictionary *dic = [self requestCurrentVersionInfo];
    if (dic)
    {
        NSString *latestVersion = [dic objectForKey:@"Version"];
        NSString *downLoadUrl = [dic objectForKey:@"DownloadUrl"];
        NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
        NSArray *arrLatestVersion = nil;
        NSArray *arrCurrentVersion = nil;
        NSUInteger minVersionDepth = 0;
        Boolean needUpdate = NO;
        
        if ([latestVersion rangeOfString: @"."].location != NSNotFound) {
            arrLatestVersion = [latestVersion componentsSeparatedByString:@"."];
        }
        
        if ([currentVersion rangeOfString: @"."].location != NSNotFound) {
            arrCurrentVersion = [currentVersion componentsSeparatedByString:@"."];
        }
        
        minVersionDepth = [arrCurrentVersion count] < [arrLatestVersion count] ? [arrCurrentVersion count] : [arrLatestVersion count];
        
        for (NSUInteger i = 0; i < minVersionDepth; i++) {
            if ([[arrCurrentVersion objectAtIndex:(i)] integerValue] > [[arrLatestVersion objectAtIndex:(i)]  integerValue])
            {
                break;
            }
            else if ([[arrCurrentVersion objectAtIndex:(i)] integerValue] < [[arrLatestVersion objectAtIndex:(i)]  integerValue])
            {
                needUpdate = YES;
                NSLog(@"Need Update");
                break;
            }
        }
        
        if (needUpdate == YES)
        {
            UIAlertView *alert = [[UIAlertView alloc] init];
            alert.tag = 1;
            [alert setTitle:@"版本更新"];
            [alert setMessage:@"有新的版本. 要进行更新吗？"];
            [alert setDelegate:self];
            [alert addButtonWithTitle:@"更新"];
            [alert show];
            
            
            
//            if ([ModalAlert queryWith:@"有新的版本. 要进行更新吗？" button1:@"更新" button2:nil] == 0) {
//                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:downLoadUrl]];
//                [[UIApplication sharedApplication] performSelector:NSSelectorFromString(@"terminateWithSuccess") withObject:nil afterDelay:0.5f];
//            }
        }
    }
}

+ (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0)
    {
        NSDictionary *dic = [self requestCurrentVersionInfo];
        //1 : version up, 2 : push service
        if (alertView.tag == 1) {
            NSString *downLoadUrl = [dic objectForKey:@"DownloadUrl"];
            if ([downLoadUrl length] > 0) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:downLoadUrl]];
                [[UIApplication sharedApplication] performSelector:NSSelectorFromString(@"terminateWithSuccess") withObject:nil afterDelay:0.5f];
            }
        }
    }
}

@end
