

#import <Foundation/Foundation.h>
#import "web.h"

@interface PlayerCallFun : NSObject
{
    Web *webUtil;
}
@property (nonatomic, weak) UIViewController *delegateViewController;

- (void)sangsangmaruInit:(NSArray*)array;
- (void)sangsangmaruDrm:(NSArray*)array;
- (void)sangsangmaruDrmDownload:(NSDictionary*)result;
- (void)sangsangmaruDownloadPush:(NSString*)param;
- (void)sangsangmaruDrmStreaming:(NSArray*)array;

@end
