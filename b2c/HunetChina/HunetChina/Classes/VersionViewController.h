//
//  VersionViewController.h
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 7. 6..
//  Copyright 2011 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface VersionViewController : UITableViewController {
	NSDictionary *versionInfo;
	NSString *currentVersion;
}

@property (nonatomic, strong) NSDictionary *versionInfo;
@property (nonatomic, strong) NSString *currentVersion;


@end
