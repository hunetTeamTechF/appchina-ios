
#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface SelfStudyViewController : BaseViewController
{
    
}

@property (nonatomic, strong) NSString *selfStudyUrl;
@property (nonatomic, strong) NSString *detailView;


- (void)detailView:(NSString*)url;
- (void)processAction;
@end
