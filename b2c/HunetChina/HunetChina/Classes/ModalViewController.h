//
//  ModalViewController.h
//  MLCenterForBusiness
//
//  Created by Park Gi-Bok on 12. 4. 9..
//  Copyright (c) 2012 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModalViewController : UIViewController <UIWebViewDelegate> {
    NSString *url;
    IBOutlet UIWebView *viewweb;
	IBOutlet UIActivityIndicatorView *indicator;
}

@property (nonatomic, strong) NSString *url;

- (void)indicatorCenter;

@end

