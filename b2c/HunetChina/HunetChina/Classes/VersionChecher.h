//
//  VersionChecher.h
//  MLCenterForBusiness
//
//  Created by Park SeungKyun on 2015. 7. 27..
//
//

#import <Foundation/Foundation.h>

@interface VersionChecher : NSObject

+ (NSDictionary *)requestCurrentVersionInfo;
+ (void)checkWithUpdate;

@end
