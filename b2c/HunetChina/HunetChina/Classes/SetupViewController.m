//
//  SetupViewController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 5. 17..
//  Copyright 2011 hunet. All rights reserved.
//

#import "SetupViewController.h"
#import "VolumeCell.h"
#import "MLCenterForBusinessAppDelegate.h"


@implementation SetupViewController


#pragma mark -
#pragma mark View lifecycle

- (BOOL)prefersStatusBarHidden {
    return NO;
}

/*
- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
*/


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/

/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)switchValueDidChange:(UISwitch *)sender {
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
	switch ([sender tag]) {
		case 0:
		{
            [app setNoticeOption:sender.isOn withKey:@"autologin"];
		}
			break;
		case 1:
		{
            [app setNoticeOption:sender.isOn withKey:@"threegnoti"];
		}
			break;
		case 2:
		{
            [app setNoticeOption:sender.isOn withKey:@"threegvideo"];
		}
			break;
		default:
			break;
	}
	
	 
}



#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;  //볼륨설정 기능이 필요시 3
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 1) {
		return 2;
	}
	else return 1;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
	static NSString *CellVolumeIdentifier = @"VolumeCell";
    
	if (indexPath.section > 1) {
		
		VolumeCell* cell = (VolumeCell*) [tableView dequeueReusableCellWithIdentifier:CellVolumeIdentifier];
		if (cell == nil) {
			NSArray* arr = [[NSBundle mainBundle] loadNibNamed:CellVolumeIdentifier owner:nil options:nil];
			cell = [arr objectAtIndex:0];
		}
		
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		return cell;
		
	} else {
		
		
		
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		}
		
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.accessoryView = nil;
		cell.accessoryType = UITableViewCellAccessoryNone;	
		cell.textLabel.font = [UIFont systemFontOfSize:16];
		//cell.userInteractionEnabled = NO;
		
		switch (indexPath.section) {
			case 0:
			{
				cell.textLabel.text = @"自动登录";
				
				UISwitch *switchAutoLogin = [[UISwitch alloc] initWithFrame:CGRectZero];
				switchAutoLogin.tag = 0;
				[switchAutoLogin addTarget:self action:@selector(switchValueDidChange:) forControlEvents:UIControlEventValueChanged];
				
                MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
				switchAutoLogin.on = [[app.profileUser objectForKey:@"autologin"] boolValue];
				
				cell.accessoryView = switchAutoLogin;	
								
			}
				break;
			case 1:
			{
				if (indexPath.row == 0) {
					cell.textLabel.text = @"允许3G/4G提醒";
					
					UISwitch *switchNoti = [[UISwitch alloc] initWithFrame:CGRectZero];
					switchNoti.tag = 1;
					[switchNoti addTarget:self action:@selector(switchValueDidChange:) forControlEvents:UIControlEventValueChanged];			
                    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
                    
					switchNoti.on = [[app.profileUser objectForKey:@"threegnoti"] boolValue];
					
					cell.accessoryView = switchNoti;				
					
				} else {
					cell.textLabel.text = @"允许3在3G/4G上收看视频";
					
					UISwitch *switchMov = [[UISwitch alloc] initWithFrame:CGRectZero];
					switchMov.tag = 2;
					[switchMov addTarget:self action:@selector(switchValueDidChange:) forControlEvents:UIControlEventValueChanged];			
					
                    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
					switchMov.on = [[app.profileUser objectForKey:@"threegvideo"] boolValue];
					
					cell.accessoryView = switchMov;
				}			
			}
				break;
			default:
				break;
		}
		
		return cell;
	}  
    
    
    return nil;
}


- (NSString*)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section {
	switch (section) {
		case 0:
		{
			return @"登录";
		}
			break;
		case 1:	
		{
			return @"无限网络使用与否";
		}
			break;
		case 2:		
		{
			return @"视频基本声音";
		}
			break;
		default:
			break;
	}
	return @"";
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	 [tableView deselectRowAtIndexPath:indexPath animated:YES]; 
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
    [super viewDidUnload];
}





@end

