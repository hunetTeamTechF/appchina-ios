

#import <UIKit/UIKit.h>
#import "DownloadingView.h"
#import "PlayerCallFun.h"
#import "MoviePlayerController.h"
#import "web.h"

@interface MyViewController : UIViewController <UIWebViewDelegate, DownloadingViewDelegate, MoviePlayerControllerDelegate> {
	
	IBOutlet UIWebView *viewWeb;
	IBOutlet UIActivityIndicatorView *indicator;
	NSString *urlMark;
    Web *webUtil;
    PlayerCallFun *playerCallFun;

}

@property (nonatomic, strong) NSString *urlMark;
@property (nonatomic, strong) PlayerCallFun *playerCallFun;

- (void)viewLoad;
- (void)processAction;
- (void)openerCallFun;

@end
