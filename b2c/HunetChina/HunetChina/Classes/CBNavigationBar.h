//
//  CBNavigationBar.h
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 5. 9..
//  Copyright 2011 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CBNavigationBar : UINavigationBar<UINavigationControllerDelegate>  {

}

@end
