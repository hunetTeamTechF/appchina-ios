

#import <UIKit/UIKit.h>
#import "DownloadingView.h"
#import "PlayerCallFun.h"
#import "MoviePlayerController.h"
#import "MLCenterForBusinessAppDelegate.h"

@interface BaseViewController : UIViewController <UIWebViewDelegate, DownloadingViewDelegate, MoviePlayerControllerDelegate>
{
    BOOL ISEndPoint;
    BOOL ISFirstLoad;
    UIView *addStatusBar;
    BOOL isFirstLoaded;
    NSString *currentUrl;
    PlayerCallFun *playerCallFun;
}

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (strong, nonatomic) UIView *addStatusBar;
@property (readwrite, nonatomic, assign) BOOL statusBarHidden;
@property (nonatomic, strong) NSString *currentUrl;
@property (nonatomic, strong) PlayerCallFun *playerCallFun;
- (void)processAction;
- (void)openerCallFun;
- (MLCenterForBusinessAppDelegate*)appDelegate;
- (void)settingStatusBar;
- (void)webViewpushUrl:(NSString *)webViewpushUrl;

@end
