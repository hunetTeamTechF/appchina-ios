#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "DrmManager.h"
#import "MLCenterForBusinessAppDelegate.h"

@protocol MoviePlayerControllerDelegate;
@interface MoviePlayerController : UIViewController {
    
    id __weak _delegate;
	AVPlayer *player;
    MPMusicPlayerController *iPod;
    AVPlayerLayer *playerLayer;
    
    
	IBOutlet UIView *playerView;
    IBOutlet UIView *dControllerView;
    IBOutlet UIView *uControllerView;
    IBOutlet UIView *indexControllerView;
    IBOutlet UIView *bookControllerView;
    
    IBOutlet UIButton *repeatBtn; //구간 반복 버튼.
    IBOutlet UIButton *playBtn;
    IBOutlet UIButton *viewBtn;
    IBOutlet UILabel *lProgLbl;
    IBOutlet UILabel *rProgLbl;
    IBOutlet UILabel *speedLabel;
    //IBOutlet UILabel *volumeLabel;
    IBOutlet UILabel *avTitle;
    IBOutlet UISlider *progresSlider;
    
    int plusX;
    int plusY;
    MLCenterForBusinessAppDelegate *app;
    
    //자막
    NSMutableArray *subtitles;
    int subTitlesTotalCnt;
    int subtitlesArrayIndex;
    
    
@private
	id timeObserver;
	double totalTime;
    
    BOOL playBool; //플레이 유무
    BOOL touchBool; // 화면 컨트롤러 표시 유무
    BOOL scnFullBool; // 화면 사이즈 유무
    
    bool isSpeedPushed;
    bool isVolumePushed;
    
    
    BOOL isStreamingPlay; //true:streaming false:dnFile
    
    int modeFlg; //모두 설정- 0:일반모드 1:반복모두 시작설정 2:반복모드 종료 설정.
    float realSpeed;
    float repeatStime;  // 구간반복 시작 시간.
    float repeatEtime;  // 구간반복 종료 시간.
    
    int currentSecond;          // 학습 시작 초
    int totalProgressCount;     // 60초 간격으로 진도 저장
    int scrollType;             // progress touch 여부 0 : 진도바 이동 불가, 1 : 진도바 이동 가능
    int takecourseSeq;
    BOOL movieExitBool;         //동영상 정상
    int maxSec;        //최대학습한 시간
    
    int markNo;         //markNo
    int realFrameNo;         //realFrameNo
    int markValue;         //markValue
    int markStudySec;    //markNo별 순수 학습시간
    
    int pptIndex;    //pptIndex
    NSTimer *timer;
    int pureStudySec;
    int timerCounter;
}
@property (nonatomic, strong) DrmManager *drmManager;
@property (nonatomic, weak) id <MoviePlayerControllerDelegate> delegate;
@property (nonatomic, strong) AVPlayer *player;

@property (nonatomic, strong) UIButton *playBtn;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UIImageView *imgPlayBtn;
@property (strong, nonatomic) IBOutlet UIImageView *imgBackBtn;
@property (strong, nonatomic) IBOutlet UIImageView *imgNextBtn;

@property (strong, nonatomic) IBOutlet UILabel *LblSubtitles;
@property (nonatomic, assign) BOOL playBool;
@property (nonatomic, assign) BOOL isStreamingPlay;
@property (nonatomic, assign) int currentSecond;
@property (nonatomic, assign) int scrollType;
@property (strong, nonatomic) NSString *courseCd;
@property (nonatomic, assign) int takecourseSeq;
@property (strong, nonatomic) NSString *chapterNo;
@property (nonatomic, strong) NSString *progressNo;
@property (nonatomic, assign) int frameNo;
@property (nonatomic, assign) int lastMarkNo;
@property (nonatomic, assign) int maxSec;
@property (strong, nonatomic) NSString *movieTitle;
@property (strong, nonatomic) NSArray *movContents;
@property (nonatomic, strong) NSString *evaluationProgressRatio;
@property (nonatomic, strong) NSString *sequenceProgressType;

@property (strong, nonatomic) IBOutlet UILabel *lblVolume;
@property (strong, nonatomic) IBOutlet UIButton *btnSpeed;
@property (strong, nonatomic) IBOutlet UIImageView *imgVolumeBg;

@property (strong, nonatomic) IBOutlet UIButton *imgSpeedbg;
@property (strong, nonatomic) IBOutlet UIButton *btnSpeedDown;
@property (strong, nonatomic) IBOutlet UIButton *btnSpeedUp;

@property (strong, nonatomic) IBOutlet UISlider *volumeSlider;

@property (strong, nonatomic) IBOutlet UILabel *lblClock;

@property (strong, nonatomic) IBOutlet UIButton *index;
@property (strong, nonatomic) IBOutlet UIButton *book;
@property (strong, nonatomic) IBOutlet UITableView *indexTable;
@property (strong, nonatomic) IBOutlet UIButton *indexBookCloseBtn;

//진도저장필요여부(예, 컨텐츠 모듈화과정)
@property (nonatomic, strong) NSString *progressSaveYn;

//상상마루
@property (nonatomic, assign) int cno;
@property (nonatomic, assign) int cseq;
@property (nonatomic, assign) int viewNo;
@property (nonatomic, strong) NSString *gid;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, assign) int viewSec;
@property (nonatomic, assign) int currentViewSec;
@property (nonatomic, assign) int lastViewSec;
@property (nonatomic, assign) int viewSecMobile;
@property (nonatomic, strong) NSString *passYn;
@property (nonatomic, strong) NSString *quizYn;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *pptImageUrl;
@property (nonatomic, strong) NSString *catption_yn;
@property (nonatomic, strong) NSString *catption_cn;
@property (nonatomic, strong) NSString *catption_kr;
@property (nonatomic, strong) NSString *catption_en;
@property (nonatomic, strong) NSString *currentCaption;
@property (strong, nonatomic) IBOutlet UIImageView *imgMagnify;

@property (nonatomic, strong) NSArray *sangsangArray;
@property (nonatomic, strong) NSArray *sagnsangStudyArray;
@property (nonatomic, strong) NSArray *studyArray;

@property (strong, nonatomic) IBOutlet UILabel *studyLimitMessage;
@property (strong, nonatomic) IBOutlet UIImageView *pptImageView;
@property (strong, nonatomic) IBOutlet UIImageView *bookLeftImg;
@property (strong, nonatomic) IBOutlet UIImageView *bookRightImg;
@property (strong, nonatomic) IBOutlet UIImageView *currentPlayImg;
@property (strong, nonatomic) IBOutlet UIImageView *bookCloseImg;

@property (nonatomic, strong) NSMutableArray *subtitles;
@property (nonatomic, assign) int subTitlesTotalCnt;
@property (nonatomic, assign) int subtitlesArrayIndex;


@property (strong, nonatomic) IBOutlet UIButton *btnSubtitle;

-(void)studySecondSave:(NSTimer *)timer;
-(void)avPlay;
-(void)updateVolume;
-(void)cleanup;
-(void)updateClock;

-(IBAction) sliderProgressEvent:(id)sender;
-(IBAction) playStopAction:(id)sender;
-(IBAction) frontTimeAction:(id)sender;
-(IBAction) nextTimeAction:(id)sender;
//-(IBAction) slowPlayAction:(id)sender;
//-(IBAction) fastPlayAction:(id)sender;
-(IBAction) VolumeLargeEvent:(id)sender;
-(IBAction) VolumeSmallEvent:(id)sender;
-(IBAction) screenResize:(id)sender;
-(IBAction) endAVPlay:(id)sender;
-(IBAction) sliderTouchesInsideEvent:(id)sender;
-(IBAction) sliderTouchesOutsideEvent:(id)sender;
-(IBAction) startRepeatMode:(id)sender;
-(IBAction) endRepeatMode:(id)sender;
-(IBAction) setRepeatSTime:(id)sender;
-(IBAction) setRepeatETime:(id)sender;
-(IBAction) ShowSubTitles:(id)sender;


- (IBAction)volumeSpeakerTouch:(id)sender;
- (IBAction)btnSpeedTouch:(id)sender;
- (IBAction)speedDown:(id)sender;
- (IBAction)speedUp:(id)sender;
- (IBAction)adjustVolume:(id)sender;
- (void)setVolumeCustom:(float)volumeAmount;

- (void) msgScreen;
- (void)saveMovieTime:(NSString *)movieTime;
- (void)saveMovieTimeNew:(NSArray*)array;
- (void)saveMovieTimeV2:(NSArray*)array;
- (int)seekMarkNo:(int)getCurrentSecond;
- (void)saveCoredataMovieTimeV2:(int)getStudySec andMarkValue:(int)getMarkValue;
- (void)saveMovieTimeV2Stop:(NSArray*)array;

- (IBAction)bookClick:(id)sender;
- (IBAction)indexClick:(id)sender;
- (IBAction)indexBookClose:(id)sender;
- (IBAction)bookBeforeBtn:(id)sender;
- (IBAction)bookNextBtn:(id)sender;
- (IBAction)currentBookBtn:(id)sender;
- (IBAction)bookCloseBtn:(id)sender;

@end

@protocol MoviePlayerControllerDelegate
@optional
- (void)openerCallFun;
@end


