

#import <UIKit/UIKit.h>

@interface DownloadIndexSelfViewController : UITableViewController


@property (nonatomic, strong) NSArray *courseArray;
@property (nonatomic, strong) NSArray *studyArray;

@property (nonatomic, strong) NSString *coursecd;
@property (nonatomic, strong) NSString *takecourseseq;
@property (nonatomic, assign) NSInteger expirationDay;
@property (strong, nonatomic) NSString *synMessage;
@end
