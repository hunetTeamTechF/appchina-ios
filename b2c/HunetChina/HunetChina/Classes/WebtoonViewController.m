//
//  WebtoonViewController.m
//  MLCenterForBusiness
//
//  Created by Park Gi-Bok on 12. 3. 13..
//  Copyright (c) 2012 hunet. All rights reserved.
//

#import "WebtoonViewController.h"

@implementation WebtoonViewController
@synthesize webtoonUrl;
@synthesize togle;

- (IBAction)webtoonClose {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void) webViewDidFinishLoad:(UIWebView *)webView {
	[indicator stopAnimating];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
	[indicator startAnimating];
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
	[indicator stopAnimating];	
	//[ModalAlert notify:@"네트워크 연결에 문제가 생겼습니다."];	
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}
	
- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.webtoonUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];    
	[self.viewWeb loadRequest:request];
	self.viewWeb.scalesPageToFit = YES;
    
    UITapGestureRecognizer *tabGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleDoubleTap:)];
    
    tabGesture.numberOfTapsRequired = 2;
	tabGesture.delegate = self;
    [_viewWeb addGestureRecognizer:tabGesture];
    togle = true;

}


- (IBAction)handleSingleDoubleTap:(UIGestureRecognizer *)sender {
	if([sender numberOfTouches] == 1) {
        //이벤트 발생
        CGRect newBouds = _viewWeb.bounds;
        if(togle)
        {
            togle = false;
            newBouds.size.height = _viewWeb.bounds.size.height+90;
        }
        else{
            togle = true;
            newBouds.size.height = _viewWeb.bounds.size.height-90;
        }
        _viewWeb.bounds = newBouds;
        
	}
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
	return YES;
}

- (void)viewDidUnload
{
    
	_viewWeb = nil;
	indicator = nil;
    self.webtoonUrl = nil;
    
    [super viewDidUnload];
}

- (void)dealloc {
	_viewWeb.delegate = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    indicator.center = _viewWeb.center;
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSUInteger)supportedInterfaceOrientations
{
    indicator.center = _viewWeb.center;
    //return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskAllButUpsideDown;
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return NO;
}


@end
