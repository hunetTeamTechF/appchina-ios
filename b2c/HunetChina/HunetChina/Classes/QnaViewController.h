//
//  QnaViewController.h
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 6. 14..
//  Copyright 2011 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface QnaViewController : UIViewController <UIWebViewDelegate> {
	IBOutlet UIWebView *viewWeb;
	IBOutlet UIActivityIndicatorView *indicator;
}

@property (strong, nonatomic) IBOutlet UIButton *GoNewPage;

@end
