//
//  CounselViewController.h
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 5. 13..
//  Copyright 2011 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CounselViewController : UITableViewController {
	NSString *newIcon;
	NSString *linkUrl;
	NSInteger row;
}

@property (nonatomic, strong) NSString *newIcon;
@property (nonatomic, strong) NSString *linkUrl;
@property (readwrite, assign) NSInteger row;

- (void)cellSelected;

@end
