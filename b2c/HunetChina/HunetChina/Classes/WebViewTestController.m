//
//  WebViewTestController.m
//  MLCenterForB2c
//
//  Created by 알버트 on 2016. 2. 22..
//
//

#import "WebViewTestController.h"
#import "GraphicsUtile.h"
@interface WebViewTestController ()

@end

@implementation WebViewTestController
@synthesize WebView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        self.title = NSLocalizedString(@"测试", @"厕所");
        self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_lecturelist"];
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    NSURL *myURL = [NSURL URLWithString:@"http://study.xiunaichina.com/Study/Contents/Sample/videoTest/"];
    NSURLRequest *myURLReq = [NSURLRequest requestWithURL:myURL];
    [WebView loadRequest:myURLReq];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
