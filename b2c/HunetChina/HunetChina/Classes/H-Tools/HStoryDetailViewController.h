

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface HStoryDetailViewController : BaseViewController

@property (nonatomic, strong) NSString *webviewUrl;

@end
