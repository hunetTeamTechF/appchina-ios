//
//  PlayerViewController.h
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 6. 3..
//  Copyright 2011 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@class MLCenterForBusinessAppDelegate;
@class MarkViewController;
@class OverlayView;

@interface PlayerViewController : UIViewController {
	NSString *ccd;
	NSString *tcseq;
	NSString *cno;
	NSString *fno;
	
	NSDictionary *playerInfo;
	UIActivityIndicatorView* indicator;
    
	UIImageView *viewImageppt;
    
	MarkViewController *markView;
	OverlayView *overlayView;
	NSTimer *currentTimer;
	NSUInteger lastMarkIndexNo;
    
    MLCenterForBusinessAppDelegate *appDelegate;
    NSInteger sumTimer;
	
}

@property (nonatomic, strong) NSString *ccd;
@property (nonatomic, strong) NSString *tcseq;
@property (nonatomic, strong) NSString *cno;
@property (nonatomic, strong) NSString *fno;

@property (nonatomic, strong) NSDictionary *playerInfo;
@property (nonatomic, strong) NSTimer *currentTimer;

- (void)updateForPlayerInfo;
- (void)progressSave:(int)index;
- (void) timerStart;
- (int)currentSliderPosition;

@end
