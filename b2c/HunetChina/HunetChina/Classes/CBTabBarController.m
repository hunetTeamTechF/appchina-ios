    //
//  CBTabBarController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 5. 9..
//  Copyright 2011 hunet. All rights reserved.
//

#import "CBTabBarController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "LectureViewController.h"


@implementation CBTabBarController

#pragma mark -
#pragma mark UITabBarDelegate implementation
- (void)tabBar:(UITabBar*)tabBar didSelectItem:(UITabBarItem*)item {
	
    if (![MLCenterForBusinessAppDelegate sharedAppDelegate].flagLogin) {
        [[MLCenterForBusinessAppDelegate sharedAppDelegate] loginViewControllerDisplay];
    }
    
	/*
	if ((item.tag == 0 && tagPrevious != 0) ||
		(item.tag == 1 && tagPrevious != 1) ||
		(item.tag == 2 && tagPrevious != 2) ||
		(item.tag == 4 && tagPrevious != 4) ) {		// just not the case "current tab == previous tab"
		UINavigationController* nav = [self.viewControllers objectAtIndex:item.tag]; 
		if (nav) {	// always set to the top Home menu view
			[nav popToRootViewControllerAnimated:NO];
		}
	}
	tagPrevious = item.tag;
	
		
	NSLog(@"didSelectItem : %d", item.tag);
     */
}

- (BOOL)tabBarController:(CBTabBarController *)tabBarController shouldSelectViewController:(UIViewController*)viewController {
    if (![MLCenterForBusinessAppDelegate sharedAppDelegate].flagLogin) {
        return NO;
    }
	/*
	if (tagPrevious == 2) {

		IPhoneSpicusAppDelegate* app = [[UIApplication sharedApplication] delegate];
		if ([app.keyUser isEqualToString:@""] ) { // not logged in yet
			[ModalAlert notify:@"먼저 로그인해 주시기 바립니다."];
			LoginViewController* viewLogin = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
			[[viewLogin view] setFrame:[[UIScreen mainScreen] applicationFrame]];
			viewLogin.indexNextTab = tagPrevious;
			[app.window addSubview:[viewLogin view]]; 
			return NO;
		}			
	}
	*/
	
	if ([[tabBarController viewControllers] objectAtIndex:[tabBarController selectedIndex]] == viewController) {
		//return NO;
	}
	return YES;
	
}

- (void)tabBarController:(CBTabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
	
	/*
	if (tabBarController.selectedIndex == 1) {
		
		Membership *member = [Membership MembershipInit];
		MembershipData *data = [member getMembershipData];
		
		if (!data.isLogin) {
			
			UINavigationController *navMyLectureroom = (UINavigationController *)viewController;
			LoginViewController *login = [[LoginViewController alloc] init];
			login.backTabbarIndex = self.previousTabbarItemIndex;
			[navMyLectureroom pushViewController:login animated:YES];
			[login release];
			
			//tabBarController.selectedIndex = self.previousTabbarItemIndex;
		}
	}
	*/
    
    UINavigationController *navController = (UINavigationController *)[tabBarController.viewControllers objectAtIndex:tabBarController.selectedIndex];
    if ([navController isKindOfClass:[UINavigationController class]])
    {
        [navController popToRootViewControllerAnimated:YES];
    }
    else
    {
        UIViewController *view = (UIViewController*)[tabBarController.viewControllers objectAtIndex:tabBarController.selectedIndex];
        [view viewDidLoad];
    }
}


- (void)viewDidLoad {
	[super viewDidLoad];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return [self.selectedViewController supportedInterfaceOrientations];
}

- (BOOL)shouldAutorotate
{
    return [self.selectedViewController shouldAutorotate];
}


@end
