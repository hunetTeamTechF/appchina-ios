//
//  MoreCell.h
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 5. 17..
//  Copyright 2011 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MoreCell : UITableViewCell {

}

@property (nonatomic, strong) IBOutlet UILabel *labelMenuNm;
@property (nonatomic, strong) IBOutlet UILabel *labelInfo;
@property (strong, nonatomic) IBOutlet UIImageView *accessoryImageView;

@end
