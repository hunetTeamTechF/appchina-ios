//
//  WebViewTestController.h
//  MLCenterForB2c
//
//  Created by 알버트 on 2016. 2. 22..
//
//

#import <UIKit/UIKit.h>

@interface WebViewTestController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *WebView;

@end
