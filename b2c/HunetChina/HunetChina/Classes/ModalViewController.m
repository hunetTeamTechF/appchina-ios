

#import "ModalViewController.h"
#import "ModalDetailViewController.h"
#import "Global.h"

@implementation ModalViewController
@synthesize url;

- (void)closed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
		
- (void) webViewDidFinishLoad:(UIWebView *)webView {
	[indicator stopAnimating];
    
    NSString *title = [viewweb stringByEvaluatingJavaScriptFromString:@"fnTitleRequest();"];
    self.title = title;
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
	[indicator startAnimating];
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
	[indicator stopAnimating];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
	NSString *urlString = request.URL.absoluteString;    
    if (![urlString.lowercaseString hasPrefix:[[Global sharedSingleton] testPrimaryDomainUrl:@"http://mlc.hunet.co.kr"]]) {
        ModalDetailViewController *modalDetail = [[ModalDetailViewController alloc] initWithNibName:@"ModalDetailView" bundle:nil];
        modalDetail.url = urlString;
        [self.navigationController pushViewController:modalDetail animated:YES];
        return NO;
    }
	
    // 모달 창 종료
    if ([urlString hasPrefix:@"finish://"] || [urlString hasPrefix:@"close://"]) {
        [self.navigationController popViewControllerAnimated:YES];
        return NO;
    }
    
    // 페이지 뒤로가기 (뒤로갈 수 없으면 모달 창 종료)
    if ([urlString hasPrefix:@"back://"]) {
        if (webView.canGoBack) {
            [webView goBack];
        }
        else {
            [self.navigationController popViewControllerAnimated:YES];
        }
        return NO;
    }
    
    [self indicatorCenter];
	return YES;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        // Custom initialization
        //self.title = @"VISION DREAM";
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];    
    
    UIBarButtonItem *_close = [[UIBarButtonItem alloc] initWithTitle:@"닫기" style:UIBarButtonItemStylePlain target:self action:@selector(closed:)];
    self.navigationItem.leftBarButtonItem = _close;
    
    
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    
    [viewweb loadRequest:request]; 
}

- (void)viewDidUnload
{

    self.url = nil;
    viewweb = nil;
    indicator = nil;
    
    [super viewDidUnload];
}

- (void)dealloc {
    viewweb.delegate = nil;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    [self indicatorCenter];
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)indicatorCenter {
    indicator.center = viewweb.center;
}

@end
