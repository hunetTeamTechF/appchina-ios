//
//  OverlayViewController.h
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 6. 8..
//  Copyright 2011 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MLCenterForBusinessAppDelegate;

@interface OverlayView : UIView <UITableViewDelegate> {
	IBOutlet UIView *mainView;
	IBOutlet UIBarButtonItem *btnDone;
	IBOutlet UIBarButtonItem *btnMagnify;
	IBOutlet UISlider *sliderProcess;
	IBOutlet UILabel *lblCurrent;
	IBOutlet UILabel *lblDuration;
	
	IBOutlet UIButton *btnBack;
	IBOutlet UIButton *btnPlay;
	IBOutlet UIButton *btnForward;
	IBOutlet UIButton *btnPause;
    
    IBOutlet UIButton *btnMarkShow;
    IBOutlet UITableView *tb;
    IBOutlet UIImageView *markImageView;
    IBOutlet UIButton *btnMarkClose;    
    NSMutableArray *markList;   
    
    MLCenterForBusinessAppDelegate *appDelegate;
	NSTimer *alphaTimer;
}

@property (nonatomic, strong) IBOutlet UIView *mainView;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *btnDone;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *btnMagnify;
@property (nonatomic, strong) IBOutlet UISlider *sliderProcess;
@property (nonatomic, strong) IBOutlet UILabel *lblCurrent;
@property (nonatomic, strong) IBOutlet UILabel *lblDuration;
@property (nonatomic, strong) IBOutlet UIButton *btnBack;
@property (nonatomic, strong) IBOutlet UIButton *btnPlay;
@property (nonatomic, strong) IBOutlet UIButton *btnForward;
@property (nonatomic, strong) IBOutlet UIButton *btnPause;

@property (nonatomic, strong) IBOutlet UIButton *btnMarkShow;
@property (nonatomic, strong) IBOutlet UITableView *tb;
@property (nonatomic, strong) IBOutlet UIImageView *markImageView;
@property (nonatomic, strong) IBOutlet UIButton *btnMarkClose;
@property (nonatomic, strong) NSMutableArray *markList;

- (IBAction)Done;
- (IBAction)Magnify;
- (IBAction)Play;
- (IBAction)Pause;
- (IBAction)Forward;
- (IBAction)Back;
- (IBAction)SliderDown;
- (IBAction)SliderUp;
- (void)displayItems;
- (void)statusBarDisplay;
- (IBAction) MarkDisplay;
- (IBAction) MarkHidden;
- (void)tableViewDataReplace:(NSUInteger)index;

@end
