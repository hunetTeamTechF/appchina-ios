

#import "SelfStudyViewController.h"
#import "GraphicsUtile.h"

@interface SelfStudyViewController ()

@end

@implementation SelfStudyViewController

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (void)viewDidUnload {
    [self setSelfStudyUrl:nil];
    [self setDetailView:nil];
    [super viewDidUnload];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        if ([[self appDelegate].eduType isEqualToString:@"LearningAndImagine"]) {
            self.title = NSLocalizedString(@"想象", @"想象");
            
            
            self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_free_study"];
        }
        else
        {
            self.title = NSLocalizedString(@"강의목록", @"강의목록");
            
            #ifdef PRO
            #elif hyundai_16597//현대백화점
                self.title = NSLocalizedString(@"教育课程", @"教育课程");
            #elif snuh_3838
                self.title = NSLocalizedString(@"지식라이브", @"지식라이브");

            #endif
            
            self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_lecturelist"];
        }
        
    }
    
    self.selfStudyUrl = [NSString stringWithFormat:@"%@?type=35&uid=%@&pw=%@&aa=11", [self appDelegate].urlBase, [[self appDelegate].profileUser objectForKey:@"userid"], [[self appDelegate].profileUser objectForKey:@"password"]];
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:_selfStudyUrl]
                                                cachePolicy: NSURLRequestReloadIgnoringCacheData
                                            timeoutInterval:60.0
                                ];
    [self.webView loadRequest:requestObj];
    
    NSArray *vComp = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[vComp objectAtIndex:0] intValue] >= 7) {
        [self setNeedsStatusBarAppearanceUpdate];
        self.edgesForExtendedLayout = UIRectEdgeNone;
        // iOS 7 or above
        CGRect oldBounds = [self.view bounds];
        CGRect newViewBounds = CGRectMake(0, -10, oldBounds.size.width, oldBounds.size.height - 20);
        CGRect newWebViewBounds = CGRectMake(0, -20, oldBounds.size.width, oldBounds.size.height - 40);
        if (self.view.bounds.origin.y != newViewBounds.origin.y) {
            [self.view setBounds:newViewBounds];
            [self.webView setBounds:newWebViewBounds];
        }
    }
}

- (void)detailView:(NSString*)url
{
    [self viewDidLoad];
    self.detailView = url;
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
    [super webViewDidStartLoad:webView];
    if (self.detailView != nil && self.detailView.length > 0) {
        self.webView.hidden = YES;
        [self.indicator stopAnimating];
    }
}


- (void) webViewDidFinishLoad:(UIWebView *)webView {
    [super webViewDidFinishLoad:webView];
    if (self.detailView != nil && self.detailView.length > 0) {
        [self.webView stopLoading];
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:_detailView]
                                                    cachePolicy: NSURLRequestReloadIgnoringCacheData
                                                timeoutInterval:60.0
                                    ];
        [self.webView loadRequest:requestObj];
        self.detailView = @"";
        
        [self performSelector:@selector(webViewShow) withObject:nil afterDelay:0.9];
    }
}

- (void)processAction {
    [super processAction];
}

- (void)openerCallFun {
    [super openerCallFun];
}

- (void)webViewShow {
    self.webView.hidden = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





@end
