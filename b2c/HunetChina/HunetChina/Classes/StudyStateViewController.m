

#import "StudyStateViewController.h"
#import "GraphicsUtile.h"

@interface StudyStateViewController ()

@end

@implementation StudyStateViewController

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        self.title = NSLocalizedString(@"나의강의실", @"나의강의실");
        self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_lectureroom"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *_url = [NSString stringWithFormat:@"%@?type=36&uid=%@&pw=%@", [self appDelegate].urlBase, [[self appDelegate].profileUser objectForKey:@"userid"], [[self appDelegate].profileUser objectForKey:@"password"]];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:_url]
                                                cachePolicy: NSURLRequestReloadIgnoringCacheData
                                            timeoutInterval:60.0
                                ];
    [self.webView loadRequest:requestObj];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
