#define FILENAME                @"filename"
#define DOCSFOLDER				[NSHomeDirectory() stringByAppendingPathComponent: @"Documents"]
#define PREFERENCE				[NSHomeDirectory() stringByAppendingPathComponent: @"Library/Preferences"]
#define TEMP					[NSHomeDirectory() stringByAppendingPathComponent: @"tmp"]
#define DOWNLOAD				[NSHomeDirectory() stringByAppendingPathComponent: @"Documents/Download"]
#define CACHESFOLDER			[NSHomeDirectory() stringByAppendingPathComponent: @"Documents/ImgCaches"]
#define BASERATE                1

#define AVFILENM				@"playAv"
#define PLAYER_JUMP_TIME        10