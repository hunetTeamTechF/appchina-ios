

#import <Foundation/Foundation.h>

@interface Study : NSObject

@property (strong, nonatomic) NSString *course_cd;
@property (strong, nonatomic) NSNumber *take_course_seq;
@property (strong, nonatomic) NSString *chapter_no;
@property (strong, nonatomic) NSString *user_id;
@property (strong, nonatomic) NSNumber *max_sec;
@property (strong, nonatomic) NSNumber *total_sec;
@property (strong, nonatomic) NSNumber *display_no;
@property (strong, nonatomic) NSString *index_nm;
@property (strong, nonatomic) NSString *file_nm;
@property (strong, nonatomic) NSDate *latest_study_date;
@property (strong, nonatomic) NSDate *latest_sync_date;

@end
