#import <Foundation/Foundation.h>
#import <HDRM/HdrmHTTPResponse.h>

@class HdrmHTTPConnection;

static UInt64 hdrmBlockSize;
static NSString *beforeFilePath;
static UInt64 beforeFileLength;

@interface HdrmHTTPFileResponse : NSObject <HdrmHTTPResponse>
{
	HdrmHTTPConnection *connection;
	
	NSString *filePath;
	UInt64 fileLength;
	UInt64 fileOffset;
	
	BOOL aborted;
	
	int fileFD;
	void *buffer;
	NSUInteger bufferSize;
}

- (id)initWithFilePath:(NSString *)filePath forConnection:(HdrmHTTPConnection *)connection;
- (NSString *)filePath;
- (UInt64)getHdrmBlockSize;

@end
