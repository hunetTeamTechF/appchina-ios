#import <Foundation/Foundation.h>
#import <HDRM/HdrmHTTPResponse.h>


@interface HdrmHTTPRedirectResponse : NSObject <HdrmHTTPResponse>
{
	NSString *redirectPath;
}

- (id)initWithPath:(NSString *)redirectPath;

@end
