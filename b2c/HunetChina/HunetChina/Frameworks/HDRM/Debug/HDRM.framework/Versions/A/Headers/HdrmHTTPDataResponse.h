#import <Foundation/Foundation.h>
#import <HDRM/HdrmHTTPResponse.h>


@interface HdrmHTTPDataResponse : NSObject <HdrmHTTPResponse>
{
	NSUInteger offset;
	NSData *data;
}

- (id)initWithData:(NSData *)data;

@end
