#import <HDRM/HdrmHTTPResponse.h>

@interface HdrmHTTPErrorResponse : NSObject <HdrmHTTPResponse> {
    NSInteger _status;
}

- (id)initWithErrorCode:(int)httpErrorCode;

@end
