#define IS_4_INCH CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(320, 568)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(568, 320))
#define IS_iPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_iPhone4 CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(320, 480)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(480, 320))
#define IS_iPhone5 CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(320, 568)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(568, 320))
#define IS_iPhone6 CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(375, 667)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(667, 375))
#define IS_iPhone6_plus CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(414, 736)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(736, 414))
#define IS_Height_More_Then_568 ([UIScreen mainScreen].bounds.size.height >= 568)


// 8.0 and above
#define IS_DEVICE_RUNNING_IOS_8_AND_ABOVE() ([[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] != NSOrderedAscending)

// 7.0 and above
#define IS_DEVICE_RUNNING_IOS_7_AND_ABOVE() ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending)

// 6.0, 6.0.x, 6.1, 6.1.x
#define IS_DEVICE_RUNNING_IOS_6_OR_BELOW() ([[[UIDevice currentDevice] systemVersion] compare:@"6.2" options:NSNumericSearch] != NSOrderedDescending)

///NSArray *vComp = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
//if ([[vComp objectAtIndex:0] intValue] >= 7) { }


