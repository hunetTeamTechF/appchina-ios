//
//  SangsangStudy.h
//  MLCenterForBusiness
//
//  Created by Joey on 13. 9. 30..
//
//

#import <Foundation/Foundation.h>

@interface SangsangStudy : NSObject
@property (strong, nonatomic) NSNumber *contents_seq;
@property (strong, nonatomic) NSString *goods_id;
@property (strong, nonatomic) NSString *se_goods_id;
@property (strong, nonatomic) NSString *contents_nm;
@property (strong, nonatomic) NSNumber *view_no;
@property (strong, nonatomic) NSString *user_id;
@property (strong, nonatomic) NSString *study_end_date;
@property (strong, nonatomic) NSNumber *view_sec;
@property (strong, nonatomic) NSString *goods_nm;
@property (strong, nonatomic) NSNumber *contents_sec;
@property (strong, nonatomic) NSNumber *last_view_sec;
@property (strong, nonatomic) NSNumber *view_sec_mobile;
@property (strong, nonatomic) NSNumber *contract_no;
@property (strong, nonatomic) NSString *file_nm;
@property (strong, nonatomic) NSDate *latest_study_date;
@property (strong, nonatomic) NSDate *latest_sync_date;
@end
