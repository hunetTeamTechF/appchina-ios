

#import <Foundation/Foundation.h>


@interface Global : NSObject {
//    NSString *_drmInfo;
}

//@property (strong, nonatomic) NSString *_drmInfo;

@property (strong, nonatomic) NSString *testPrimaryDomainFix;
@property (strong, nonatomic) NSString *testPrimaryDomain;

+(Global *) sharedSingleton;
- (NSString *)testPrimaryDomainUrl:(NSString *)url;
- (NSString *)encodePassword:(NSString *)passwd;

@end
