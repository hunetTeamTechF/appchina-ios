//
//  HomeViewController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 5. 17..
//  Copyright 2011 hunet. All rights reserved.
//

#import "HomeViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "ModalAlert.h"
#import "PlayerViewController.h"
#import "SSViewController.h"
#import "ModalViewController.h"
#import "MoreViewController.h"
#import "NoticeViewController.h"
#import "DownloadViewController.h"
#import "SelfStudyViewController.h"
#import "UserViewController.h"
#import "Header.h"
#import "LectureViewController.h"
#import "ELCImagePickerController.h"
#import "ELCAlbumPickerController.h"
#import "ELCAssetTablePicker.h"
#import "WebModalViewController.h"
#import "GraphicsUtile.h"

@implementation HomeViewController
@synthesize playerCallFun = _playerCallFun;

#pragma mark -
#pragma mark UIWebViewDelegate implementation

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        self.title = NSLocalizedString(@"홈", @"홈");
        self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_home"];
    }
    return self;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    if ([viewWeb.subviews count] > 0) {
        UIView *scrollView = viewWeb.subviews[0];
        
        for (UIView *childView in scrollView.subviews) {
            if ([childView isKindOfClass:[UIWebDocumentView class]]) {
                UIWebDocumentView *documentView = (UIWebDocumentView *)childView;
                WebScriptObject *wso = documentView.webView.windowScriptObject;
                
                [wso setValue:[WebScriptBridge getWebScriptBridge] forKey:@"ScriptBridge"];
            }
        }
    }
	[indicator stopAnimating];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
	[indicator startAnimating];
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
	[indicator stopAnimating];	
	//[ModalAlert notify:@"네트워크 연결에 문제가 생겼습니다."];	
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    NSURL *url = request.URL;
    NSString *urlString = url.absoluteString.lowercaseString;
    
    NSLog(@"%@, %@", urlString, app.eduType);
    
    //모달 팝업
    if ([urlString hasPrefix:@"modal://"])
    {
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        int qLocation = [url.absoluteString rangeOfString:@"?"].location;
        NSString *query = [url.absoluteString substringFromIndex:qLocation + 1];
        query = [query stringByReplacingOccurrencesOfString:@"://" withString:@""];
        
        
        
        
        for (NSString *item in [query componentsSeparatedByString:@"&"]) {
            NSArray *keyValue = [item componentsSeparatedByString:@"="];
            if (![keyValue objectAtIndex:0])
                continue;
            NSString *key = [keyValue objectAtIndex:0];
            NSString *value = (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (CFStringRef)[keyValue objectAtIndex:1], CFSTR(""), kCFStringEncodingUTF8));
            [params addEntriesFromDictionary:[[NSDictionary alloc]
                                              initWithObjects:[[NSArray alloc] initWithObjects:value, nil]
                                              forKeys:[[NSArray alloc] initWithObjects:key, nil]]];
        }
        
        //[Util valueForKey:@"url" fromQuery:[request URL].query];
        
        NSString *urlParamDecoded = [params valueForKey:@"url"];
        NSLog(@"urlParamDecoded : %@", urlParamDecoded);
        //?url query parameter가 없는 경우,
        if (urlParamDecoded == nil) {
            WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
            webModal.webViewUrl = [request.URL.absoluteString stringByReplacingOccurrencesOfString:@"modal://" withString:@"http://"];
            //webModal.title = @"";
            [self presentViewController:webModal animated:YES completion:nil];
        }
        //query에 url parameter가 있으면 해당 url로 modal 팝업
        else {
            
            NSURL *urlParsed = [NSURL URLWithString:[[urlParamDecoded componentsSeparatedByString:@"?"] firstObject]];
            NSString *method = [params valueForKey:@"method"];
            NSString *title = [params valueForKey:@"title"];
            //post parameter가 있으면 url을 파싱해서, queryString은 post로 전송
            if ([method.lowercaseString isEqualToString:@"post"])
            {
                WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
                
                //webViewRequestMethod
                NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:urlParsed
                                                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                   timeoutInterval:20.0];
                [request setHTTPMethod:@"POST"];
                NSString *bodyString = (NSString *)[[urlParamDecoded componentsSeparatedByString:@"?"] lastObject];
                [request setHTTPBody:[bodyString dataUsingEncoding:NSUTF8StringEncoding]];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                webModal.title = title;
                [self presentViewController:webModal animated:YES completion:nil];
                [webModal.WebView loadRequest:request];
                
                
            }
            //query에 url parameter가 없으면
            else {
                WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
                webModal.webViewUrl = urlParamDecoded;
                webModal.title = title ? title : @"";
                if ([urlParamDecoded rangeOfString:@"/eBook/"].location != NSNotFound)
                    [webModal.ButtonClose setHidden:NO];
                else
                    [webModal.ButtonClose setHidden:YES];
                
                [self presentViewController:webModal animated:YES completion:nil];
            }
        }
        
        return NO;
        
    }
    
    // 모달 창 종료
    if ([urlString hasPrefix:@"finish://"] || [urlString hasPrefix:@"close://"]) {
        [self dismissViewControllerAnimated:YES completion:nil];
        return NO;
    }
    
    // 페이지 뒤로가기 (뒤로갈 수 없으면 모달 창 종료)
    if ([urlString hasPrefix:@"back://"]) {
        if (webView.canGoBack) {
            [webView goBack];
        }
        else {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        return NO;
    }
    
    //웹 로그아웃 제어
    if([urlString hasPrefix:@"logout://"])
    {
        [[MLCenterForBusinessAppDelegate sharedAppDelegate] loginViewControllerDisplay];
        return YES;
    }
    
    //나의 강의실, 사용자 즐겨찾기
    if ([urlString rangeOfString:@"/my/my.aspx"].location != NSNotFound
        || [urlString rangeOfString:@"/my/mycourse"].location != NSNotFound
        || [urlString rangeOfString:@"/classroom/online"].location != NSNotFound){

        [app setSelectedTabIndex:1 indexOrUrl:urlString];
        
        return NO;
    }
    
    // 회원정보 페이지로 이동
    if ([urlString hasPrefix:@"membermodify://"]) {
        self.tabBarController.selectedIndex = 3;
        
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
        UserViewController *second = [[UserViewController alloc] initWithNibName:@"UserViewController" bundle:nil];
        NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
        [navi setViewControllers:controllersArray animated:YES];
        
        
        return NO;
    }
    
    //상상마루 전용  version = 5, imagine_contract_yn = 'Y', only_imagine_yn = 'Y'
    if ([app.eduType isEqualToString:@"OnlyImagine"]) {
        if ([urlString rangeOfString:@"/imgcategory/contents?"].location != NSNotFound
            || [urlString rangeOfString:@"/imgclassroom/series?"].location != NSNotFound
            || [urlString rangeOfString:@"/imgclassroom/contents?"].location != NSNotFound
            || [urlString rangeOfString:@"/imgclassroom/requiredlecture?headertitle="].location != NSNotFound) {
            self.tabBarController.selectedIndex = 1;
            SelfStudyViewController *selfStudy = (SelfStudyViewController*)self.tabBarController.selectedViewController;
            if ([urlString rangeOfString:@"childcategoryyn=y"].location != NSNotFound) {
                
                NSString *param =[urlString substringFromIndex:[urlString rangeOfString:@"?"].location + 1];
                
                urlString = [NSString stringWithFormat:@"%@/imgCategory/ContentsCategoryListAll?%@&layoutdisplayyn=y", app.urlCenter, param];
                
                //return NO;
            }
            [selfStudy detailView:urlString];
            return NO;
        }
        
        //웹 로그아웃 제어
        if([urlString hasPrefix:@"logout://"])
        {
            [[MLCenterForBusinessAppDelegate sharedAppDelegate] loginViewControllerDisplay];
        }
        
        return YES;
    }
    
    //행복한 인문학당
    if ([urlString rangeOfString:@"/lecture/category_inmun.aspx"].location != NSNotFound){
        [app setLectureUrlType:@"7"];
        [app setSelectedTabIndex:1];
        return NO;
    }
    
    
    if ([urlString rangeOfString:@"/lecture/category.aspx?param="].location != NSNotFound){
        self.tabBarController.selectedIndex = 1;
        LectureViewController *view = (LectureViewController*)self.tabBarController.selectedViewController;
        view._currentUrl = @"";
        //view._param = [param objectAtIndex:1];
        [self.tabBarController.selectedViewController viewWillAppear:YES];
        return NO;
    }
    
    
    //주제별 교육과정
    if ([urlString rangeOfString:@"/lecture/category.aspx"].location != NSNotFound){
        [app setLectureUrlType:@"6"];
        [app setSelectedTabIndex:1];
        return NO;
    }
    
    
    //주제별 교육과정 상세
    if ([urlString rangeOfString:@"/lecture/depth.aspx"].location != NSNotFound){
        [app setLectureUrlType:@"6"];
        [app setSelectedTabIndex:1 indexOrUrl:urlString];
        return NO;
    }
    
    //다운로드
    if ([urlString isEqualToString:@"downloadcenter://"]) {
        if ([app.eduType isEqualToString:@"LearningAndImagine"]) {
            self.tabBarController.selectedIndex = 3;
            UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
            MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
            DownloadViewController *second = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
            NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
            [navi setViewControllers:controllersArray animated:YES];
        } else {
            
            self.tabBarController.selectedIndex = 3;
            
            UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
            [navi popToRootViewControllerAnimated:YES];
        }
        return NO;
    }
    
    //상담실-공지사항, 공지사항 상세
    if ([urlString rangeOfString:@"/counsel/counsel_noticelist.aspx"].location != NSNotFound
        || [urlString rangeOfString:@"/counsel/counsel_noticedetail.aspx"].location != NSNotFound){
        
        self.tabBarController.selectedIndex = 3;
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
        NoticeViewController *second = [[NoticeViewController alloc] initWithNibName:@"NoticeViewController" bundle:nil];
        second.title = @"공지사항";
        second.url = urlString;
        NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
        [navi setViewControllers:controllersArray animated:YES];
        
        return NO;
    }
    
    //홍화면 편집
    if ([urlString rangeOfString:@"/home/home_edit.aspx"].location != NSNotFound){
        return YES;
    }
    
    
    //pc버전 링크
    if ([urlString isEqualToString:[NSString stringWithFormat:@"%@/", app.urlCenter]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:app.urlCenter]];
        return NO;
    }
    
    //상상마루
    if ([urlString rangeOfString:@"/sangsang/sangsanglink.aspx"].location != NSNotFound)
    {
        [viewWeb stringByEvaluatingJavaScriptFromString:@"location.href = '/imgcategory/contents';"];
        
        return NO;
    }
    if ([urlString hasPrefix:@"sangsangdrmplayer://"] ||
        [urlString hasPrefix:@"sangsangdownload://"] ||
        [urlString hasPrefix:@"sangsangdownloadplay://"]) {
        
        NSArray *tmpArray = [urlString componentsSeparatedByString:@"://"];
        NSString *act = [tmpArray objectAtIndex:0];
        NSString *param = [tmpArray objectAtIndex:1];
        
        BOOL checkConfigSettings = false;
        //상상마루 바로학습
        if ([urlString hasPrefix:@"sangsangdrmplayer://"]) {
            checkConfigSettings = [[MLCenterForBusinessAppDelegate sharedAppDelegate] moviePlayCheck];
        } //상상마루 다운로드 플레이
        else {
            checkConfigSettings = [[MLCenterForBusinessAppDelegate sharedAppDelegate] isWiFi];
        }
        if (checkConfigSettings) {
            _playerCallFun = [[PlayerCallFun alloc] init];
            _playerCallFun.delegateViewController = self;
            [_playerCallFun sangsangmaruInit:[NSArray arrayWithObjects:act, param, nil]];
            
            
            //self.currentUrl = [_webView stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
            
        }
        
        return NO;
    }
    
    // 외부 사이트로 이동
    if ([urlString hasPrefix:@"escape://"]) {
        NSString *escapeUrl = [urlString stringByReplacingOccurrencesOfString:@"escape://" withString:@"http://"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:escapeUrl]];
        return NO;
    }
    
    
    if ([urlString hasPrefix:@"appstore://"]) {
        NSString *escapeUrl = [urlString stringByReplacingOccurrencesOfString:@"appstore://" withString:@""];
        escapeUrl = [escapeUrl stringByReplacingOccurrencesOfString:@"https//" withString:@"https://"];
        escapeUrl = [escapeUrl stringByReplacingOccurrencesOfString:@"http//" withString:@"http://"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:escapeUrl]];
        return NO;
    }
    
    
    
    if([[[request URL] absoluteString] hasPrefix:@"jscall"])
    {
        NSString *requestString = [[request URL] absoluteString];
        NSArray *components = [requestString componentsSeparatedByString:@"://"];
        NSString *functionName = [components objectAtIndex:1];
        
        SEL selector = NSSelectorFromString(functionName);
        IMP imp = [self methodForSelector:selector];
        void (*func)(id, SEL) = (void *)imp;
        func(self, selector);
        
        return NO;
    }
    
    // 나의강의실
    if ([urlString rangeOfString:@"/classroom/online"].location != NSNotFound){
        self.tabBarController.selectedIndex = 1;
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        [navi popToRootViewControllerAnimated:YES];
        return NO;
    }
    // 상상마루
    //    if ([urlString.lowercaseString rangeOfString:@"/imghome"].location != NSNotFound){
    //        self.tabBarController.selectedIndex = 1;
    //        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
    //        [navi popToRootViewControllerAnimated:YES];
    //        return NO;
    //    }
    
    // 공지사항
    if ([urlString rangeOfString:@"/counsel/noticelist"].location != NSNotFound){
        self.tabBarController.selectedIndex = 3;
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        [navi popToRootViewControllerAnimated:YES];
        return NO;
    }
    
    // 더보기, 학습지원센터
    if ([urlString hasPrefix:@"more://"]
        || [urlString rangeOfString:@"/more/more.aspx"].location != NSNotFound) {
        self.tabBarController.selectedIndex = 3;
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        [navi popToRootViewControllerAnimated:YES];
        return NO;
    }
    
    return YES;
}

- (void)callPhotoView {
    ELCAlbumPickerController *albumController = [[ELCAlbumPickerController alloc] initWithNibName: nil bundle: nil];
	ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initWithRootViewController:albumController];
    [albumController setParent:elcPicker];
	[elcPicker setDelegate:self];
    elcPicker.maximumImagesCount = 4;
    
    [self presentViewController:elcPicker animated:YES completion:nil];
    
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    if ([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]){
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)pickerWebSave:(NSArray *)info
{
    @autoreleasepool {
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        
        NSLog(@"%@", app.urlCenter);
        
        NSString *urlString, *boundary;
        imgFileName = @"";
        NSInteger i = 0;
        urlString = [NSString stringWithFormat:@"%@/Common/MobileUpload?path=Community", app.urlCenter];
        boundary = @"CommunityPicImage";
        
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setCachePolicy:NSURLRequestUseProtocolCachePolicy];
        [request setHTTPMethod:@"POST"];
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        
        /*
         NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
         
         [[NSRunLoop currentRunLoop] run];
         
         
         // Schedule your connection to run on threads runLoop.
         [connection scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
         */
        
        
        /*
         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         NSRunLoop *loop = [NSRunLoop currentRunLoop];
         [connection scheduleInRunLoop:loop forMode:NSRunLoopCommonModes];
         [loop run]; // make sure that you have a running run-loop.
         });
         */
        
        
	for(NSDictionary *dict in info) {
            UIImage *image = [dict objectForKey:UIImagePickerControllerOriginalImage];
            NSData *ImageData = UIImageJPEGRepresentation(image, 1.0);
            NSMutableData *body = [NSMutableData data];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image%d\"; filename=\"Image%d.jpg\"\r\n", i, i] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:ImageData]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [request setHTTPBody:body];
            
            NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
            NSString *returnString = [[NSString alloc] initWithData:urlData encoding:NSUTF8StringEncoding];
            
            NSLog(@"%@", returnString);
            
            imgFileName = [imgFileName stringByAppendingFormat:@"|%@", returnString];
            
            
            i=i+1;
	}
        
        if ([info count] == i) {
            [self performSelectorOnMainThread:@selector(saveEnd:) withObject:imgFileName waitUntilDone:NO];
        }
    }
}


- (void)saveEnd:(NSString*) ImgFileNames
{
	[mask removeFromSuperview];
	[uploadIndicator stopAnimating];
    [viewWeb stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"ReadingPhoto('%@');", ImgFileNames]];
}


- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    if ([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]){
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    uploadIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    uploadIndicator.hidesWhenStopped = YES;
    
    
    mask = [[UIView alloc] initWithFrame:[[self.view window] frame]];
	mask.userInteractionEnabled = NO;
	mask.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.50];
    [mask addSubview:uploadIndicator];
    uploadIndicator.center = mask.center;
	[self.view addSubview:mask];
	
	[uploadIndicator startAnimating];
    
    
    [NSThread detachNewThreadSelector:@selector(pickerWebSave:) toTarget:self withObject:info];
    
}

- (void)viewDidLoad {
    //NSLog(@"viewDidLoad");
    
    [super viewDidLoad];
	[self viewLoad];
	
	
}

- (void)viewLoad {
    NSLog(@"viewLoad");
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
	if (app.flagLogin)
    {
        NSString *urlText = [NSString stringWithFormat:@"%@?type=4&uid=%@&pw=%@", app.urlBase, [app.profileUser objectForKey:@"userid"], [app.profileUser objectForKey:@"password"]];
        
        [viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0]];
        
        viewWeb.scalesPageToFit = YES;
    }
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (NSUInteger)supportedInterfaceOrientations
{
    indicator.center = viewWeb.center;
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskAllButUpsideDown;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {

	viewWeb = nil;
	indicator = nil;
    [super viewDidUnload];
}


- (void)dealloc {
	viewWeb.delegate = nil;
}

- (void)webViewpushUrl:(NSString *)webViewpushUrl {
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    [viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:webViewpushUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0]];
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    app.pushService = nil;
    //self.appDelegate.pushService = nil;
}

- (void)processAction {
    NSLog(@"다운로드 완료 버튼 클릭");
}


- (void)openerCallFun {
    NSLog(@"player close");
    
    [viewWeb stringByEvaluatingJavaScriptFromString:@"location.reload();"];
}

@end
