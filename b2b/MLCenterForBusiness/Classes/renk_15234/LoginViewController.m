
#import "LoginViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "MoreViewController.h"
#import "DownloadViewController.h"
#import "ModalAlert.h"
#import "Header.h"

@interface LoginViewController ()
{
    CGPoint initLoginBoxPoint;
	CGPoint initTextIDPoint;
	CGPoint initTextPasswordPoint;
    CGPoint initButtonLoginPoint;
}
@end
@implementation LoginViewController
@synthesize viewImageCover;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /* 컨트롤들 백그라운드 이미지 사이즈 재설정 */
    CGRect idRect = self.textID.frame;
    idRect.size.width = 154;
    idRect.size.height = 16;
    self.textID.frame = idRect;
     
    CGRect pwRect = self.textPassword.frame;
    pwRect.size.width = 154;
    pwRect.size.height = 16;
    self.textPassword.frame = pwRect;
     
    CGRect btnRect = self.buttonLogin.frame;
    btnRect.size.width = 72;
    btnRect.size.height = 67;
    self.buttonLogin.frame = btnRect;
     
     
    CGRect bgRect = self.viewImageLoginBox.frame;
    bgRect.size.width = 260;
    bgRect.size.height = 126;
    self.viewImageLoginBox.frame = bgRect;
    
    CGRect offBtnRect = self.offlineButton.frame;
    offBtnRect.size.width = 323;
    offBtnRect.size.height = 65;
    self.offlineButton.frame = offBtnRect;
    self.offlineButton.titleLabel.hidden = true;
    
    
    CGFloat y = 80;
    
    self.viewImageBackground.image = [UIImage imageNamed:@"login_bg"];
    
    self.viewImageLoginBox.center = CGPointMake(self.viewImageLoginBox.center.x + 20, self.viewImageLoginBox.center.y + y - 5);

    self.textID.center = CGPointMake(self.textID.center.x + 11, self.textID.center.y + y);
    self.textPassword.center = CGPointMake(self.textPassword.center.x + 11, self.textPassword.center.y + y - 5);
    self.buttonLogin.center = CGPointMake(self.buttonLogin.center.x - 18, self.buttonLogin.center.y + y - 11);
    
    initLoginBoxPoint = self.viewImageLoginBox.center;
	initTextIDPoint = self.textID.center;
	initTextPasswordPoint = self.textPassword.center;
    initButtonLoginPoint = self.buttonLogin.center;
    
    self.textID.backgroundColor = [UIColor clearColor];
    self.textPassword.backgroundColor = [UIColor clearColor];
    self.textID.borderStyle = [UIColor clearColor];
    self.textPassword.borderStyle = [UIColor clearColor];
    
    self.textID.background =[UIImage imageNamed:@"idpw_login_bg"];
    self.textPassword.background =[UIImage imageNamed:@"idpw_login_bg"];
    
    self.textID.textColor = [UIColor blackColor];
    self.textPassword.textColor = [UIColor blackColor];
    
    
    UIColor *color  = [UIColor blackColor];
    
    self.textID.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"아이디" attributes:@{NSForegroundColorAttributeName:color}];
    
    self.textPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"패스워드" attributes:@{NSForegroundColorAttributeName:color}];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [paths objectAtIndex:0];
    NSString *drmFolder = [NSString stringWithFormat:@"%@/%@", documentFolder, @"hunet_docroot"];
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSDirectoryEnumerator *fileEnumerator = [fileManager enumeratorAtPath:drmFolder];
    
    int filecount = 0;
    for (NSString *filename in fileEnumerator) {
        if ([filename isEqualToString:@".DS_Store"]) {
            continue;
        }
        filecount = 1;
        break;
    }
    if (filecount > 0)
    {
        self.offlineButton.hidden = NO;
        self.offlineButton.center = CGPointMake(self.offlineButton.center.x + 11, self.offlineButton.center.y + y - 5);
    }
}


- (IBAction) onLogin {
	self.buttonLogin.userInteractionEnabled = NO;
    self.textID.text = [self.textID.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.textPassword.text = [self.textPassword.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
	if ([self.textID isFirstResponder]) {
		[self.textID resignFirstResponder];
	}
	
	if ([self.textPassword isFirstResponder]) {
		[self.textPassword resignFirstResponder];
	}
	
	if (self.textID.text.length == 0 || self.textPassword.text.length == 0) {
		[ModalAlert notify:@"로그인에 실패했습니다.\n아이디와 패스워드를 다시 확인해 주세요"];
		self.buttonLogin.userInteractionEnabled = YES;
		return;
	}
	
	mask = [[UIView alloc] initWithFrame:[[self.view window] frame]];
	mask.userInteractionEnabled = NO;
	mask.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.50];
	[self.view addSubview:mask];
    
    
    
	
	[indicator startAnimating];
	[NSThread detachNewThreadSelector:@selector(threadLogin:) toTarget:self withObject:nil];
}



- (BOOL) textFieldShouldReturn:(UITextField *)textField {
	if ([self.textID isFirstResponder]) {
		[self.textPassword becomeFirstResponder];
	}
	else {
		[self onLogin];
	}
	return YES;
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    if (IS_4_INCH) {
        return YES;
    }
    
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.2f];
	//[UIView setAnimationDelegate:self];
    
    CGFloat y = 70;
    if (IS_4_INCH) {
        y = 100;
    }
	
    self.viewImageLoginBox.center = CGPointMake(initLoginBoxPoint.x, initLoginBoxPoint.y - y);
    self.textID.center = CGPointMake(initTextIDPoint.x, initTextIDPoint.y - y);
    self.textPassword.center = CGPointMake(initTextPasswordPoint.x, initTextPasswordPoint.y - y);
    self.buttonLogin.center = CGPointMake(initButtonLoginPoint.x, initButtonLoginPoint.y - y);
    [UIView commitAnimations];
    
	return YES;
}

- (void) threadLogin:(id)somedata {
	BOOL result = YES;
	@autoreleasepool {
	
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        self.textID.text = [self.textID.text stringByReplacingOccurrencesOfString:@"renk-" withString:@""];
        result = [app loginForID:[NSString stringWithFormat:@"renk-%@", self.textID.text] withPassword:self.textPassword.text];
	[self performSelectorOnMainThread:@selector(callbackAfterLogin:) withObject:[NSNumber numberWithBool:result] waitUntilDone:NO];
	}
}

// callback for the login result
- (void) callbackAfterLogin:(NSNumber*)resultLogin {
	
    
	BOOL result = [resultLogin boolValue];
	
	[mask removeFromSuperview];
	[indicator stopAnimating];
	
	self.buttonLogin.userInteractionEnabled = YES;
	
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    if (result) {
        //[self.view removeFromSuperview];
        [app openTrainingInstitute];
    }
    else {
        if ([app isNotInternet]) {
            [ModalAlert notify:@"인터넷에 연결되어 있지 않습니다.\n인터넷 연결을 확인해 주세요."];
        }
        else {
            [ModalAlert notify:@"로그인에 실패했습니다.\n아이디와 패스워드를 다시 확인해 주세요."];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setOfflineButton:nil];
    [self setViewImageBackground:nil];
    [self setViewImageLoginBox:nil];
    [self setTextID:nil];
    [self setTextPassword:nil];
    [self setButtonLogin:nil];

	indicator = nil;
	viewImageCover = nil;
    
    [super viewDidUnload];
}


- (BOOL) shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)offlineClick:(id)sender {
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] offlineStudy];
    
}
@end
