//
//  DownloadTabViewController.m
//  MLCenterForBusiness
//
//  Created by Joey on 13. 10. 15..
//
//

#import "DownloadTabViewController.h"
#import "DownloadViewController.h"
#import "DownloadCourseCell.h"
#import "DownloadIndexViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "Util.h"

@interface DownloadTabViewController ()
@property (retain, nonatomic) IBOutlet UIView *downloadView;

@end

@implementation DownloadTabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)courseAll
{
    self.courseArray = [Util getCourse];
}

- (void)snagsangAll
{
    self.sangsangArray = [Util getSangsang];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *barBut = [[UIBarButtonItem alloc] initWithTitle:@"더보기" style:UIBarButtonItemStylePlain target:self action:@selector(doSomething)];
    
    self.navigationItem.leftBarButtonItem=barBut;
    
    
    [self.view addSubview:_tableView];
    
    [barBut release];
    
                    
}

- (void)doSomething
{
    if ([MLCenterForBusinessAppDelegate sharedAppDelegate].flagLogin) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [[MLCenterForBusinessAppDelegate sharedAppDelegate] loginViewControllerDisplay];
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self courseAll];
    [self snagsangAll];
    //[self.tableView reloadData];
}


- (void)dealloc {
    [_tableView release];
    [super dealloc];
    [_courseArray release];
    [_sangsangArray release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setDownloadView:nil];
    [self setTableView:nil];
    [super viewDidUnload];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.courseArray count] + 1 ;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 40;
    }
    return 84;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell ==nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        uint64_t totalSize = strtoull([[[Util freeDiskspace] objectAtIndex:0] UTF8String], NULL, 0);
        uint64_t freeSize = strtoull([[[Util freeDiskspace] objectAtIndex:1] UTF8String], NULL, 0);
        
        UIFont *myFont = [ UIFont fontWithName: @"Arial" size: 13.0 ];
        cell.textLabel.font  = myFont;
        cell.textLabel.textAlignment = UITextAlignmentCenter;
        cell.textLabel.textColor = [UIColor redColor];
        cell.textLabel.text = [NSString stringWithFormat:@"총 공간 : %@, 남은 용량 : %@", [Util prettyBytes:totalSize], [Util prettyBytes:freeSize]];
        cell.userInteractionEnabled = NO;
        
        return cell;
    } else {
        static NSString *CellIdentifier = @"DownloadCourseCell";
        DownloadCourseCell *cell = (DownloadCourseCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            NSArray *arr = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil];
            cell = [arr objectAtIndex:0];
        }
        NSManagedObject *obj = [self.courseArray objectAtIndex:indexPath.row - 1];
        cell.courseNm.text = [obj valueForKey:@"course_nm"];
        cell.studyEndDate.text = [NSString stringWithFormat:@"%@까지", [obj valueForKey:@"study_end_date"]];
        if ([Util expirationDay:[obj valueForKey:@"study_end_date"]] >= 0) {
            cell.expirationDay.text = [NSString stringWithFormat:@"(%d일 남음)", [Util expirationDay:[obj valueForKey:@"study_end_date"]]];
        }
        
        return cell;
    }
    return nil;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *obj = [self.courseArray objectAtIndex:indexPath.row - 1];
    NSString *cd = [obj valueForKey:@"course_cd"];
    NSString *seq = [obj valueForKey:@"take_course_seq"];
    NSString *date = [obj valueForKey:@"study_end_date"];
    NSInteger expirationDay = [Util expirationDay:date];
    
    DownloadIndexViewController *view = [[DownloadIndexViewController alloc] initWithStyle:UITableViewStylePlain];
    view.coursecd = cd;
    view.takecourseseq = seq;
    view.expirationDay = expirationDay;
    [self.navigationController pushViewController:view animated:YES];
    [view release];
    
}

@end
