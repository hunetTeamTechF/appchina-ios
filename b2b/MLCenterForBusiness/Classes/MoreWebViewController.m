//
//  MoreWebViewController.m
//  MLCenterForBusiness
//
//  Created by hunet-mac-npc92 on 1/30/15.
//
//

#import "MoreWebViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "UserViewController.h"
#import "SetupViewController.h"
#import "VersionViewController.h"
#import "DownloadViewController.h"

@interface MoreWebViewController ()
    
@end

@implementation MoreWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"설정", @"설정");
        self.tabBarItem.image = [UIImage imageNamed:@"ti_setting"];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Config" ofType:@"html"]];
    NSLog(@"url : %@", url);
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([request.URL.absoluteString hasPrefix:@"nhlife-member://"])
    {
        UserViewController *userVC = [[[UserViewController alloc] initWithNibName:@"UserViewController" bundle:nil] autorelease];
        userVC.title = @"회원정보 관리";
        [self.navigationController pushViewController:userVC animated:YES];
        
    }
    else if ([request.URL.absoluteString hasPrefix:@"nhlife-setting://"])
    {
        SetupViewController *setupVC = [[[SetupViewController alloc] initWithNibName:@"SetupViewController" bundle:nil] autorelease];
        setupVC.title = @"환경설정";
        [self.navigationController pushViewController:setupVC animated:YES];
    }
    else if ([request.URL.absoluteString hasPrefix:@"nhlife-version://"])
    {
        VersionViewController *versionVC = [[[VersionViewController alloc] initWithNibName:@"VersionViewController" bundle:nil] autorelease];
        versionVC.title = @"버전정보";
        [self.navigationController pushViewController:versionVC animated:YES];
    }
    else if ([request.URL.absoluteString hasPrefix:@"nhlife-download://"])
    {
        DownloadViewController *downloadVC = [[[DownloadViewController alloc] initWithStyle:UITableViewStylePlain] autorelease];
        downloadVC.title = @"다운로드파일 관리";
        [self.navigationController pushViewController:downloadVC animated:YES];
    }
    else if ([request.URL.absoluteString hasPrefix:@"logout://"])
    {
        [MLCenterForBusinessAppDelegate sharedAppDelegate].flagLogin = NO;
        [[MLCenterForBusinessAppDelegate sharedAppDelegate] loginViewControllerDisplay];
    }
    
    return YES;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
