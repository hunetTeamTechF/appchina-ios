//
//  OtherViewController.h
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 6. 15..
//  Copyright 2011 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface OtherViewController : UIViewController <UIWebViewDelegate> {
	IBOutlet UIWebView *viewWeb;
	IBOutlet UIActivityIndicatorView *indicator;
	NSString *urlString;
}

@property (nonatomic, strong) NSString *urlString;

@end
