
#import "DownloadIndexViewController.h"
#import "DownloadCourseCell.h"
#import "DownloadStudyCell.h"
#import "ModalAlert.h"
#import "Util.h"
#import "STUtil.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "MoviePlayerController.h"
#import "StwayHeader.h"


@interface DownloadIndexViewController ()

@end

@implementation DownloadIndexViewController

- (NSUInteger) supportedInterfaceOrientations
{
    //    return UIInterfaceOrientationMaskPortrait; //세로 화면만 허용
    return UIInterfaceOrientationMaskAll; //전체 화면 허용
    //    return UIInterfaceOrientationMaskPortraitUpsideDown; //거꾸로만 허용
    //    return UIInterfaceOrientationMaskLandscape; //가로화면만 허용
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (void)coredata
{
    self.courseArray = [Util getCourse:self.coursecd andTakeCourseSeq:self.takecourseseq];
    self.studyArray = [Util getStudy:self.coursecd andTakeCourseSeq:self.takecourseseq];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super init];
    if (self != nil) {
#ifdef nhlife_14384
        self.title = NSLocalizedString(@"다운로드 파일 관리", @"다운로드 파일 관리");
#else
        self.title = NSLocalizedString(@"다운로드 센터", @"다운로드 센터");
#endif

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self coredata];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.studyArray count] > 0) {
        return [self.studyArray count] + 1;;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 84;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        static NSString *CellIdentifier = @"DownloadCourseCell";
        DownloadCourseCell *cell = (DownloadCourseCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            NSArray *arr = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil];
            cell = [arr objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSManagedObject *obj = [self.courseArray objectAtIndex:indexPath.row];
        cell.courseNm.text = [obj valueForKey:@"course_nm"];
        cell.studyEndDate.text = [NSString stringWithFormat:@"%@까지", [obj valueForKey:@"study_end_date"]];
        if ([Util expirationDay:[obj valueForKey:@"study_end_date"]] >= 0) {
            //cell.expirationDay.text = [NSString stringWithFormat:@"(%d일 남음)", [Util expirationDay:[obj valueForKey:@"study_end_date"]]];
            cell.expirationDay.text = [NSString stringWithFormat:@"(%li일 남음)", [Util calculatePeroidOfDay:nil to:[obj valueForKey:@"study_end_date"]]];
        }
        
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"DownloadStudyCell";
        DownloadStudyCell *cell = (DownloadStudyCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            NSArray *arr = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil];
            cell = [arr objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSManagedObject *obj = [self.studyArray objectAtIndex:indexPath.row - 1];
        cell.displayNoLabel.text = [NSString stringWithFormat:@"%@차시", [obj valueForKey:@"display_no"]];
        cell.indexNmLabel.text = [obj valueForKey:@"index_nm"];
        
        cell.synButton.tag = indexPath.row - 1;
        cell.studyButton.tag = indexPath.row - 1;
        cell.delButton.tag = indexPath.row - 1;
        [cell.synButton addTarget:self action:@selector(ClickSync:) forControlEvents:UIControlEventTouchUpInside];
        [cell.studyButton addTarget:self action:@selector(ClickStudy:) forControlEvents:UIControlEventTouchUpInside];
        [cell.delButton addTarget:self action:@selector(ClickDel:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    
    return nil;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}


- (void)ClickSync:(id)sender
{
    if (self.expirationDay < 0) {
        [ModalAlert notify:@"학습 기간이 종료되어 동기화 할 수 없습니다."];
        return;
    }
#ifdef nhlife_14384
    BOOL syncContinue = [ModalAlert ask:@"오프라인(Wi-Fi 또는 3G/4G가 연결되지 않은 상태) 상태에서 다운로드 받은 과정을 학습시 진도 연동이 되지 않습니다.\n진도를 연동하시겠습니까?"];
    if (syncContinue == NO)
        return;
#endif
    
    UIButton *button = (UIButton*)sender;
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] maskLoading];
    [NSThread detachNewThreadSelector:@selector(sync:) toTarget:self withObject:[NSArray arrayWithObjects:
                                                                                 [NSString stringWithFormat:@"%d", button.tag], @"sync", nil]];
}

- (void)ClickStudy:(id)sender
{
    if (self.expirationDay < 0) {
        [ModalAlert notify:@"학습 기간이 종료되어 학습을 할 수 없습니다."];
        return;
    }
    
    UIButton *button = (UIButton*)sender;
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] maskLoading];
    [NSThread detachNewThreadSelector:@selector(sync:) toTarget:self withObject:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%d", button.tag], @"play", nil] ];
}

- (void)ClickDel:(id)sender
{
    UIButton *button = (UIButton*)sender;
    
    if ([ModalAlert ask:@"정말 삭제 하시겠습니까?"]) {
        NSManagedObject *obj = [self.studyArray objectAtIndex:button.tag];
        NSString *takeCourseSeq = [obj valueForKey:@"take_course_seq"];
        NSString *courseCd = [obj valueForKey:@"course_cd"];
        NSString *chapterNo = [obj valueForKey:@"chapter_no"];
        NSString *fileNm = [obj valueForKey:@"file_nm"];
        
        [Util deleteDocrootFile:fileNm];
        [Util deleteStudy:courseCd andTakeCourseSeq:takeCourseSeq andChapterNo:chapterNo];
        
        [self coredata];
        [self.tableview reloadData];
    }
}


- (void)sync:(NSArray*)array {
    
    @autoreleasepool {
    
        NSManagedObject *obj = [self.studyArray objectAtIndex:[[array objectAtIndex:0] intValue]];
        NSString *courseCd = [obj valueForKey:@"course_cd"];
        NSString *getTakeCourseSeq = [obj valueForKey:@"take_course_seq"];
        NSString *chapterNo = [obj valueForKey:@"chapter_no"];
        NSString *maxSec2 = [obj valueForKey:@"max_sec"];
        NSString *totalSec = [obj valueForKey:@"total_sec"];
        NSString *userId = [obj valueForKey:@"user_id"];
        NSString *fileNm = [obj valueForKey:@"file_nm"];
        NSString *indexNm = [obj valueForKey:@"index_nm"];
        
        if (![[MLCenterForBusinessAppDelegate sharedAppDelegate] isNotInternet])
        {
            //NSDictionary *result = [Util syn:courseCd andTakeCourseSeq:takeCourseSeq andChapterNo:chapterNo andMaxSec:maxSec andTotalSec:totalSec andUserId:userId];
            NSDictionary *result = [Util synV2:courseCd andTakeCourseSeq:getTakeCourseSeq andChapterNo:chapterNo andStudySec:totalSec andMarkValue:maxSec2 andUserId:userId];
            if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
                
                if ([[result objectForKey:@"max_study_sec"] intValue]>0) {
                    maxSec2 = [result objectForKey:@"max_study_sec"];
                }
                
                NSLog(@"%maxSec : %@", maxSec2);
                self.synMessage = @"학습 이력이 동기화 되었습니다.";
            } else {
                self.synMessage = @"동기화 중 오류 발생 되었습니다.";
            }
        }
        else {
            self.synMessage = @"네트웍 연결을 확인하세요.";
        }
        
        [self performSelectorOnMainThread:@selector(downloadPlay:) withObject:[NSArray arrayWithObjects:courseCd,
                                                                               getTakeCourseSeq,
                                                                               chapterNo,
                                                                               maxSec2,
                                                                               fileNm,
                                                                               indexNm,
                                                                               [array objectAtIndex:1], nil] waitUntilDone:NO];
    }
}

- (void)downloadPlay:(NSArray*)array
{
    NSString *courseCd = [array objectAtIndex:0];
    NSString *takeCourseSeq = [array objectAtIndex:1];
    NSString *chapterNo = [array objectAtIndex:2];
    NSString *maxSec = [array objectAtIndex:3];
    NSString *fileNm = [array objectAtIndex:4];
    NSString *indexNm = [array objectAtIndex:5];
    NSString *act = [array objectAtIndex:6];
    
    
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] removemaskLoading];
    if ([act isEqualToString:@"play"]) {
        NSUserDefaults *userdeDefaults = [NSUserDefaults standardUserDefaults];
        [userdeDefaults setObject:fileNm forKey:FILENAME];
        NSLog(@"download filename = %@", [userdeDefaults objectForKey:FILENAME]);
        MoviePlayerController * movieCon = [[MoviePlayerController alloc] initWithNibName:@"MoviePlayerController" bundle:nil];
        movieCon.isStreamingPlay = FALSE;
        movieCon.currentSecond = [maxSec intValue];
        movieCon.scrollType = 0;
        movieCon.courseCd = courseCd;
        movieCon.takecourseSeq = [takeCourseSeq intValue];
        movieCon.chapterNo = chapterNo;
        movieCon.movieTitle = indexNm;
        [MLCenterForBusinessAppDelegate sharedAppDelegate].moviePlayerController = movieCon;
        [self presentViewController:movieCon animated:YES completion:nil];
    } else { //sync
        [ModalAlert notify:self.synMessage];
    }
}

- (BOOL)prefersStatusBarHidden
{
    //return UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]);
    return NO;
}

@end
