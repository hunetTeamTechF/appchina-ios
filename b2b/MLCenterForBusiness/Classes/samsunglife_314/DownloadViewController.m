
#import "DownloadViewController.h"
#import "DownloadCourseCell.h"
#import "DownloadIndexViewController.h"
#import "DownloadIndexViewSangController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "Util.h"
#import "Header.h"
#import "GraphicsUtile.h"

@interface DownloadViewController ()

@end

@implementation DownloadViewController


- (NSUInteger) supportedInterfaceOrientations
{
    //    return UIInterfaceOrientationMaskPortrait; //세로 화면만 허용
    return UIInterfaceOrientationMaskAll; //전체 화면 허용
    //    return UIInterfaceOrientationMaskPortraitUpsideDown; //거꾸로만 허용
    //    return UIInterfaceOrientationMaskLandscape; //가로화면만 허용
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self != nil) {
        self.title = NSLocalizedString(@"다운로드 센터", @"다운로드 센터");
        self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_download"];
    }
    return self;
}

-(void)removeControll{
    [MLCenterForBusinessAppDelegate sharedAppDelegate].window.rootViewController = [MLCenterForBusinessAppDelegate sharedAppDelegate].tabBarController;
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)closeViewController{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)showLogin {
    [self closeViewController];
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] loginViewControllerDisplay];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialSegmentedControl];
}

-(void)initialSegmentedControl{
    if (![MLCenterForBusinessAppDelegate sharedAppDelegate].flagLogin) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"로그인" style:UIBarButtonItemStyleDone target:self action:@selector(showLogin)];
        self.navigationItem.hidesBackButton = YES;
    }
    else{
        NSString *className =  NSStringFromClass([MLCenterForBusinessAppDelegate sharedAppDelegate].window.rootViewController.class);
        
        if ([className isEqualToString:@"UINavigationController"]) { //밑의 상태메뉴가 있는 없는 경우만 닫기 버튼으로 바로 닫을 수 있도록(공통app적용)
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"닫기" style:UIBarButtonItemStyleDone target:self action:@selector(removeControll)];
            self.navigationItem.hidesBackButton = YES;
        }
    }
    
    NSArray *itemArray;
    
    self.downloadInfoArray = [Util getDownloadInfo];
    
    NSManagedObject *obj = [self.downloadInfoArray objectAtIndex:0];
    NSString *imagineMenuAlias = [obj valueForKey:@"imagine_menu_alias"];

    itemArray = [NSArray arrayWithObjects:imagineMenuAlias, nil];
    _segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
    _segmentedControl.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40);
    _segmentedControl.segmentedControlStyle = UISegmentedControlStylePlain;
    
    _segmentedControl.selectedSegmentIndex = 0;
    
    self.segmentedControlPortraitOrientationFrame = _segmentedControl.frame;
    [_segmentedControl addTarget:self action:@selector(valueChanged:) forControlEvents: UIControlEventValueChanged];
    
    //상상마루 학습창의 다운로드되어 있다면 바로 연결
    if (self.goodsId!=Nil) {
        _segmentedControl.selectedSegmentIndex = 0;
        
        DownloadIndexViewSangController *view = [[DownloadIndexViewSangController alloc] initWithNibName:@"DownloadIndexViewSangController" bundle:nil];
        
        view.goodsId = self.goodsId;
        view.userId = self.userId;
        view.expirationDay = self.expirationDay;
        view.seGoodsId = self.seGoodsId;
        
        [self.navigationController pushViewController:view animated:YES];
        return;
    }
    
    //상상마루 학습창의 다운로드되어 있다면 바로 연결
//    if (self.courseCd!=Nil) {
//        _segmentedControl.selectedSegmentIndex = 0;
//        
//        DownloadIndexViewController *view = [[DownloadIndexViewController alloc] initWithStyle:UITableViewStylePlain];
//        
//        view.coursecd = self.courseCd;
//        view.takecourseseq = self.takeCourseSeq;
//        //view.expirationDay = self.expirationDay;
//        
//        [self.navigationController pushViewController:view animated:YES];
//        return;
//    }
    
    [self tableData];
    if ([self.courseArray count]<1 && [self.sangsangArray count]>0) {
        _segmentedControl.selectedSegmentIndex = 1;
        
        self.segmentedControlPortraitOrientationFrame = _segmentedControl.frame;
        [_segmentedControl addTarget:self action:@selector(valueChanged:) forControlEvents: UIControlEventValueChanged];
        
    }
    
    //tab메뉴 인덱스 재설정
//    if (_downloadInfoArray.count > 0) {
//        if ([onlyImagineYn isEqualToString:@"Y"]) {
//            _segmentedControl.selectedSegmentIndex = 1;
//        }
//        else if ([onlyImagineYn isEqualToString:@"N"]) {
//            _segmentedControl.selectedSegmentIndex = 0;
//        }
//        else{
//           // _segmentedControl.selectedSegmentIndex = 0;
//        }
//    }


}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    [super didRotateFromInterfaceOrientation:interfaceOrientation];
    _segmentedControl.frame = CGRectMake(_segmentedControl.frame.origin.x
                                         , _segmentedControl.frame.origin.y
                                         , [UIScreen mainScreen].bounds.size.width
                                         , 40);

}

- (BOOL)prefersStatusBarHidden
{
    //return UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]);
    return NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    [self tableData];
    [self.tableView reloadData];

#if nhlife_14384
    self.navigationController.navigationBar.hidden = NO;
#endif
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void)tableData{
    self.courseArray = [Util getCourse];
    self.sangsangArray = [Util getSangsang];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int defaultRowCount = (_segmentedControl.numberOfSegments > 1 ? 2 : 1);
    return [self.sangsangArray count] + defaultRowCount;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 40;
    }
    return 84;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self tableData];
    //[self.tableView reloadData];
    int defaultRowCount = (_segmentedControl.numberOfSegments > 1 ? 2 : 1);
    if (indexPath.row == 0)
    {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell ==nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        uint64_t totalSize = strtoull([[[Util freeDiskspace] objectAtIndex:0] UTF8String], NULL, 0);
        uint64_t freeSize = strtoull([[[Util freeDiskspace] objectAtIndex:1] UTF8String], NULL, 0);
        
        UIFont *myFont = [ UIFont fontWithName: @"Arial" size: 13.0 ];
        cell.textLabel.font  = myFont;
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor redColor];
        cell.textLabel.text = [NSString stringWithFormat:@"총 공간 : %@, 남은 용량 : %@", [Util prettyBytes:totalSize], [Util prettyBytes:freeSize]];
        cell.userInteractionEnabled = NO;
        
        return cell;
        
    }
    else if (defaultRowCount == 2 && indexPath.row == 1)
    {
        static NSString *CellIdentifier = @"TabCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell ==nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
             [cell addSubview:_segmentedControl];
        }
        
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"DownloadCourseCell";
        DownloadCourseCell *cell = (DownloadCourseCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            NSArray *arr = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil];
            cell = [arr objectAtIndex:0];
        }
        
        NSManagedObject *obj = [self.downloadInfoArray objectAtIndex:0];
        NSString *onlyImagineYn = [obj valueForKey:@"only_imagine_yn"];
        
        //tab메뉴 인덱스 재설정
        //다운로드 상상마루 학습
        if (_downloadInfoArray.count > 0 && [onlyImagineYn isEqualToString:@"Y"]) {
            NSManagedObject *obj = [self.sangsangArray objectAtIndex:indexPath.row - defaultRowCount];
            cell.courseNm.text = [obj valueForKey:@"goods_nm"];
            cell.studyEndDate.text = [NSString stringWithFormat:@"%@까지", [obj valueForKey:@"study_end_date"]];
            if ([Util expirationDay:[obj valueForKey:@"study_end_date"]] >= 0) {
                cell.expirationDay.text = [NSString stringWithFormat:@"(%li일 남음)", [Util calculatePeroidOfDay:nil to:[obj valueForKey:@"study_end_date"]]];
            }
        }//LMS과목차시학습
        else if (_downloadInfoArray.count > 0 && [onlyImagineYn isEqualToString:@"N"]) {
            NSManagedObject *obj = [self.courseArray objectAtIndex:indexPath.row - defaultRowCount];
            cell.courseNm.text = [obj valueForKey:@"course_nm"];
            cell.studyEndDate.text = [NSString stringWithFormat:@"%@까지", [obj valueForKey:@"study_end_date"]];
            if ([Util expirationDay:[obj valueForKey:@"study_end_date"]] >= 0) {
                cell.expirationDay.text = [NSString stringWithFormat:@"(%li일 남음)", [Util calculatePeroidOfDay:nil to:[obj valueForKey:@"study_end_date"]]];
            }
        }
        else
        {
            NSManagedObject *obj = [self.sangsangArray objectAtIndex:indexPath.row - defaultRowCount];
            cell.courseNm.text = [obj valueForKey:@"goods_nm"];
            cell.studyEndDate.text = [NSString stringWithFormat:@"%@까지", [obj valueForKey:@"study_end_date"]];
            if ([Util expirationDay:[obj valueForKey:@"study_end_date"]] >= 0) {
                cell.expirationDay.text = [NSString stringWithFormat:@"(%li일 남음)", [Util calculatePeroidOfDay:nil to:[obj valueForKey:@"study_end_date"]]];
            }
            NSLog(@"%@", [obj valueForKey:@"goods_nm"]);
        }
        return cell;
    }
    return nil;
}


-(IBAction)valueChanged:(id)sender {
    _segmentedControl = (UISegmentedControl *)sender;
    [self.tableView reloadData];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *obj = [self.downloadInfoArray objectAtIndex:0];
    NSString *onlyImagineYn = [obj valueForKey:@"only_imagine_yn"];
    
    int defaultRowCount = (_segmentedControl.numberOfSegments > 1 ? 2 : 1);
    
    //tab메뉴 인덱스 재설정
    if (_downloadInfoArray.count > 0 && [onlyImagineYn isEqualToString:@"Y"]) {
        DownloadIndexViewSangController *view = [[DownloadIndexViewSangController alloc] initWithNibName:@"DownloadIndexViewSangController" bundle:nil];
        NSManagedObject *obj = [self.sangsangArray objectAtIndex:indexPath.row - defaultRowCount];
        NSString *goodsId = [obj valueForKey:@"goods_id"];
        NSString *userId = [obj valueForKey:@"user_id"];
        NSString *date = [obj valueForKey:@"study_end_date"];
        NSInteger expirationDay = [Util expirationDay:date];
        
        view.goodsId = goodsId;
        view.userId = userId;
        view.expirationDay = expirationDay;
        view.seGoodsId = [obj valueForKey:@"se_goods_id"];
        
        [self.navigationController pushViewController:view animated:YES];
            }
    else if (_downloadInfoArray.count > 0 && [onlyImagineYn isEqualToString:@"N"]) {
        
        DownloadIndexViewController *view = [[DownloadIndexViewController alloc] initWithNibName:@"DownloadIndexViewController" bundle:nil];
        
        NSManagedObject *obj = [self.courseArray objectAtIndex:indexPath.row - defaultRowCount];
        NSString *cd = [obj valueForKey:@"course_cd"];
        NSString *seq = [obj valueForKey:@"take_course_seq"];
        NSString *date = [obj valueForKey:@"study_end_date"];
        NSInteger expirationDay = [Util expirationDay:date];
        
        view.coursecd = cd;
        view.takecourseseq = seq;
        view.expirationDay = expirationDay;
        [self.navigationController pushViewController:view animated:YES];

    }
    else{
        DownloadIndexViewSangController *view = [[DownloadIndexViewSangController alloc] initWithNibName:@"DownloadIndexViewSangController" bundle:nil];
        NSManagedObject *obj = [self.sangsangArray objectAtIndex:indexPath.row - defaultRowCount];
        NSString *goodsId = [obj valueForKey:@"goods_id"];
        NSString *userId = [obj valueForKey:@"user_id"];
        NSString *date = [obj valueForKey:@"study_end_date"];
        NSInteger expirationDay = [Util expirationDay:date];
        
        view.goodsId = goodsId;
        view.userId = userId;
        view.expirationDay = expirationDay;
        view.seGoodsId = [obj valueForKey:@"se_goods_id"];

        [self.navigationController pushViewController:view animated:YES];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
