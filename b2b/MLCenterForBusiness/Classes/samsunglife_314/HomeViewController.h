//
//  HomeViewController.h
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 5. 17..
//  Copyright 2011 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ELCImagePickerController.h"
#import "DownloadingView.h"
#import "PlayerCallFun.h"
#import "MoviePlayerController.h"

@interface HomeViewController : UIViewController <ELCImagePickerControllerDelegate, UIWebViewDelegate, DownloadingViewDelegate, MoviePlayerControllerDelegate> {
	
    NSString *imgFileName;
    UIActivityIndicatorView *uploadIndicator;
    UIView *mask;
    PlayerCallFun *playerCallFun;
    
	IBOutlet UIWebView *viewWeb;
	IBOutlet UIActivityIndicatorView *indicator;
}

@property (nonatomic, strong) PlayerCallFun *playerCallFun;

- (void)viewLoad;
- (void)webViewpushUrl:(NSString *)webViewpushUrl;
- (void)processAction;
- (void)openerCallFun;

@end
