

#import "MLCenterForBusinessAppDelegate.h"
#import "LoginViewController.h"
#import	"Reachability.h"
#import "ModalAlert.h"

#import "CJSONDeserializer.h"
#import "Util.h"
#import "Utility.h"
#import "EncryptUtility.h"
#import "MoviePlayerController.h"
#import "DownloadViewController.h"
#import "DownloadIndexViewSangController.h"

#import "HomeViewController.h"
#import "LectureViewController.h"
#import "AllianzLectureViewController.h"
#import "SelfStudyViewController.h"
#import "StudyStateViewController.h"
#import "MyViewController.h"
#import "CounselViewController.h"
#import "MoreViewController.h"
#import "SSViewController.h"
#import "HStoryViewController.h"
#import "HStoryDetailViewController.h"
#import "HReminderViewController.h"
#import "Header.h"
#import "HLeaderController.h"
#import "Global.h"
#import "NoticeViewController.h"
#import "MoreWebViewController.h"
#import "VersionChecher.h"

#define ENCODE(X) [(X) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]


@implementation UINavigationController (Rotation_IOS6)


-(BOOL)shouldAutorotate
{
    return [[self.viewControllers firstObject] shouldAutorotate];
}

-(NSUInteger)supportedInterfaceOrientations
{
    return [[self.viewControllers firstObject] supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return [[self.viewControllers firstObject] preferredInterfaceOrientationForPresentation];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return [[self.viewControllers firstObject] preferredStatusBarStyle];
}

@end

@implementation UITabBarController (autoRotate)

- (BOOL)shouldAutorotate {
    return [self.selectedViewController shouldAutorotate];
}
- (NSUInteger)supportedInterfaceOrientations {
    return [self.selectedViewController supportedInterfaceOrientations];
}

@end

@implementation MLCenterForBusinessAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


@synthesize managedObjectContext2 = _managedObjectContext2;
@synthesize managedObjectModel2 = _managedObjectModel2;
@synthesize persistentStoreCoordinator2 = _persistentStoreCoordinator2;
@synthesize flagAutoGo = _flagAutoGo;

@synthesize window;
@synthesize profileUser, companySeq, urlBase, urlSite, urlCenter, flagLogin, lectureUrlType, downLoadUrl, eduType, workStateType;
@synthesize moviePlayer;
@synthesize etc1, etc2, etc3, gotoUrlAfterLogin;
@synthesize pushService;
@synthesize tagPrevious;
@synthesize tagCurrent;

@synthesize courseTab;
@synthesize imagineTab;

- (void)dealloc {
	self.lectureUrlType = nil;
	internetReach = nil;
	//[moviePlayer release];
    

    //[_tabBarController release];
}

/////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Network Reachability callback implementation
// callback used by Reachability whenever status changes
- (void) reachabilityChanged: (NSNotification*)note {
	
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	NetworkStatus netStatus = [curReach currentReachabilityStatus];
	
	switch (netStatus) {
		case NotReachable: {
			// show alert, "You are not connected to the Internet"
			[ModalAlert notify:@"인터넷에 연결되어 있지 않습니다. 인터넷 연결을 확인해 주세요"];
			break;
		}
		case ReachableViaWWAN: {
			// check the wifionly
			if ([[self.profileUser objectForKey:@"threegnoti"] boolValue] == YES) {
				[ModalAlert notify:@"3G/4G로 접속되었습니다"];
			}
			break;
		}
		case ReachableViaWiFi:
			// ok, it's clear. nothing to do, just keep going
			break;
			
	}
}

- (CGSize)currentSize {
    CGSize viewSize = [[UIScreen mainScreen] bounds].size;
    UIInterfaceOrientation CURRENT_ORIENTATION = [[UIApplication sharedApplication] statusBarOrientation];
    if(UIInterfaceOrientationIsLandscape(CURRENT_ORIENTATION)){
        viewSize = CGSizeMake(viewSize.height, viewSize.width);
    } else {
        viewSize = CGSizeMake(viewSize.width, viewSize.height);
    }
    return viewSize;
}


- (void)maskLoading {
    self.mask = [[UIView alloc] init];
    self.mask.frame = CGRectMake(0, 0, [self currentSize].width, [self currentSize].height);
    self.mask.userInteractionEnabled = YES;
    self.mask.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.50];
    self.mask.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    [self.window.rootViewController.view addSubview:self.mask];
}

- (void)removemaskLoading
{
    [self.mask removeFromSuperview];
}

-(void) drmloadingView:(id)delegate {
    self.loading = [[DownloadingView alloc] initWithNibName:@"DownloadingView" bundle:nil];
    self.loading.delegate = delegate;
    
//    if ([delegate isKindOfClass:[SSViewController class]]) {
//        SSViewController *s = (SSViewController*)delegate;
//        [s.view addSubview:self.loading.view];
//    }
//    else if ([delegate isKindOfClass:[HLeaderController class]]) {
//        HLeaderController *h = (HLeaderController*)delegate;
//        [h.view addSubview:self.loading.view];
//    }
    
    if ([delegate isKindOfClass:[UIViewController class]]) {
        UIViewController *viewController = (UIViewController*)delegate;
        [viewController.view addSubview:self.loading.view];
    }
    else
        [self.window.rootViewController.view addSubview:self.loading.view];
    
    self.loading.view.center = self.window.rootViewController.view.center;
}

-(void)drmremoveLodingView {
    [self.loading.view removeFromSuperview];
}

+ (MLCenterForBusinessAppDelegate *)sharedAppDelegate
{
    return (MLCenterForBusinessAppDelegate *) [UIApplication sharedApplication].delegate;
}


- (void) threadAutoLogin:(id)somedata {
    


        NSString *param = @"";
        NSString *some = [NSString stringWithFormat:@"%@", somedata];

        if ([some isEqualToString:@"sso_login"]) {
            param = [NSString stringWithFormat:@"type=24&etc1=%@&etc2=%@&etc3=%@", self.etc1, self.etc2, self.etc3];
            //result = [self loginSSOForEtc1:self.etc1 withEtc2:self.etc2 withEtc3:self.etc3];
        }
        else
        {
            NSString *uid = [self.profileUser objectForKey:@"userid"];
            NSString *passwd = [[Global sharedSingleton] encodePassword:[self.profileUser objectForKey:@"password"]];
            NSString *tk = [self.profileUser objectForKey:@"deviceToken"];
            param = [NSString stringWithFormat:@"type=1&uid=%@&pw=%@&token=%@", uid, passwd, tk];
        }
        BOOL result = [self loginForParam:param];
        
	[self performSelectorOnMainThread:@selector(callbackAfterAutoLogin:) withObject:[NSNumber numberWithBool:result] waitUntilDone:NO];
    
	
}

// callback for the autologin result
- (void) callbackAfterAutoLogin:(NSNumber*)resultLogin {
    BOOL result = [resultLogin boolValue];
    if (result) {
        // login success
        [self initializeMainView];
    }
    else {
        // auto login failure
        if ([self isNotInternet]) {
            [ModalAlert notify:@"인터넷에 연결되어 있지 않습니다. 인터넷 연결을 확인해 주세요."];
        }
        else {
            [ModalAlert notify:@"자동로그인에 실패했습니다. 아이디와 패스워드를 다시 확인해 주세요."];
        }
        [self initializeMainView];
    }
}

- (BOOL)loginForParam:(NSString *)param
{
    NSError *errorReq = nil;
	BOOL fLoginResult = NO;
    NSLog(@"AppDelegate loginForParam: %@?%@",self.urlBase, param);

	NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:self.urlBase] andQuery:param andError:&errorReq];
	if (jsonData)
    {
        NSError *errorJSON = nil;
        NSDictionary *result = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
        
		if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
            NSString *uid           = [[result objectForKey:@"UserId"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            NSString *pw            = [[result objectForKey:@"Password"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
			self.companySeq         = [result objectForKey:@"CompanySeq"];
			self.urlCenter          = [result objectForKey:@"Url"];
            self.drmUseType         = [result objectForKey:@"DrmUseType"];
            self.eduType            = [result objectForKey:@"EduType"];
            self.hReminderUseType   = [result objectForKey:@"HReminderUseType"];
            self.hStoryUseType      = [result objectForKey:@"HStoryUseType"];
            fLoginResult            = YES;
			self.flagLogin          = YES;
            
            [self.profileUser setObject:uid forKey:@"userid"];
            [self.profileUser setObject:[[Global sharedSingleton] encodePassword:pw] forKey:@"password"];
            
            [Util updateDownloadInfo:[self.companySeq intValue] andProcessMenuAlias:[result objectForKey:@"ProcessMenuAlias"] andImagineMenuAlias:[result objectForKey:@"ImagineMenuAlias"]
                    andOnlyImagineYn:[result objectForKey:@"OnlyImagineYn"] andTabBgColr:@""];
		} else {
			fLoginResult = NO;
            self.workStateType = [result objectForKey:@"WorkStateType"];
			self.companySeq = @"";
		}
	}
	return fLoginResult;
}

//ect1=companySeq, etc2=uid, ect3=공통ID

- (BOOL) loginSSOForEtc1:(NSString*)etcVal1 withEtc2:(NSString*)etcVal2 withEtc3:(NSString*)etcVal3
{
    [self.profileUser setObject:@"" forKey:@"userid"];
    [self.profileUser setObject:@"" forKey:@"password"];
    NSString *param = [NSString stringWithFormat:@"type=24&etc1=%@&etc2=%@&etc3=%@", etcVal1, etcVal2, etcVal3];
    BOOL result = [self loginForParam:param];
    return result;
}


- (BOOL)loginForID:(NSString*)userid withPassword:(NSString*)password
{
    NSString *uid       = userid;
    NSString *passwd    = [[Global sharedSingleton] encodePassword:password];
    NSString *tk        = [self.profileUser objectForKey:@"deviceToken"];
    NSString *param     = [NSString stringWithFormat:@"type=1&uid=%@&pw=%@&token=%@", uid, passwd, tk];
    BOOL result         = [self loginForParam:param];
    return result;
}

- (void) initializeMainView {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:20.f];
    
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(setRootViewController)];
    
    /*
     if (flagLogin) {
     viewLogin.view.frame = CGRectMake(0,0-[self currentSize].height, [self currentSize].width, [self currentSize].height);
     [viewLogin release];
     viewLogin = nil;
     } else
     viewLogin.viewImageCover.frame = CGRectMake(0,0-[self currentSize].height, [self currentSize].width, [self currentSize].height);
     */
    
    [UIView commitAnimations];
    [self checkVersion];
}

- (void) openTrainingInstitute {
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration: 1.f];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(setRootViewController)];
	[UIView commitAnimations];
}

- (void)setRootViewController {
    
    if (self.flagLogin) {
        [self settingtabBarController];
        [viewLogin removeFromParentViewController];
		viewLogin = nil;
    } else {
        [viewLogin.viewImageCover removeFromSuperview];
    }
    
    if ([self.etc1 length] > 0) {
        [self setSelectedTabIndex:2];
    }
}

- (void)setRootViewSangDownloadCenter:(NSString*)goodsId
                        andContractNo:(int)contractNo
                       andContentsSeq:(int)contentsSeq
                         andSeGoodsId:(NSString*)seGoodsId
                     andExpirationDay:(int)expirationDay {
    
    if (self.flagLogin) {
        //[self settingtabBarController];
        
        UINavigationController *navi = nil;
        
        if ([self.eduType isEqualToString:@"LearningAndImagine"])
        {
            self.tabBarController.selectedIndex = 4;
            navi = (UINavigationController*)self.tabBarController.selectedViewController;
            MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
            DownloadViewController *second = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
            DownloadIndexViewSangController *third = [[DownloadIndexViewSangController alloc] initWithNibName:@"DownloadIndexViewSangController" bundle:nil];
            third.goodsId = goodsId;
            third.contractNo = contractNo;
            third.contentsSeq = contentsSeq;
            third.seGoodsId = seGoodsId;
            third.userId = [self.profileUser objectForKey:@"userid"];
            third.expirationDay = expirationDay;
            
            NSArray *controllersArray = [NSArray arrayWithObjects: first, second, third, nil];
            [navi setViewControllers:controllersArray animated:YES];
        }
        else
        {
            self.tabBarController.selectedIndex = 3;
            
            navi = (UINavigationController*)self.tabBarController.selectedViewController;
            DownloadViewController *first = [[navi viewControllers] objectAtIndex:0];
            DownloadIndexViewSangController *second = [[DownloadIndexViewSangController alloc] initWithNibName:@"DownloadIndexViewSangController" bundle:nil];
            second.goodsId = goodsId;
            second.contractNo = contractNo;
            second.contentsSeq = contentsSeq;
            second.seGoodsId = seGoodsId;
            second.userId = [self.profileUser objectForKey:@"userid"];
            second.expirationDay = expirationDay;
            
            NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
            [navi setViewControllers:controllersArray animated:YES];
        }
    }
}

- (void)offlineStudy {
    self.flagLogin = NO;
    self.drmUseType = @"Y";
    
    [self.window.rootViewController removeFromParentViewController];
    UITableViewController *viewController = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"닫기" style:UIBarButtonItemStyleDone target:viewController action:@selector(closeViewController)];
    viewController.navigationItem.hidesBackButton = YES;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    [[self topMostController] presentViewController:nav animated:YES completion:nil];
}

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}


- (void)onlineStudyDownload:(NSString*)courseCd andTakeCourseSeq:(int)takeCourseSeq{
    self.flagLogin = YES;
    self.drmUseType = @"Y";
    
    [self.window.rootViewController removeFromParentViewController];
    DownloadViewController *viewController = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
    viewController.courseCd = courseCd;
    viewController.takeCourseSeq = [@(takeCourseSeq) stringValue];
    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"닫기" style:UIBarButtonItemStyleDone target:viewController action:@selector(closeViewController)];
    viewController.navigationItem.hidesBackButton = YES;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    [[self topMostController] presentViewController:nav animated:YES completion:nil];
}

- (void)sangStudyDownload:(NSString*)goodsId
            andContractNo:(int)contractNo
           andContentsSeq:(int)contentsSeq
             andSeGoodsId:(NSString*)seGoodsId
         andExpirationDay:(int)expirationDay {
    self.flagLogin = YES;
    self.drmUseType = @"Y";
    
    [self.window.rootViewController removeFromParentViewController];
    DownloadViewController *viewController = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
    viewController.goodsId = goodsId;
    viewController.contractNo = contractNo;
    viewController.contentsSeq = contentsSeq;
    viewController.seGoodsId = seGoodsId;
    viewController.userId = [self.profileUser objectForKey:@"userid"];
    viewController.expirationDay = expirationDay;
    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"닫기" style:UIBarButtonItemStyleDone target:viewController action:@selector(closeViewController)];
    viewController.navigationItem.hidesBackButton = YES;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    [[self topMostController] presentViewController:nav animated:YES completion:nil];
}

- (void)loginViewControllerDisplay
{
    [self.window.rootViewController.view removeFromSuperview];
    if (!viewLogin) {
        viewLogin = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        viewLogin.view.frame = [[UIScreen mainScreen] applicationFrame];
        viewLogin.viewImageCover.hidden = YES;
    }

    window.rootViewController = viewLogin;
}

- (void) setNoticeOption:(BOOL)option withKey:(NSString*)key
{
    [self.profileUser setObject:[NSNumber numberWithBool:option] forKey:key];
	[self profileUserInfoSave];
}

- (void) setSelectedTabIndex:(NSUInteger)indexSelected {
	[self setSelectedTabIndex:indexSelected indexOrUrl:@""];
}

- (void)profileUserInfoSave {
	
	NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString* documentDirectory = [paths objectAtIndex:0];
	NSString* myPath = [documentDirectory stringByAppendingPathComponent:@"b2b.plist"];
	
	[profileUser writeToFile:myPath atomically:YES];
}


- (void) setSelectedTabIndex:(NSUInteger)indexSelected indexOrUrl:(NSString*)indexOrUrl {
    self.tabBarController.selectedIndex = indexSelected;
	if ([indexOrUrl isEqualToString:@""]) {
		[self.tabBarController.selectedViewController viewWillAppear:YES];
		return;
	}
	
    if (indexSelected == 1) {
        LectureViewController *view = (LectureViewController*)self.tabBarController.selectedViewController;
        view._currentUrl = indexOrUrl;
        
        [view viewWillAppear:YES];
        return;
    }
    
    MyViewController *view = (MyViewController*)self.tabBarController.selectedViewController;
    view.urlMark = indexOrUrl;
    [view viewWillAppear:YES];
    return;
}


- (void) setLectureUrlType:(NSString *)type {
	lectureUrlType = type;
}


- (void) setNavagitionBarStyle:(UINavigationBar *) navigationBar {
    if (IS_DEVICE_RUNNING_IOS_7_AND_ABOVE())
    {
        navigationBar.tintColor = [UIColor whiteColor];
        navigationBar.translucent = NO;
        navigationBar.barTintColor = [UIColor colorWithRed:48.0/255.0 green:70.0/255.0 blue:143.0/255.0 alpha:1.0];
        navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    }

    return;
}

- (UINavigationController *) getNavigationController: (Class)viewControllerClass withNibName:(NSString *)nibName {
    
    UIViewController *viewController = [[viewControllerClass alloc] initWithNibName:nibName bundle:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    return navController;
}

- (UINavigationController *) getNavigationController: (Class)viewControllerClass initWithStyle:(UITableViewStyle)style {
    
    UIViewController *viewController = [[viewControllerClass alloc] initWithStyle:style];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    return navController;
}

- (void)settingtabBarController
{
    [self.window.rootViewController removeFromParentViewController];
    
    UINavigationController *navDownload = [self getNavigationController:[DownloadViewController class] initWithStyle:UITableViewStylePlain];
    [self setNavagitionBarStyle: navDownload.navigationBar];
    
    UINavigationController *navMore = [self getNavigationController:[MoreViewController class] withNibName:@"MoreViewController"];
    [self setNavagitionBarStyle: navMore.navigationBar];
    
    if (_tabBarController != nil) {
        _tabBarController = nil;
    }
    
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.delegate = self;
    self.window.rootViewController = self.tabBarController;
    
    ////////////////////////////////////////////////
    // tabBar 버튼 갯수와 종류 셋팅
    self.tabBarController.viewControllers = @[
                                              [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                              [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                              navDownload,
                                              navMore
                                              ];
    //
    ////////////////////////////////////////////////


    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:6.0/255.0 green:40.0/255.0 blue:83.0/255.0 alpha:1.0];
        self.tabBarController.tabBar.barTintColor = [UIColor whiteColor];
        self.tabBarController.tabBar.translucent = false;
    }
    
    [self performSelector:@selector(pushServiceURL) withObject:nil afterDelay:0.5];
}

//바로학습 클릭시
- (BOOL)moviePlayCheck {
    
	Reachability *isConnect = [Reachability reachabilityForInternetConnection];

    //셀룰러 데이터 (3G,4G) 접속
    if ([isConnect currentReachabilityStatus] == ReachableViaWWAN)
    {
        //설정 > 동영상 강의 시청 == NO
        if ([[self.profileUser objectForKey:@"threegvideo"] boolValue] == NO)
        {
#if nhlife_14384
            [ModalAlert notify:@"현재 3G/4G망 동영상 강의시청이 제한되어 있습니다.\n(시청을 원하시면 설정 > 환경설정 > 동영상 강의시청을 \"on\"으로 변경하시기 바랍니다.)"];
#else
            [ModalAlert notify:@"현재 3G/4G망 동영상 강의시청이 제한되어 있습니다.\n(시청을 하시려면 더보기(설정,고객센터) > 환경설정 변경가능합니다."];
#endif
            return NO;
        }
        
        //셀룰러 데이터 동영상 강의 시청 == YES && 셀룰러 데이터 접속알림 == YES
        if ([[self.profileUser objectForKey:@"threegnoti"] boolValue] == YES)
        {
            return [ModalAlert ask:@"현재 3G/4G망에서 접속중으로 별도의 무선데이터 이용료가 발생할 수 있습니다.\n진행 하시겠습니까?"];
        }

    }
    
    return YES;
}

-(BOOL)isNotInternet {
    Reachability *isConnect = [Reachability reachabilityForInternetConnection];
	if ([isConnect currentReachabilityStatus] == NotReachable) {
        return YES;
    }
	return NO;
}

-(BOOL)isWiFi {
    Reachability *isConnect = [Reachability reachabilityForInternetConnection];
//	if ([isConnect currentReachabilityStatus] != ReachableViaWiFi) {
//#if nhlife_14384
//        return [ModalAlert ask:@"현재 3G/4G망에서 접속중으로 별도의 무선데이터 이용료가 발생할 수 있습니다.\n진행 하시겠습니까?"];
//#else
//        return [ModalAlert ask:@"현재 3G/4G망에 접속중으로 별도의 무선데이터 이용료가 발생할 수 있습니다.\n(본 알림은 더보기(설정, 고객센터) > 환경설정에서 취소가능합니다."];
//#endif
//    }
    
    //셀룰러 데이터 (3G,4G) 접속
    if ([isConnect currentReachabilityStatus] == ReachableViaWWAN)
    {
        //설정 > 동영상 강의 시청 == NO
        if ([[self.profileUser objectForKey:@"threegvideo"] boolValue] == NO)
        {
            [ModalAlert notify:@"현재 3G/4G망 동영상 강의시청이 제한되어 있습니다.\n(시청을 하시려면 더보기(설정,고객센터) > 환경설정 변경가능합니다."];
            return NO;
        }
        
        //셀룰러 데이터 동영상 강의 시청 == YES && 셀룰러 데이터 접속알림 == YES
        if ([[self.profileUser objectForKey:@"threegnoti"] boolValue] == YES)
        {
            return [ModalAlert ask:@"현재 3G/4G망에서 접속중으로 별도의 무선데이터 이용료가 발생할 수 있습니다.\n진행 하시겠습니까?"];
        }
        
    }
    
    return YES;

}

-(void)initAndPlayMovie:(NSURL *)movieURL
{
	MPMoviePlayerController *mp = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
	if (mp)
	{
		self.moviePlayer = mp;
	}
}

- (void) clearNotifications {
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	
    if (launchOptions != nil)
    {
        NSDictionary* dictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (dictionary != nil)
        {
            NSLog(@"Launched from push notification: %@", dictionary);
            
            [self clearNotifications];
        }
    }
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;  //핸드폰이 잠들지 않게
    [Utility createDirecotryBy:@"hunet_docroot"];               //다운로드 될 폴더 지정
    [EncryptUtility bitTest];                                   //암복호화 테스트
    [self AddAttributeToAllFolder];
    
    NSDictionary *userInfo = [launchOptions objectForKey:
                              UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if(userInfo != nil) {
        [self application:application badge:userInfo];
        self.pushService = userInfo;
    }
    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        // use registerUserNotificationSettings
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil]];
        [application registerForRemoteNotifications];
         
    } else {
        // use registerForRemoteNotificationTypes:
        [application registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
	
	//[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
	
    self.companySeq = @"";
	self.urlBase = @"http://mlc.hunet.co.kr/App/JLog.aspx";
	self.urlSite = @"http://mlc.hunet.co.kr/";
	self.profileUser = nil;
    
    viewLogin = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    viewLogin.view.frame = [[UIScreen mainScreen] applicationFrame];
    
    window.rootViewController = viewLogin;
    [window makeKeyAndVisible];
    
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString* documentDirectory = [paths objectAtIndex:0];
	NSString* myPath = [documentDirectory stringByAppendingPathComponent:@"b2b.plist"];
	
	//self.profileUser = [NSDictionary dictionaryWithContentsOfFile:myPath];
    self.profileUser = [NSMutableDictionary dictionaryWithContentsOfFile:myPath];
	
	if (self.profileUser == nil) {		// if there is no profile file, create one
		// create the user profile file
		self.profileUser = [[NSMutableDictionary alloc]
							initWithDictionary:[NSDictionary dictionaryWithObjectsAndKeys:
												@"", @"userid",
												@"", @"password",
                                                @"", @"deviceToken",
												[NSNumber numberWithBool:YES], @"autologin",
                                                [NSNumber numberWithBool:YES], @"saveid",
                                                [NSNumber numberWithBool:YES], @"threegnoti",
                                                [NSNumber numberWithBool:YES], @"threegvideo",
                                                [NSNumber numberWithBool:NO], @"IsStaging",
												[NSNumber numberWithBool:NO], @"schedulenoti",
												[NSNumber numberWithBool:NO], @"schedule_monday",
												[NSNumber numberWithBool:NO], @"schedule_tuesday",
												[NSNumber numberWithBool:NO], @"schedule_wednesday",
												[NSNumber numberWithBool:NO], @"schedule_thursday",
												[NSNumber numberWithBool:NO], @"schedule_friday",
												[NSNumber numberWithBool:NO], @"schedule_saturday",
												[NSNumber numberWithBool:NO], @"schedule_sunday", nil]];
		
	}
    
    [profileUser writeToFile:myPath atomically:YES];
    
    NSString *_urlBase = self.urlBase;
    NSString *_urlSite = self.urlSite;
    
    self.urlBase = [[Global sharedSingleton] testPrimaryDomainUrl:_urlBase];
    self.urlSite = [[Global sharedSingleton] testPrimaryDomainUrl:_urlSite];
    
	[[NSNotificationCenter defaultCenter] addObserver: self
											 selector: @selector(reachabilityChanged:)
												 name: kReachabilityChangedNotification
											   object: nil];

	internetReach = [Reachability reachabilityForInternetConnection];
	[internetReach startNotifier];
	
    //application.applicationIconBadgeNumber = 0;
	
	// Handle launching from a notification
	UILocalNotification *localNotif =
	[launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (localNotif) {
		//NSLog(@"Recieved Notification %@",localNotif);
	}
    
    
    NSURL *url = [launchOptions objectForKey:UIApplicationLaunchOptionsURLKey];
    if(url) {
        [self application:application handleOpenURL:url];
    }
    
    if (self.etc1 != nil && self.etc2 != nil && self.etc3 != nil) {
        [NSThread detachNewThreadSelector:@selector(threadAutoLogin:) toTarget:self withObject:@"sso_login"];
        return NO;
    }
	
    if ([[self.profileUser objectForKey:@"autologin"] boolValue] && [[self.profileUser objectForKey:@"userid"] length] > 0 && [[self.profileUser objectForKey:@"password"] length] > 0) {
        //flagLogin = true;
		[NSThread detachNewThreadSelector:@selector(threadAutoLogin:) toTarget:self withObject:@"login"];
	}
	else {
		[self initializeMainView];
	}
    
    courseTab = @"직무교육과정";
    imagineTab = @"자율수강과정";
    
    
    
    return YES;
}


//push : APNS 에 장치 등록 성공시 자동실행
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
	//NSLog(@"deviceToken : %@", deviceToken);
    NSString *dt = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
	[self.profileUser setObject:dt forKey:@"deviceToken"];
    
    NSLog(@"token : %@", dt);
}

//push : APNS 에 장치 등록 오류시 자동실행
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
	//NSLog(@"deviceToken error : %@", error);
}


//push : 어플 실행중에 알림도착
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [self application:application badge:userInfo];
    if (userInfo != nil)
        self.pushService = userInfo;

    if (self.flagLogin)
    {
        NSString *_alert        = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        UIAlertView *pushAlert  = [[UIAlertView alloc] initWithTitle:@"알림"
                                                          message:_alert
                                                         delegate:self
                                                cancelButtonTitle:nil
                                                otherButtonTitles:@"닫기", @"실행", nil];
        pushAlert.tag = 2;
        [pushAlert show];
    }
}



- (void)applicationWillResignActive:(UIApplication *)application {
    if (moviePlayer.playbackState == MPMoviePlaybackStatePlaying || moviePlayer.playbackState == MPMoviePlaybackStateInterrupted) {
		[moviePlayer pause];
	}
    
    //drm player
    if (self.moviePlayerController.playBool) {
        [self.moviePlayerController.player pause];
        self.moviePlayerController.playBool = FALSE;
        [self.moviePlayerController.playBtn setImage:[UIImage imageNamed:@"playBtn.png"] forState:UIControlStateNormal];
    }
    
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
	
	[self profileUserInfoSave];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    NSLog(@"applicationWillEnterForeground");
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
	//application.applicationIconBadgeNumber = 0;
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    if (moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
		[moviePlayer pause];
	}
    
    if (self.moviePlayerController.playBool) {
        [self.moviePlayerController.player pause];
        self.moviePlayerController.playBool = FALSE;
        [self.moviePlayerController.playBtn setImage:[UIImage imageNamed:@"playBtn.png"] forState:UIControlStateNormal];
    }
    
    [self clearNotifications];
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate. See also applicationDidEnterBackground:.
     */
    [self saveContext];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    //tcenter://etc1=214&etc2=uid&etc3=사번
    if (!url) {  return NO; }
    
    NSString *strURL = [url absoluteString];
    if ([strURL.lowercaseString hasPrefix:@"tcenter://"]) {
        
        NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:url resolvingAgainstBaseURL:NO];
        NSArray *queryItems = urlComponents.queryItems;
        
        self.etc1 = [Util valueForKey:@"etc1" fromQueryItems:queryItems];
        self.etc2 = [Util valueForKey:@"etc2" fromQueryItems:queryItems];
        self.etc3 = [Util valueForKey:@"etc3" fromQueryItems:queryItems];
        
        if (self.etc1 == nil) {
            //녹십자 앱투앱 로그인을 위한 custom
            self.etc1 = [Util valueForKey:@"cSeq" fromQueryItems:queryItems];
            
            if ([self.etc1 isEqual:@"13830"]) {
                self.etc2 = [NSString stringWithFormat:@"gc%@", [Util valueForKey:@"userId" fromQueryItems:queryItems]];
                self.etc3 = [Util valueForKey:@"userId" fromQueryItems:queryItems];
                
                //과정상세 URL로 Redirect
                //http://mlc.hunet.co.kr/SSO/StudyGate.aspx?processCd=HLSP10845&processYear=2015&processTerm=1&cSeq=2162&userId=magic7k
                //processcd=hlsp03975&processyear=2012&processterm=813
                self.gotoUrlAfterLogin = [NSString stringWithFormat:@"%@/SSO/StudyGate.aspx?processCd=%@&processYear=%@&processTerm=%i&cSeq=%@&userId=%@",
                                          self.urlSite,
                                          [Util valueForKey:@"processCd" fromQueryItems:queryItems],
                                          [Util valueForKey:@"processYear" fromQueryItems:queryItems],
                                          [[Util valueForKey:@"processTerm" fromQueryItems:queryItems] intValue],
                                          [Util valueForKey:@"cSeq" fromQueryItems:queryItems],
                                          [NSString stringWithFormat:@"gc%@", [Util valueForKey:@"userId" fromQueryItems:queryItems]]
                                          ];
            }
            else if ([self.etc1 isEqual:@"9242"])
            {
                self.etc2 = [NSString stringWithFormat:@"gg-%@", [Util valueForKey:@"userId" fromQueryItems:queryItems]];
                self.etc3 = [Util valueForKey:@"userId" fromQueryItems:queryItems];
                
                //과정상세 URL로 Redirect
                //http://mlc.hunet.co.kr/SSO/StudyGate.aspx?processCd=HLSP10845&processYear=2015&processTerm=1&cSeq=2162&userId=magic7k
                //processcd=hlsp03975&processyear=2012&processterm=813
                self.gotoUrlAfterLogin = [NSString stringWithFormat:@"%@/SSO/StudyGate.aspx?processCd=%@&processYear=%@&processTerm=%i&cSeq=%@&userId=%@",
                                          self.urlSite,
                                          [Util valueForKey:@"processCd" fromQueryItems:queryItems],
                                          [Util valueForKey:@"processYear" fromQueryItems:queryItems],
                                          [[Util valueForKey:@"processTerm" fromQueryItems:queryItems] intValue],
                                          [Util valueForKey:@"cSeq" fromQueryItems:queryItems],
                                          [NSString stringWithFormat:@"gg-%@", [Util valueForKey:@"userId" fromQueryItems:queryItems]]
                                          ];
            }
        }
        
        return YES;
    }
    return NO;
}




/**
 
 * @brief <#Description#>
 URL 스키마 호출시, 이 delegate 메서드를 호출함
 Default > tcenter://?etc1=13830&etc2={uid}&etc3={공통아이디를 제외한 ID}
 녹십자-13830 > tcenter://cSeq=13830&processCd=HLSP03975&processYear=2015&processTerm=0002&userId=1206005
 
 * @param <#Parameter#>
 
 * @return <#Return#>
 
 * @remark <#Remark#>
http://test.mlc.hunet.co.kr/App/JLog.aspx?type=24&etc1=13830&etc2=&etc3=1206005
{"IsSuccess":"YES","CompanySeq":13830,"Url":"http://test.visioncenter.hunet.co.kr","UserId":"gc1206005","Password":"1206005","HReminderUseType":"0","HStoryUseType":"0","EduType":"OnlyLearning"}
 
 * @see <#See#>
 
 * @author magic7k
 
 */
-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if (!url) {  return NO; }
    
    NSString *strURL = [url absoluteString];
    if ([strURL.lowercaseString hasPrefix:@"tcenter://"]) {
        
        [self application:application handleOpenURL:url];
        
        if (self.etc1 != nil && self.etc2 != nil && self.etc3 != nil) {
            [NSThread detachNewThreadSelector:@selector(threadAutoLogin:) toTarget:self withObject:@"sso_login"];
        }
        
        return YES;
    }
    return NO;
}

#pragma mark -
#pragma mark UITabBarControllerDelegate methods


 // Optional UITabBarControllerDelegate method.
 - (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
     if (!self.flagLogin) {
         [self loginViewControllerDisplay];
     }
     
     NSUInteger indexOfTab = [tabBarController.viewControllers indexOfObject:viewController];
     
     tagPrevious = tagCurrent;
     tagCurrent = indexOfTab;
     
     UINavigationController *navController = (UINavigationController *)[tabBarController.viewControllers objectAtIndex:tabBarController.selectedIndex];
     if ([navController isKindOfClass:[UINavigationController class]])
     {
         [navController popToRootViewControllerAnimated:YES];
         NSArray *viewControllers = navController.viewControllers;         
         UIViewController *rootViewController = [viewControllers objectAtIndex:0];
         if ([rootViewController isKindOfClass:[UIViewController class]])
         {
             [rootViewController viewDidLoad];
         }
     }
     else
     {
         UIViewController *view = (UIViewController*)[tabBarController.viewControllers objectAtIndex:tabBarController.selectedIndex];
         [view viewDidLoad];
     }
}

/*
 // Optional UITabBarControllerDelegate method.
 - (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed {
 }
 */

#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notif {
	// Handle the notificaton when the app is running
	
	//application.applicationIconBadgeNumber = 0;
	//NSLog(@"Recieved Notification %@",notif);
}

- (void)checkVersion {
    [VersionChecher checkWithUpdate];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1)
        [self performSelector:@selector(pushServiceURL) withObject:nil afterDelay:0.2];
    else
        self.pushService = nil;
}

- (void)application:(UIApplication *)application badge:(NSDictionary *)userInfo {
    NSString *badge = [[userInfo valueForKey:@"aps"] valueForKey:@"badge"];
    application.applicationIconBadgeNumber = [badge intValue];
}



- (void)pushServiceURL
{
    if (self.pushService == nil)
        return;

    long long userSeq                   = [[self.pushService valueForKey:@"userSeq"] unsignedLongLongValue];
    NSString *selectedTabIndex          = [self.pushService valueForKey:@"selectedTabIndex"];
    NSString *selectedTabIndexUrl       = [self.pushService valueForKey:@"selectedTabIndexUrl"];
    NSString *selectedTabIndexDetail    = [self.pushService valueForKey:@"selectedTabIndexDetail"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSError *errorReq = nil;
        NSString *url = [NSString stringWithFormat:@"%@?type=1004&userSeq=%llu&eventType=click", self.urlBase, userSeq];
        [Util getNSDataByRequestUrl:[NSURL URLWithString:url]
                           andQuery:nil
                           andError:&errorReq];
        
    });

    
    self.tabBarController.selectedIndex = [selectedTabIndex integerValue];
    UIViewController *view = self.tabBarController.selectedViewController;
    
    if ([view isKindOfClass:[UINavigationController class]]) {
        
        NSMutableArray *controllers                     = [[NSMutableArray alloc] init];
        UINavigationController *navigationController    = (UINavigationController*)view;
        
        if (selectedTabIndexDetail && selectedTabIndexDetail.length)
        {
            MoreViewController *moreViewController = [[navigationController viewControllers] objectAtIndex:0];
            [controllers addObject:moreViewController];
            
            if ([selectedTabIndexDetail isEqualToString:@"공지사항"])
            {
                NoticeViewController *noticeViewController = [[NoticeViewController alloc] initWithNibName:@"NoticeViewController" bundle:nil];
                noticeViewController.title  = selectedTabIndexDetail;
                noticeViewController.url    = selectedTabIndexUrl;
                [controllers addObject:noticeViewController];
            }

            if ([selectedTabIndexDetail isEqualToString:@"H-스토리"])
            {
                BaseViewController *hStoryViewController = [[HStoryViewController alloc]
                                                            initWithNibName:@"BaseViewController" bundle:nil];
                if (selectedTabIndexUrl && selectedTabIndexUrl.length)
                {
                    hStoryViewController.currentUrl = selectedTabIndexUrl;
                    [hStoryViewController openerCallFun];
                }
                [controllers addObject:hStoryViewController];
            }
            
            if ([selectedTabIndexDetail isEqualToString:@"H-리마인더"])
            {
                BaseViewController *hReminderViewController = [[HReminderViewController alloc]
                                                 initWithNibName:@"BaseViewController" bundle:nil];
                if (selectedTabIndexUrl && selectedTabIndexUrl.length)
                {
                    hReminderViewController.currentUrl = selectedTabIndexUrl;
                    [hReminderViewController openerCallFun];
                }
                [controllers addObject:hReminderViewController];
            }
            
            [navigationController setViewControllers:controllers animated:YES];
        }
        
    }
    else
    {
        if (selectedTabIndexUrl && selectedTabIndexUrl.length)
        {
            if ([view isKindOfClass:[HomeViewController class]]) {
                [((HomeViewController*)view) webViewpushUrl:selectedTabIndexUrl];
            }
            
            if ([view isKindOfClass:[BaseViewController class]]) {
                [((BaseViewController*)view) webViewpushUrl:selectedTabIndexUrl];
            }
            
            if ([view isKindOfClass:[LectureViewController class]]) {
                [((LectureViewController*)view) webViewpushUrl:selectedTabIndexUrl];
            }
        }
    }
    
    self.pushService = nil;
}

//https://developer.apple.com/library/ios/qa/qa1719/_index.html
//How do I prevent files from being backed up to iCloud and iTunes?
- (void)AddAttributeToAllFolder{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsPath error:nil];
    
    for (int i =0; i < [dirContents count]; i++) {
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@",documentsPath,[dirContents objectAtIndex:i]]];
        if ([self addSkipBackupAttributeToItemAtURL:url]) {
            NSLog(@"success! could add do not backup attribute to folder");
        }
    }
}

- (void)AddAttributeToFolder:(NSString*)folderName{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@",documentsPath,folderName]];
    if ([self addSkipBackupAttributeToItemAtURL:url]) {
        NSLog(@"success! could add do not backup attribute to folder");
    }
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL{
    int result = 0;
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version < 5.1) {
        assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
        const char* filePath = [[URL path] fileSystemRepresentation];
        const char* attrName = "com.apple.MobileBackup";
        u_int8_t attrValue = 1;
        result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
        return result == 0;
    } else {
        assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
        NSError *error = nil;
        BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                      forKey: NSURLIsExcludedFromBackupKey error: &error];
        
        if(!success){
            NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
        }
        return success;
    }
}


- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}


#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"training" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"training.sqlite"];
    
    // handle db upgrade
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        // Handle error
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext2
{
    if (_managedObjectContext2 != nil) {
        return _managedObjectContext2;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator2];
    if (coordinator != nil) {
        _managedObjectContext2 = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext2 setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext2;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel2
{
    if (_managedObjectModel2 != nil) {
        return _managedObjectModel2;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"addinfo" withExtension:@"momd"];
    _managedObjectModel2 = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel2;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator2
{
    if (_persistentStoreCoordinator2 != nil) {
        return _persistentStoreCoordinator2;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"addinfo.sqlite"];
    
    // handle db upgrade
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    NSError *error = nil;
    _persistentStoreCoordinator2 = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel2]];
    if (![_persistentStoreCoordinator2 addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        // Handle error
    }
    
    return _persistentStoreCoordinator2;
}


#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

/*** for WebScriptBridge Begin ***/
- (void)goToLogin
{
}
/*
- (void)DoLogin:(NSString *)userId :(NSString *)userPwd
{
}
*/
- (void)DoLogin
{
}
/*** for WebScriptBridge End ***/

@end


/*** for WebScriptBridge Begin ***/
static WebScriptBridge *gWebScriptBridge = nil;

@implementation WebScriptBridge
- (void)goToLogin:(NSString *)returnUrl
{
    NSLog(@"call goToLogin from WebScriptBridge");
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    [app loginViewControllerDisplay];
}

- (void)DoLogin:(NSString *)userId :(NSString *)userPwd
{
    NSLog(@"call DoLogin from WebScriptBridge");
    if (!viewLogin) {
        viewLogin = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    }
    [viewLogin onWebLogin:userId :userPwd];
}

- (void)changePassword:(NSString *)userPwd
{
    NSLog(@"call ChangePassword from WebScriptBridge");
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    [app.profileUser setObject:userPwd forKey:@"password"];
    
    //[ModalAlert notify:@"정상적으로 수정되었습니다."];
    
    [app.window.rootViewController.navigationController popToRootViewControllerAnimated:YES];
    NSObject *obj = [app.tabBarController.viewControllers objectAtIndex:[app.tabBarController.viewControllers count] - 1];
    UINavigationController *nc = (UINavigationController*)obj;
    if ([nc isKindOfClass:[UINavigationController class]])
    {
        [nc popToRootViewControllerAnimated:YES];
    }
    else
    {
        [nc.navigationController popToRootViewControllerAnimated:YES];
    }
}

+ (BOOL)isKeyExcludedFromWebScript:(const char *)name;
{
    return NO;
}

+ (BOOL)isSelectorExcludedFromWebScript:(SEL)aSelector;
{
    return NO;
}

+ (NSString *)webScriptNameForSelector:(SEL)sel
{
    if (sel == @selector(goToLogin:)) return @"goToLogin";
    if (sel == @selector(DoLogin::)) return @"DoLogin";
    if (sel == @selector(changePassword:)) return @"changePassword";
    
    return nil;
}

+ (WebScriptBridge*)getWebScriptBridge {
    if (gWebScriptBridge == nil)
        gWebScriptBridge = [WebScriptBridge new];
    
    return gWebScriptBridge;
}
@end
/*** for WebScriptBridge End ***/

