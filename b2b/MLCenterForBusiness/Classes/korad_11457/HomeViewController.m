//
//  HomeViewController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 5. 17..
//  Copyright 2011 hunet. All rights reserved.
//

#import "HomeViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "ModalAlert.h"
#import "PlayerViewController.h"
#import "SSViewController.h"
//#import "ModalViewController.h"
#import "MoreViewController.h"
#import "NoticeViewController.h"
#import "DownloadViewController.h"
#import "SelfStudyViewController.h"
#import "UserViewController.h"
#import "Header.h"
#import "LectureViewController.h"
#import "ELCImagePickerController.h"
#import "ELCAlbumPickerController.h"
#import "ELCAssetTablePicker.h"
#import "WebModalViewController.h"
#import "PlayerCallFun.h"
#import "Util.h"
#import "CJSONDeserializer.h"
#import "StwayHeader.h"
#import "GraphicsUtile.h"

@implementation HomeViewController
@synthesize urlMark;
@synthesize playerCallFun = _playerCallFun;

#pragma mark -
#pragma mark UIWebViewDelegate implementation

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        self.title = NSLocalizedString(@"首页", @"首页");
        self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_home"];

    }
    return self;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    if ([viewWeb.subviews count] > 0) {
        UIView *scrollView = viewWeb.subviews[0];
        
        for (UIView *childView in scrollView.subviews) {
            if ([childView isKindOfClass:[UIWebDocumentView class]]) {
                UIWebDocumentView *documentView = (UIWebDocumentView *)childView;
                WebScriptObject *wso = documentView.webView.windowScriptObject;
                
                [wso setValue:[WebScriptBridge getWebScriptBridge] forKey:@"ScriptBridge"];
            }
        }
    }
	[indicator stopAnimating];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
	[indicator startAnimating];
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
	[indicator stopAnimating];	
	//[ModalAlert notify:@"네트워크 연결에 문제가 생겼습니다."];	
}


- (int) getTabIndexFromRequestUrl:(NSURL *)url {
    
    // http://app.hunet.co.kr/api.aspx?cmd=tab&cseq={company_seq}&url={encode_url}
    //NSString *apiUrl = @"http://app.hunet.co.kr/api.aspx?cmd=tab&cseq={company_seq}&url={encode_url}";
    
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    NSString *param = [NSString stringWithFormat:@"cmd=tab&cseq=%@&url=%@", app.companySeq, url.absoluteString];
    NSError *errorReq = nil;
    
    NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:@"http://app.hunet.co.kr/api.aspx"] andQuery:param andError:&errorReq];
    if (jsonData)
    {
        NSError *errorJSON = nil;
        NSDictionary *result = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
        return [[result objectForKey:@"tab_index"] integerValue];
    }
    
    return 0;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    //http://app.hunet.co.kr/api.aspx?cmd=tab&cseq={company_seq}&url={encode_url}
    
    
    
	MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
	NSURL *url = request.URL;
	NSString *urlString = url.absoluteString;
	
    NSLog(@"%@, %@", urlString, app.eduType);
    
    //int tab_index = [self getTabIndexFromRequestUrl:request.URL];
    //self.tabBarController.selectedIndex = tab_index;
    
    // 외부 사이트로 이동
    if ([urlString.lowercaseString hasPrefix:@"escape://"]) {
        NSString *escapeUrl = [urlString stringByReplacingOccurrencesOfString:@"escape://" withString:@"http://"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:escapeUrl]];
        return NO;
    }
    
    
    if ([urlString.lowercaseString hasPrefix:@"appstore://"]) {
        NSString *escapeUrl = [urlString stringByReplacingOccurrencesOfString:@"appstore://" withString:@""];
        escapeUrl = [escapeUrl stringByReplacingOccurrencesOfString:@"https//" withString:@"https://"];
        escapeUrl = [escapeUrl stringByReplacingOccurrencesOfString:@"http//" withString:@"http://"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:escapeUrl]];
        return NO;
    }
    
    if ([urlString.lowercaseString hasPrefix:@"sangsangdrmplayer://"] ||
        [urlString.lowercaseString hasPrefix:@"sangsangdownload://"] ||
        [urlString.lowercaseString hasPrefix:@"sangsangdownloadplay://"]) {
        
        NSLog(@"sangsangdrmp");
        
        NSArray *tmpArray = [urlString componentsSeparatedByString:@"://"];
        NSString *act = [tmpArray objectAtIndex:0];
        NSString *param = [tmpArray objectAtIndex:1];
        
        if ([app isWiFi]) {
            
            _playerCallFun = [[PlayerCallFun alloc] init];
            _playerCallFun.delegateViewController = self;
            
            self.urlMark = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
            [_playerCallFun sangsangmaruInit:[NSArray arrayWithObjects:act, param, nil]];
        }
        
        return NO;
    }
    
    //모달 창으로 뛰우기
    if ([urlString.lowercaseString hasPrefix:@"modal://"] ||
        ([urlString.lowercaseString hasPrefix:@"http://"] && [urlString.lowercaseString hasSuffix:@".mp4"]))
    {
        NSDictionary *simpleModalUrlDic = [NSDictionary  dictionaryWithObjectsAndKeys:
                                           @"스피쿠스 전화외국어", @"://m.spicus.com",
                                           @"알파코", @"://www.alpaco.co.kr",
                                           @"지식영상", @"://w.hunet.hscdn.com/hunet/vod/knowledge/", nil];
        BOOL popupSimpleModal = false;

        NSString *simpleModalPopupTitle = nil;
        for (NSString *domainKey in simpleModalUrlDic) {
            
            if ([urlString.lowercaseString rangeOfString:domainKey].location != NSNotFound) {
                popupSimpleModal = true;
                simpleModalPopupTitle = [simpleModalUrlDic objectForKey: domainKey];
                break;
            }
        }
        
        WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
        webModal.webViewUrl = [request.URL.absoluteString stringByReplacingOccurrencesOfString:@"modal://" withString:@"http://"];
        webModal.title = simpleModalPopupTitle;
        [self presentViewController:webModal animated:YES completion:nil];
        
        return NO;
    }
    
    // 회원정보 페이지로 이동
    if ([urlString.lowercaseString hasPrefix:@"membermodify://"]) {
        self.tabBarController.selectedIndex = 4;
        
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
        UserViewController *second = [[UserViewController alloc] initWithNibName:@"UserViewController" bundle:nil];
        NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
        [navi setViewControllers:controllersArray animated:YES];
        
        return NO;
    }
    
    if ([urlString.lowercaseString hasPrefix:@"customercenter://"]) {
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Customer" ofType:@"html"]];
        NSLog(@"url : %@", url);
        [viewWeb loadRequest:[NSURLRequest requestWithURL:url]];
        return NO;
    }
 
    //웹 로그아웃 제어
    if([urlString.lowercaseString hasPrefix:@"logout://"])
    {
        [[MLCenterForBusinessAppDelegate sharedAppDelegate] loginViewControllerDisplay];
        return YES;
    }
  
    
    //다운로드
    if ([urlString.lowercaseString isEqualToString:@"downloadcenter://"]) {
        self.tabBarController.selectedIndex = 3;
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
        DownloadViewController *second = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
        NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
        [navi setViewControllers:controllersArray animated:YES];
        
        return NO;
    }
    
    //나의 강의실, 사용자 즐겨찾기
    if ([urlString.lowercaseString rangeOfString:@"/my/my.aspx"].location != NSNotFound
        || [urlString.lowercaseString rangeOfString:@"/my/mycourse"].location != NSNotFound
        || [urlString.lowercaseString rangeOfString:@"/classroom/online"].location != NSNotFound
        || [urlString.lowercaseString rangeOfString:@"/imgclassroom/studysummary"].location != NSNotFound){
        
        int _tabIndex = 1;
        
        if ([urlString.lowercaseString rangeOfString:@"/my/my.aspx"].location != NSNotFound) {
            [app setSelectedTabIndex:_tabIndex indexOrUrl:nil];
            return NO;
        }
        
        [app setSelectedTabIndex:_tabIndex indexOrUrl:urlString];
        return NO;
    }
    
    //상상마루 전용  version = 5, imagine_contract_yn = 'Y', only_imagine_yn = 'Y'
    if ([app.eduType isEqualToString:@"OnlyImagine"]) {
        if ([urlString.lowercaseString rangeOfString:@"/imgcategory/contents?"].location != NSNotFound
            || [urlString.lowercaseString rangeOfString:@"/imgclassroom/series?"].location != NSNotFound
            || [urlString.lowercaseString rangeOfString:@"/imgclassroom/contents?"].location != NSNotFound
            || [urlString.lowercaseString rangeOfString:@"/imgclassroom/requiredlecture?headertitle="].location != NSNotFound)
        {
            self.tabBarController.selectedIndex = 1;
            SelfStudyViewController *selfStudy = (SelfStudyViewController*)self.tabBarController.selectedViewController;
            if ([urlString.lowercaseString rangeOfString:@"childcategoryyn=y"].location != NSNotFound) {
                
                NSString *param =[urlString substringFromIndex:[urlString rangeOfString:@"?"].location + 1];
                
                urlString = [NSString stringWithFormat:@"%@/imgCategory/ContentsCategoryListAll?%@&layoutdisplayyn=y", app.urlCenter, param];
                
                //return NO;
            }
            [selfStudy detailView:urlString];
            return NO;
        }
        
        //웹 로그아웃 제어
        if([urlString.lowercaseString hasPrefix:@"logout://"])
        {
            [[MLCenterForBusinessAppDelegate sharedAppDelegate] loginViewControllerDisplay];
        }
        
        return YES;
    }
    
	//행복한 인문학당 2.0
	if ([urlString.lowercaseString rangeOfString:@"/lecture/category_inmun.aspx"].location != NSNotFound){
		[app setLectureUrlType:@"7"];
		[app setSelectedTabIndex:1];		
		return NO;
	}
    
    //행복한 인문학당 4.0
    if ([urlString.lowercaseString rangeOfString:@"/knowledgelive"].location != NSNotFound){
        return YES;
    }
    
    
    if ([urlString.lowercaseString rangeOfString:@"/lecture/category.aspx?param="].location != NSNotFound){
        NSArray * param = [urlString componentsSeparatedByString:@"="];
        self.tabBarController.selectedIndex = 1;
        LectureViewController *view = (LectureViewController*)self.tabBarController.selectedViewController;
        view._currentUrl = @"";
        view._param = [param objectAtIndex:1];
        [self.tabBarController.selectedViewController viewWillAppear:YES];
		return NO;
	}
    
	//주제별 교육과정
//	if ([urlString.lowercaseString rangeOfString:@"/lecture/category"].location != NSNotFound){
//		[app setLectureUrlType:@"6"];
//		[app setSelectedTabIndex:1];		
//		return NO;
//	}
    
    
    //주제별 교육과정 상세
//	if ([urlString.lowercaseString rangeOfString:@"/lecture/depth.aspx"].location != NSNotFound){
//		[app setLectureUrlType:@"6"];	
//        [app setSelectedTabIndex:1 indexOrUrl:urlString];
//		return NO;
//	}
    
	//상담실-공지사항, 공지사항 상세
	if ([urlString.lowercaseString rangeOfString:@"/counsel/counsel_noticelist.aspx"].location != NSNotFound
        || [urlString.lowercaseString rangeOfString:@"/counsel/counsel_noticedetail.aspx"].location != NSNotFound
        || [urlString.lowercaseString rangeOfString:@"/counsel/notice"].location != NSNotFound){

        self.tabBarController.selectedIndex = 3;
        
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
        NoticeViewController *second = [[NoticeViewController alloc] initWithNibName:@"NoticeViewController" bundle:nil];
        second.title = @"공지사항";
        second.url = urlString;
        NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
        [navi setViewControllers:controllersArray animated:YES];
        
		return NO;
	}
	
	//홍화면 편집
	if ([urlString.lowercaseString rangeOfString:@"/home/home_edit.aspx"].location != NSNotFound){
		return YES;
	}	
	
    
	//pc버전 링크
	if ([urlString.lowercaseString isEqualToString:[NSString stringWithFormat:@"%@/", app.urlCenter]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:app.urlCenter]];
		return NO;
	}
    
    
	//상상마루
	if ([urlString.lowercaseString rangeOfString:@"/sangsang/sangsanglink.aspx"].location != NSNotFound
		|| [urlString.lowercaseString rangeOfString:@"/login/mlclogin.aspx"].location != NSNotFound
		|| [urlString.lowercaseString rangeOfString:@"/submain.aspx?type=app"].location != NSNotFound
        || [urlString.lowercaseString rangeOfString:@"https://ssl.hunet.co.kr"].location != NSNotFound
        || [urlString.lowercaseString rangeOfString:@"/imghome"].location != NSNotFound) {
        
        if ([app.eduType isEqualToString:@"LearningAndImagine"]) {
            self.tabBarController.selectedIndex = 2;
            return NO;
        }
		
		//NSString *ssUrl = [NSString stringWithFormat:@"%@/submain.aspx?type=app", [app.urlCenter stringByReplacingOccurrencesOfString:@"http://" withString:@"http://m."]];
        
        //NSLog(@"%@", ssUrl);
        
//		if ([urlString.lowercaseString isEqualToString:ssUrl.lowercaseString] || [urlString.lowercaseString rangeOfString:@"https://ssl.hunet.co.kr"].location != NSNotFound) {
//			SSViewController *viewToPush = [[SSViewController alloc] initWithNibName:@"SSViewController" bundle:nil];
//			if (viewToPush) {
//				viewToPush.url = urlString;
//				[self presentViewController:viewToPush animated:YES completion:nil];
//			}
//            
//            [self performSelector:@selector(viewLoad) withObject:nil afterDelay:0.3];
//            
//			return NO;
//		}
		return YES;
	}
    
    //htmlpalyer:// 프로토콜로 호출은 영상url에 대한 단순 스트리밍을 용도로 사용
    if ([urlString hasSuffix:@".mp4"]) {
        //    if ([urlString.lowercaseString rangeOfString:@"htmlplayer://"].location != NSNotFound) {
        NSLog(@"htmlplayer");
        
        //urlString = @"http://w.hunet.hscdn.com/hunet/files/knowledge/High_20150209034650_08_%EC%A7%81%EC%9E%A5%EC%9D%B8%EC%9D%98%EA%B8%B0%EB%B3%B8%EC%83%81%EC%8B%9D_540.mp4";
        
        NSLog(@"urlString = %@", urlString);
        
        //NSString *currentURL = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
        //self.urlMark = currentURL;
        
        NSString *mp4Url = [urlString stringByReplacingOccurrencesOfString:@"htmlplayer://" withString:@"http://"];
        
        NSUserDefaults *userdeDefaults = [NSUserDefaults standardUserDefaults];
        [userdeDefaults setObject:mp4Url forKey:FILENAME];
        //NSLog(@"Streaming filename = %@", [userdeDefaults objectForKey:FILENAME]);
        
        
        MoviePlayerController * movieCon = [[MoviePlayerController alloc] initWithNibName:@"MoviePlayerController" bundle:nil];
        movieCon.isStreamingPlay = TRUE;
        movieCon.currentSecond = 0;
        movieCon.scrollType =  1;   // progress touch 여부 0 : 진도바 이동 불가, 1 : 진도바 이동 가능
        [MLCenterForBusinessAppDelegate sharedAppDelegate].moviePlayerController = movieCon;
        [self presentViewController:movieCon animated:YES completion:nil];
        
        return NO;
    }
    
    if([[[request URL] absoluteString] hasPrefix:@"jscall"])
    {
        NSString *requestString = [[request URL] absoluteString];
        NSArray *components = [requestString componentsSeparatedByString:@"://"];
        NSString *functionName = [components objectAtIndex:1];
        
        //[self performSelector:NSSelectorFromString(functionName)];
        SEL selector = NSSelectorFromString(functionName);
        IMP imp = [self methodForSelector:selector];
        void (*func)(id, SEL) = (void *)imp;
        func(self, selector);
        
        return NO;
    }
    
	return YES;
}

- (void)callPhotoView {
    ELCAlbumPickerController *albumController = [[ELCAlbumPickerController alloc] initWithNibName: nil bundle: nil];
	ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initWithRootViewController:albumController];
    [albumController setParent:elcPicker];
	[elcPicker setDelegate:self];
    elcPicker.maximumImagesCount = 4;
    
    [self presentViewController:elcPicker animated:YES completion:nil];
    
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    if ([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]){
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)pickerWebSave:(NSArray *)info
{
    @autoreleasepool {
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        
        NSLog(@"%@", app.urlCenter);
        
        NSString *urlString, *boundary;
        imgFileName = @"";
        NSInteger i = 0;
        urlString = [NSString stringWithFormat:@"%@/Common/MobileUpload?path=Community", app.urlCenter];
        boundary = @"CommunityPicImage";
        
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setCachePolicy:NSURLRequestUseProtocolCachePolicy];
        [request setHTTPMethod:@"POST"];
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        
        /*
         NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
         
         [[NSRunLoop currentRunLoop] run];
         
         
         // Schedule your connection to run on threads runLoop.
         [connection scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
         */
        
        
        /*
         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         NSRunLoop *loop = [NSRunLoop currentRunLoop];
         [connection scheduleInRunLoop:loop forMode:NSRunLoopCommonModes];
         [loop run]; // make sure that you have a running run-loop.
         });
         */
        
        
	for(NSDictionary *dict in info) {
            UIImage *image = [dict objectForKey:UIImagePickerControllerOriginalImage];
            NSData *ImageData = UIImageJPEGRepresentation(image, 1.0);
            NSMutableData *body = [NSMutableData data];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image%d\"; filename=\"Image%d.jpg\"\r\n", i, i] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:ImageData]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [request setHTTPBody:body];
            
            NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
            NSString *returnString = [[NSString alloc] initWithData:urlData encoding:NSUTF8StringEncoding];
            
            NSLog(@"%@", returnString);
            
            imgFileName = [imgFileName stringByAppendingFormat:@"|%@", returnString];
            
            
            i=i+1;
	}
        
        if ([info count] == i) {
            [self performSelectorOnMainThread:@selector(saveEnd:) withObject:imgFileName waitUntilDone:NO];
        }
    }
}


- (void)saveEnd:(NSString*) ImgFileNames
{
	[mask removeFromSuperview];
	[uploadIndicator stopAnimating];
    [viewWeb stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"ReadingPhoto('%@');", ImgFileNames]];
}


- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    if ([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]){
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    uploadIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    uploadIndicator.hidesWhenStopped = YES;
    
    
    mask = [[UIView alloc] initWithFrame:[[self.view window] frame]];
	mask.userInteractionEnabled = NO;
	mask.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.50];
    [mask addSubview:uploadIndicator];
    uploadIndicator.center = mask.center;
	[self.view addSubview:mask];
	
	[uploadIndicator startAnimating];
    
    
    [NSThread detachNewThreadSelector:@selector(pickerWebSave:) toTarget:self withObject:info];
    
}

- (void)viewDidLoad {
    //NSLog(@"viewDidLoad");
    
    [super viewDidLoad];
	[self viewLoad];
	
	
}

- (void)viewLoad {
    NSLog(@"viewLoad");
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
	if (app.flagLogin)
    {
        NSString *urlText = [NSString stringWithFormat:@"%@?type=4&uid=%@&pw=%@", app.urlBase, [app.profileUser objectForKey:@"userid"], [app.profileUser objectForKey:@"password"]];
        
        [viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0]];
        
        viewWeb.scalesPageToFit = YES;
    }
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (NSUInteger)supportedInterfaceOrientations
{
    indicator.center = viewWeb.center;
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskAllButUpsideDown;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {

	viewWeb = nil;
	indicator = nil;
    [super viewDidUnload];
}


- (void)dealloc {
	viewWeb.delegate = nil;
}

- (void)webViewpushUrl:(NSString *)webViewpushUrl {
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    [viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:webViewpushUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0]];
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    app.pushService = nil;
    //self.appDelegate.pushService = nil;
}

- (void)openerCallFun {
    NSLog(@"player close");
    if (self.urlMark != nil) {
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:self.urlMark]
                                                    cachePolicy: NSURLRequestReloadIgnoringCacheData
                                                timeoutInterval:60.0
                                    ];
        [viewWeb loadRequest:requestObj];
        self.urlMark = @"";
    }
}

@end
