//
//  OverlayViewController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 6. 8..
//  Copyright 2011 hunet. All rights reserved.
//

#import "OverlayView.h"
#import "MLCenterForBusinessAppDelegate.h"

#define IS_4_INCH CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(320, 568)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(568, 320))

@implementation OverlayView
@synthesize mainView, btnDone, btnMagnify, sliderProcess, lblCurrent, lblDuration, btnBack, btnPlay, btnForward, btnPause;
@synthesize btnMarkShow;
@synthesize tb;
@synthesize markImageView;
@synthesize btnMarkClose;
@synthesize markList;

- (id)init {
    CGSize viewSize = [[UIScreen mainScreen] bounds].size;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        self = [super initWithFrame:CGRectMake(0, 0, viewSize.height, viewSize.width)];
    }
    else{
        self = [super initWithFrame:CGRectMake(0, 40, viewSize.height, viewSize.width)];
    }
        if (self != nil) {
        NSString *loadNibNamed = @"OverlayView";
        if (IS_4_INCH) {
            loadNibNamed = @"OverlayView~568h";
        }
        appDelegate = (MLCenterForBusinessAppDelegate*)[[UIApplication sharedApplication] delegate];
		NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:loadNibNamed owner:self options:nil];
		[self addSubview:[nibViews objectAtIndex:0]];
        [self displayItems];
	}
    return self;
}


-(IBAction)Done {
    [appDelegate.moviePlayer stop];
}

- (IBAction)Magnify {
    if (appDelegate.moviePlayer.scalingMode == MPMovieScalingModeAspectFill) {
		appDelegate.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
		//self.btnMagnify.image = [UIImage imageNamed:@"btn_expand.png"];
        self.btnMagnify.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"btn_expand" ofType:@"png"]];
	} else if (appDelegate.moviePlayer.scalingMode == MPMovieScalingModeAspectFit) {
		appDelegate.moviePlayer.scalingMode = MPMovieScalingModeAspectFill;
		//self.btnMagnify.image = [UIImage imageNamed:@"btn_reduce.png"];		
        self.btnMagnify.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"btn_reduce" ofType:@"png"]];
	}
    
}

- (IBAction)Play {
	[appDelegate.moviePlayer play];
}

- (IBAction)Pause {	
	[appDelegate.moviePlayer pause];
}


- (IBAction)Forward {
    if (appDelegate.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
		self.sliderProcess.value = appDelegate.moviePlayer.currentPlaybackTime += 10;
	}
}

- (IBAction)Back {
    if (appDelegate.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
		self.sliderProcess.value = appDelegate.moviePlayer.currentPlaybackTime -= 10;
	}
}

- (IBAction)SliderDown {
    if (appDelegate.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
		[appDelegate.moviePlayer pause];
	}
}

- (IBAction)SliderUp {
    if (appDelegate.moviePlayer.playbackState == MPMoviePlaybackStatePaused) {
		[appDelegate.moviePlayer play];
	}	
	
	appDelegate.moviePlayer.currentPlaybackTime = self.sliderProcess.value;
}


- (IBAction) MarkHidden {
    tb.hidden = YES;
    markImageView.hidden = YES;
    btnMarkClose.hidden = YES;
}

- (IBAction) MarkDisplay {
    tb.hidden = !tb.hidden;
    markImageView.hidden = NO;
    btnMarkClose.hidden = NO;
}


- (void)displayItems {
	
	CGFloat alphaVal = ((UIView*)[self.subviews objectAtIndex:0]).alpha;
	SEL sel;
	
	if (alphaVal == 1) {
		[UIApplication sharedApplication].statusBarHidden = YES;
		sel = @selector(viewAlphaZero);
	}
	else {
		[UIApplication sharedApplication].statusBarHidden = NO;
		sel = @selector(viewAlphaOne);
	}

	alphaTimer = [NSTimer scheduledTimerWithTimeInterval:0.07
												  target:self 
												selector:sel
												userInfo:nil
												 repeats:YES];
}	

- (void)viewAlphaZero {

	CGFloat alphaVal = ((UIView*)[self.subviews objectAtIndex:0]).alpha;
	if (alphaVal == 0) {
		if (alphaTimer) [alphaTimer invalidate];
		alphaTimer = nil;
		return;
	}
	
	for (UIView *view in self.subviews) {		
		view.alpha += -0.5;
	}
}


- (void)viewAlphaOne {
	
	CGFloat alphaVal = ((UIView*)[self.subviews objectAtIndex:0]).alpha;
	if (alphaVal == 1) {
		if (alphaTimer) [alphaTimer invalidate];
		alphaTimer = nil;
		return;
	}
	
	for (UIView *view in self.subviews) {		
		view.alpha += 0.5;
	}
}


- (void)statusBarDisplay {
	CGFloat alphaVal = ((UIView*)[self.subviews objectAtIndex:0]).alpha;
	if (alphaVal == 0) {
        //NSLog(@"hidden");
		[UIApplication sharedApplication].statusBarHidden = YES;
	} else {
        //NSLog(@"show");
		[UIApplication sharedApplication].statusBarHidden = NO;
	}
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.markList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell"; 
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    //cell.backgroundColor = [UIColor whiteColor];
    
    cell.accessoryView = nil;
    cell.accessoryType = UITableViewCellAccessoryNone;	
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    cell.textLabel.textColor = [UIColor whiteColor];


    if ([[[self.markList objectAtIndex:indexPath.row] objectForKey:@"MarkYn"] isEqualToString:@"Y"]) {
        UIImageView *accImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_check.png"]];
        cell.accessoryView = accImageView; 
    } else {
        UIImageView *accNoneImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_none_check.png"]];
        cell.accessoryView = accNoneImageView; 
    }
    
    cell.textLabel.text = [[self.markList objectAtIndex:indexPath.row] objectForKey:@"MarkNm"];
    return cell;                   
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 40;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int value = [[[self.markList objectAtIndex:indexPath.row] objectForKey:@"MarkValue"] intValue];
    
    if (appDelegate.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
		self.sliderProcess.value = appDelegate.moviePlayer.currentPlaybackTime = value;
	}
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	UIImageView *accImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_check.png"]];
    cell.accessoryView = accImageView; 
    
    [self tableViewDataReplace:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableViewDataReplace:(NSUInteger)index {
	
	NSDictionary *dic = (NSDictionary *)[self.markList objectAtIndex:index];
	NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:
																	[dic objectForKey:@"MarkNo"],
																	[dic objectForKey:@"MarkValue"],
																	[dic objectForKey:@"PPTUrl"], 
																	[dic objectForKey:@"MarkNm"],
																	@"Y", 
																	[dic objectForKey:@"RealFrameNo"], nil] forKeys:[NSArray arrayWithObjects:@"MarkNo", @"MarkValue", @"PPTUrl", @"MarkNm", @"MarkYn", @"RealFrameNo",                                                                                                                     nil]];
    
	[self.markList replaceObjectAtIndex:index withObject:dictionary];
}



@end
