//
//  SSViewController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 7. 25..
//  Copyright 2011 hunet. All rights reserved.
//

#import "SSViewController.h"
#import "ModalAlert.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "PlayerViewController.h"
#import "SSViewController.h"
#import "HLeaderController.h"
#import	"Reachability.h"
#import "WebtoonViewController.h"
#import "ModalViewController.h"
#import "CJSONDeserializer.h"
#import "Util.h"
#import "MoviePlayerController.h"
#import "MoreViewController.h"
#import "DownloadViewController.h"
#import "DownloadIndexViewSangController.h"
#import "SangPlayerViewController.h"
#import "GraphicsUtile.h"

@implementation SSViewController
@synthesize url, home, back, forward;

#define IS_4_INCH CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(320, 568)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(568, 320))

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (IBAction) goHome {
	[self dismissViewControllerAnimated:YES completion:nil];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        self.title = NSLocalizedString(@"sangsang", @"sangsang");
        self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_home"];
        
    }
    return self;
}


- (void) webViewDidFinishLoad:(UIWebView *)webView {
    if ([viewWeb.subviews count] > 0) {
        UIView *scrollView = viewWeb.subviews[0];
        
        for (UIView *childView in scrollView.subviews) {
            if ([childView isKindOfClass:[UIWebDocumentView class]]) {
                UIWebDocumentView *documentView = (UIWebDocumentView *)childView;
                WebScriptObject *wso = documentView.webView.windowScriptObject;
                
                [wso setValue:[WebScriptBridge getWebScriptBridge] forKey:@"ScriptBridge"];
            }
        }
    }
	[indicator stopAnimating];
	
	if(webView.canGoBack == YES)
	{
		back.enabled = YES;
	}
	
	if(webView.canGoForward == YES)
	{
		forward.enabled = YES;
	}	
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
	[indicator startAnimating];
	back.enabled = NO;
	forward.enabled = NO;
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
	[indicator stopAnimating];	
	//[ModalAlert notify:@"네트워크 연결에 문제가 생겼습니다."];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSURL *webUrl = request.URL;
	NSString *urlString = webUrl.absoluteString;

    //NSLog(@"%@", urlString);

    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    //상상마루(기존플레이어)
    if ([urlString rangeOfString:@"sangsangplayer://"].location != NSNotFound) {

        if ([app moviePlayCheck]) {
            SangPlayerViewController *viewToPush = [[SangPlayerViewController alloc] initWithNibName:@"SangPlayerViewController" bundle:nil];
            
            if (viewToPush) {
                
                urlString = [urlString stringByReplacingOccurrencesOfString:@"sangsangplayer://" withString:@""];
                NSArray *pairs = [urlString componentsSeparatedByString:@"/"];
                viewToPush.cno = [pairs objectAtIndex:0];
                viewToPush.cseq = [pairs objectAtIndex:1];
                viewToPush.gid = [pairs objectAtIndex:2];
                
                /*
                 viewToPush.cno = @"2";
                 viewToPush.cseq = @"10466";
                 viewToPush.gid = @"Y00014867";
                 */
                [self presentViewController:viewToPush animated:YES completion:nil];
            }
        }
        return NO;
	}
    //drm
    if ([urlString.lowercaseString hasPrefix:@"sangsangdrmplayer://"] ||
        [urlString.lowercaseString hasPrefix:@"sangsangdownload://"] ||
        [urlString.lowercaseString hasPrefix:@"sangsangdownloadplay://"]) {
        
        NSArray *tmpArray = [urlString componentsSeparatedByString:@"://"];
        NSString *act = [tmpArray objectAtIndex:0];
        NSString *param = [tmpArray objectAtIndex:1];
        
        
        if ([act isEqualToString:@"sangsangdrmplayer"]) {
            if ([app moviePlayCheck]) {
                [[MLCenterForBusinessAppDelegate sharedAppDelegate] maskLoading];
            }
            else
                return NO;
        }
        else if ([act isEqualToString:@"sangsangdownload"]){
            if ([app isWiFi]) {
                [[MLCenterForBusinessAppDelegate sharedAppDelegate] drmloadingView:self];
            }
            else
                return NO;
        }
        else {
            [self sangsangDownloadPush:param];
            return NO;
        }
        
        [NSThread detachNewThreadSelector:@selector(sangDrm:) toTarget:self withObject:[NSArray arrayWithObjects:act, param, nil]];
        return NO;
    }

    //웹 로그아웃 제어
    if([urlString.lowercaseString hasPrefix:@"logout://"])
    {
        app.flagLogin = NO;
        [app loginViewControllerDisplay];
        return NO;
    }
    
	return YES;
}




// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        // Custom initialization.
    }
    return self;
}
*/

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isFirstLoaded = YES;
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    NSString *uid = [app.profileUser objectForKey:@"userid"];
        
    //국민연금 관리 공단 
    if ([uid.lowercaseString rangeOfString:@"nps-2"].location != NSNotFound
        || [uid.lowercaseString rangeOfString:@"nps-7"].location != NSNotFound
        || [uid.lowercaseString rangeOfString:@"nps-8"].location != NSNotFound
        || [uid.lowercaseString rangeOfString:@"nps-9"].location != NSNotFound) {
        home.enabled = NO;
    }
    
	[viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
	viewWeb.scalesPageToFit = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    //[super viewWillAppear:animated];
    if (!isFirstLoaded) {
        [viewWeb reload];
    } else {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            if(IS_4_INCH)
                viewWeb.frame = CGRectMake(0, 18, viewWeb.frame.size.width, 500);
            else
                viewWeb.frame = CGRectMake(0, 18, viewWeb.frame.size.width, 413);
        }
    }
    [super viewWillAppear:animated];
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
	return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    indicator.center = viewWeb.center;
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskAllButUpsideDown;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    
	viewWeb = nil;
	indicator = nil;
	self.url = nil;
    self.home = nil;
	self.back = nil;
	self.forward = nil;
    
    [super viewDidUnload];
}


- (void)dealloc {
	viewWeb.delegate = nil;
}

//상상마루
- (void)sangDrm:(NSArray*)array
{
    //NSLog(@"url : %@", [array objectAtIndex:1]);
    NSString *action = [array objectAtIndex:0];
    NSString *url2 = [array objectAtIndex:1];
    NSArray *pairs = [url2 componentsSeparatedByString:@"/"];
    NSString *cno = [pairs objectAtIndex:0];
    NSString *cseq = [pairs objectAtIndex:1];
    NSString *gid = [pairs objectAtIndex:2];
    NSString *seGoodsId;
    if ([pairs count]>3) {
        seGoodsId =[pairs objectAtIndex:3];
    }
    else{
        seGoodsId = gid;
    }
    
    //NSString *viewNo = [pairs objectAtIndex:3];
         
    NSError *errorReq = nil;
    NSString* jsonParam = nil;
    NSString* jsonUrl = [MLCenterForBusinessAppDelegate sharedAppDelegate].urlBase;
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    if ([action isEqualToString:@"sangsangdrmplayer"]) {
        jsonParam = [NSString stringWithFormat:@"type=43&company_seq=%@&contract_no=%@&uid=%@&contents_seq=%@&goods_id=%@", app.companySeq, cno, [app.profileUser objectForKey:@"userid"], cseq, gid];
    }
    else
        if ([action isEqualToString:@"sangsangdownload"]){
         jsonParam = [NSString stringWithFormat:@"type=43&company_seq=%@&contract_no=%@&uid=%@&contents_seq=%@&goods_id=%@&drm_yn=Y&se_goods_id=%@", app.companySeq, cno, [app.profileUser objectForKey:@"userid"], cseq, gid, seGoodsId];
    }
    
    
    NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:jsonUrl]
                                          andQuery:jsonParam
                                          andError:&errorReq];
    //NSLog(@"%@?takecourseSeq=%@&indexNo=%@", [[Global sharedSingleton] _downloadInfoUrl], [pairs objectAtIndex:0], [pairs objectAtIndex:1]);
    
 
	if (jsonData)
    {
        NSError *errorJSON = nil;
        NSMutableDictionary *result = [[[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON] mutableCopy];
        
        if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
            
            if ([action isEqualToString:@"sangsangdrmplayer"]) {
                [self performSelectorOnMainThread:@selector(sangDrmStreaming:) withObject:[NSArray arrayWithObjects:result,
                                                                                           cno,
                                                                                           cseq,
                                                                                           gid,
                                                                                           [result objectForKey:@"view_no"],
                                                                                           nil] waitUntilDone:YES];
            } else {
               [self performSelectorOnMainThread:@selector(sangsangDrmDownload:) withObject:result waitUntilDone:YES];
            }
		}
    }
}

- (void)sangsangDrmDownload:(NSDictionary*)result
{
    NSString *mp4Url = @"";
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    if (result != nil) {
        mp4Url = [NSString stringWithFormat:@"%@?%@", [result objectForKey:@"url"], [app.profileUser objectForKey:@"userid"]];
        NSUserDefaults *userdeDefaults = [NSUserDefaults standardUserDefaults];
        [userdeDefaults setObject:mp4Url forKey:FILENAME];
    }

    if (webUtil != nil) {
        webUtil = nil;
    }
    webUtil = [[Web alloc] init];
    [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.webUtil = webUtil;
    [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.webUtil.downloadInfo = result;
    [[MLCenterForBusinessAppDelegate sharedAppDelegate].loading.webUtil requestDownLoad:mp4Url];
}

//상상마루 다운로드
- (void)sangsangDownloadPush:(NSString*)param {

    NSArray *pairs = [param componentsSeparatedByString:@"/"];
    NSInteger contractNo = [[pairs objectAtIndex:0] intValue];
    NSInteger contentsSeq = [[pairs objectAtIndex:1] intValue];
    NSString *goodsId = [pairs objectAtIndex:2];
    NSString *seGoodsId = [pairs objectAtIndex:3];
    
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];

    
    NSArray *tmp = [Util getSangStudy:goodsId andContentsSeq:[pairs objectAtIndex:1] andUserId:[app.profileUser objectForKey:@"userid"]];
    if (tmp.count <= 0) {
        if ([app isWiFi]) {
            [[MLCenterForBusinessAppDelegate sharedAppDelegate] drmloadingView:self];
        }
        [NSThread detachNewThreadSelector:@selector(sangDrm:) toTarget:self withObject:[NSArray arrayWithObjects:@"sangsangdownload", param, nil]];
    }
    else
    {
        [self viewDidLoad];
        [self dismissViewControllerAnimated:YES completion:nil];
        //[self performSelector:@selector(dismissModalClose) withObject:nil afterDelay:0.5];
        
        NSManagedObject *tempObject = [tmp objectAtIndex:0];
        NSInteger expirationDay = [Util expirationDay:[tempObject valueForKey:@"study_end_date"]];

        [app setRootViewSangDownloadCenter:goodsId andContractNo:contractNo andContentsSeq:contentsSeq andSeGoodsId:seGoodsId andExpirationDay:expirationDay];
    }
    
}

//상상마루
- (void)sangDrmStreaming:(NSArray*)array
{
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] removemaskLoading];
    NSDictionary *result = [array objectAtIndex:0];
    NSString *cno = [array objectAtIndex:1];
    NSString *cseq = [array objectAtIndex:2];
    //NSString *gid = [array objectAtIndex:3];
    //NSString *viewNo = [array objectAtIndex:4];
   
    NSUserDefaults *userdeDefaults = [NSUserDefaults standardUserDefaults];
    [userdeDefaults setObject:[result objectForKey:@"url"] forKey:FILENAME];
    NSLog(@"Streaming filename = %@", [userdeDefaults objectForKey:FILENAME]);
    
    MoviePlayerController * movieCon = [[MoviePlayerController alloc] initWithNibName:@"MoviePlayerController" bundle:nil];
    movieCon.isStreamingPlay = TRUE;
    movieCon.currentSecond = [[result objectForKey:@"last_view_sec"] intValue];
    
    NSString *pass_yn = [result objectForKey:@"pass_yn"];
    
    movieCon.scrollType = [pass_yn isEqualToString:@"Y"] ? 1 : 0;   // progress touch 여부 0 : 진도바 이동 불가, 1 : 진도바 이동 가능
    
    movieCon.cno = [cno intValue];
    movieCon.cseq = [cseq intValue];
    movieCon.gid = [result objectForKey:@"goods_id"];
    movieCon.viewSec = [[result objectForKey:@"view_sec"] intValue];
    movieCon.lastViewSec = [[result objectForKey:@"last_view_sec"] intValue];
    movieCon.viewSecMobile = [[result objectForKey:@"view_sec_mobile"] intValue];
    movieCon.viewSec = [[result objectForKey:@"view_sec"] intValue];
    movieCon.passYn = pass_yn;
    movieCon.quizYn = [result objectForKey:@"quiz_yn"];
    movieCon.url = [result objectForKey:@"url"];
    movieCon.pptImageUrl = [result objectForKey:@"ppt_image_url"];
    movieCon.viewNo = [[result objectForKey:@"view_no"] intValue];
    movieCon.userId = [result objectForKey:@"user_id"];

    
    [MLCenterForBusinessAppDelegate sharedAppDelegate].moviePlayerController = movieCon;
    [self presentViewController:movieCon animated:YES completion:nil];
}


@end
