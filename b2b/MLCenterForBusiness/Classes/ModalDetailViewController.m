//
//  ModalDetailViewController.m
//  MLCenterForBusiness
//
//  Created by Park Gi-Bok on 12. 4. 9..
//  Copyright (c) 2012 hunet. All rights reserved.
//

#import "ModalDetailViewController.h"

@implementation ModalDetailViewController
@synthesize url;

- (void) webViewDidFinishLoad:(UIWebView *)webView {
	[indicator stopAnimating];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
	[indicator startAnimating];
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
	[indicator stopAnimating];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    [self indicatorCenter];
	return YES;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"상 세";
    
    viewweb.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;    
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    
    [viewweb loadRequest:request];
}




- (void)viewDidUnload
{

    self.url = nil;
    viewweb = nil;
    indicator = nil;
    
    [super viewDidUnload];    
}

- (void)dealloc {

    viewweb.delegate = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    [self indicatorCenter];
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)indicatorCenter {
    indicator.center = viewweb.center;
}

@end
