//
//  VersionViewController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 7. 6..
//  Copyright 2011 hunet. All rights reserved.
//

#import "VersionViewController.h"
#import "ModalAlert.h"
#import "VersionChecher.h"

#define ENCODE(X) [(X) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]

@implementation VersionViewController
@synthesize versionInfo, currentVersion;

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (NSDictionary*) versionInfo {
	if (versionInfo == nil) {
        versionInfo = [VersionChecher requestCurrentVersionInfo];
		if (versionInfo) {		// success
			if ([[versionInfo objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {  // success... 
			} else {
				//[ModalAlert notify:@"리스트 받아오는데 실패했습니다."];
			}
		} else {
			[ModalAlert notify:@"网络不稳定！"];
		}		
	}
	
	return versionInfo;
}




#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];

	self.currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
}


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 2;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.accessoryView = nil;
	cell.accessoryType = UITableViewCellAccessoryNone;	
	cell.textLabel.font = [UIFont systemFontOfSize:16];	
    cell.backgroundColor = [UIColor whiteColor];
	
	
	if (indexPath.row == 0) {
		
		cell.selectionStyle = UITableViewCellSelectionStyleNone;

		UILabel *cellcurtitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 80, 30)];
		cellcurtitle.text = NSLocalizedString(@"nowVersion", @"当前版本");
		[cell.contentView addSubview:cellcurtitle];
		
		UILabel *cellcurversion = [[UILabel alloc] initWithFrame:CGRectMake(100, 5, 80, 30)];
		cellcurversion.text = self.currentVersion;
		cellcurversion.textColor = [UIColor grayColor];
		
		//cellcurversion.backgroundColor = [UIColor grayColor];
		[cell.contentView addSubview:cellcurversion];		
		
	} else {
		
        NSString *newVersion = [self.versionInfo objectForKey:@"Version"];

		if ([self.currentVersion isEqualToString:newVersion]) {
			cell.selectionStyle = UITableViewCellSelectionStyleNone;
			cell.userInteractionEnabled = NO;
		} else {
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		}

		UILabel *cellnewtitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 80, 30)];
		cellnewtitle.text = NSLocalizedString(@"newVersion", @"最新版本");
		[cell.contentView addSubview:cellnewtitle];
		
		UILabel *cellnewversion = [[UILabel alloc] initWithFrame:CGRectMake(100, 5, 80, 30)];
		cellnewversion.text = newVersion;
		cellnewversion.textColor = [UIColor grayColor];

		[cell.contentView addSubview:cellnewversion];
		
	}
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (indexPath.row == 1) {
        NSString *url = [self.versionInfo objectForKey:@"DownloadUrl"];
		if ([url length] > 0) {
			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
		}
	}
	
    [tableView deselectRowAtIndexPath:indexPath animated:NO]; 
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    self.versionInfo = nil;
	self.currentVersion = nil;
    [super viewDidUnload];
}




@end

