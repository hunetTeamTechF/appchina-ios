
#import "LoginViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "MoreViewController.h"
#import "DownloadViewController.h"
#import "ModalAlert.h"
#import "Header.h"

@interface LoginViewController ()
{
    CGPoint initLoginBoxPoint;
	CGPoint initTextIDPoint;
	CGPoint initTextPasswordPoint;
    CGPoint initButtonLoginPoint;
}
@end
@implementation LoginViewController
@synthesize viewImageCover;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.textID.text = [self.textID.text stringByReplacingOccurrencesOfString:@"kiki"
                                   withString:@""];
    
    self.viewImageBackground.image = [UIImage imageNamed:@"login_bg"];
    
    CGFloat y = 0;
    CGFloat x = 0;
    
    /* 컨트롤들 백그라운드 이미지 사이즈 재설정 */
    CGRect idRect = self.textID.frame;
    idRect.size.width = 172;
    idRect.size.height = 31;
    self.textID.frame = idRect;
    
    CGRect pwRect = self.textPassword.frame;
    pwRect.size.width = 172;
    pwRect.size.height = 31;
    self.textPassword.frame = pwRect;
    
    CGRect btnRect = self.buttonLogin.frame;
    btnRect.size.width = 80;
    btnRect.size.height = 72;
    self.buttonLogin.frame = btnRect;
    
    CGRect boxBgRect = self.viewImageLoginBox.frame;
    boxBgRect.size.width = 291;
    boxBgRect.size.height = 122;
    self.viewImageLoginBox.frame = boxBgRect;
    
    if (IS_4_INCH) {
        y = 20;
        x = 5;
        self.viewImageLoginBox.center = CGPointMake(self.viewImageLoginBox.center.x + x, self.viewImageLoginBox.center.y + y);
        self.textID.center = CGPointMake(self.textID.center.x, self.textID.center.y + y);
        self.textPassword.center = CGPointMake(self.textPassword.center.x, self.textPassword.center.y + y + 4);
        self.buttonLogin.center = CGPointMake(self.buttonLogin.center.x - x - 10, self.buttonLogin.center.y + y + 5);
    }
    else
    {
        y = 10;
        x = 5;
        self.viewImageLoginBox.center = CGPointMake(self.viewImageLoginBox.center.x + x, self.viewImageLoginBox.center.y + y);
        self.textID.center = CGPointMake(self.textID.center.x, self.textID.center.y + y + 4);
        self.textPassword.center = CGPointMake(self.textPassword.center.x, self.textPassword.center.y + y + 3);
        self.buttonLogin.center = CGPointMake(self.buttonLogin.center.x - x - 4, self.buttonLogin.center.y + y + 5);
    }
    
    initLoginBoxPoint = self.viewImageLoginBox.center;
	initTextIDPoint = self.textID.center;
	initTextPasswordPoint = self.textPassword.center;
    initButtonLoginPoint = self.buttonLogin.center;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [paths objectAtIndex:0];
    NSString *drmFolder = [NSString stringWithFormat:@"%@/%@", documentFolder, @"hunet_docroot"];
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSDirectoryEnumerator *fileEnumerator = [fileManager enumeratorAtPath:drmFolder];
    
    int filecount = 0;
    for (NSString *filename in fileEnumerator) {
        if ([filename isEqualToString:@".DS_Store"]) {
            continue;
        }
        filecount = 1;
        break;
    }
    [fileManager release];
    if (filecount > 0)
    {
        y += 110;
        if (IS_4_INCH) {
            y += 10;
        }
        
        self.offlineButton.hidden = NO;
        self.offlineButton.center = CGPointMake(self.offlineButton.center.x, self.offlineButton.center.y + y);
    }
}


- (IBAction) onLogin {
	
	self.buttonLogin.userInteractionEnabled = NO;
    
    self.textID.text = [self.textID.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.textPassword.text = [self.textPassword.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
	if ([self.textID isFirstResponder]) {
		[self.textID resignFirstResponder];
	}
	
	if ([self.textPassword isFirstResponder]) {
		[self.textPassword resignFirstResponder];
	}
	
	if (self.textID.text.length == 0 || self.textPassword.text.length == 0) {
		[ModalAlert notify:@"로그인에 실패했습니다.\n아이디와 패스워드를 다시 확인해 주세요"];
		self.buttonLogin.userInteractionEnabled = YES;
		return;
	}
	
	mask = [[UIView alloc] initWithFrame:[[self.view window] frame]];
	mask.userInteractionEnabled = NO;
	mask.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.50];
	[self.view addSubview:mask];
	[mask release];
	
	[indicator startAnimating];
	[NSThread detachNewThreadSelector:@selector(threadLogin:) toTarget:self withObject:nil];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
	if ([self.textID isFirstResponder]) {
		[self.textPassword becomeFirstResponder];
	}
	else {
		[self onLogin];
	}
	return YES;
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    if (IS_4_INCH) {
        return YES;
    }

    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.2f];
	//[UIView setAnimationDelegate:self];
    
    CGFloat y = 70;
    if (IS_4_INCH) {
        y = 100;
    }
	
    self.viewImageLoginBox.center = CGPointMake(initLoginBoxPoint.x, initLoginBoxPoint.y - y);
    self.textID.center = CGPointMake(initTextIDPoint.x, initTextIDPoint.y - y);
    self.textPassword.center = CGPointMake(initTextPasswordPoint.x, initTextPasswordPoint.y - y);
    self.buttonLogin.center = CGPointMake(initButtonLoginPoint.x, initButtonLoginPoint.y - y);
    [UIView commitAnimations];

	return YES;
}

- (void) threadLogin:(id)somedata {
	BOOL result = YES;
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];

    result = [app loginForID:[NSString stringWithFormat:@"kiki%@", self.textID.text] withPassword:self.textPassword.text];

	[self performSelectorOnMainThread:@selector(callbackAfterLogin:) withObject:[NSNumber numberWithBool:result] waitUntilDone:NO];
	[pool release];
}

// callback for the login result
- (void) callbackAfterLogin:(NSNumber*)resultLogin {
	
    
	BOOL result = [resultLogin boolValue];
	
	[mask removeFromSuperview];
	[indicator stopAnimating];
	
	self.buttonLogin.userInteractionEnabled = YES;
	
	if (result) {
		//[self.view removeFromSuperview];
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
		[app openTrainingInstitute];
	}
	else {
		[ModalAlert notify:@"로그인에 실패했습니다.\n아이디와 패스워드를 다시 확인해 주세요"];
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setOfflineButton:nil];
    [self setViewImageBackground:nil];
    [self setViewImageLoginBox:nil];
    [self setTextID:nil];
    [self setTextPassword:nil];
    [self setButtonLogin:nil];
    [super viewDidUnload];
	[indicator release];
	[viewImageCover release];
	indicator = nil;
	viewImageCover = nil;
}

- (void)dealloc {
    [_offlineButton release];
    [_viewImageBackground release];
    [_viewImageLoginBox release];
    [_textID release];
    [_textPassword release];
    [_buttonLogin release];
	[super dealloc];
}

- (BOOL) shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)offlineClick:(id)sender {
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] offlineStudy];
    
}
@end
