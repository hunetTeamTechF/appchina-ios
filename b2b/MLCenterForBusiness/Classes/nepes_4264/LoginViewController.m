
#import "LoginViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "MoreViewController.h"
#import "DownloadViewController.h"
#import "ModalAlert.h"
#import "Header.h"

@interface LoginViewController ()
{
    CGPoint initLoginBoxPoint;
    CGPoint initTextIDPoint;
    CGPoint initTextPasswordPoint;
    CGPoint initButtonLoginPoint;
}
@end
@implementation LoginViewController
@synthesize viewImageCover;

- (void) setControlsPosition{
    CGFloat y = 0;
    if (IS_4_INCH) {
        y = 50;
    }
    //self.viewImageBackground.image = [UIImage imageNamed:@"login_bg_5198"];
    //self.viewImageLoginBox.image = [UIImage imageNamed:@"login_box_5198"];
    //self.offlineButton.imageView.image = [UIImage imageNamed:@"btn_download_offline"];
    
    self.viewImageLoginBox.center = CGPointMake(self.viewImageLoginBox.center.x, self.viewImageLoginBox.center.y + y);
    self.textID.center = CGPointMake(self.textID.center.x, self.textID.center.y + y);
    self.textPassword.center = CGPointMake(self.textPassword.center.x, self.textPassword.center.y + y);
    self.buttonLogin.center = CGPointMake(self.buttonLogin.center.x, self.buttonLogin.center.y + y);
    self.textID.text = @"nepes-";
    initLoginBoxPoint = self.viewImageLoginBox.center;
    initTextIDPoint = self.textID.center;
    initTextPasswordPoint = self.textPassword.center;
    initButtonLoginPoint = self.buttonLogin.center;
    
    self.offlineButton.center = CGPointMake(self.offlineButton.center.x, self.offlineButton.center.y + y);

}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setControlsPosition];
    
    self.textID.attributedPlaceholder =  [[NSAttributedString alloc] initWithString:@"아이디" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.textPassword.attributedPlaceholder =  [[NSAttributedString alloc] initWithString:@"비밀번호" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [paths objectAtIndex:0];
    NSString *drmFolder = [NSString stringWithFormat:@"%@/%@", documentFolder, @"hunet_docroot"];
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSDirectoryEnumerator *fileEnumerator = [fileManager enumeratorAtPath:drmFolder];
    
    int filecount = 0;
    for (NSString *filename in fileEnumerator) {
        if ([filename isEqualToString:@".DS_Store"]) {
            continue;
        }
        filecount = 1;
        break;
    }

    self.offlineButton.hidden = filecount == 0;
        
}


- (IBAction) onLogin {
    self.buttonLogin.userInteractionEnabled = NO;
    
    self.textID.text = [self.textID.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.textPassword.text = [self.textPassword.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    if ([self.textID isFirstResponder]) {
        [self.textID resignFirstResponder];
    }
    
    if ([self.textPassword isFirstResponder]) {
        [self.textPassword resignFirstResponder];
    }
    
    if (self.textID.text.length == 0 || self.textPassword.text.length == 0) {
        [ModalAlert notify:@"로그인에 실패했습니다.\n아이디와 패스워드를 다시 확인해 주세요"];
        self.buttonLogin.userInteractionEnabled = YES;
        [self toggleLoginBoxPosition:NO];
        return;
    }
    [self toggleLoginBoxPosition:NO];
    mask = [[UIView alloc] initWithFrame:[[self.view window] frame]];
    mask.userInteractionEnabled = NO;
    mask.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.50];
    [self.view addSubview:mask];
    
    [indicator startAnimating];
    [NSThread detachNewThreadSelector:@selector(threadLogin:) toTarget:self withObject:nil];
}

- (void)onWebLogin:(NSString *)userId :(NSString *)userPwd{
    BOOL result = YES;
    @autoreleasepool {
        
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        result = [app loginForID:userId withPassword:userPwd];
        [self performSelectorOnMainThread:@selector(callbackAfterLogin:) withObject:[NSNumber numberWithBool:result] waitUntilDone:NO];
    }
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    if ([self.textID isFirstResponder]) {
        [self.textPassword becomeFirstResponder];
    }
    else {
        [self toggleLoginBoxPosition:NO];
        [self onLogin];
    }
    
    return YES;
}

- (void) toggleLoginBoxPosition:(BOOL)textInput {

    [UIView beginAnimations:nil context:NULL];
    //[UIView setAnimationDuration:0.2f];
    [UIView setAnimationDelegate:self];
    
    //로그인 박스 위치이동
    CGFloat y = IS_4_INCH ? 0 : 0;
    
    self.viewImageLoginBox.center = CGPointMake(initLoginBoxPoint.x, textInput ? initLoginBoxPoint.y - y : initLoginBoxPoint.y);
    self.textID.center = CGPointMake(initTextIDPoint.x, textInput ? initTextIDPoint.y - y : initTextIDPoint.y);
    self.textPassword.center = CGPointMake(initTextPasswordPoint.x, textInput ? initTextPasswordPoint.y - y : initTextPasswordPoint.y);
    self.buttonLogin.center = CGPointMake(initButtonLoginPoint.x, textInput ? initButtonLoginPoint.y - y: initButtonLoginPoint.y);
    [UIView commitAnimations];
    
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    
    [self toggleLoginBoxPosition:YES];
    
    return YES;
}

- (void) threadLogin:(id)somedata {
    BOOL result = YES;
    @autoreleasepool {
        
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        result = [app loginForID:[NSString stringWithFormat:@"%@", self.textID.text] withPassword:self.textPassword.text];
        [self performSelectorOnMainThread:@selector(callbackAfterLogin:) withObject:[NSNumber numberWithBool:result] waitUntilDone:NO];
    }
}

// callback for the login result
- (void) callbackAfterLogin:(NSNumber*)resultLogin {
    
    
    BOOL result = [resultLogin boolValue];
    
    [mask removeFromSuperview];
    [indicator stopAnimating];
    
    self.buttonLogin.userInteractionEnabled = YES;
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    if (result) {
        //[self.view removeFromSuperview];
        [app openTrainingInstitute];
    }
    else {
        if ([app isNotInternet]) {
            [ModalAlert notify:@"인터넷에 연결되어 있지 않습니다.\n인터넷 연결을 확인해 주세요."];
        }
        else {
            [ModalAlert notify:@"로그인에 실패했습니다.\n아이디와 패스워드를 다시 확인해 주세요."];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backgroundTab:(id)sender{
    [_textID resignFirstResponder];
    [_textPassword resignFirstResponder];
    [self toggleLoginBoxPosition:NO];
}

- (void)viewDidUnload {
    [self setOfflineButton:nil];
    [self setViewImageBackground:nil];
    [self setViewImageLoginBox:nil];
    [self setTextID:nil];
    [self setTextPassword:nil];
    [self setButtonLogin:nil];
    
    indicator = nil;
    viewImageCover = nil;
    
    [super viewDidUnload];
}


- (BOOL) shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)offlineClick:(id)sender {
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] offlineStudy];
}

@end
