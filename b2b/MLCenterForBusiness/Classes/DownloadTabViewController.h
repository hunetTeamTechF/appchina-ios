//
//  DownloadTabViewController.h
//  MLCenterForBusiness
//
//  Created by Joey on 13. 10. 15..
//
//

#import <UIKit/UIKit.h>

@interface DownloadTabViewController : UIViewController
@property (retain, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *courseArray;
@property (strong, nonatomic) NSArray *sangsangArray;

@end
