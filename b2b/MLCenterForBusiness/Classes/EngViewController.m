//
//  EngViewController.m
//  Learning
//
//  Created by Joey on 2013. 12. 18..
//
//

#import "EngViewController.h"
#import "MyViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "ModalAlert.h"
#import "Util.h"
#import "Header.h"
#import "MP3PlayerController.h"
#import "MoviePlayerController.h"
#import "GraphicsUtile.h"

@interface UITabBarController (Extras)
- (void)showTabBar:(BOOL)show;
@end

@implementation EngViewController
@synthesize engUrl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self viewLoad];
    
    NSArray *vComp = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[vComp objectAtIndex:0] intValue] >= 7) {
        [self setNeedsStatusBarAppearanceUpdate];
        self.edgesForExtendedLayout = UIRectEdgeNone;
        // iOS 7 or above
        CGRect oldBounds = [self.view bounds];
        CGRect newViewBounds = CGRectMake(0, -10, oldBounds.size.width, oldBounds.size.height - 20);
        CGRect newWebViewBounds = CGRectMake(0, -20, oldBounds.size.width, oldBounds.size.height - 40);
        if (self.view.bounds.origin.y != newViewBounds.origin.y) {
            [self.view setBounds:newViewBounds];
            [viewWeb setBounds:newWebViewBounds];
        }
    }
}

-(void) viewLoad
{
    self.contentType = [Util valueForKey:@"contentsType" fromQuery:[NSURL URLWithString:engUrl].query].uppercaseString;
    
    self.navigationController.navigationBarHidden = YES;
	[self.tabBarController showTabBar:YES];

    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.engUrl] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0];
    [viewWeb loadRequest:request];
    viewWeb.scalesPageToFit = [self.contentType isEqualToString:@"L"];
    

}


- (void) webViewDidFinishLoad:(UIWebView *)webView {
	[indicator stopAnimating];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
	[indicator startAnimating];
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
	[indicator stopAnimating];
}



-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *requestStr = [[request URL] absoluteString];
    
    if([requestStr rangeOfString:@"close://"].location != NSNotFound){
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
        
        return YES;
    }
    
    if ([requestStr rangeOfString:@"htmlplayer://"].location != NSNotFound) {
        //NSLog(@"player");
        
		NSString *currentURL = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
        self.engUrl = currentURL;
       
        NSString *mp4Url = [requestStr stringByReplacingOccurrencesOfString:@"htmlplayer://" withString:@"http://"];
        
        NSUserDefaults *userdeDefaults = [NSUserDefaults standardUserDefaults];
        [userdeDefaults setObject:mp4Url forKey:FILENAME];
        NSLog(@"Streaming filename = %@", [userdeDefaults objectForKey:FILENAME]);
        
        
        MoviePlayerController * movieCon = [[MoviePlayerController alloc] initWithNibName:@"MoviePlayerController" bundle:nil];
        movieCon.isStreamingPlay = TRUE;
        movieCon.currentSecond = 0;
        movieCon.scrollType =  1;   // progress touch 여부 0 : 진도바 이동 불가, 1 : 진도바 이동 가능
        [MLCenterForBusinessAppDelegate sharedAppDelegate].moviePlayerController = movieCon;
        [self presentViewController:movieCon animated:YES completion:nil];
        return NO;
	}
    
    if([requestStr rangeOfString:@"mp3player://"].location != NSNotFound){

        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        if (![app moviePlayCheck]) {
			return NO;
		}
        
       		NSString *currentURL = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
        self.engUrl = currentURL;

        
        NSString *mp3Url = [requestStr stringByReplacingOccurrencesOfString:@"mp3player://" withString:@"http://"];
        
        MP3PlayerController * mp3Player = [[MP3PlayerController alloc] initWithNibName:@"MP3PlayerController" bundle:nil];
        
        mp3Player.returnUrl = currentURL;
        mp3Player.mp3Url = mp3Url;
        
        [self presentViewController:mp3Player animated:YES completion:nil];
        
        return NO;
        
    }
    
    return YES;
}

- (id)init {
	if (self = [super init]) {
		self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"menu_lectureroom.png"];
		self.tabBarItem.title = @"나의 강의실";
	}
	return self;
}

-(BOOL)shouldAutorotate {
    return YES;
}


- (NSUInteger)supportedInterfaceOrientations
{
    indicator.center = viewWeb.center;
    return UIInterfaceOrientationMaskPortrait;
//    if ([self.contentType isEqualToString:@"L"]) {
//        return UIInterfaceOrientationMaskLandscape;
//    } else {
//        return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskAllButUpsideDown;
//    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    //[[NSURLCache sharedURLCache] removeAllCachedResponses];
    // Dispose of any resources that can be recreated.
}


//for status bar hidden - iOS7
//-(BOOL)prefersStatusBarHidden{
//    return YES;
//}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

- (void)viewWillAppear:(BOOL)animated {
	if (animated) {
		//NSLog(@"viewWillAppear : %@", self.urlMark);
		//[self viewLoad];
        [[UIApplication sharedApplication] setStatusBarHidden:[self.contentType isEqualToString:@"L"]];
	}
    self.navigationController.navigationBarHidden = YES;
	[self.tabBarController showTabBar:YES];

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        if(IS_4_INCH)
        {
            viewWeb.frame = CGRectMake(0, 18, viewWeb.frame.size.width, 560);
        }
        else
        {
            viewWeb.frame = CGRectMake(0, 18, viewWeb.frame.size.width, 473);
        }
    }
    
	[super viewWillAppear:animated];
}


- (void)dealloc {
    
    viewWeb = nil;
    indicator = nil;
    
}

- (void)viewDidUnload {
    [super viewDidUnload];
}
@end
