//
//  WebRequestUtil.m
//  MLCenterForBusiness
//
//  Created by hunet-mac-npc92 on 2/7/15.
//
//

#import "UIViewController+RequestProtocol.h"

@implementation UIViewController (RequestProtocol)

- (BOOL)webView:(UIWebView *)webView customShouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    /*
    NSURL *url = request.URL;
    NSString *urlString = url.absoluteString;
    
    NSLog(@"base : %@", urlString);
    
    if ([urlString.lowercaseString hasPrefix:@"back://"]) {
        [self.webView goBack];
    }
    
    if ([urlString.lowercaseString hasPrefix:@"sangsangdrmplayer://"] ||
        [urlString.lowercaseString hasPrefix:@"sangsangdownload://"] ||
        [urlString.lowercaseString hasPrefix:@"sangsangdownloadplay://"]) {
        
        NSArray *tmpArray = [urlString componentsSeparatedByString:@"://"];
        NSString *act = [tmpArray objectAtIndex:0];
        NSString *param = [tmpArray objectAtIndex:1];
        
        BOOL checkConfigSettings = false;
        //상상마루 바로학습
        if ([urlString.lowercaseString hasPrefix:@"sangsangdrmplayer://"]) {
            checkConfigSettings = [[self appDelegate] moviePlayCheck];
        } //상상마루 다운로드 플레이
        else {
            checkConfigSettings = [[self appDelegate] isWiFi];
        }
        if (checkConfigSettings) {
            if (_playerCallFun == nil) {
                _playerCallFun = [[PlayerCallFun alloc] init];
                _playerCallFun.delegateViewController = self;
            }
            self.currentUrl = [webView stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
            [_playerCallFun sangsangmaruInit:[NSArray arrayWithObjects:act, param, nil]];
        }
        
        return NO;
    }*/
    return YES;
}
    
@end
