//
//  HTMLPlayerController.h
//  MLCenterForBusiness
//
//  Created by Seungkil Ryu on 2013. 12. 4..
//
//

#import <UIKit/UIKit.h>
#import "DownloadingView.h"
#import "PlayerCallFun.h"
#import "MoviePlayerController.h"
#import "web.h"

@interface HTMLPlayerController : UIViewController <UIWebViewDelegate, DownloadingViewDelegate, MoviePlayerControllerDelegate>
{
    NSString *url;
    IBOutlet UIWebView *viewweb;
    PlayerCallFun *playerCallFun;


}
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) PlayerCallFun *playerCallFun;

@end
