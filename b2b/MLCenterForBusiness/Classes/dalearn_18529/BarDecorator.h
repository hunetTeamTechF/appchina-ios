//
//  BarDecorator.h
//  MLCenterForBusiness
//
//  Created by Park SeungKyun on 2015. 7. 20..
//
//

#import <Foundation/Foundation.h>
#import "MLCenterForBusinessAppDelegate.h"

@interface BarDecorator : NSObject

+ (void)setting:(MLCenterForBusinessAppDelegate *) app;

@end
