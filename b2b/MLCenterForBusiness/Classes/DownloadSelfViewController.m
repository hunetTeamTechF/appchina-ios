
#import "DownloadSelfViewController.h"
#import "DownloadCourseCell.h"
#import "DownloadIndexSelfViewController.h"
#import "DownloadIndexSelfViewSangController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "Util.h"
#import "Header.h"
#import "HLeaderController.h"

@interface DownloadSelfViewController ()

@end

@implementation DownloadSelfViewController
@synthesize hleaderBackUrl;

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self != nil) {
//        self.title = NSLocalizedString(@"다운로드", @"다운로드");
//        self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_download"];
    }
    return self;
}


- (IBAction)goBack:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialSegmentedControl];
}

-(void)initialSegmentedControl{
    
    NSArray *itemArray;
    
    self.downloadInfoArray = [Util getDownloadInfo];
    
    NSManagedObject *obj = [self.downloadInfoArray objectAtIndex:0];
    NSString *processMenuAlias = [obj valueForKey:@"process_menu_alias"];
    NSString *imagineMenuAlias = [obj valueForKey:@"imagine_menu_alias"];
    NSString *onlyImagineYn = [obj valueForKey:@"only_imagine_yn"];

    if (_downloadInfoArray.count > 0) {
        if ([onlyImagineYn isEqualToString:@"Y"]) {
            itemArray = [NSArray arrayWithObjects:imagineMenuAlias, nil];
        }
        else if ([onlyImagineYn isEqualToString:@"N"]) {
            itemArray = [NSArray arrayWithObjects:processMenuAlias, nil];
        }
        else{
            itemArray = [NSArray arrayWithObjects: processMenuAlias, imagineMenuAlias, nil];
        }
    }
    else{
        itemArray = [NSArray arrayWithObjects: [MLCenterForBusinessAppDelegate sharedAppDelegate].courseTab, [MLCenterForBusinessAppDelegate sharedAppDelegate].imagineTab, nil];
    }
    _segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
    _segmentedControl.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40);
    _segmentedControl.segmentedControlStyle = UISegmentedControlStylePlain;
    
#ifdef emart_13037
    [_segmentedControl setTintColor:[UIColor colorWithRed:102.0/255.0 green:110.0/255.0 blue:126.0/255.0 alpha:1.0]];
#else
    //[_segmentedControl setTintColor:self.navigationController.navigationBar.barTintColor];
    
#endif
    
    //_segmentedControl.momentary = NO;
    _segmentedControl.selectedSegmentIndex = 0;
    
    
    self.segmentedControlPortraitOrientationFrame = _segmentedControl.frame;
    [_segmentedControl addTarget:self action:@selector(valueChanged:) forControlEvents: UIControlEventValueChanged];
    
    
//    if (_downloadInfoArray.count > 0) {
//        if ([onlyImagineYn isEqualToString:@"Y"]) {
//            itemArray = [NSArray arrayWithObjects:imagineMenuAlias, nil];
//        }
//        else if ([onlyImagineYn isEqualToString:@"N"]) {
//            itemArray = [NSArray arrayWithObjects:processMenuAlias, nil];
//        }
//        else{
//            itemArray = [NSArray arrayWithObjects: processMenuAlias, imagineMenuAlias, nil];
//        }
//    }
//    else{
//        itemArray = [NSArray arrayWithObjects: [MLCenterForBusinessAppDelegate sharedAppDelegate].courseTab, [MLCenterForBusinessAppDelegate sharedAppDelegate].imagineTab, nil];
//    }
    //상상마루 학습창의 다운로드되어 있다면 바로 연결
    if (self.goodsId!=Nil) {
        _segmentedControl.selectedSegmentIndex = 1;
        
        DownloadIndexSelfViewSangController *view = [[DownloadIndexSelfViewSangController alloc] initWithStyle:UITableViewStylePlain];
        
        view.goodsId = self.goodsId;
        view.userId = self.userId;
        view.expirationDay = self.expirationDay;
        view.seGoodsId = self.seGoodsId;
        
        [self.navigationController pushViewController:view animated:YES];
        return;
    }
    
    //상상마루 학습창의 다운로드되어 있다면 바로 연결
    if (self.courseCd!=Nil) {
        _segmentedControl.selectedSegmentIndex = 0;
        
        DownloadIndexSelfViewController *view = [[DownloadIndexSelfViewController alloc] initWithStyle:UITableViewStylePlain];
        
        view.coursecd = self.courseCd;
        view.takecourseseq = self.takeCourseSeq;
        //view.expirationDay = self.expirationDay;
        
        [self.navigationController pushViewController:view animated:YES];
        return;
    }
    
    [self tableData];
    if ([self.courseArray count]<1 && [self.sangsangArray count]>0) {
        _segmentedControl.selectedSegmentIndex = 1;
        
        self.segmentedControlPortraitOrientationFrame = _segmentedControl.frame;
        [_segmentedControl addTarget:self action:@selector(valueChanged:) forControlEvents: UIControlEventValueChanged];
        
    }
    
    //tab메뉴 인덱스 재설정
    if (_downloadInfoArray.count > 0) {
        if ([onlyImagineYn isEqualToString:@"Y"]) {
            _segmentedControl.selectedSegmentIndex = 1;
        }
        else if ([onlyImagineYn isEqualToString:@"N"]) {
            _segmentedControl.selectedSegmentIndex = 0;
        }
        else{
           // _segmentedControl.selectedSegmentIndex = 0;
        }
    }


}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    [super didRotateFromInterfaceOrientation:interfaceOrientation];
   // if(UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
        //_segmentedControl.frame = self.segmentedControlPortraitOrientationFrame;
        _segmentedControl.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40);

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    [self tableData];
    [self.tableView reloadData];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void)tableData{
    self.courseArray = [Util getCourse];
    self.sangsangArray = [Util getSangsang];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSManagedObject *obj = [self.downloadInfoArray objectAtIndex:0];
    NSString *onlyImagineYn = [obj valueForKey:@"only_imagine_yn"];

    //tab메뉴 인덱스 재설정
    if (_downloadInfoArray.count > 0 && [onlyImagineYn isEqualToString:@"Y"]) {
        return [self.sangsangArray count] + 3;
    }
    else if (_downloadInfoArray.count > 0 && [onlyImagineYn isEqualToString:@"N"]) {
        return [self.courseArray count] + 3;
    }
    else{
        if ([_segmentedControl selectedSegmentIndex] == 0) {
            return [self.courseArray count] + 3;
        }
        return [self.sangsangArray count] + 3;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 60;
    }
    else if (indexPath.row < 3) {
        return 40;
    }
    return 84;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self tableData];
    //[self.tableView reloadData];
    
    if (indexPath.row == 0) {
        static NSString *CellIdentifier = @"Cell";
        
        UIColor *bgColor = [UIColor colorWithRed:48.0/255.0 green:70.0/255.0 blue:143.0/255.0 alpha:1.0];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell ==nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        
        UIFont *myFont = [ UIFont fontWithName: @"Arial" size: 20.0 ];
        cell.textLabel.font  = myFont;
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.backgroundColor = bgColor;
        cell.textLabel.text = [NSString stringWithFormat:@"下载"];
        //cell.userInteractionEnabled = NO;
        
        UIButton *btnClose = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btnClose setFrame:CGRectMake(0.0f, 17.0f, 70.0f, 30.0f)];
        [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnClose setTitle:@"关闭" forState:UIControlStateNormal];
        [btnClose.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
        [btnClose.titleLabel setBackgroundColor:bgColor];
        [btnClose.titleLabel setTextAlignment:NSTextAlignmentLeft];
     
        [cell addSubview:btnClose];
        [btnClose addTarget:self
                            action:@selector(goBack:)
                  forControlEvents:UIControlEventTouchUpInside];

        
        
        return cell;
    }
    else if (indexPath.row == 1) {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell ==nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        uint64_t totalSize = strtoull([[[Util freeDiskspace] objectAtIndex:0] UTF8String], NULL, 0);
        uint64_t freeSize = strtoull([[[Util freeDiskspace] objectAtIndex:1] UTF8String], NULL, 0);
        
        UIFont *myFont = [ UIFont fontWithName: @"Arial" size: 13.0 ];
        cell.textLabel.font  = myFont;
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor redColor];
        cell.textLabel.text = [NSString stringWithFormat:@"总空间 : %@, 剩余空间 : %@", [Util prettyBytes:totalSize], [Util prettyBytes:freeSize]];
        cell.userInteractionEnabled = NO;
        
        return cell;
    }
    else if (indexPath.row == 2) {
        static NSString *CellIdentifier = @"TabCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell ==nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
             [cell addSubview:_segmentedControl];
        }
        
        return cell;
    }
    else {
        static NSString *CellIdentifier = @"DownloadCourseCell";
        DownloadCourseCell *cell = (DownloadCourseCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            NSArray *arr = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil];
            cell = [arr objectAtIndex:0];
        }
        
        NSManagedObject *obj = [self.downloadInfoArray objectAtIndex:0];
        NSString *onlyImagineYn = [obj valueForKey:@"only_imagine_yn"];
        
        //tab메뉴 인덱스 재설정
        if (_downloadInfoArray.count > 0 && [onlyImagineYn isEqualToString:@"Y"]) {
            NSManagedObject *obj = [self.sangsangArray objectAtIndex:indexPath.row-3];
            cell.courseNm.text = [obj valueForKey:@"goods_nm"];
            cell.studyEndDate.text = [NSString stringWithFormat:@"%@", [obj valueForKey:@"study_end_date"]];
            if ([Util expirationDay:[obj valueForKey:@"study_end_date"]] >= 0) {
                //cell.expirationDay.text = [NSString stringWithFormat:@"(%d일 남음)", [Util expirationDay:[obj valueForKey:@"study_end_date"]]];
                cell.expirationDay.text = [NSString stringWithFormat:@"(剩余%li天)", [Util calculatePeroidOfDay:nil to:[obj valueForKey:@"study_end_date"]]];
            }
        }
        else if (_downloadInfoArray.count > 0 && [onlyImagineYn isEqualToString:@"N"]) {
            NSManagedObject *obj = [self.courseArray objectAtIndex:indexPath.row-3];
            cell.courseNm.text = [obj valueForKey:@"course_nm"];
            cell.studyEndDate.text = [NSString stringWithFormat:@"%@", [obj valueForKey:@"study_end_date"]];
            if ([Util expirationDay:[obj valueForKey:@"study_end_date"]] >= 0) {
                //cell.expirationDay.text = [NSString stringWithFormat:@"(%d일 남음)", [Util expirationDay:[obj valueForKey:@"study_end_date"]]];
                cell.expirationDay.text = [NSString stringWithFormat:@"(剩余%li天)", [Util calculatePeroidOfDay:nil to:[obj valueForKey:@"study_end_date"]]];
            }
        }
        else{
            if ([_segmentedControl selectedSegmentIndex]==0) {
                
                NSManagedObject *obj = [self.courseArray objectAtIndex:indexPath.row-3];
                cell.courseNm.text = [obj valueForKey:@"course_nm"];
                cell.studyEndDate.text = [NSString stringWithFormat:@"%@", [obj valueForKey:@"study_end_date"]];
                if ([Util expirationDay:[obj valueForKey:@"study_end_date"]] >= 0) {
                    //cell.expirationDay.text = [NSString stringWithFormat:@"(%d일 남음)", [Util expirationDay:[obj valueForKey:@"study_end_date"]]];
                    cell.expirationDay.text = [NSString stringWithFormat:@"(剩余%li天)", [Util calculatePeroidOfDay:nil to:[obj valueForKey:@"study_end_date"]]];
                }
            }
            else{
                NSManagedObject *obj = [self.sangsangArray objectAtIndex:indexPath.row-3];
                cell.courseNm.text = [obj valueForKey:@"goods_nm"];
                cell.studyEndDate.text = [NSString stringWithFormat:@"%@", [obj valueForKey:@"study_end_date"]];
                if ([Util expirationDay:[obj valueForKey:@"study_end_date"]] >= 0) {
                    cell.expirationDay.text = [NSString stringWithFormat:@"(剩余%d天)", [Util expirationDay:[obj valueForKey:@"study_end_date"]]];
                }
                NSLog(@"%@", [obj valueForKey:@"goods_nm"]);
            }
        }
        return cell;
    }
    return nil;
}


-(IBAction)valueChanged:(id)sender {
    _segmentedControl = (UISegmentedControl *)sender;
    [self.tableView reloadData];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *obj = [self.downloadInfoArray objectAtIndex:0];
    NSString *onlyImagineYn = [obj valueForKey:@"only_imagine_yn"];
    
    //tab메뉴 인덱스 재설정
    if (_downloadInfoArray.count > 0 && [onlyImagineYn isEqualToString:@"Y"]) {
        DownloadIndexSelfViewSangController *view = [[DownloadIndexSelfViewSangController alloc] initWithStyle:UITableViewStylePlain];
        
        NSManagedObject *obj = [self.sangsangArray objectAtIndex:indexPath.row - 3];
        NSString *goodsId = [obj valueForKey:@"goods_id"];
        NSString *userId = [obj valueForKey:@"user_id"];
        NSString *date = [obj valueForKey:@"study_end_date"];
        NSInteger expirationDay = [Util expirationDay:date];
        
        view.goodsId = goodsId;
        view.userId = userId;
        view.expirationDay = expirationDay;
        view.seGoodsId = [obj valueForKey:@"se_goods_id"];
        
        //[self.navigationController pushViewController:view animated:YES];
        [self presentViewController:view animated:YES completion:nil];
    }
    else if (_downloadInfoArray.count > 0 && [onlyImagineYn isEqualToString:@"N"]) {
        
        DownloadIndexSelfViewController *view = [[DownloadIndexSelfViewController alloc] initWithStyle:UITableViewStylePlain];
        
        NSManagedObject *obj = [self.courseArray objectAtIndex:indexPath.row - 3];
        NSString *cd = [obj valueForKey:@"course_cd"];
        NSString *seq = [obj valueForKey:@"take_course_seq"];
        NSString *date = [obj valueForKey:@"study_end_date"];
        NSInteger expirationDay = [Util expirationDay:date];
        
        view.coursecd = cd;
        view.takecourseseq = seq;
        view.expirationDay = expirationDay;
        //[self.navigationController pushViewController:view animated:YES];
        [self presentViewController:view animated:YES completion:nil];

    }
    else{
        if ([_segmentedControl selectedSegmentIndex]==0) {
            DownloadIndexSelfViewController *view = [[DownloadIndexSelfViewController alloc] initWithStyle:UITableViewStylePlain];
        
            NSManagedObject *obj = [self.courseArray objectAtIndex:indexPath.row - 3];
            NSString *cd = [obj valueForKey:@"course_cd"];
            NSString *seq = [obj valueForKey:@"take_course_seq"];
            NSString *date = [obj valueForKey:@"study_end_date"];
            NSInteger expirationDay = [Util expirationDay:date];
            
            view.coursecd = cd;
            view.takecourseseq = seq;
            view.expirationDay = expirationDay;
            //[self.navigationController pushViewController:view animated:YES];
            [self presentViewController:view animated:YES completion:nil];
        }
        else{
            DownloadIndexSelfViewSangController *view = [[DownloadIndexSelfViewSangController alloc] initWithStyle:UITableViewStylePlain];
            
            NSManagedObject *obj = [self.sangsangArray objectAtIndex:indexPath.row - 3];
            NSString *goodsId = [obj valueForKey:@"goods_id"];
            NSString *userId = [obj valueForKey:@"user_id"];
            NSString *date = [obj valueForKey:@"study_end_date"];
            NSInteger expirationDay = [Util expirationDay:date];
            
            view.goodsId = goodsId;
            view.userId = userId;
            view.expirationDay = expirationDay;
            view.seGoodsId = [obj valueForKey:@"se_goods_id"];

            //[self.navigationController pushViewController:view animated:YES];
            [self presentViewController:view animated:YES completion:nil];
        }
    }
}

- (void)showLogin {
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] loginViewControllerDisplay];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

@end
