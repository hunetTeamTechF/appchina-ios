

#import "HStoryDetailViewController.h"
#import "SocialViewController.h"

@interface HStoryDetailViewController ()

@end

@implementation HStoryDetailViewController

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (void)viewDidUnload {
    [self setWebviewUrl:nil];
    [super viewDidUnload];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    if (isFirstLoaded) {
        return;
    }
    
    [self settingStatusBar];
    isFirstLoaded = YES;
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:self.webviewUrl]
                                                cachePolicy: NSURLRequestReloadIgnoringCacheData
                                            timeoutInterval:60.0];
    [self.webView loadRequest:requestObj];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = YES;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSURL *url = request.URL;
	NSString *urlString = url.absoluteString.lowercaseString;
    
    //NSLog(@"%@", urlString);
    if ([urlString isEqualToString:@"back://"])
    {
        [self.navigationController popViewControllerAnimated:YES];
        return NO;
    }
    
    if ([urlString rangeOfString:@"m.facebook.com"].location != NSNotFound
        || [urlString rangeOfString:@"twitter.com"].location != NSNotFound) {
        
        SocialViewController *viewController = [[SocialViewController alloc] initWithNibName:@"BaseViewController" bundle:nil];
        
        viewController.socialUrl = url.absoluteString;
        viewController.title = @"공유";
        
        self.navigationController.navigationBar.hidden = NO;
        [self.navigationController pushViewController:viewController animated:YES];
        

		return NO;
	}
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
