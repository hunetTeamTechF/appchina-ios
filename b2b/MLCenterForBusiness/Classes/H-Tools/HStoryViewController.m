

#import "HStoryViewController.h"
#import "HStoryDetailViewController.h"
#import "Header.h"

@interface HStoryViewController ()

@end

@implementation HStoryViewController
@synthesize addStatusBar = _addStatusBar;

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (isFirstLoaded) {
        return;
    }
    
    [self settingStatusBar];
    isFirstLoaded = YES;
        
	NSString *_url = [NSString stringWithFormat:@"%@?type=37&uid=%@&pw=%@", [self appDelegate].urlBase, [[self appDelegate].profileUser objectForKey:@"userid"], [[self appDelegate].profileUser objectForKey:@"password"]];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:_url]
                                                cachePolicy: NSURLRequestReloadIgnoringCacheData
                                            timeoutInterval:60.0];
    [self.webView loadRequest:requestObj];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = YES;
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSURL *url = request.URL;
	NSString *urlString = url.absoluteString.lowercaseString;
    
    if ([urlString isEqualToString:@"back://"])
    {
        [self.navigationController popViewControllerAnimated:YES];
        return NO;
    }
    
    if ([urlString rangeOfString:@"/hstory/detail"].location != NSNotFound
        || [urlString rangeOfString:@"/hstory/reject"].location != NSNotFound) {
        
        HStoryDetailViewController *view = [[HStoryDetailViewController alloc]
                                      initWithNibName:@"BaseViewController" bundle:nil];
        view.webviewUrl = urlString;
        [self.navigationController pushViewController:view animated:YES];
		
		return NO;
	}
    
    
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
