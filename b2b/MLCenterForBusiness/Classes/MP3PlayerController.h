//
//  MP3PlayerController.h
//  Learning
//
//  Created by Joey on 2013. 12. 24..
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVAudioPlayer.h>
#import <AVFoundation/AVAudioSession.h>

@interface MP3PlayerController : UIViewController<AVAudioSessionDelegate, AVAudioPlayerDelegate>{
    
@private
	double realSpeed;
    bool isSpeedPushed;
    bool isVolumePushed;
    bool isPause;
}

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (strong, nonatomic) IBOutlet UIButton *btnClose;
@property (strong, nonatomic) IBOutlet UIView *progressBar;
@property (strong, nonatomic) IBOutlet UILabel *lblPast;
@property (strong, nonatomic) IBOutlet UILabel *lblLeft;
@property (strong, nonatomic) NSString *mp3Url;
@property (strong, nonatomic) NSString *returnUrl;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnPlayPause;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *next;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (strong, nonatomic) IBOutlet UISlider *volumeSlider;
@property (strong, nonatomic) IBOutlet UISlider *progressSlider;
@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) IBOutlet UILabel *lblVolume;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *bgView;
@property (strong, nonatomic) IBOutlet UIButton *btnSpeed;
@property (strong, nonatomic) IBOutlet UIButton *imgSpeedbg;
@property (strong, nonatomic) IBOutlet UIButton *btnSpeedDown;
@property (strong, nonatomic) IBOutlet UIButton *btnSpeedUp;
@property (strong, nonatomic) NSMutableData *songData;
- (IBAction)volumeSpeakerTouch:(id)sender;
- (IBAction)closePlayer:(id)sender;
- (IBAction)PlaynPause:(id)sender;
- (IBAction)next:(id)sender;
- (IBAction)rewind:(id)sender;
- (IBAction)adjustVolume:(id)sender;
- (IBAction)btnSpeed:(id)sender;
- (IBAction)adjustProgress:(id)sender;
- (IBAction)speedUp:(id)sender;
- (IBAction)speedDown:(id)sender;
@end
