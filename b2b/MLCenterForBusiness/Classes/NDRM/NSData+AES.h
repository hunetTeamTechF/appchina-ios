//
//  NSData+AES.h
//  Stway
//
//  Created by  on 12. 6. 30..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (AES)
- (NSData*)AES256EncryptWithKey:(NSString *)key;
- (NSData*)AES256DecryptWithKey:(NSString *)key;
@end
