//
//  PasswordViewController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 6. 14..
//  Copyright 2011 hunet. All rights reserved.
//

#import "PasswordViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "ModalAlert.h"

@implementation PasswordViewController

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    if ([viewWeb.subviews count] > 0) {
        UIView *scrollView = viewWeb.subviews[0];
        
        for (UIView *childView in scrollView.subviews) {
            if ([childView isKindOfClass:[UIWebDocumentView class]]) {
                UIWebDocumentView *documentView = (UIWebDocumentView *)childView;
                WebScriptObject *wso = documentView.webView.windowScriptObject;
                
                [wso setValue:[WebScriptBridge getWebScriptBridge] forKey:@"ScriptBridge"];
            }
        }
    }
    [indicator stopAnimating];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
	[indicator startAnimating];
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
	[indicator stopAnimating];	
	[ModalAlert notify:@"네트워크 연결에 문제가 생겼습니다."];	
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    NSURL *url = request.URL;
    NSString *urlString = url.absoluteString;
    NSLog(@"%@, %@", urlString, app.eduType);
    
    // 모달 창 종료
    if ([urlString hasPrefix:@"finish://"] || [urlString hasPrefix:@"close://"]) {
        [self.navigationController popViewControllerAnimated:YES];
        return NO;
    }
    
    // 페이지 뒤로가기 (뒤로갈 수 없으면 모달 창 종료)
    if ([urlString hasPrefix:@"back://"]) {
        if (webView.canGoBack) {
            [webView goBack];
        }
        else {
            [self.navigationController popViewControllerAnimated:YES];
        }
        return NO;
    }

	return YES;
}


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self != nil) {
//    }
//    return self;
//}

- (void)viewDidLoad {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString * keyFindPw = @"hunet_user_changed_password";
    if ([defaults objectForKey:keyFindPw] && [[defaults objectForKey:keyFindPw] isEqualToString:@"NO"]) {
        self.navigationController.navigationBar.hidden = YES;
        self.tabBarController.tabBar.hidden = YES;
    }
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
	NSString *urlText = [NSString stringWithFormat:@"%@?type=40&uid=%@&pw=%@", app.urlBase, [app.profileUser objectForKey:@"userid"], [app.profileUser objectForKey:@"password"]];
	[viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];
    [super viewDidLoad];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (NSUInteger)supportedInterfaceOrientations
{
    indicator.center = viewWeb.center;
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskAllButUpsideDown;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    
	viewWeb = nil;
	indicator = nil;
    
    [super viewDidUnload];
}


- (void)dealloc {
	viewWeb.delegate = nil;
}


@end
