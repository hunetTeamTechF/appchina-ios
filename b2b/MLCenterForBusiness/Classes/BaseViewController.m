

#import "BaseViewController.h"
#import "ModalAlert.h"
#import "Header.h"
#import "SocialViewController.h"
#import "WebModalViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController
@synthesize addStatusBar = _addStatusBar;
@synthesize playerCallFun = _playerCallFun;
@synthesize currentUrl = _currentUrl;

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        isFirstLoaded = NO;
        self.statusBarHidden = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.statusBarHidden)
        return;
    
    
    
    NSArray *vComp = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[vComp objectAtIndex:0] intValue] >= 7) {
        [self setNeedsStatusBarAppearanceUpdate];
        self.edgesForExtendedLayout = UIRectEdgeNone;
        // iOS 7 or above
        CGRect oldBounds = [self.view bounds];
        CGRect newViewBounds = CGRectMake(0, -10, oldBounds.size.width, oldBounds.size.height - 20);
        CGRect newWebViewBounds = CGRectMake(0, -20, oldBounds.size.width, oldBounds.size.height - 40);
        if (self.view.bounds.origin.y == newViewBounds.origin.y)
            return;

        [self.view setBounds:newViewBounds];
        [self.webView setBounds:newWebViewBounds];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    // View defaults to full size.  If you want to customize the view's size, or its subviews (e.g. webView),
    // you can do so here.
    //Lower screen 20px on ios 7
    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setWebView:nil];
    [self setIndicator:nil];
    //[self setAddStatusBar:nil];
    [self setCurrentUrl:nil];
    [self setPlayerCallFun:nil];
    [super viewDidUnload];
}


- (void) webViewDidFinishLoad:(UIWebView *)webView {
	[self.indicator stopAnimating];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
	[self.indicator startAnimating];
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
	[self.indicator stopAnimating];
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	NSURL *url = request.URL;
	NSString *urlString = url.absoluteString;
    
    NSLog(@"base : %@", urlString);
    
    if ([urlString.lowercaseString hasPrefix:@"back://"]) {
        [self.webView goBack];
    }
    
    //모달 팝업
    if ([urlString.lowercaseString.lowercaseString hasPrefix:@"modal://"])
    {
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        int qLocation = [url.absoluteString rangeOfString:@"?"].location;
        NSString *query = [url.absoluteString substringFromIndex:qLocation + 1];
        query = [query stringByReplacingOccurrencesOfString:@"://" withString:@""];
        
        
        
        
        for (NSString *item in [query componentsSeparatedByString:@"&"]) {
            NSArray *keyValue = [item componentsSeparatedByString:@"="];
            if (![keyValue objectAtIndex:0])
                continue;
            NSString *key = [keyValue objectAtIndex:0];
            NSString *value = (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (CFStringRef)[keyValue objectAtIndex:1], CFSTR(""), kCFStringEncodingUTF8));
            [params addEntriesFromDictionary:[[NSDictionary alloc]
                                              initWithObjects:[[NSArray alloc] initWithObjects:value, nil]
                                              forKeys:[[NSArray alloc] initWithObjects:key, nil]]];
        }
        
        //[Util valueForKey:@"url" fromQuery:[request URL].query];
        
        NSString *urlParamDecoded = [params valueForKey:@"url"];
        NSLog(@"urlParamDecoded : %@", urlParamDecoded);
        //?url query parameter가 없는 경우,
        if (urlParamDecoded == nil) {
            WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
            webModal.webViewUrl = [request.URL.absoluteString stringByReplacingOccurrencesOfString:@"modal://" withString:@"http://"];
            //webModal.title = @"";
            [self presentViewController:webModal animated:YES completion:nil];
        }
        //query에 url parameter가 있으면 해당 url로 modal 팝업
        else {
            
            NSURL *urlParsed = [NSURL URLWithString:[[urlParamDecoded componentsSeparatedByString:@"?"] firstObject]];
            NSString *method = [params valueForKey:@"method"];
            NSString *title = [params valueForKey:@"title"];
            //post parameter가 있으면 url을 파싱해서, queryString은 post로 전송
            if ([method.lowercaseString isEqualToString:@"post"])
            {
                WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
                
                //webViewRequestMethod
                NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:urlParsed
                                                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                   timeoutInterval:20.0];
                [request setHTTPMethod:@"POST"];
                NSString *bodyString = (NSString *)[[urlParamDecoded componentsSeparatedByString:@"?"] lastObject];
                [request setHTTPBody:[bodyString dataUsingEncoding:NSUTF8StringEncoding]];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                webModal.title = title;
                [self presentViewController:webModal animated:YES completion:nil];
                [webModal.WebView loadRequest:request];
                
                
            }
            //query에 url parameter가 없으면
            else {
                WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
                webModal.webViewUrl = urlParamDecoded;
                webModal.title = title ? title : @"";
                if ([urlParamDecoded rangeOfString:@"/eBook/"].location != NSNotFound)
                    [webModal.ButtonClose setHidden:NO];
                else
                    [webModal.ButtonClose setHidden:YES];
                
                [self presentViewController:webModal animated:YES completion:nil];
            }
        }
        
        return NO;
        
    }

    if ([urlString.lowercaseString hasPrefix:@"sangsangdrmplayer://"] ||
        [urlString.lowercaseString hasPrefix:@"sangsangdownload://"] ||
        [urlString.lowercaseString hasPrefix:@"sangsangdownloadplay://"] ||
        [urlString.lowercaseString hasPrefix:@"sangsangplayer://"]) {
        
        NSLog(@"sangsangdrmp");
        
        if ([urlString.lowercaseString hasPrefix:@"sangsangplayer://"]) {
            urlString = [urlString.lowercaseString stringByReplacingOccurrencesOfString:@"sangsangplayer://" withString:@"sangsangdrmplayer://"];
        }
        
        NSArray *tmpArray = [urlString componentsSeparatedByString:@"://"];
        NSString *act = [tmpArray objectAtIndex:0];
        NSString *param = [tmpArray objectAtIndex:1];
        
        BOOL checkConfigSettings = false;
        //상상마루 바로학습
        if ([urlString.lowercaseString hasPrefix:@"sangsangdrmplayer://"]) {
            checkConfigSettings = [[self appDelegate] moviePlayCheck];
        } //상상마루 다운로드 플레이
        else {
            checkConfigSettings = [[self appDelegate] isWiFi];
        }
        if (checkConfigSettings) {
            _playerCallFun = [[PlayerCallFun alloc] init];
            _playerCallFun.delegateViewController = self;
            [_playerCallFun sangsangmaruInit:[NSArray arrayWithObjects:act, param, nil]];
            
            
            //self.currentUrl = [_webView stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
            
        }

        return NO;
    }
    return YES;
}


- (void)processAction {
    [self.webView reload];
    NSLog(@"다운로드 완료 버튼 클릭");
}


- (void)openerCallFun {
    NSLog(@"player close1");
    
    if (self.currentUrl.length > 0) {
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:self.currentUrl]
                                                    cachePolicy: NSURLRequestReloadIgnoringCacheData
                                                timeoutInterval:60.0
                                    ];
        [_webView loadRequest:requestObj];
        self.currentUrl = nil;;
    }
    else {
        //NSString *webViewUrl = [_webView stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
        [self.webView reload];
    }
}

- (NSUInteger)supportedInterfaceOrientations
{
    //NSLog(@"base supportedInterfaceOrientations");
    
    self.indicator.center = self.webView.center;
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskAllButUpsideDown;
}

- (BOOL)shouldAutorotate
{
    //NSLog(@"base shouldAutorotate");
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    //return UIStatusBarStyleLightContent;
    return UIStatusBarStyleDefault;
}

- (MLCenterForBusinessAppDelegate*)appDelegate
{
    return [MLCenterForBusinessAppDelegate sharedAppDelegate];
}


- (void)settingStatusBar {
    if(IS_DEVICE_RUNNING_IOS_7_AND_ABOVE()) {
        if (_addStatusBar != nil) {
            [_addStatusBar removeFromSuperview];
            _addStatusBar = nil;
        }
        
        _addStatusBar = [[UIView alloc] init];
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        _addStatusBar.frame = CGRectMake(0, -10, screenBounds.size.width, 20);
        
#ifdef emart_13037
        _addStatusBar.backgroundColor = [UIColor colorWithRed:76.0/255.0 green:45.0/255.0 blue:27.0/255.0 alpha:1.0];
#elif hyundai_16597 || hyundai_16596
        _addStatusBar.backgroundColor = [UIColor colorWithRed:97.0/255.0 green:169.0/255.0 blue:132.0/255.0 alpha:1.0];
#else
        _addStatusBar.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:78/255.0 blue:165/255.0 alpha:1.0];
#endif
        
        _addStatusBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self.view addSubview:_addStatusBar];
    }
}


- (void)webViewpushUrl:(NSString *)webViewpushUrl {
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:webViewpushUrl]
                                                cachePolicy: NSURLRequestReloadIgnoringCacheData
                                            timeoutInterval:60.0];
    
    [self.webView loadRequest:requestObj];
    
    self.appDelegate.pushService = nil;
}





@end
