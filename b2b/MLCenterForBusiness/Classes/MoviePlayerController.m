#import "MoviePlayerController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "CJSONDeserializer.h"
#import "Util.h"
#import "ModalAlert.h"
#import "Reachability.h"
#import "DrmManager.h"

@interface MoviePlayerController ()
{
    
}
@end

@implementation MoviePlayerController
@synthesize player, playBtn, playBool, isStreamingPlay, currentSecond, scrollType, takecourseSeq, maxSec;
@synthesize delegate=_delegate;

#define degreesToRadians(x) (M_PI * x / 180.0)
#define isPortrait [[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortrait || [[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown

#define IS_4_INCH CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(320, 568)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(568, 320))
#define IS_iPhone4 CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(320, 480)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(480, 320))
#define IS_iPhone5 CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(320, 568)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(568, 320))
#define IS_iPhone6 CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(375, 667)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(667, 375))
#define IS_iPhone6_plus CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(414, 736)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(736, 414))

- (void)dismissModalClose
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)studyLimitMessageHide
{
    [UIView animateWithDuration:1.0 animations:^(void) {
        self.studyLimitMessage.alpha -= 0.1;
        self.studyLimitMessage.alpha -= 0.1;
        self.studyLimitMessage.alpha -= 0.1;
        self.studyLimitMessage.alpha -= 0.1;
    }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!movieExitBool) {
        [self performSelector:@selector(dismissModalClose) withObject:nil afterDelay:0.5];
    }
    
    //if (!progresSlider.userInteractionEnabled) {
    if(self.scrollType == 0){
        [self performSelector:@selector(studyLimitMessageHide) withObject:nil afterDelay:3.0];
    }
    
    //controll auto hide
    [self performSelector:@selector(autoHide) withObject:nil afterDelay:3.0];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self coredata];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(reachabilityChanged:)
                                                 name: kReachabilityChangedNotification
                                               object: nil];
}

- (void)viewWillDisappear:(BOOL)animated {
     [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

-(void)autoHide{
    dControllerView.alpha = 0.0f;
    uControllerView.alpha = 0.0f;
    touchBool = FALSE;
}

-(BOOL)prefersStatusBarHidden { return YES; }

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


//IOS 6.0 회전 관련 시작
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape; //가로화면만 허용
//    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotate
{
    //UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    // Disable autorotation of the interface.
    return YES;
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
    //기존 뷰 배치 로직 수행...
    NSLog(@"회전");
}

//IOS 6.0 회전 관련 끝
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation //IOS 6.0 이하 버전
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
    //return interfaceOrientation == UIInterfaceOrientationLandscapeRight;
    if((interfaceOrientation == UIInterfaceOrientationLandscapeLeft)
	   || (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
	{
		return YES;
	}
	
	return NO;
}


- (void) cleanup {
    
    [UIApplication sharedApplication].statusBarHidden = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation: UIInterfaceOrientationPortrait animated:NO];
    
    /*
     BOOL isLessIOS5 = ([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending);
     if(isPortrait || isLessIOS5)
     {
     [[UIApplication sharedApplication] setStatusBarOrientation: UIInterfaceOrientationPortrait animated:YES];
     CGRect viewFrame = CGRectMake(0.0f, 0.0f, self.navigationController.view.frame.size.width, self.navigationController.view.frame.size.height);
     //CGRect viewFrame = CGRectMake(0.0f, 0.0f, 320, 480);
     self.navigationController.view.bounds = viewFrame;
     self.navigationController.view.center = CGPointMake(viewFrame.size.width / 2.0, viewFrame.size.height / 2.0);
     self.navigationController.view.transform = CGAffineTransformMakeRotation(degreesToRadians(0));
     }
     */
    
    [playerView removeFromSuperview];
    [dControllerView removeFromSuperview];
    [uControllerView removeFromSuperview];
    [indexControllerView removeFromSuperview];
    [bookControllerView removeFromSuperview];
    [timer invalidate];

    
    if (timeObserver != nil) {
		[player removeTimeObserver:timeObserver];
		timeObserver = nil;
	}
    
    if (timer != nil) {
		timer = nil;
	}
    
    if (self.drmManager != nil) {
        [self.drmManager stopHttpServer];
    }
    
    if (player != nil) {
        player = nil;
    }
    
    if (playerView != nil) {
        playerView = nil;
    }
    if (dControllerView != nil) {
        dControllerView = nil;
    }
    if (uControllerView != nil) {
        uControllerView = nil;
    }
    if (playBtn != nil) {
        playBtn = nil;
    }
    if (viewBtn != nil) {
        viewBtn = nil;
    }
    if (lProgLbl != nil) {
        lProgLbl = nil;
    }
    if (rProgLbl != nil) {
        rProgLbl = nil;
    }
    if (speedLabel != nil) {
        speedLabel = nil;
    }
    
    if (avTitle != nil) {
        avTitle = nil;
    }
    if (progresSlider != nil) {
        progresSlider = nil;
    }
}

static Float64 secondsWithCMTimeOrZeroIfInvalid(CMTime time) {
	return CMTIME_IS_INVALID(time) ? 0.0f : CMTimeGetSeconds(time);
}

- (void)coredata
{
    self.sagnsangStudyArray = [Util getSangStudy:self.gid andContentsSeq:[NSString stringWithFormat:@"%d", self.cseq] andUserId:[[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"userid"]];
}



- (void)syncSangBeforeStudy {
    
    @autoreleasepool {
    
        NSManagedObject *obj = [self.sagnsangStudyArray objectAtIndex:0];
        NSString *contentsSeq = [obj valueForKey:@"contents_seq"];
        NSString *goodsId = [obj valueForKey:@"goods_id"];
        NSString *viewNo = [obj valueForKey:@"view_no"];
        NSString *userId = [obj valueForKey:@"user_id"];
        NSString *viewSec = [obj valueForKey:@"view_sec"];
        NSString *lastViewSec = [obj valueForKey:@"last_view_sec"];
        NSString *viewSecMobile = [obj valueForKey:@"view_sec_mobile"];
        NSString *contractNo = [obj valueForKey:@"contract_no"];
        NSString *mode = @"S";
        
        if (![[MLCenterForBusinessAppDelegate sharedAppDelegate] isNotInternet])
        {
            MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
            [Util synSang:userId andCompanySeq:app.companySeq andContractNo:contractNo andContentsSeq:contentsSeq andGoodsId:goodsId andviewNo:viewNo andViewSec:viewSec andViewSecMobile:viewSecMobile andLastViewSec:lastViewSec andMode:mode];

        }
    }
}

- (void)updateControl {

#ifdef ksan_17898
    Reachability *isConnect = [Reachability reachabilityForInternetConnection];
    
    //셀룰러 데이터 (3G,4G) 접속
    if (isStreamingPlay && [isConnect currentReachabilityStatus] == ReachableViaWWAN)
    {
        MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        //설정 > 동영상 강의 시청 == NO
        if ([[app.profileUser objectForKey:@"threegvideo"] boolValue] == NO)
        {
            [ModalAlert notify:@"3G/LTE 시청이 제한되어 있습니다."];
            [self StopPlay];
        }
    }
#endif
    
	double d = secondsWithCMTimeOrZeroIfInvalid([player currentTime]);
    
    NSLog(@"TotalTime %@",[Util timeMmddss:(int)totalTime]) ;
    double lastViewSec = secondsWithCMTimeOrZeroIfInvalid([player currentTime]);
    NSLog(@"lastViewSec %f , totalTime %f",lastViewSec,totalTime);
    if ((int)lastViewSec ==(int)totalTime)
    {
        NSLog(@"Finish Play!");
        [self finishPlay];
    }
    
    NSLog(@"MoviePlayerController -updateControl: %f", d);
    
	progresSlider.value = d;
    lProgLbl.text = [Util timeMmddss:(int)d];
    
    totalProgressCount++;
    markStudySec++;

    if (_cseq > 0) {//상상마루
        
        //처음 실행 시, 싱크(오프라인 다운로드창)
        if (self.sagnsangStudyArray.count > 0 && timerCounter == 1) {
            [self syncSangBeforeStudy];
        }
        else if(timerCounter==1){ //처음 진입시 로깅(S)를 위해 1번 진도 저장(온라인)
          //viewdidload로 옮김
        }
        else if (timerCounter % 30 == 0 && timerCounter > 1)
        {
            //_currentViewSec += 30;
            _currentViewSec += pureStudySec;//순수한 초로 변경
            [self saveCoredataMovieTimeSang:[NSString stringWithFormat:@"%d", (int)d]];

            [NSThread detachNewThreadSelector:@selector(saveSangMovieTime:)
                                     toTarget:self
                                   withObject:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%d", (int)d],
                                               [NSString stringWithFormat:@"%@", @"E"], nil]];
            
            pureStudySec = 0;
        }
    }
    // 온라인 과정
    else
    {
        //새로운 진도저장방식 적용인경우
        if (_movContents != nil && _movContents.count > 1 && _movContents.count >= markNo) {//마크값이 2개 이상인경우적용
            
            markValue =[[[_movContents objectAtIndex:markNo-1] objectForKey:@"MarkValue"] intValue];
            realFrameNo =[[[_movContents objectAtIndex:markNo-1] objectForKey:@"RealFrameNo"] intValue];
            
            //currentSecond = lastMarkValue 임
            //NSLog(@"진도저장전 -> studySec:%@, markNo:%@, frameNo:%d | markValue:%d, currentSecond:%d, markStudySec:%d, pureStudySec:%d",
            //      [NSString stringWithFormat:@"%d", markStudySec], [NSString stringWithFormat:@"%d", markNo], realFrameNo, markValue, currentSecond, markStudySec, pureStudySec);
            
            if (markValue == currentSecond + markStudySec-1) {
                NSLog(@"[진도저장!] -> studySec:%@, markNo:%@, frameNo:%d | markValue:%d, currentSecond:%d, markStudySec:%d",
                      [NSString stringWithFormat:@"%d", markStudySec], [NSString stringWithFormat:@"%d", markNo], realFrameNo, markValue, currentSecond, markStudySec);
                
                [self saveCoredataMovieTimeV2:pureStudySec andMarkValue:markValue];
                
                //markStudySec -> pureStudySec
                if (realFrameNo == 0) {
                    realFrameNo = 1;
                }
                
                [NSThread detachNewThreadSelector:@selector(saveMovieTimeV2:)
                                         toTarget:self
                                       withObject:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%d", pureStudySec], [NSString stringWithFormat:@"%d", markNo],
                                                   [NSString stringWithFormat:@"%d", realFrameNo],
                                                   nil]];
                
                markStudySec = 0;//초기화
                pureStudySec = 0;
                currentSecond = markValue;
                
                [self setImageView:markNo-1];
                
                if (_movContents.count >= markNo) {
                    markNo++;
                }
            }
        }
        else if (totalProgressCount % 60 == 0)
        {
            [self saveCoredataMovieTime:[NSString stringWithFormat:@"%d", (int)d]];
            [NSThread detachNewThreadSelector:@selector(saveMovieTime:)
                                     toTarget:self
                                   withObject:[NSString stringWithFormat:@"%d", (int)d]];
        }
        //종료시
        //NSLog(@"progresSlider.value : %d, totalTime : %d", (int)progresSlider.value, (int)totalTime);
        if ((int)progresSlider.value >= (int)totalTime) {
            [self StopPlay];
        }
        
        
        //보았던 부분은 접근 가능하도록
        if (self.scrollType == 0) {
            maxSec = (int)d > maxSec ? (int)d : maxSec;
            if (maxSec > (int)progresSlider.value) {
                self.btnBack.userInteractionEnabled = YES;
                self.btnNext.userInteractionEnabled = YES;
                progresSlider.userInteractionEnabled = YES;
            }
            else{
                self.btnBack.userInteractionEnabled = YES;
                self.btnNext.userInteractionEnabled = NO;
                progresSlider.userInteractionEnabled = YES;
            }
        }
        else
        {
            maxSec = totalTime;
        }

    }
    
    
    if (modeFlg == 2 && repeatEtime > 0.0f) {
        if (d >= repeatEtime) {
            [player pause];
            [player seekToTime:CMTimeMakeWithSeconds(repeatStime,NSEC_PER_SEC) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
            [self avPlay];
        }
    }
}

- (void)updateVolumeProgress {
    float volumeProgress = _volumeSlider.value;
    NSString *volumStr = [[[NSString stringWithFormat:@"%f",(volumeProgress/0.0625)] componentsSeparatedByString:@"."] objectAtIndex:0];
    _lblVolume.text =  volumStr;
    //player.volume = volumeProgress;
    [self setVolumeCustom:volumeProgress];
}


- (void)updateVolume {
    NSString *volumStr = [[[NSString stringWithFormat:@"%f",(iPod.volume/0.0625)] componentsSeparatedByString:@"."] objectAtIndex:0];
    _lblVolume.text = volumStr;
}

-(int)getRandomNumber:(int)from to:(int)to {
    return (int)from + arc4random() % (to-from+1);
}

-(void)getContentsModuleCurrentTime{
     if([_progressSaveYn isEqualToString:@"N"]){
 
         NSString *hostUrl = @"http://b2bapp.xiunaichina.com/Jlog/SavePlayer";
         NSError *errorReq = nil;
         NSString* url = [NSString stringWithFormat:@"action=SelectLastViewSecByHtmlModule&takeCourseSeq=%d&courseCd=%@&progressNo=%@&frameNo=%@", self.takecourseSeq, self.courseCd, self.progressNo, [NSString stringWithFormat:@"%d", self.frameNo]];
         NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:hostUrl]
                                               andQuery:url
                                               andError:&errorReq];
         NSLog(@"saveUrl : %@", url);
         if (jsonData)
         {
             NSError *errorJSON = nil;
             NSDictionary *result = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
             if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
                 currentSecond = [[result objectForKey:@"lastViewSec"] intValue];
                 @try {
                     scrollType = [[result objectForKey:@"progressRestrictionYN"] isEqualToString:@"Y"] ? 0 : 1;
                 }
                 @catch (NSException *exception) {
                     scrollType = 0;
                 }
                 
             }
         }
     }
}

-(void)viewDidLoad {
    [super viewDidLoad];

    if (app.isRuningMovieController == YES) {
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    app.isRuningMovieController = YES;
    
    if (IS_iPhone6) plusX = 99;
    else if (IS_iPhone6_plus) plusX = 168;
    else plusX = 0;
    
    if (IS_iPhone6) plusY = 55;
    else if (IS_iPhone6_plus) plusY = 94;
    else plusY = 0;
    
    //컨텐츠모듈화 currentTime 세팅
    [self getContentsModuleCurrentTime];
    [MLCenterForBusinessAppDelegate sharedAppDelegate].flagAutoGo = false;
    totalProgressCount = 0;
    
    isSpeedPushed = false;
    isVolumePushed = false;
    maxSec = 0;
    markNo=1;
    markValue = 0;
    markStudySec = 0;
    
    pptIndex = 0;
    pureStudySec = 0;
    timerCounter = 0;
    
    if (self.lastMarkNo > 0) {
        markNo = self.lastMarkNo;
    }

    avTitle.text = self.movieTitle;
    
    [self SetInitSpeedControls];
    [self SetInitVolumeControls];
    
    self.studyArray = [Util getStudy:self.courseCd andTakeCourseSeq:[NSString stringWithFormat:@"%d", self.takecourseSeq] andChapterNo:self.chapterNo];
    
    NSURL *url = NULL;
    
    NSString *moviePath = [[NSUserDefaults standardUserDefaults] objectForKey:FILENAME];
    //tcl_training_center.mobile_drm_type='1'이면, api에서 DRM서버의 주소인 http://m.hunet.hscdn.com/hunet/Mvod/10478.MP4 형태의 경로를 반환해 줌
    
    //tcl_training_center.mobile_drm_type='0'인 경우 http://hunet2.hscdn.com/hunetvod/_definst_/mp4:Mvod/10478.mp4/playlist.m3u8 형태의 와우자 서버 경로를 반환하기 때문에, 이를 DRM 서버의 경로로 치환해 줌.
    //http://hunet2.hscdn.com/hunetvod/_definst_/mp4:M_Learning/HLSC11763/01.mp4/playlist.m3u8
    //http://hunet2.hscdn.com/hunetvod/mp4://M_Learning/HLSC02191/01.mp4/playlist.m3u8
    NSLog(@"MoviePlayerContrimmer.m > moviePath(before) : %@", moviePath);
    if ([moviePath.lowercaseString hasPrefix:@"http://hunet2.hscdn.com/hunetvod/_definst_/mp4:"]) {
        moviePath = [moviePath.lowercaseString stringByReplacingOccurrencesOfString:@"http://hunet2.hscdn.com/hunetvod/_definst_/mp4:" withString:@"http://m.hunet.hscdn.com/hunet/"];
    }
    moviePath = [moviePath stringByReplacingOccurrencesOfString:@"/playlist.m3u8" withString:@""];
    NSLog(@"MoviePlayerContrimmer.m > moviePath(after) : %@", moviePath);
    
    [progresSlider setBackgroundColor:[UIColor clearColor]];
	[progresSlider setThumbImage:[UIImage imageNamed:@"av_bottom_progress_btn.png"] forState:UIControlStateNormal];
	[repeatBtn setImage:[UIImage imageNamed:@"btn_section.png"] forState:UIControlStateNormal];
    
    [UIApplication sharedApplication].statusBarHidden = YES;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight animated:NO];
    
    scnFullBool = TRUE;
    
    //시스템 사운드 조절용
    iPod = [MPMusicPlayerController iPodMusicPlayer];
    
    
    if (!isStreamingPlay) { //로컬 재생일 경우 http 데몬 상태 채크 후 실행
        self.drmManager = [[DrmManager alloc] init];
        url = [self.drmManager startHttpServer:moviePath];
    }
    else
    {
        //스트리밍 재생일 경우 url 지정
        url = [NSURL URLWithString:moviePath];
    }
    
    // 플레이어 컨트럴 생성 및 재생 시작
    
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:url
											options:nil];
    
    
	AVPlayerItem *playerItem = [AVPlayerItem playerItemWithAsset:asset];
    
	player = [AVPlayer playerWithPlayerItem:playerItem];
    modeFlg = 0; //일반 모드.
    
	// enable/disable UI as appropriate
	NSArray *visualTracks = [asset tracksWithMediaCharacteristic:AVMediaCharacteristicVisual];
    
    
    NSLog(@"visualTracks : %d", [visualTracks count]);
    
	if ((!visualTracks) ||
		([visualTracks count] == 0)) {
        [self cleanup];
        movieExitBool = NO;
        return;
	}
    else
    {
		playerView.hidden = NO;
        // 위 컨트롤러 표시
        uControllerView.frame = CGRectMake(0, 0, uControllerView.frame.size.width, 30);
        // 아래 컨트롤러 표시
        dControllerView.frame = CGRectMake(0, 200 + plusY, dControllerView.frame.size.width, 124);
            
        
        [self.view addSubview:uControllerView];
        [self.view addSubview:dControllerView];
        
        playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
		[playerView.layer addSublayer:playerLayer];
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
        playerLayer.frame = playerView.layer.bounds;
        
        
        scnFullBool = false;
        
        
        playBool = FALSE;
        touchBool = TRUE;
        movieExitBool = YES;
        
        realSpeed = 1.0;
        
        //[player setVolume:iPod.volume];
        [self setVolumeCustom:_volumeSlider.value];
        _volumeSlider.value = iPod.volume;
        
        // 현재 볼륨 표시.
        //[self updateVolume];
        
        CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI * -0.5);
        _volumeSlider.transform = trans;
        
        [self updateVolumeProgress];
        
        // 현재 스피드 표시.
        speedLabel.text = @"1.0";
        
        //Streaming Play
        if (isStreamingPlay)
        {
            [_imgPlayBtn setImage:[UIImage imageNamed:@"av_bottom_pause.png"]];
        }
        else //Download Play
        {
            //마크값기준 진도호출을 위해 반드시 필요
            //currentSecond = seekTime;
            markStudySec = 0;
            markNo = [self seekMarkNo:currentSecond];
            [player seekToTime:CMTimeMakeWithSeconds(currentSecond,NSEC_PER_SEC) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
        }
	}
    
    CMTime duration = self.player.currentItem.asset.duration;
	totalTime = secondsWithCMTimeOrZeroIfInvalid(duration);
    
    // 총플레이 시간을 표시
    rProgLbl.text = [Util timeMmddss:(int)totalTime];
	progresSlider.maximumValue = totalTime;
	
    float centerValue = ((progresSlider.minimumValue + progresSlider.maximumValue) / 2) + progresSlider.minimumValue;
    [progresSlider setValue:centerValue animated:YES];
    progresSlider.value = 0;
    
    //NSLog(@"currentSecond : %d, scrollType : %d", currentSecond, scrollType);
    
    if (self.scrollType == 0) {
        progresSlider.userInteractionEnabled = NO;
        self.btnBack.userInteractionEnabled = NO;
        self.btnNext.userInteractionEnabled = NO;
    }
    
    
    //magic7k-20150306 : 현재 컨텐츠의 CoreData.traning.StudyHistory.last_view_sec를 가져와서 값이 있으면 해당 값으로 currentSecond를 설정
//    int last_view_sec = [self getLastViewSecFromCoreData];
//
//    if (last_view_sec > 0) {
//        
//        if ([ModalAlert ask:@"최근 학습하신 지점에서 시작 하시겠습니까?"]) {
//            currentSecond = last_view_sec;
//        } else {
//            currentSecond = 0;
//        }
//    }
    
    //Streaming Play
    if (isStreamingPlay)
    {
        //모두 들었을 경우 다시 처음
        if (currentSecond >= ((int)totalTime - 10)) {
            currentSecond = 0;
        }
        if (currentSecond > 0 && currentSecond >= ((int)totalTime - 10)) {
            currentSecond = (int)totalTime - 10;
        }
        [player seekToTime:CMTimeMakeWithSeconds(currentSecond,NSEC_PER_SEC) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    }
    else //Download Play
    {
        //다운로드 된 동영상 파일
        //복습일경우 currentSecond값을 max값으로 리턴하여 이동가능하도록 처리
        NSLog(@"시작 시간 : %d, 동영상 총 시간 : %f, %d", currentSecond, totalTime, (int)totalTime);
        if (currentSecond >= (int)totalTime) {
            progresSlider.userInteractionEnabled = YES;
            self.btnBack.userInteractionEnabled = YES;
            self.btnNext.userInteractionEnabled = YES;
            currentSecond = 0;
            self.scrollType = 1;
        }
        if (currentSecond > 0 && currentSecond >= ((int)totalTime - 10)) {
            currentSecond = (int)totalTime - 10;
        }
        [player seekToTime:CMTimeMakeWithSeconds(currentSecond,NSEC_PER_SEC) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    }
    
    //if (!progresSlider.userInteractionEnabled) {
     if(self.scrollType == 0){
//        self.studyLimitMessage.center = CGPointMake(self.studyLimitMessage.center.x, 230 + plusY);
//        [self.view addSubview:self.studyLimitMessage];
//        self.studyLimitMessage.alpha = 0.4;
//         [ModalAlert toast:@"최초 학습 완료 시 이동이 가능합니다." andDelay:1.0f];
    }
    
	[self playStopAction:nil];
    __weak MoviePlayerController *self_ = self;
	timeObserver = [player addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(1, NSEC_PER_SEC) queue:nil usingBlock:^(CMTime time) {
		[self_ updateControl];
	}];
    
    //순수 학습시간 저장용
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                             target:self
                                           selector:@selector(studySecondSave:)
                                           userInfo:nil
                                            repeats:YES];

    
    //상상마루
    _currentViewSec = 0;
    
    //clock
    [self updateClock];
    
    //교안처리
    if (_movContents != nil && _movContents.count > 1) {
        if(markNo > _movContents.count)
        {
            markNo = _movContents.count;
        }
        pptIndex = markNo-1;
        [self setImageView:pptIndex];
    }
    [self SetBookNIndexControls];
    
    //처음상상마루 접속시(스트리밍인경우)
    if (_cseq && isStreamingPlay) {
        //
        [NSThread detachNewThreadSelector:@selector(saveSangMovieTime:)
                                 toTarget:self
                               withObject:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%d", (int)secondsWithCMTimeOrZeroIfInvalid([player currentTime])],
                                           [NSString stringWithFormat:@"%@", @"S"], nil]];
    }
}

-(int)seekMarkNo:(int)getCurrentSecond {
    int eachMarkValue = 1;
    int returnMarkNo = 1;
    
    for (int i=0; i<_movContents.count; i++) {
        eachMarkValue =[[[_movContents objectAtIndex:i] objectForKey:@"MarkValue"] intValue];//마크 duration seconds
        if (eachMarkValue >= getCurrentSecond) {
            returnMarkNo = [[[_movContents objectAtIndex:i] objectForKey:@"MarkNo"] intValue];//마크 값;
            return returnMarkNo;
        }
    }
    
    return 1; //처음
}

-(void)setVolumeCustom:(float)volumeAmount{
    
    AVAsset *avAsset = [[self.player currentItem] asset] ;
    NSArray *audioTracks = [avAsset tracksWithMediaType:AVMediaTypeAudio] ;
    
    NSMutableArray *allAudioParams = [NSMutableArray array] ;
    for(AVAssetTrack *track in audioTracks){
        AVMutableAudioMixInputParameters *audioInputParams = [AVMutableAudioMixInputParameters audioMixInputParameters] ;
        [audioInputParams setVolume:volumeAmount atTime:kCMTimeZero] ;
        [audioInputParams setTrackID:[track trackID]] ;
        [allAudioParams addObject:audioInputParams];
    }
    AVMutableAudioMix *audioVolMix = [AVMutableAudioMix audioMix] ;
    [audioVolMix setInputParameters:allAudioParams];
    [[self.player currentItem] setAudioMix:audioVolMix];
}

- (void)viewDidUnload {
    [self setLblClock:nil];
    [self setImgMagnify:nil];
    [self setBtnSpeed:nil];
    [self setLblVolume:nil];
    [self setVolumeSlider:nil];
    [self setBtnSpeedUp:nil];
    [self setBtnSpeedDown:nil];
    [self setImgSpeedbg:nil];
    [self setImgNextBtn:nil];
    [self setImgBackBtn:nil];
    [self setImgPlayBtn:nil];
    [self setBtnNext:nil];
    [self setBtnBack:nil];
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
    [super viewDidUnload];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    if (touchBool) {
        dControllerView.alpha = 0.0f;
        uControllerView.alpha = 0.0f;
        touchBool = FALSE;
    } else {
        dControllerView.alpha = 1.0f;
        uControllerView.alpha = 1.0f;
        touchBool = TRUE;

    }
    
    
}

- (void)avPlay {
    [player play];
    player.rate = realSpeed;
    playBool = TRUE;
    
    //[player setVolume:_volumeSlider.value];
    [self setVolumeCustom:_volumeSlider.value];
    [_imgPlayBtn setImage:[UIImage imageNamed:@"av_bottom_pause.png"]];
    NSLog(@"avPlay");
}
-(IBAction) sliderProgressEvent:(id)sender{
    [progresSlider setThumbImage:[UIImage imageNamed:@"av_bottom_progress_btn.png"] forState:UIControlStateNormal];
    
    touchBool = FALSE;
    NSLog(@"sliderProgressEvent");
    
}

-(IBAction) sliderTouchesInsideEvent:(id)sender{
    
	touchBool = TRUE;
    [progresSlider setThumbImage:[UIImage imageNamed:@"av_bottom_progress_btn.png"] forState:UIControlStateNormal];
    
    UISlider *sliderProgress = (UISlider *)sender;
	float seekTime = sliderProgress.value;
    
    //최초학습완료가 아닌경우, 가장많이 학습한곳이상으로 이동불가
    if (self.scrollType==0 && maxSec < sliderProgress.value) {
        seekTime = maxSec;
        [ModalAlert toast:@"최초 학습 완료 시 이동이 가능합니다."];
    }
    
    //마크값기준 진도호출을 위해 반드시 필요
    currentSecond = seekTime;
    markStudySec = 0;
    markNo = [self seekMarkNo:currentSecond];
	[player seekToTime:CMTimeMakeWithSeconds(seekTime,NSEC_PER_SEC) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    
    NSLog(@"sliderTouchesInsideEvent");
}


-(IBAction) sliderTouchesOutsideEvent:(id)sender{
    NSLog(@"sliderTouchesOutsideEvent");
}

-(IBAction) VolumeLargeEvent:(id)sender{
    float volumeLevel = iPod.volume;
    iPod.volume = volumeLevel + 0.0625;
	[self updateVolume];
}

-(IBAction) VolumeSmallEvent:(id)sender{
    float volumeLevel = iPod.volume;
    
    iPod.volume = volumeLevel - 0.0625;
	[self updateVolume];
}

-(IBAction) playStopAction:(id)sender{
    if (playBool) {
        [player pause];
        playBool = FALSE;
        [_imgPlayBtn setImage:[UIImage imageNamed:@"av_bottom_play.png"]];
    } else {
        [player play];
        playBool = TRUE;
        [_imgPlayBtn setImage:[UIImage imageNamed:@"av_bottom_pause.png"]];
    }
    
    realSpeed = player.rate;
    
    NSString *speed= [[NSString stringWithFormat:@"%@%f",NSLocalizedString(@"playSpeed", @"倍数"), 1.0] substringToIndex:5];
    [_btnSpeed setTitle:speed forState:UIControlStateNormal];
    
    NSLog(@"playStopAction");
}

-(IBAction) frontTimeAction:(id)sender{
	float d = secondsWithCMTimeOrZeroIfInvalid([player currentTime]);
    //마크값기준 진도호출을 위해 반드시 필요
    currentSecond = d-PLAYER_JUMP_TIME < 0 ? 0 : d-PLAYER_JUMP_TIME;
    markStudySec = 0;
    markNo = [self seekMarkNo:currentSecond];
	[player seekToTime:CMTimeMakeWithSeconds(d-PLAYER_JUMP_TIME,NSEC_PER_SEC) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    NSLog(@"frontTimeAction");
    
}

-(IBAction) nextTimeAction:(id)sender{
	float d = secondsWithCMTimeOrZeroIfInvalid([player currentTime]);
    //마크값기준 진도호출을 위해 반드시 필요
    currentSecond = d+PLAYER_JUMP_TIME;
    markStudySec = 0;
    markNo = [self seekMarkNo:currentSecond];
	[player seekToTime:CMTimeMakeWithSeconds(d+ PLAYER_JUMP_TIME ,NSEC_PER_SEC) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    NSLog(@"nextTimeAction");
    
}

-(IBAction) screenResize:(id)sender{
    if (scnFullBool) {
        //기본 화면 사이즈 표시
		playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
        playerLayer.frame = playerView.layer.bounds;
        [_imgMagnify setImage:[UIImage imageNamed:@"av_top_magnify.png"]];
        scnFullBool = FALSE;
    } else {
        //기본 화면 사이즈 표시
		playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        playerLayer.frame = playerView.layer.bounds;
        [_imgMagnify setImage:[UIImage imageNamed:@"av_top_minimize.png"]];
        
        scnFullBool = TRUE;
    }
}

-(IBAction) endAVPlay:(id)sender{
    NSLog(@"endAVPlay");
    [self StopPlay];
}

-(void) StopPlay{
    
    double lastViewSec = secondsWithCMTimeOrZeroIfInvalid([player currentTime]);
	progresSlider.value = lastViewSec;
    lProgLbl.text = [Util timeMmddss:(int)lastViewSec];
    
    totalProgressCount++;
    
    if (_cseq > 0) {
        _currentViewSec += pureStudySec;//순수한 초로 변경
        [self saveCoredataMovieTimeSang:[NSString stringWithFormat:@"%d", (int)lastViewSec]];
        
        [NSThread detachNewThreadSelector:@selector(saveSangMovieTime:)
                                 toTarget:self
                               withObject:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%d", (int)lastViewSec],
                                           [NSString stringWithFormat:@"%@", @"E"], nil]];
    }else{
        //새로운 진도저장방식 적용인경우
        if (_movContents != nil && _movContents.count > 1 && markNo > 1) {
            //진도저장시간이 아직 안되었을 것이므로 markNo-2, 이전데이터를 호출하고, 학습 시간만 업데이트
            markValue =[[[_movContents objectAtIndex:markNo-2] objectForKey:@"MarkValue"] intValue];
            realFrameNo =[[[_movContents objectAtIndex:markNo-2] objectForKey:@"RealFrameNo"] intValue];
        }
        //2014.08.01 시간수정
        //[self saveCoredataMovieTimeV2:markStudySec andMarkValue:[NSString stringWithFormat:@"%d", (int)d]];
        [self saveCoredataMovieTimeV2:pureStudySec andMarkValue:(int)lastViewSec];
        
        //2015-03-13:magic7k: 학습 로깅
        /*
        NSDate *nowDate = [NSDate date];
        [self saveCoredataStudyHistory:[nowDate dateByAddingTimeInterval:-lastViewSec]
                       andStudyEndDate:nowDate
                        andLastViewSec:lastViewSec
                           andStudySec:pureStudySec
                    andStudyMethodType:self.isStreamingPlay ? 1 : 2
         ];
         */
        
        [self saveMovieTimeV2Stop:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%d", markValue], [NSString stringWithFormat:@"%d", pureStudySec], [NSString stringWithFormat:@"%d",(int)lastViewSec], nil]];//markStudySec ->pureStudySec
    }

    
    [player pause];
    
    //컨텐츠 모듈화과정 진도 자동 넘기기용
    //컨텐츠모듈화과정인경우
    if([_progressSaveYn isEqualToString:@"N"]){
        if (lastViewSec>= totalTime-1) {
             [MLCenterForBusinessAppDelegate sharedAppDelegate].flagAutoGo = true;
        }
    }
    
    if ([_delegate respondsToSelector:@selector(openerCallFun)]) {
        [_delegate performSelector:@selector(openerCallFun) withObject:nil afterDelay:0.3];
        _delegate = nil;
    }
    
    [self cleanup];
    app.isRuningMovieController = NO;
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void) finishPlay{
    
    double lastViewSec = secondsWithCMTimeOrZeroIfInvalid([player currentTime]);
    progresSlider.value = lastViewSec;
    lProgLbl.text = [Util timeMmddss:(int)lastViewSec];
    
    totalProgressCount++;
    
    if (_cseq > 0) {
        _currentViewSec += pureStudySec;//순수한 초로 변경
        [self saveCoredataMovieTimeSang:[NSString stringWithFormat:@"%d", (int)lastViewSec]];
        
        [NSThread detachNewThreadSelector:@selector(saveSangMovieTime:)
                                 toTarget:self
                               withObject:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%d", (int)lastViewSec],
                                           [NSString stringWithFormat:@"%@", @"E"], nil]];
    }else{
        //새로운 진도저장방식 적용인경우
        if (_movContents != nil && _movContents.count > 1 && markNo > 1) {
            //진도저장시간이 아직 안되었을 것이므로 markNo-2, 이전데이터를 호출하고, 학습 시간만 업데이트
            markValue =[[[_movContents objectAtIndex:markNo-2] objectForKey:@"MarkValue"] intValue];
            realFrameNo =[[[_movContents objectAtIndex:markNo-2] objectForKey:@"RealFrameNo"] intValue];
        }
        //2014.08.01 시간수정
        //[self saveCoredataMovieTimeV2:markStudySec andMarkValue:[NSString stringWithFormat:@"%d", (int)d]];
        [self saveCoredataMovieTimeV2:pureStudySec andMarkValue:(int)lastViewSec];
        
        //2015-03-13:magic7k: 학습 로깅
        /*
         NSDate *nowDate = [NSDate date];
         [self saveCoredataStudyHistory:[nowDate dateByAddingTimeInterval:-lastViewSec]
         andStudyEndDate:nowDate
         andLastViewSec:lastViewSec
         andStudySec:pureStudySec
         andStudyMethodType:self.isStreamingPlay ? 1 : 2
         ];
         */
        
        [self saveMovieTimeV2Stop:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%d", markValue], [NSString stringWithFormat:@"%d", pureStudySec], [NSString stringWithFormat:@"%d",(int)lastViewSec], nil]];//markStudySec ->pureStudySec
    }
    
    
    [player pause];
    
    //컨텐츠 모듈화과정 진도 자동 넘기기용
    //컨텐츠모듈화과정인경우
    if([_progressSaveYn isEqualToString:@"N"]){
        if (lastViewSec>= totalTime-1) {
            [MLCenterForBusinessAppDelegate sharedAppDelegate].flagAutoGo = true;
        }
    }
    
    if ([_delegate respondsToSelector:@selector(openQuiz)]) {
        [_delegate performSelector:@selector(openQuiz) withObject:nil afterDelay:0.3];
        _delegate = nil;
    }
    
    [self cleanup];
    app.isRuningMovieController = NO;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)studySync {
    
    @autoreleasepool {
    
        NSManagedObject *obj = [self.studyArray objectAtIndex:0];
        NSString *courseCd = [obj valueForKey:@"course_cd"];
        NSString *getTakeCourseSeq = [obj valueForKey:@"take_course_seq"];
        NSString *chapterNo = [obj valueForKey:@"chapter_no"];
        NSString *maxSec2 = [obj valueForKey:@"max_sec"];
        NSString *totalSec = [obj valueForKey:@"total_sec"];
        NSString *userId = [obj valueForKey:@"user_id"];

        
        if (![[MLCenterForBusinessAppDelegate sharedAppDelegate] isNotInternet])
        {
            NSDictionary *result = [Util synV2:courseCd andTakeCourseSeq:getTakeCourseSeq andChapterNo:chapterNo andStudySec:totalSec andMarkValue:maxSec2 andUserId:userId];
            if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
                [Util updateLatestSyncDate:self.courseCd andTakeCourseSeq:[NSString stringWithFormat:@"%d", self.takecourseSeq] andChapterNo:self.chapterNo];
                
//            if ([[result objectForKey:@"max_study_sec"] intValue]>0) {
//                maxSec = [result objectForKey:@"max_study_sec"];
//            }
                
               // NSLog(@"%maxSec2 : %@", maxSec);
                //self.synMessage = @"학습 이력이 동기화 되었습니다.";
            }
        }
   
    }
}

//반복 모드 설정
-(IBAction) startRepeatMode:(id)sender {
    NSLog(@"startRepeatMode");
    if(modeFlg == 0){
        repeatStime = 0.0f;
        repeatEtime = 0.0f;
        [self setRepeatSTime:nil];
        modeFlg = 1;
        [repeatBtn setImage:[UIImage imageNamed:@"btn_section-A.png"] forState:UIControlStateNormal];
    } else if (modeFlg == 1){
        [self setRepeatETime:nil];
        modeFlg = 2;
        [repeatBtn setImage:[UIImage imageNamed:@"btn_section-B.png"] forState:UIControlStateNormal];
    } else {
        repeatStime = 0.0f;
        repeatEtime = 0.0f;
        modeFlg =0;
        [repeatBtn setImage:[UIImage imageNamed:@"btn_section.png"] forState:UIControlStateNormal];
    }
    
}

-(IBAction) endRepeatMode:(id)sender{
    
}

-(IBAction) setRepeatSTime:(id)sender{
    NSLog(@"setRepeatSTime");
    repeatStime = secondsWithCMTimeOrZeroIfInvalid([player currentTime]);
    
}
-(IBAction) setRepeatETime:(id)sender{
    NSLog(@"setRepeatETime");
    repeatEtime = secondsWithCMTimeOrZeroIfInvalid([player currentTime]);
    [player pause];
    
    //마크값기준 진도호출을 위해 반드시 필요
    currentSecond = repeatEtime;
    markStudySec = 0;
    markNo = [self seekMarkNo:currentSecond];
    [player seekToTime:CMTimeMakeWithSeconds(repeatStime,NSEC_PER_SEC) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    [self avPlay];
    
}

-(void)msgScreen {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"消息" message:[NSString stringWithFormat:@"영상의 외부출력이 불가능합니다."]
                                                   delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
    [alert show];
}

- (void)saveMovieTime:(NSString *)movieTime {
    //NSLog(@"movieTime : %@", movieTime);
    @autoreleasepool {
        if ([[MLCenterForBusinessAppDelegate sharedAppDelegate] isNotInternet] || !isStreamingPlay) {
            //if ([[MLCenterForBusinessAppDelegate sharedAppDelegate] isNotInternet]) {
            return;
        }
        
        NSString *uid = [[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"userid"];
        NSError *errorReq = nil;
        NSString* url = [NSString stringWithFormat:@"type=32&course_cd=%@&take_course_seq=%d&chapter_no=%@&mark_sec=%@&study_sec=%d&device_nm=ios&reg_id=%@&frame_no=%d"
                         , self.courseCd, self.takecourseSeq, self.chapterNo, movieTime, totalProgressCount, uid, self.frameNo];
        NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:[MLCenterForBusinessAppDelegate sharedAppDelegate].urlBase]
                                              andQuery:url
                                              andError:&errorReq];
        NSLog(@"saveUrl : %@", url);
        if (jsonData)
        {
            NSError *errorJSON = nil;
            NSDictionary *result = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
            if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
                NSLog(@"success ok");
            }
        }
    
    }
}

//현재 문제점만 해결(60초->동영상마크정보마다 호출)
- (void)saveMovieTimeV2:(NSArray*)array{
    //NSLog(@"movieTime : %@", movieTime);
//    NSString *getMarkValue = [array objectAtIndex:0];
//    NSString *getStudySec = [array objectAtIndex:1];
    NSString *getStudySec = [array objectAtIndex:0];
    NSString *getMarkNo = [array objectAtIndex:1];
    NSString *getRealFrameNo = [array objectAtIndex:2];
    
    @autoreleasepool {
        if ([[MLCenterForBusinessAppDelegate sharedAppDelegate] isNotInternet] || !isStreamingPlay) {
            if ([[MLCenterForBusinessAppDelegate sharedAppDelegate] isNotInternet] && isStreamingPlay) {
                //스트리밍 학습 중 인터넷 끊기면 진도 문제 발생 않도록 플레이어창 닫기
                [player pause];
                [self cleanup];
                [self dismissViewControllerAnimated:YES completion:nil];

            }
            return;
        }
        
        NSString *uid = [[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"userid"];
        NSError *errorReq = nil;
        
//    NSString* url = [NSString stringWithFormat:@"type=32&course_cd=%@&take_course_seq=%d&chapter_no=%@&mark_sec=%@&study_sec=%d&device_nm=ios&reg_id=%@&frame_no=%d", self.courseCd, self.takecourseSeq, self.chapterNo, getMarkValue, [getStudySec intValue], uid, self.frameNo];
        //41->44번으로 업그레이드
        NSString* url = [NSString stringWithFormat:@"type=44&course_cd=%@&take_course_seq=%d&progress_no=%@&frame_no=%@&real_frame_no=%@&mark_no=%@&chapter_no=%@&study_sec=%d&device_nm=ios&reg_id=%@&evaluation_progress_ratio=%@&sequence_progress_type=%@", self.courseCd, self.takecourseSeq,
                         self.progressNo, [NSString stringWithFormat:@"%d", self.frameNo],getRealFrameNo, getMarkNo,
                         self.chapterNo, [getStudySec intValue], uid, self.evaluationProgressRatio, self.sequenceProgressType];
        NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:[MLCenterForBusinessAppDelegate sharedAppDelegate].urlBase]
                                              andQuery:url
                                              andError:&errorReq];
        NSLog(@"saveUrl : %@", url);
        if (jsonData)
        {
            NSError *errorJSON = nil;
            NSDictionary *result = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
            if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
                NSLog(@"success ok1");
                [Util updateLatestSyncDate:self.courseCd andTakeCourseSeq:[NSString stringWithFormat:@"%d", self.takecourseSeq] andChapterNo:self.chapterNo];
            }
        }

    }
}

//종료시 진도저장(로컬확인)
- (void)saveMovieTimeV2Stop:(NSArray*)array{
    //NSLog(@"movieTime : %@", movieTime);
    NSString *getMarkValue = [array objectAtIndex:0];
    NSString *getStudySec = [array objectAtIndex:1];
    NSString *currentSec = [array objectAtIndex:2];
    
    @autoreleasepool {

        NSError *errorReq2 = nil;
  
        if ([[MLCenterForBusinessAppDelegate sharedAppDelegate] isNotInternet]) {
            return;
        }
        
        //컨텐츠모듈화과정인경우
        if([_progressSaveYn isEqualToString:@"N"]){
            
          // NSLog(@"frameNo:%d", self.frameNo);
            
            NSString* jsonUrl2 = @"http://b2bapp.xiunaichina.com/Jlog/SavePlayer";
            //http://apps.hunet.co.kr/Hunet_Player/Android.aspx?action=ProgressUpdateByHtmlModule&courseCd=HLSC09514&takeCourseSeq=4624872&progressNo=4&frameNo=1&lastViewSec=12
            NSString* url2 = [NSString stringWithFormat:@"action=ProgressUpdateByHtmlModule&courseCd=%@&takeCourseSeq=%d&progressNo=%@&frameNo=%@&lastViewSec=%d"
                              , self.courseCd, self.takecourseSeq, self.progressNo, [NSString stringWithFormat:@"%d", self.frameNo],
                              //(int)floor(secondsWithCMTimeOrZeroIfInvalid([player currentTime]))];
                              [currentSec intValue]];
                              //, self.courseCd, self.takecourseSeq, @"1", 1, 1];

            NSData *jsonData2 = [Util getNSDataByRequestUrl:[NSURL URLWithString:jsonUrl2]
                                                  andQuery:url2
                                                  andError:&errorReq2];
            
                             
            if (jsonData2)
            {
                NSError *errorJSON2 = nil;
                NSDictionary *result = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData2 error:&errorJSON2];
                if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
                    NSLog(@"success ok2");
                    [Util updateLatestSyncDate:self.courseCd andTakeCourseSeq:[NSString stringWithFormat:@"%d", self.takecourseSeq] andChapterNo:self.chapterNo];
                }
            }
            
            return;
        }
        
        
        //로컬플레이면서, 인터넷이 연결되어있을때 싱크
        if (!isStreamingPlay) {
            [self studySync];
        }
        else{
            NSString *uid = [[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"userid"];
            NSError *errorReq = nil;
            
            NSString* url = [NSString stringWithFormat:@"type=32&course_cd=%@&take_course_seq=%d&chapter_no=%@&mark_sec=%@&study_sec=%d&device_nm=ios&reg_id=%@&frame_no=%d",
                             self.courseCd, self.takecourseSeq, self.chapterNo, getMarkValue, [getStudySec intValue], uid, self.frameNo];
            NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:[MLCenterForBusinessAppDelegate sharedAppDelegate].urlBase]
                                                  andQuery:url
                                                  andError:&errorReq];
            NSLog(@"saveUrl2 : %@", url);
            if (jsonData)
            {
                NSError *errorJSON = nil;
                NSDictionary *result = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
                if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
                    NSLog(@"success ok3");
                    [Util updateLatestSyncDate:self.courseCd andTakeCourseSeq:[NSString stringWithFormat:@"%d", self.takecourseSeq] andChapterNo:self.chapterNo];
                }
            }
        }
    
    }
}

//교안시작

-(void)studySecondSave:(NSTimer *)timer {
    if (playBool) {
        pureStudySec = pureStudySec + 1;
    }
    timerCounter = timerCounter + 1;
    NSLog(@"초(pureStudySec) : %d", pureStudySec);
}


-(void)setImageView:(int)getMarkNo{
    NSString *urlString = [[_movContents objectAtIndex:getMarkNo] objectForKey:@"PPTUrl"];
    NSString *escapedUrl = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:escapedUrl];
    NSData *data = [NSData dataWithContentsOfURL:url];
    [_pptImageView setImage:[UIImage imageWithData:data]];
}


-(void)SetBookNIndexControls{
    if (_movContents != nil && _movContents.count > 1) {
        NSString *urlString = [[_movContents objectAtIndex:0] objectForKey:@"PPTUrl"];
        if ( urlString != nil && [urlString length] > 0) {
            [_index setHidden:false];
            [_book setHidden:false];
            [_indexTable setBackgroundColor:[UIColor clearColor]];
            _indexTable.opaque = NO;
            _indexTable.backgroundView = nil;
            
        }
        else{
            [_index setHidden:true];
            [_book setHidden:true];
        }
    }
    else{
        [_index setHidden:true];
        [_book setHidden:true];
    }
    
}


- (IBAction)bookClick:(id)sender {
    bookControllerView.frame = CGRectMake(IS_4_INCH ? 90 : 40, 20, bookControllerView.frame.size.width, bookControllerView.frame.size.height);
    bookControllerView.alpha = 1.0f;
    [self.view addSubview:bookControllerView];
}

- (IBAction)indexClick:(id)sender {
    indexControllerView.frame = CGRectMake(IS_4_INCH ? 130 : 100, 50, indexControllerView.frame.size.width, indexControllerView.frame.size.height);
    indexControllerView.alpha = 1.0f;
    [self.view addSubview:indexControllerView];
}

- (IBAction)indexBookClose:(id)sender {
    //indexControllerView.alpha = 1.0f;
    [indexControllerView removeFromSuperview];
}

- (IBAction)bookBeforeBtn:(id)sender {
    if(pptIndex > 0){
        pptIndex -= 1;
        
        [self setImageView:pptIndex];
    }else{
        [ModalAlert toast:@"처음입니다."];
    }
}

- (IBAction)bookNextBtn:(id)sender {
    
    if(pptIndex < _movContents.count-1){
        pptIndex += 1;
        [self setImageView:pptIndex];
    }
    else{
        [ModalAlert toast:@"마지막입니다."];
    }
}

- (IBAction)currentBookBtn:(id)sender {
    //    pptIndex = markNo-2;
    //    [self setImageView:pptIndex];
    
    [self goViaMarkNo:pptIndex];
    
}

- (IBAction)bookCloseBtn:(id)sender {
    [bookControllerView   removeFromSuperview];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_movContents count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier = @"Cell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier]; if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
	// Configure the cell.
    cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, tableView.frame.size.width, cell.frame.size.height);
    
    UIFont *myFont = [ UIFont fontWithName: @"Arial" size: 15.0 ];
    cell.textLabel.font  = myFont;
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor clearColor];
    
    NSString *markNm = [[_movContents objectAtIndex:indexPath.row] objectForKey:@"MarkNm"];
    NSInteger mark = [[[_movContents objectAtIndex:indexPath.row] objectForKey:@"MarkValue"] intValue];
    //NSInteger markIndex = [[[_movContents objectAtIndex:indexPath.row] objectForKey:@"MarkNo"] intValue];
    
    NSUInteger durationInSeconds = mark;
    NSUInteger durationInMinutes = durationInSeconds / 60;
    NSUInteger durationInRemainder = durationInSeconds % 60;
    
    NSString *finalDurationString = [NSString stringWithFormat:@"[%@:%@] ",
                                     durationInMinutes<10 ? [NSString stringWithFormat:@"0%i", durationInMinutes] : [NSString stringWithFormat:@"%i", durationInMinutes],
                                     durationInRemainder<10 ? [NSString stringWithFormat:@"0%i", durationInRemainder] : [NSString stringWithFormat:@"%i", durationInRemainder]];
    cell.textLabel.text =  [NSString stringWithFormat:@"%@%@", finalDurationString, markNm];
    
    
    //    if (markNo-1 == markIndex) {
    //        UITableViewCell *cell = [_indexTable cellForRowAtIndexPath:indexPath];
    //        [cell setSelected:YES animated:YES];
    //        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    //    }
    
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self goViaMarkNo:indexPath.row];
}

-(void)goViaMarkNo:(NSInteger)getMarkNo{
    NSInteger gotoMarkValue = [[[_movContents objectAtIndex:getMarkNo] objectForKey:@"MarkValue"] intValue];
    
    //최초학습완료가 아닌경우, 가장많이 학습한곳이상으로 이동불가
    if (self.scrollType==0 && maxSec < gotoMarkValue) {
        gotoMarkValue = maxSec;
        [ModalAlert toast:@"최초 학습 완료 시 이동이 가능합니다."];
    }
    
    //마크값기준 진도호출을 위해 반드시 필요
    currentSecond = gotoMarkValue;
    markStudySec = 0;
    markNo = [self seekMarkNo:currentSecond];
	[player seekToTime:CMTimeMakeWithSeconds(gotoMarkValue,NSEC_PER_SEC) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
}


-(void)playerMessage:(NSString*)msg {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg
                                                   delegate:self cancelButtonTitle:NSLocalizedString(@"confirm", @"确认")  otherButtonTitles:nil, nil];
    [alert show];
}
//교안끝



//DB팀과 합의하여 안드로이드와 통합 - 진도저장방식 튜닝
- (void)saveMovieTimeNew:(NSArray*)array{
    //NSLog(@"movieTime : %@", movieTime);
    NSString *getStudySec = [array objectAtIndex:0];
    NSString *getMarkNo = [array objectAtIndex:1];
    NSString *getRealFrameNo = [array objectAtIndex:2];
    
    @autoreleasepool {
        if ([[MLCenterForBusinessAppDelegate sharedAppDelegate] isNotInternet] || !isStreamingPlay) {
            return;
        }
        
        NSString *hostUrl = @"http://b2bapp.xiunaichina.com/Jlog/SavePlayer";
        NSString *uid = [[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"userid"];
        NSError *errorReq = nil;
        NSString* url = [NSString stringWithFormat:@"action=NewProgressUpdate&user_id=%@&course_cd=%@&take_course_seq=%d&chapter_no=%@&progress_no=%@&frame_no=%@&mark_no=%@&study_sec=%@&mobile_study_sec=%@&device_nm=ios"
                         , uid, self.courseCd, self.takecourseSeq, self.chapterNo, self.progressNo, getRealFrameNo, getMarkNo, getStudySec, getStudySec];
        NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:hostUrl]
                                              andQuery:url
                                              andError:&errorReq];
        NSLog(@"saveUrl : %@", url);
        if (jsonData)
        {
            NSError *errorJSON = nil;
            NSDictionary *result = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
            if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
                NSLog(@"success ok4");
            }
        }
    
    }
}


//상상마루
- (void)saveSangMovieTime:(NSArray*)array{
    //NSLog(@"movieTime : %@", movieTime);
    NSString *movieTime = [array objectAtIndex:0];
    NSString *mode = [array objectAtIndex:1];
        
    @autoreleasepool {
          MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    if ([[MLCenterForBusinessAppDelegate sharedAppDelegate] isNotInternet]) {
             return;
    }
   
    NSError *errorReq = nil;
    
    //NSString* jsonUrl = @"http://apps.hunet.co.kr/Hunet_Player/Android.aspx";
    //NSString* jsonUrl = @"http://172.20.80.100:5555/Jlog/SavePlayer";
    NSString* jsonUrl = @"http://b2bapp.xiunaichina.com/Jlog/SavePlayer";
    NSString* url = [NSString stringWithFormat:@"action=SangSangProgressUpdateV2&userId=%@&goodsId=%@&contentsSeq=%d&viewSec=%d&viewSecMobile=%d&lastViewSec=%d&viewNo=%d&mode=%@&contractNo=%d",
                          [app.profileUser objectForKey:@"userid"],
                          self.gid,
                          self.cseq,
                          self.viewSec + _currentViewSec,//[viewSec intValue],
                          self.viewSecMobile + _currentViewSec,//[viewSecMobile intValue],
                          //(int)floor(progresSlider.value),//[lastViewSec intValue],
                          [movieTime intValue],
                          self.viewNo,
                          mode,
                          self.cno
                          ];
        
        
        
    NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:jsonUrl]
                                               andQuery:url
                                               andError:&errorReq];
    
    NSLog(@"saveUrl : %@", url);
    if (jsonData)
    {
             NSError *errorJSON = nil;
             NSDictionary *result = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
             if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
                 NSLog(@"success ok5");
                 [Util updateLatestSyncDateSang:[[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"userid"] andContractNo:self.cno andContentsSeq:self.cseq andviewNo:self.viewNo];
             }
    }
    
    }
}

- (void)saveCoredataMovieTime:(NSString *)movieTime {
    if (!isStreamingPlay) { //다운로드 진도 저장
        [Util updateTotalSec:self.courseCd andTakeCourseSeq:[NSString stringWithFormat:@"%d", self.takecourseSeq] andChapterNo:self.chapterNo andTotalSec:totalProgressCount];
        
        [Util updateMaxSec:self.courseCd andTakeCourseSeq:[NSString stringWithFormat:@"%d", self.takecourseSeq] andChapterNo:self.chapterNo andMaxSec:movieTime];
    }
}
//void)updateTotalSec:(NSString*)cd andTakeCourseSeq:(NSString*)seq andChapterNo:(NSString*)no andTotalSec:(int)sec
- (void)saveCoredataMovieTimeV2:(int)getStudySec andMarkValue:(int)getMarkValue {
    if (!isStreamingPlay) { //다운로드 진도 저장
        //학습초 업데이트, 로컬 study테이블에서는 total_sec ->study_sec
        [Util updateTotalSec:self.courseCd andTakeCourseSeq:[NSString stringWithFormat:@"%d", self.takecourseSeq] andChapterNo:self.chapterNo andTotalSec:getStudySec];
        
        //mark값 업데이트, 로컬 study테이블에서는 max_sec, -> mark_value
        [Util updateMaxSec:self.courseCd andTakeCourseSeq:[NSString stringWithFormat:@"%d", self.takecourseSeq] andChapterNo:self.chapterNo andMaxSec:[NSString stringWithFormat:@"%d", getMarkValue]];
    }
}


- (int)getLastViewSecFromCoreData {
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate] managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"StudyHistory" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entitydesc];
    
    NSString *userId = [[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"userid"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user_id == %@ and course_cd == %@ and take_course_seq == %@ and chapter_no == %@ and study_end_date == max(study_end_date)", userId, self.courseCd, [NSString stringWithFormat:@"%d", self.takecourseSeq], self.chapterNo];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if (matchingData.count > 0) {
        return [[[matchingData objectAtIndex:0] valueForKey:@"last_view_sec"] integerValue];
    }
    else{
        return -1;
    }
    
    
}

- (void)saveCoredataStudyHistory:(NSDate *)study_start_date
                 andStudyEndDate:(NSDate *)study_end_date
                  andLastViewSec:(int)last_view_sec
                     andStudySec:(int)study_sec
              andStudyMethodType:(int)study_method_type
{
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate] managedObjectContext];
    
    NSManagedObject *newStudyHistory = [NSEntityDescription insertNewObjectForEntityForName:@"StudyHistory" inManagedObjectContext:context];

    [newStudyHistory setValue:[[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"userid"] forKey:@"user_id"];
    [newStudyHistory setValue:self.courseCd forKey:@"course_cd"];
    [newStudyHistory setValue:[NSNumber numberWithInt:self.takecourseSeq] forKey:@"take_course_seq"];
    [newStudyHistory setValue:self.chapterNo forKey:@"chapter_no"];
    [newStudyHistory setValue:study_start_date forKey:@"study_start_date"];
    [newStudyHistory setValue:study_end_date forKey:@"study_end_date"];
    [newStudyHistory setValue:[NSNumber numberWithInt:last_view_sec] forKey:@"last_view_sec"];
    [newStudyHistory setValue:[NSNumber numberWithInt:study_sec] forKey:@"study_sec"];
    [newStudyHistory setValue:[NSNumber numberWithInt:study_method_type] forKey:@"study_method_type"];
    
    [newStudyHistory setValue:[NSNumber numberWithInt:0] forKey:@"sync_type"];
    
    NSError *error = nil;
    [context save:&error];
    
}


- (void)saveCoredataMovieTimeSang:(NSString *)movieTime {
    if (!isStreamingPlay) { //다운로드 진도 저장
        
        //[Util updateTotalSecSang:self.userId andContractNo:self.cno andContentsSeq:self.cseq andGoodsId:self.gid andviewNo:self.viewNo andTotalSec:_currentViewSec andLastViewSec:(int)floor(progresSlider.value)];
        
        //core에 먼저 저장
        [Util updateTotalSecSangV2:[[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"userid"] andContractNo:self.cno andContentsSeq:self.cseq andGoodsId:self.gid andviewNo:self.viewNo andViewSec:(self.viewSec + _currentViewSec) andViewSecMobile:(self.viewSecMobile + _currentViewSec) andLastViewSec:[movieTime intValue]];

    }
}

- (IBAction)adjustVolume:(id)sender {
    [self updateVolumeProgress];
}

- (IBAction)volumeSpeakerTouch:(id)sender {
    if (isSpeedPushed) {
        isSpeedPushed = false;
        [self SetInitSpeedControls];
    }
    
    if (!isVolumePushed) {
        isVolumePushed = true;
    }
    else{
        isVolumePushed = false;
    }
    [self SetInitVolumeControls];
}


- (IBAction)btnSpeedTouch:(id)sender {
    if (isVolumePushed) {
        isVolumePushed = false;
        [self SetInitVolumeControls];
    }
    
    if (!isSpeedPushed) {
        isSpeedPushed = true;
    }
    else{
        isSpeedPushed = false;
    }
    [self SetInitSpeedControls];
}



-(IBAction) speedDown:(id)sender{
	NSLog(@"speedDown");
    if (player.rate > 0.7) {
        realSpeed = player.rate - 0.1;
    	NSString *speed= [[NSString stringWithFormat:@"%@%f",NSLocalizedString(@"playSpeed", @"倍数"), realSpeed] substringToIndex:5];
        [_btnSpeed setTitle:speed forState:UIControlStateNormal];
        
        player.rate = realSpeed;
    }
}
-(IBAction) speedUp:(id)sender{
	NSLog(@"speedUp");
    
	if (player.rate > 0.1 && player.rate < 2.0) {
        realSpeed = player.rate + 0.1;
        NSString *speed= [[NSString stringWithFormat:@"%@%f",NSLocalizedString(@"playSpeed", @"倍数"), realSpeed] substringToIndex:5];
        
        [_btnSpeed setTitle:speed forState:UIControlStateNormal];
        
        player.rate = realSpeed;
    }
}

-(void)SetInitSpeedControls{
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    if ([app.companySeq intValue] == 17970) {
        _btnSpeed.hidden = YES;
        _imgSpeedbg.hidden = YES;
        _btnSpeedUp.hidden = YES;
        _btnSpeedDown.hidden = YES;
        return;
    }
    
    //상상마루 컨텐츠이면 DB에서 가져온 배속버튼 표시여부에 따라 숨김여부 결정.
    if (_cseq > 0 && ![[app.profileUser objectForKey:@"EnableSSPlayerSpeedBar"] boolValue]) {
        _btnSpeed.hidden = YES;
        _imgSpeedbg.hidden = YES;
        _btnSpeedUp.hidden = YES;
        _btnSpeedDown.hidden = YES;
        return;
    }
    
    if (isSpeedPushed) {
        [_imgSpeedbg setHidden:false];
        [_btnSpeedUp setHidden:false];
        [_btnSpeedDown setHidden:false];
    }
    else{
        [_imgSpeedbg setHidden:true];
        [_btnSpeedUp setHidden:true];
        [_btnSpeedDown setHidden:true];
    }
}

-(void)SetInitVolumeControls{
    if (isVolumePushed) {
        [_lblVolume setHidden:false];
        [_volumeSlider setHidden:false];
        [_imgVolumeBg setHidden:false];
    }
    else{
        [_lblVolume setHidden:true];
        [_volumeSlider setHidden:true];
        [_imgVolumeBg setHidden:true];
    }
}

//clock
-(void)updateClock{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"a HH:mm"];
    _lblClock.text = [dateFormat stringFromDate:[NSDate date]];
    
    [self performSelector:@selector(updateClock) withObject:self afterDelay:1.0];
}

@end
