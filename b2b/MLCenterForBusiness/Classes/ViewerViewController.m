//
//  ViewerViewController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 6. 14..
//  Copyright 2011 hunet. All rights reserved.
//

#import "ViewerViewController.h"
#import "ModalAlert.h"
#import "MLCenterForBusinessAppDelegate.h"

@implementation ViewerViewController
@synthesize url;

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    if ([viewWeb.subviews count] > 0) {
        UIView *scrollView = viewWeb.subviews[0];
        
        for (UIView *childView in scrollView.subviews) {
            if ([childView isKindOfClass:[UIWebDocumentView class]]) {
                UIWebDocumentView *documentView = (UIWebDocumentView *)childView;
                WebScriptObject *wso = documentView.webView.windowScriptObject;
                
                [wso setValue:[WebScriptBridge getWebScriptBridge] forKey:@"ScriptBridge"];
            }
        }
    }
	[indicator stopAnimating];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
	[indicator startAnimating];
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
	[indicator stopAnimating];	
	[ModalAlert notify:@"네트워크 연결에 문제가 생겼습니다."];	
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	return YES;
}


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        // Custom initialization.
    }
    return self;
}
*/


- (void)viewDidLoad {
	[viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
    [super viewDidLoad];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    
	viewWeb = nil;
	indicator = nil;
	self.url = nil;
    
    [super viewDidUnload];
}


- (void)dealloc {
	viewWeb.delegate = nil;
}


@end
