//
//  BarDecorator.m
//  MLCenterForBusiness
//
//  Created by Park SeungKyun on 2015. 7. 20..
//
//

#import "BarDecorator.h"
#import "BaseViewController.h"
#import "MyViewController.h"
#import "LectureViewController.h"
#import "MoreWebViewController.h"
#import "DownloadViewController.h"
#import "Util.h"
#import "MoreViewController.h"
#import "HomeViewController.h"
#import "SelfStudyViewController.h"
#import "StudyStateViewController.h"
#import "Header.h"
#import "GraphicsUtile.h"

@implementation BarDecorator

+ (void)setting:(MLCenterForBusinessAppDelegate *) app
{
    // 색상 설정
    UIColor *selectedIconColor = [MLCenterForBusinessAppDelegate colorFromHexString:@"#5b91f5"];    // 선택아이콘
    UIColor *unselectedIconColor = [MLCenterForBusinessAppDelegate colorFromHexString:@"#78869d"];  // 디폴트아이콘
    UIColor *tabbarColor = [MLCenterForBusinessAppDelegate colorFromHexString:@"#d8dce4"];          // 디폴트배경(탭바)
    UIColor *navibarColor = [MLCenterForBusinessAppDelegate colorFromHexString:@"#5b91f5"];         // 디폴트배경(네비게이션바)
    
    [app.window.rootViewController removeFromParentViewController];
    if (app.tabBarController != nil) {
        app.tabBarController = nil;
    }
    app.tabBarController = [[UITabBarController alloc] init];
    app.tabBarController.delegate = app;
    app.window.rootViewController = app.tabBarController;
    
    ////////////////////////////////////////////////
    // tabBar 버튼 갯수와 종류 셋팅
    
    HomeViewController *homeViewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    MyViewController *myViewController = [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil];
    UINavigationController *downloadNavigationController = [Util getNavigationController:[DownloadViewController class] initWithStyle:UITableViewStylePlain];
    MoreViewController *moreViewController = [[MoreViewController alloc] initWithNibName:@"MoreViewController" bundle:nil];
    UINavigationController *moreNavigationController = [[UINavigationController alloc] initWithRootViewController:moreViewController];
    
    // 메뉴 제목 설정
    [downloadNavigationController.tabBarItem setTitle:@"다운로드"];
    
    app.tabBarController.viewControllers = @[
                                              homeViewController,
                                              myViewController,
                                              downloadNavigationController,
                                              moreNavigationController
                                            ];
    //
    ////////////////////////////////////////////////
    
    [GraphicsUtile setTabBarColor:app.tabBarController.tabBar barColor:tabbarColor selectedIconColor:selectedIconColor unselectedIconColor:unselectedIconColor];
    [GraphicsUtile setNavigationBarColor:navibarColor];
    
    [app performSelector:@selector(pushServiceURL) withObject:nil afterDelay:0.5];
}

@end
