//
//  SangPlayerViewController.h
//  MLCenterForBusiness
//
//  Created by Park Gi-Bok on 11. 11. 9..
//  Copyright (c) 2011 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@class MLCenterForBusinessAppDelegate;
@class OverlayView;

@interface SangPlayerViewController : UIViewController {
    
    NSString *cno;
	NSString *cseq;
	NSString *gid;
    NSString *seGoodsId;
	
	NSDictionary *playerInfo;
	UIActivityIndicatorView* indicator;
	OverlayView *overlayView;
	NSTimer *currentTimer;
	NSInteger sumTimer;
	
	MLCenterForBusinessAppDelegate *appDelegate;
    
}

@property (nonatomic, strong) NSString *cno;
@property (nonatomic, strong) NSString *cseq;
@property (nonatomic, strong) NSString *gid;
@property (nonatomic, strong) NSString *seGoodsId;

@property (nonatomic, strong) NSDictionary *playerInfo;
@property (nonatomic, strong) NSTimer *currentTimer;

- (void)updateForPlayerInfo;
- (void)progressSave:(NSString*)second;
- (int)currentSliderPosition;

@end
