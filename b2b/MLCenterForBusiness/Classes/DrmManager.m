//
//  DrmManager.m
//  MLCenterForBusiness
//
//  Created by Park SeungKyun on 2015. 9. 11..
//
//

#import "DrmManager.h"
#import <HDRM/HDRM.h>

@implementation DrmManager

// 암호화된 Bytes를 반환합니다.
+ (NSData*)encrypt:(NSData*)header totalBytes:(UInt64)totalBytes {
    return [HdrmCommon encrypt:header totalBytes:totalBytes]; //HDRM
//    return [EncryptUtility encrypt:header]; //STWDRM
}

// HTTP서버를 시작합니다.
// 로컬파일경로를 인자로 받아 http주소를 반환합니다.
- (NSURL*)startHttpServer:(NSString*)fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *wwwroot = [NSString stringWithFormat:@"%@/%@", documentsDirectory, kDrmFolderName];

    NSString *urlString;

    self.hdrmServer = [[HdrmServer alloc] init];
    [self.hdrmServer start:wwwroot];
    urlString = [NSString stringWithFormat:@"http://127.0.0.1:%d/%@", [self.hdrmServer.httpServer listeningPort], fileName];


    NSLog(@"DrmManager url : %@", urlString);
    
    return [NSURL URLWithString:urlString];
}

// HTTP서버를 종료합니다.
- (void)stopHttpServer {
    //HDRM
    if (self.hdrmServer) {
        [self.hdrmServer stop];
    }

}

@end
