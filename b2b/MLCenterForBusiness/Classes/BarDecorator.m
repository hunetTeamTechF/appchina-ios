//
//  BarDecorator.m
//  MLCenterForBusiness
//
//  Created by Park SeungKyun on 2015. 7. 20..
//
//

#import "BarDecorator.h"
#import "BaseViewController.h"
#import "MyViewController.h"
#import "LectureViewController.h"
#import "MoreWebViewController.h"
#import "DownloadViewController.h"
#import "Util.h"
#import "MoreViewController.h"
#import "HomeViewController.h"
#import "SelfStudyViewController.h"
#import "StudyStateViewController.h"
//#import "AllianzLectureViewController.h"
#import "Header.h"
#import "GraphicsUtile.h"

@implementation BarDecorator

/*** 메뉴타이틀바 & 탭바색상 커스텀 예제 : /Classes/Custom/kangwonland_3856/BarDecorator.m ***/
+ (void)setting:(MLCenterForBusinessAppDelegate *) app
{
#ifdef spc_8907
    NSString *prefixString = [app.profileUser valueForKey:@"useridPrefix"];
    if ([[prefixString lowercaseString] isEqualToString:@"pc_"]) {
        [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#2C1D22" selectedTintColorRGBHex:@"#ffffff" unSelectedTintColorRGBHex:@"#AAAAAA" naviBarColorRGBHex:@"#2C1D22"];
    }
    else if ([[prefixString lowercaseString] isEqualToString:@"sl"]) {
        [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#DD4713" selectedTintColorRGBHex:@"#ffffff" unSelectedTintColorRGBHex:@"#BBBBBB" naviBarColorRGBHex:@"#DD4713"];
    }
    else if ([[prefixString lowercaseString] isEqualToString:@"brk"]) {
        [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#114C93" selectedTintColorRGBHex:@"#ffffff" unSelectedTintColorRGBHex:@"#AAAAAA" naviBarColorRGBHex:@"#114C93"];
    }
    else if ([[prefixString lowercaseString] isEqualToString:@"spc"]) {
        [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#1A9ECC" selectedTintColorRGBHex:@"#ffffff" unSelectedTintColorRGBHex:@"#AAAAAA" naviBarColorRGBHex:@"#1A9ECC"];
    }
    else {
        [app settingtabBarControllerDefault:NO isImagine:NO useDefaultNaviStyle:YES];
    }
    return;
#elif edaekyo_76 || dkl_18508
    [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#BBBBBB" selectedTintColorRGBHex:@"#536C74" unSelectedTintColorRGBHex:@"#FFFFFF" naviBarColorRGBHex:@"#536C74"];
    return;
#elif smrateugenes_18401
    [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:YES tabBarColorRGBHex:@"#F1F1F1" selectedTintColorRGBHex:@"#1B52CE" unSelectedTintColorRGBHex:@"#BBBBBB" naviBarColorRGBHex:@"#1B52CE"];
    return;
#elif avene
    [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#4A4B4D" selectedTintColorRGBHex:@"#ffffff" unSelectedTintColorRGBHex:@"#7F7F80" naviBarColorRGBHex:@"#EF8970"];
    
    
    
    return;
#endif
    
    
    [app.window.rootViewController removeFromParentViewController];
    if (app.tabBarController != nil) {
        app.tabBarController = nil;
    }
    app.tabBarController = [[UITabBarController alloc] init];
    app.tabBarController.delegate = app;
    app.window.rootViewController = app.tabBarController;
    
    UINavigationController *navDownload = [Util getNavigationController:[DownloadViewController class] initWithStyle:UITableViewStylePlain];
    UINavigationController *navMore = [Util getNavigationController:[MoreViewController class] withNibName:@"MoreViewController"];
    
    ////////////////////////////////////////////////
    // tabBar 버튼 갯수와 종류 셋팅
    // 2015-01-29, magic7k Refactoring
#ifdef pcalife_6769
    app.tabBarController.viewControllers = @[
                                             [[HomeViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                             [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                             navDownload,
                                             navMore
                                             ];
#elif dunkinschool_16060 || baskinschool_16060
    app.tabBarController.viewControllers = @[
                                             [[HomeViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                             [[LectureViewController alloc] initWithNibName:@"LectureViewController" bundle:nil],
                                             [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                             navDownload,
                                             navMore
                                             ];
#elif allianzlife_491
    app.tabBarController.viewControllers = @[
                                             [[HomeViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                             [[LectureViewController alloc] initWithNibName:@"LectureViewController" bundle:nil],
                                             [[AllianzLectureViewController alloc] initWithNibName:@"LectureViewController" bundle:nil],
                                             [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                             navMore
                                             ];
#elif myangelfc_16448
    app.tabBarController.viewControllers = @[
                                             [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                             navMore
                                             ];
#elif khnp_4413
    app.tabBarController.viewControllers = @[
                                             [[HomeViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                             [[LectureViewController alloc] initWithNibName:@"LectureViewController" bundle:nil],
                                             [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                             navDownload,
                                             navMore
                                             ];
    
#elif sangsangstart_16477
    app.tabBarController.viewControllers = @[
                                             [[HomeViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                             [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                             navMore
                                             ];
#elif hitejinro_10148
    app.tabBarController.viewControllers = @[
                                             [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                             [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                             navDownload,
                                             navMore,
                                             ];
#elif bossi_16794
    app.tabBarController.viewControllers = @[
                                             [[SelfStudyViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                             navMore
                                             ];
#elif nhlife_14384
    
    UINavigationController *navMoreWeb = [Util getNavigationController:[MoreWebViewController class] withNibName:@"MoreWebViewController"];
    //[app setNavagitionBarStyle: navMoreWeb.navigationBar];
    UINavigationController *navHome = [Util getNavigationController:[HomeViewController class] withNibName:@"HomeViewController"];
    //[app setNavagitionBarStyle: navHome.navigationBar];
    
    app.tabBarController.viewControllers = @[
                                             navHome, //[[[HomeViewController alloc] initWithNibName:@"Base" bundle:nil] autorelease],
                                             [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                             [[SelfStudyViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                             navMoreWeb
                                             ];
#elif sknservice_12658
    
    app.tabBarController.viewControllers = @[
                                             [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                             [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                             [[SelfStudyViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                             navMore
                                             ];
#elif seoulmilk_6928
    
    app.tabBarController.viewControllers = @[
                                             [[SelfStudyViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                             [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                             navDownload,
                                             navMore
                                             ];
#elif gg_9242 || nps_4258 || sisul_family_18009 || nepes_4264 || ex_3614 || venture_15715 || keco_11868 || brkorea_2176 || taekwang_18533 || sampyo_5862 || ssyenc_13636
    
    app.tabBarController.viewControllers = @[
                                             [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                             [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                             navDownload,
                                             navMore
                                             ];
#elif snuh_3838
    LectureViewController *lecture = [[LectureViewController alloc] initWithNibName:@"LectureViewController" bundle:nil];
    [lecture setTitle:@"지식LIVE"];
    lecture.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_free_study"];
    [[navMore.viewControllers firstObject] setTitle:@"더보기"];
    navMore.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_more"];
    app.tabBarController.viewControllers = @[
                                             [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                             [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                             lecture,
                                             navMore
                                             ];
#else //기본연수원 조건 분기
    
    if ([app.eduType isEqualToString:@"OnlyImagine"])
    {
        app.tabBarController.viewControllers = @[
                                                 [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                                 [[SelfStudyViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                                 [[StudyStateViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                                 navDownload,
                                                 navMore
                                                 ];
    }
    else if ([app.eduType isEqualToString:@"LearningAndImagine"])
    {
        app.tabBarController.viewControllers = @[
                                                 [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                                 [[LectureViewController alloc] initWithNibName:@"LectureViewController" bundle:nil],
                                                 [[SelfStudyViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                                 [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                                 navMore
                                                 ];
    }
    else if ([app.eduType isEqualToString:@"OnlySangsang"])
    {
        app.tabBarController.viewControllers = @[
                                                 [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                                 [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                                 navDownload,
                                                 navMore
                                                 ];
    }
    else
    {
        app.tabBarController.viewControllers = @[
                                                 [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                                 [[LectureViewController alloc] initWithNibName:@"LectureViewController" bundle:nil],
                                                 [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                                 navDownload,
                                                 navMore
                                                 ];
    }
#endif
    //
    ////////////////////////////////////////////////
    
    
    /* 탭바 배경 설정 */
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
#if hyundai_16597 || hyundai_16596
        app.tabBarController.tabBar.tintColor = [UIColor colorWithRed:97.0/255.0 green:169.0/255.0 blue:132.0/255.0 alpha:1.0];
#elif avene
        app.tabBarController.tabBar.tintColor = [UIColor colorWithRed:43.0/255.0 green:103.0/255.0 blue:117.0/255.0 alpha:1.0];
        
#elif seoulmilk_6928
        app.tabBarController.tabBar.tintColor = [UIColor colorWithRed:30.0/255.0 green:104.0/255.0 blue:69.0/255.0 alpha:1.0];
        app.tabBarController.tabBar.barTintColor = [UIColor whiteColor];
        app.tabBarController.tabBar.translucent = false;
#endif
    }
    //    [GraphicsUtile setTabBarColor:barColor selectedIconColor:selectedIconColor unselectedIconColor:unselectedIconColor];
    
    // 디폴트배경(탭바 & 네비게이션바)
    UIColor *barColor = [UIColor colorWithRed:48.0/255.0 green:70.0/255.0 blue:143.0/255.0 alpha:1.0];
    /* 네비게이션바 배경 설정 */
#ifdef nepes_4264
    barColor = [UIColor colorWithRed:248.0/255.0 green:167.0/255.0 blue:26.0/255.0 alpha:1.0];
#elif nps_4258
    barColor = [UIColor colorWithRed:43.0/255.0 green:103.0/255.0 blue:117.0/255.0 alpha:1.0];
#endif
    [GraphicsUtile setNavigationBarColor:barColor];
    
    [app performSelector:@selector(pushServiceURL) withObject:nil afterDelay:0.5];
}

@end
