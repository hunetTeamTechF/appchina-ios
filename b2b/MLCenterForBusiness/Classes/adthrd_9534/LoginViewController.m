
#import "LoginViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "MoreViewController.h"
#import "DownloadViewController.h"
#import "ModalAlert.h"
#import "Header.h"
#import "DownPicker.h"
#import "LectureViewController.h"

@implementation LoginViewController
@synthesize viewImageCover;

- (void)viewDidAppear:(BOOL)animated {
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    [self.buttonSaveUserid setSelected:[[app.profileUser valueForKey:@"saveid"] boolValue]];
    [self.buttonAutoLogin setSelected:[[app.profileUser valueForKey:@"autologin"] boolValue]];
    
    if (userIdPrefixVisiblity) {
        [self.textID setText:userIdPrefixString];
    }
    
    if (self.buttonSaveUserid.isSelected) {
        NSString *userId = [app.profileUser objectForKey:@"userid"];
        if ([userId length] > 0) {
            if (!userIdPrefixVisiblity) {
                userId = [userId stringByReplacingOccurrencesOfString:userIdPrefixString withString:@""];
            }
            [self.textID setText:userId];
        }
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    userIdPrefixString = @"ADT";    // 공통아이디
    userIdPrefixVisiblity = YES;    // 공통아이디 노출여부
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [paths objectAtIndex:0];
    NSString *drmFolder = [NSString stringWithFormat:@"%@/%@", documentFolder, @"hunet_docroot"];
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSDirectoryEnumerator *fileEnumerator = [fileManager enumeratorAtPath:drmFolder];
    
    int filecount = 0;
    for (NSString *filename in fileEnumerator) {
        if ([filename isEqualToString:@".DS_Store"]) {
            continue;
        }
        filecount = 1;
        break;
    }

    self.offlineButton.hidden = filecount == 0;
    
    CGPoint centerPoint = self.viewControllBox.center;
    pointViewControllBox = self.viewControllBox.center = CGPointMake(centerPoint.x, centerPoint.y + (IS_Height_More_Then_568 ? 0 : -30));
    
    self.textID.attributedPlaceholder = [self setAttributedPlaceholderWithString:@"아이디"];
    self.textPassword.attributedPlaceholder = [self setAttributedPlaceholderWithString:@"비밀번호"];
}

- (NSMutableAttributedString *)setAttributedPlaceholderWithString:(NSString *)str {
    UIColor *color = [MLCenterForBusinessAppDelegate colorFromHexString:@"#FFFFFF"];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:str];
    [attrStr addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, str.length)];
    [attrStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:15] range:NSMakeRange(0, str.length)];
    
    return attrStr;
}

- (IBAction) onLogin {
    self.buttonLogin.userInteractionEnabled = NO;
    
    self.textID.text = [self.textID.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.textPassword.text = [self.textPassword.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if ([self.textID isFirstResponder]) {
        [self.textID resignFirstResponder];
    }
    
    if ([self.textPassword isFirstResponder]) {
        [self.textPassword resignFirstResponder];
    }
    
    if ([self.textUnit isFirstResponder]) {
        [self.textUnit resignFirstResponder];
    }
    
    if (self.textID.text.length == 0) {
        [ModalAlert notify:@"아이디를 입력해 주세요"];
        self.buttonLogin.userInteractionEnabled = YES;
        [self toggleControllBoxPosition:NO];
        [self.textID becomeFirstResponder];
        return;
    }

    if (self.textID.text.length == 0 || self.textPassword.text.length == 0) {
        [ModalAlert notify:@"패스워드를 입력해 주세요"];
        self.buttonLogin.userInteractionEnabled = YES;
        [self toggleControllBoxPosition:NO];
        [self.textPassword becomeFirstResponder];
        return;
    }
    [self toggleControllBoxPosition:NO];
    mask = [[UIView alloc] initWithFrame:[[self.view window] frame]];
    mask.userInteractionEnabled = NO;
    mask.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.50];
    [self.view addSubview:mask];
    
    [indicator startAnimating];
    [NSThread detachNewThreadSelector:@selector(threadLogin:) toTarget:self withObject:nil];
}

- (void)onWebLogin:(NSString *)userId :(NSString *)userPwd{
    BOOL result = YES;
    @autoreleasepool {
        
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        result = [app loginForID:userId withPassword:userPwd];
        [self performSelectorOnMainThread:@selector(callbackAfterLogin:) withObject:[NSNumber numberWithBool:result] waitUntilDone:NO];
    }
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    if ([self.textID isFirstResponder]) {
        [self.textPassword becomeFirstResponder];
    }
    else {
        [self toggleControllBoxPosition:NO];
        [self onLogin];
    }
    
    return YES;
}

- (void) toggleControllBoxPosition:(BOOL)textInput {
    [UIView beginAnimations:nil context:NULL];
    //[UIView setAnimationDuration:0.2f];
    [UIView setAnimationDelegate:self];
    
    // 로그인 박스 위치이동
    self.viewControllBox.center = CGPointMake(pointViewControllBox.x, textInput ? pointViewControllBox.y - (IS_Height_More_Then_568 ? 0 : 50) : pointViewControllBox.y);
    
    [UIView commitAnimations];
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    
    [self toggleControllBoxPosition:YES];
    
    return YES;
}

- (void) threadLogin:(id)somedata {
    BOOL result = YES;
    @autoreleasepool {
        //NSString idPrefix = self.downPicker.selected
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        NSString *userId = self.textID.text;
        if ([userIdPrefixString length] > 0 && ![userId hasPrefix:userIdPrefixString]) {
            userId = [NSString stringWithFormat:@"%@%@", userIdPrefixString, userId];
        }
        result = [app loginForID:userId withPassword:self.textPassword.text];
        [self performSelectorOnMainThread:@selector(callbackAfterLogin:) withObject:[NSNumber numberWithBool:result] waitUntilDone:NO];
    }
}

// callback for the login result
- (void) callbackAfterLogin:(NSNumber*)resultLogin {
    
    
    BOOL result = [resultLogin boolValue];
    
    [mask removeFromSuperview];
    [indicator stopAnimating];
    
    self.buttonLogin.userInteractionEnabled = YES;
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    if (result) {
        [app profileUserInfoSave];
        [app openTrainingInstitute];
    }
    else {
        if ([app isNotInternet]) {
            [ModalAlert notify:@"인터넷에 연결되어 있지 않습니다.\n인터넷 연결을 확인해 주세요."];
        }
        else {
            [ModalAlert notify:@"로그인에 실패했습니다.\n아이디와 패스워드를 다시 확인해 주세요."];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backgroundTab:(id)sender{
    [_textID resignFirstResponder];
    [_textPassword resignFirstResponder];
    [self toggleControllBoxPosition:NO];
}

- (void)viewDidUnload {
    [self setOfflineButton:nil];
    [self setViewImageBackground:nil];
    [self setViewImageLoginBox:nil];
    [self setTextID:nil];
    [self setTextPassword:nil];
    [self setButtonLogin:nil];
    [self setButtonSaveUserid:nil];
    [self setButtonAutoLogin:nil];
    
    indicator = nil;
    viewImageCover = nil;
    
    [super viewDidUnload];
}


- (BOOL) shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)offlineClick:(id)sender {
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] offlineStudy];
}

- (IBAction)clickedSaveUserId:(id)sender {
    [self.buttonSaveUserid setSelected:!self.buttonSaveUserid.isSelected];
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    [app setNoticeOption:self.buttonSaveUserid.isSelected withKey:@"saveid"];
}

- (IBAction)clickedAutoLogin:(id)sender {
    [self.buttonAutoLogin setSelected:!self.buttonAutoLogin.isSelected];
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    [app setNoticeOption:self.buttonAutoLogin.isSelected withKey:@"autologin"];
}

@end
