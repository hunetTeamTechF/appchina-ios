#import <Foundation/Foundation.h>


@interface Util : NSObject

+ (NSData *) getNSDataByRequestUrl:(NSURL *)url andQuery:(NSString *)query andError:(NSError **)error;
+ (NSArray*)freeDiskspace;
+ (NSString*)prettyBytes:(uint64_t)numBytes;
+ (NSInteger)expirationDay:(NSString*)date;

+ (NSArray *)getCourse;
//상상마루
+ (NSArray *)getSangsang;
+ (NSArray *)getSangsang:(NSString*)goodsId ;
+ (NSArray *)getSangStudy:(NSString*)goodsId andContentsSeq:(NSString*)seq andUserId:(NSString*)userId andViewNo:(NSString*)viewNo;
+ (NSArray *)getSangStudy:(NSString*)goodsId andUserId:(NSString*)userId;
+ (void) deleteSangSang:(NSString*)goodsId;


+ (NSArray *)getCourse:(NSString*)cd andTakeCourseSeq:(NSString*)seq;
+ (NSArray *)getStudy:(NSString*)cd andTakeCourseSeq:(NSString*)seq;
+ (NSArray *)getStudy:(NSString*)cd andTakeCourseSeq:(NSString*)seq andChapterNo:(NSString*)no;
+ (void)updateMaxSec:(NSString*)cd andTakeCourseSeq:(NSString*)seq andChapterNo:(NSString*)no andMaxSec:(NSString*)sec;
+ (void)updateTotalSec:(NSString*)cd andTakeCourseSeq:(NSString*)seq andChapterNo:(NSString*)no andTotalSec:(int)sec;
+ (void)updateLatestSyncDate:(NSString*)cd andTakeCourseSeq:(NSString*)seq andChapterNo:(NSString*)no;

+ (void) deleteCourse:(NSString*)cd andTakeCourseSeq:(NSString*)seq;
+ (void) deleteStudy:(NSString*)cd andTakeCourseSeq:(NSString*)seq andChapterNo:(NSString*)no;
+ (NSDictionary*)syn:(NSString*)cd andTakeCourseSeq:(NSString*)seq andChapterNo:(NSString*)no andMaxSec:(NSString*)sec andTotalSec:(NSString*)totalsec andUserId:(NSString*)uid;
+ (NSDictionary*)synV2:(NSString*)cd andTakeCourseSeq:(NSString*)seq andChapterNo:(NSString*)no andStudySec:(NSString*)sec andMarkValue:(NSString*)markValue andUserId:(NSString*)uid;
+ (void) deleteSangStudy:(NSString*)goodsId andContentsSeq:(NSString*)contentsSeq andViewNo:(NSString*)viewNo andContractNo:(NSString*) contractNo andSeGoodsId:(NSString*) seGoodsId;
+ (NSDictionary*)synSang:(NSString*)userId andCompanySeq:(NSString*)companySeq andContractNo:(NSString*)contractNo andContentsSeq:(NSString*)contentsSeq  andGoodsId:(NSString*)goodsId andviewNo:(NSString*)viewNo andViewSec:(NSString*)viewSec andViewSecMobile:(NSString*)viewSecMobile andLastViewSec:(NSString*)lastViewSec andMode:(NSString*)mode;
+ (void)updateTotalSecSang:(NSString*)userId andContractNo:(int)contractNo andContentsSeq:(int)contentsSeq  andGoodsId:(NSString*)goodsId andviewNo:(int)viewNo andTotalSec:(int)sec andLastViewSec:(int)lastViewSec;
+ (NSArray *)getSangStudy:(NSString*)goodsId andContentsSeq:(NSString*)contentsSeq andUserId:(NSString*)userId;
+ (NSArray *)getSangStudy:(NSString*)seGoodsId;
+ (void)updateDownloadInfo:(NSInteger)companySeq andProcessMenuAlias:(NSString*)downloadTab1Nm andImagineMenuAlias:(NSString*)ImagineMenuAlias andOnlyImagineYn:(NSString*)OnlyImagineYn andTabBgColr:(NSString*)tabBgColor;
+ (NSArray *)getDownloadInfo;

+ (void)updateTotalSecSangV2:(NSString*)userId andContractNo:(int)contractNo andContentsSeq:(int)contentsSeq  andGoodsId:(NSString*)goodsId andviewNo:(int)viewNo andViewSec:(int)viewSec andViewSecMobile:(int)viewSecMobile andLastViewSec:(int)lastViewSec;
+ (void)updateLatestSyncDateSang:(NSString*)userId andContractNo:(int)contractNo andContentsSeq:(int)contentsSeq  andviewNo:(int)viewNo;

+ (NSString *)valueForKey:(NSString *)key fromQueryItems:(NSArray *)queryItems;
+ (NSString *)valueForKey:(NSString *)key fromQuery:(NSString *)queryString;
+ (long) calculatePeroidOfDay:(NSString *)fromYYYY_MM_DD to:(NSString *)toYYYY_MM_DD;
+ (UINavigationController *) getNavigationController: (Class)viewControllerClass withNibName:(NSString *)nibName;
+ (UINavigationController *) getNavigationController: (Class)viewControllerClass initWithStyle:(UITableViewStyle)style;

// 서버로 전송되지 못한 학습진도 전송.
+ (int)syncAllFaileSubmitProgress;
// 서버로 전송되지 못한 학습진도 전송(상상마루).
+ (int)syncAllFaileSubmitProgressSang;

+ (NSString *)formatTime:(int)time;
+ (NSString *)timeMmddss:(int)timeSS;
+ (NSString *) filePath : (NSString *) filename;
+ (void) deleteDocrootFile :(NSString *) fileName;
+ (void) deleteFile :(NSString *) fileFullPath;

+ (NSString*)getFileFullPath:(NSString*)folder fileName:(NSString*)fileName;
+ (NSString*)getFileFullPath:(NSString*)folder;
+ (int)isFileExists:(NSString*)folder fileName:(NSString*)fileName;
+ (NSString*)createDirecotryBy:(NSString*)folderName;
+ (NSData*)fileToData:(NSString*)filePath;
+ (void)dataToFile:(NSData*)data saveFilePath:(NSString*)saveFilePath;

+ (void)moveFile:(NSString*)filePath toPath:(NSString*)toPath;
+ (NSString*)fileToString:(NSString*)filePath;
+ (void)stringToFile:(NSString*)text saveFilePath:(NSString*)saveFilePath;
+ (void)copyFile:(NSString*)filePath toPath:(NSString*)toPath;

@end
