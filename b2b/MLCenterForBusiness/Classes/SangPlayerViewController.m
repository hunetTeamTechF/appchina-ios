//
//  SangPlayerViewController.m
//  MLCenterForBusiness
//
//  Created by Park Gi-Bok on 11. 11. 9..
//  Copyright (c) 2011 hunet. All rights reserved.
//

#import "SangPlayerViewController.h"
#import "OverlayView.h"
#import "ModalAlert.h"

#import "CJSONDeserializer.h"
#import "MLCenterForBusinessAppDelegate.h"

#define FULLSIZE CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)
#define ENCODE(X) [(X) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]

@implementation SangPlayerViewController
@synthesize cno, cseq, gid, seGoodsId, playerInfo, currentTimer;

- (NSDictionary *)playerInfo {
	
	if (playerInfo == nil) {
        
		NSURLResponse* response;
		NSError* errorNet;
		NSError* errorJSON;	
		
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
		
		// send it as a POST message
		NSString* body = [NSString stringWithFormat:@"type=43&company_seq=%@&contract_no=%@&uid=%@&contents_seq=%@&goods_id=%@&se_goods_id=%@", app.companySeq, self.cno, [app.profileUser objectForKey:@"userid"], self.cseq, self.gid, self.seGoodsId];
		body = ENCODE(body);
		NSString* url = [NSString stringWithFormat:@"%@App/JLog.aspx", app.urlSite];
		NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
															   cachePolicy:NSURLRequestUseProtocolCachePolicy 
														   timeoutInterval:20.0];
        
		[request setHTTPMethod:@"POST"];
		[request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
		[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
		
		UIApplication* appIndicator = [UIApplication sharedApplication];
		appIndicator.networkActivityIndicatorVisible = YES;
		NSData* dataReceived = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorNet];
		appIndicator.networkActivityIndicatorVisible = NO;
		
		if (dataReceived) {		// success
			playerInfo = [[CJSONDeserializer deserializer] deserializeAsDictionary:dataReceived error:&errorJSON];
			if ([[playerInfo objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {  // success... 
			} else {
				//[ModalAlert notify:@"리스트 받아오는데 실패했습니다."];
			}
		} else {
			[ModalAlert notify:@"네트워크 연결에 문제가 생겼습니다."];
		}
	}
	return playerInfo;
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent];
        appDelegate = (MLCenterForBusinessAppDelegate*)[[UIApplication sharedApplication] delegate];		 
        sumTimer = 0;
        
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
	[[self view] setFrame:[[UIScreen mainScreen] applicationFrame]];
	self.view.backgroundColor = [UIColor blackColor];
	
	indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	indicator.hidesWhenStopped = YES;
	indicator.center = CGPointMake(FULLSIZE.size.width / 2, FULLSIZE.size.height / 2);
	[self.view addSubview:indicator];
	[indicator startAnimating];
	
	
    
	NSURL *movieURL = [NSURL URLWithString:[self.playerInfo objectForKey:@"url"]];
	if (movieURL)
	{
		if ([movieURL scheme])
			[appDelegate initAndPlayMovie:movieURL];
	}
	
    
	if ([appDelegate.moviePlayer respondsToSelector:@selector(loadState)]) {
		[appDelegate.moviePlayer setControlStyle:MPMovieControlStyleFullscreen];
		[appDelegate.moviePlayer setFullscreen:YES];
		appDelegate.moviePlayer.controlStyle = MPMovieControlStyleNone;
		appDelegate.moviePlayer.movieSourceType = MPMovieSourceTypeStreaming;
		appDelegate.moviePlayer.scalingMode = MPMovieScalingModeAspectFill;		
		appDelegate.moviePlayer.view.userInteractionEnabled = YES;
		[appDelegate.moviePlayer prepareToPlay];
        
		
		[[NSNotificationCenter defaultCenter] addObserver:self 
												 selector:@selector(moviePlayerLoadStateChanged:) 
													 name:MPMoviePlayerLoadStateDidChangeNotification 
												   object:nil];
		
	}	 
	
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(moviePlayBackDidFinish:) 
												 name:MPMoviePlayerPlaybackDidFinishNotification 
											   object:nil];
	
	
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(moviePlayBackStateDidChange:)
												 name:MPMoviePlayerPlaybackStateDidChangeNotification 
											   object:nil];
	
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
    
	if((interfaceOrientation == UIInterfaceOrientationLandscapeLeft)
	   || (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
	{
        //NSLog(@"1");
		return YES;
	}
    
    //NSLog(@"2");
	return NO;
}

- (BOOL) shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    
    if (UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
    return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;
{
    if(toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft 
       || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        //NSLog(@"2");
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation { 
    UIInterfaceOrientation toOrientation = self.interfaceOrientation;
    if(toOrientation == UIInterfaceOrientationLandscapeLeft || toOrientation == UIInterfaceOrientationLandscapeRight) {
        //NSLog(@"3");
        [overlayView statusBarDisplay];
    }
}





- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    
    
	self.cno = nil;
	self.cseq = nil;
	self.gid = nil;
    self.seGoodsId = nil;
	self.playerInfo = nil;
	self.currentTimer = nil;
	indicator = nil;
	overlayView = nil;
    
    [super viewDidUnload];
    
}


- (void)dealloc {
	
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:MPMoviePlayerPlaybackStateDidChangeNotification
												  object:nil];
}




- (void) moviePlayerLoadStateChanged:(NSNotification*)notification {
    
	if (appDelegate.moviePlayer.loadState & MPMovieLoadStateStalled) {
        [appDelegate.moviePlayer pause];
    } else if (appDelegate.moviePlayer.loadState & MPMovieLoadStatePlaythroughOK) {
		
		[indicator stopAnimating];
		
		[[NSNotificationCenter defaultCenter] removeObserver:self
														name:MPMoviePlayerLoadStateDidChangeNotification 
													  object:nil];
        
		[[self view] setBounds:FULLSIZE];
		[[appDelegate.moviePlayer view] setFrame:FULLSIZE];
		[[self view] addSubview:[appDelegate.moviePlayer view]];
		[appDelegate.moviePlayer play];
		appDelegate.moviePlayer.currentPlaybackTime = [self currentSliderPosition];
		
        
        
		overlayView = [[OverlayView alloc] init];
		overlayView.tag = 300;
		overlayView.sliderProcess.maximumValue = appDelegate.moviePlayer.duration;
        
        //overlayView.sliderProcess.userInteractionEnabled = NO;
        
		[appDelegate.moviePlayer.view addSubview:overlayView];
        //[UIApplication sharedApplication].statusBarHidden = NO;
        //-----   sangsangplayer:// --------//
        
        
        currentTimer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                        target:self
                                                      selector:@selector(onTimer:)
                                                      userInfo:nil
                                                       repeats:YES];
        
        
    }	
}


- (int)currentSliderPosition {
	
	int lsec = [[self.playerInfo objectForKey:@"last_view_sec"] intValue] + 2;
	if (lsec >= appDelegate.moviePlayer.duration)
		return 0;
	
	return lsec;
	
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {   
	
	//NSLog(@"stop");
	
	[currentTimer invalidate];
    
	[self progressSave:[NSString stringWithFormat:@"%d", (int)floor(sumTimer / 2.0)]];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:MPMoviePlayerPlaybackDidFinishNotification 
												  object:nil];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
	
	//[super viewWillAppear:NO];
	
	[self dismissViewControllerAnimated:YES completion:nil];
}


- (void)moviePlayBackStateDidChange:(NSNotification*)notification {
	if (appDelegate.moviePlayer.playbackState == MPMoviePlaybackStatePaused) {
		overlayView.btnPause.hidden = YES;
		overlayView.btnPlay.hidden = NO;	
	} else if (appDelegate.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
		overlayView.btnPlay.hidden = YES;
		overlayView.btnPause.hidden = NO;
	}
}



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
	UITouch *touch = [touches anyObject];
	NSUInteger tapCount = [touch tapCount];	
	
	
	//NSLog(@"%d", touch.view.tag);
	
    switch (tapCount) {
        case 1:
			[self performSelector:@selector(singleTapMethod:) withObject:[NSNumber numberWithInt:touch.view.tag] afterDelay:.4];
            break;
        case 2:
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(singleTapMethod:) object:[NSNumber numberWithInt:touch.view.tag]];
            break;
        default:
            break;
			
    }
}

-(void)singleTapMethod:(NSNumber*)tag {
	
	if ([tag intValue] != 300) {
		return;
	}
	
	[overlayView displayItems];
}


- (void)onTimer:(NSTimer *)timer {
	
	if (appDelegate.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
		[self updateForPlayerInfo];
	}
}

- (void)updateForPlayerInfo {
	
	long currentPlaybackTime = appDelegate.moviePlayer.currentPlaybackTime;
	int currentHours = (currentPlaybackTime / 3600);
	int currentMins = ((currentPlaybackTime / 60) - currentHours * 60);
	int currentSecs = (currentPlaybackTime % 60);
	overlayView.lblCurrent.text = [NSString stringWithFormat:@"%i:%02d:%02d", currentHours, currentMins, currentSecs];
	overlayView.sliderProcess.value = currentPlaybackTime;
	
	long totalPlaybackTime = appDelegate.moviePlayer.duration - appDelegate.moviePlayer.currentPlaybackTime;
	int tHours = (totalPlaybackTime / 3600);
	int tMins = ((totalPlaybackTime / 60) - tHours * 60);
	int tSecs = (totalPlaybackTime % 60);
	overlayView.lblDuration.text = [NSString stringWithFormat:@"-%i:%02d:%02d", tHours, tMins, tSecs];
	
	
	sumTimer += 1;
	
	//NSLog(@"sumtimer : %d", sumTimer);
	if ((sumTimer % 40) == 0) {
		[self progressSave:[NSString stringWithFormat:@"%d", (int)floor(sumTimer / 2.0)]];
	}
	
	
}

-(BOOL)prefersStatusBarHidden { return YES; }


- (void)progressSave:(NSString*)second {	
	[NSThread detachNewThreadSelector:@selector(viewSecSave:) 
							 toTarget:self 
						   withObject:second];
}


- (void)viewSecSave:(NSString*)second {
	
	@autoreleasepool {
	
	
		NSURLResponse* response;
		NSError* errorNet;
		NSError* errorJSON;	
		
		// send it as a POST message
		
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
		
		int sec = [[self.playerInfo objectForKey:@"view_sec"] intValue];
    int sec_mobile = [[self.playerInfo objectForKey:@"view_sec_mobile"] intValue];
    
		NSString* body = [NSString stringWithFormat:@"type=22&company_seq=%@&contract_no=%@&uid=%@&contents_seq=%@&view_sec=%d&view_sec_mobile=%d&last_view_sec=%d", 
                      app.companySeq,
						  self.cno,
						  [app.profileUser objectForKey:@"userid"],
						  self.cseq,
						  sec + [second intValue],
                      sec_mobile + [second intValue],
						  (int)floor(overlayView.sliderProcess.value)];
		
		//NSLog(@"%@", body);
		
		
		body = ENCODE(body);
		NSString* url = [NSString stringWithFormat:@"%@", app.urlBase];
		
		NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
															   cachePolicy:NSURLRequestUseProtocolCachePolicy 
														   timeoutInterval:20.0];
		[request setHTTPMethod:@"POST"];
		[request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
		[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
		
		UIApplication* appIndicator = [UIApplication sharedApplication];
		appIndicator.networkActivityIndicatorVisible = YES;
		NSData* dataReceived = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorNet];
		appIndicator.networkActivityIndicatorVisible = NO;
		
		if (dataReceived) {		// successfully received the result
			NSDictionary* result = [[CJSONDeserializer deserializer] deserializeAsDictionary:dataReceived error:&errorJSON];
			
			if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"NO"]) {  // success... 
				//[ModalAlert notify:@"진도 저장중 오류 발생했습니다."];
			}
		} else {
			//[ModalAlert notify:@"네트워크 연결에 문제가 생겼습니다."];		
		}
    
	}
}


@end
