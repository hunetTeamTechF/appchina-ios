
#import "WebModalViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "Header.h"
#import "MoreViewController.h"
#import "DownloadViewController.h"
#import "PlayerCallFun.h"

@interface WebModalViewController ()

@end

@implementation WebModalViewController
//@synthesize webViewRequest;
//@synthesize WebView;
@synthesize playerCallFun = _playerCallFun;

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
	[self.Indicator stopAnimating];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
	[self.Indicator startAnimating];
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
	[self.Indicator stopAnimating];
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {

//	NSURL *url = request.URL;
//	NSString *urlString = url.absoluteString.lowercaseString;
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    NSURL *url = request.URL;
    NSString *urlString = url.absoluteString;
    
    //모달 팝업
    if ([urlString.lowercaseString hasPrefix:@"modal://"])
    {
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        int qLocation = [url.absoluteString rangeOfString:@"?"].location;
        NSString *query = [url.absoluteString substringFromIndex:qLocation + 1];
        query = [query stringByReplacingOccurrencesOfString:@"://" withString:@""];
        for (NSString *item in [query componentsSeparatedByString:@"&"])
        {
            NSArray *keyValue = [item componentsSeparatedByString:@"="];
            if (![keyValue objectAtIndex:0])
                continue;
            
            NSString *key = [keyValue objectAtIndex:0];
            NSString *value = (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (CFStringRef)[keyValue objectAtIndex:1], CFSTR(""), kCFStringEncodingUTF8));
            [params addEntriesFromDictionary:[[NSDictionary alloc]
                                               initWithObjects:[[NSArray alloc] initWithObjects:value, nil]
                                               forKeys:[[NSArray alloc] initWithObjects:key, nil]]];
        }
        
        NSString *urlParamDecoded = [params valueForKey:@"url"];
        NSLog(@"urlParamDecoded : %@", urlParamDecoded);
        //?url query parameter가 없는 경우,
        if (urlParamDecoded == nil) {
            WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
            webModal.webViewUrl = [request.URL.absoluteString stringByReplacingOccurrencesOfString:@"modal://" withString:@"http://"];
            //webModal.title = @"";
            [self presentViewController:webModal animated:YES completion:nil];
        }
        //query에 url parameter가 있으면 해당 url로 modal 팝업
        else {
            
            NSURL *urlParsed = [NSURL URLWithString:[[urlParamDecoded componentsSeparatedByString:@"?"] firstObject]];
            NSString *method = [params valueForKey:@"method"];
            NSString *title = [params valueForKey:@"title"];
            //post parameter가 있으면 url을 파싱해서, queryString은 post로 전송
            if ([method.lowercaseString isEqualToString:@"post"])
            {
                WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
                
                //webViewRequestMethod
                NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:urlParsed
                                                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                   timeoutInterval:20.0];
                [request setHTTPMethod:@"POST"];
                NSString *bodyString = (NSString *)[[urlParamDecoded componentsSeparatedByString:@"?"] lastObject];
                [request setHTTPBody:[bodyString dataUsingEncoding:NSUTF8StringEncoding]];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                webModal.title = title;
                [self presentViewController:webModal animated:YES completion:nil];
                [webModal.WebView loadRequest:request];
                
                
            }
            //query에 url parameter가 없으면
            else {
                WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
                webModal.webViewUrl = urlParamDecoded;
                webModal.title = title ? title : @"";
                [self presentViewController:webModal animated:YES completion:nil];
            }
        }
        
        return NO;
        
    }
    
    // 모달 창 종료
    if ([urlString hasPrefix:@"finish://"] || [urlString hasPrefix:@"close://"]) {
        [self dismissViewControllerAnimated:YES completion:nil];
        return NO;
    }
    
    // 페이지 뒤로가기 (뒤로갈 수 없으면 모달 창 종료)
    if ([urlString hasPrefix:@"back://"]) {
        if (webView.canGoBack) {
            [webView goBack];
        }
        else {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        return NO;
    }
    
    //다운로드
    if ([urlString isEqualToString:@"downloadcenter://"]) {
        if ([app.eduType isEqualToString:@"LearningAndImagine"]) {
            self.tabBarController.selectedIndex = 2;
            UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
            MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
            DownloadViewController *second = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
            NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
            [navi setViewControllers:controllersArray animated:YES];
        } else {
            
            self.tabBarController.selectedIndex = 2;
            
            UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
            [navi popToRootViewControllerAnimated:YES];
        }
        return NO;
    }
    
    if ([urlString.lowercaseString hasPrefix:@"sangsangdrmplayer://"] ||
        [urlString.lowercaseString hasPrefix:@"sangsangdownload://"] ||
        [urlString.lowercaseString hasPrefix:@"sangsangdownloadplay://"]) {
        
        NSArray *tmpArray = [urlString componentsSeparatedByString:@"://"];
        NSString *act = [tmpArray objectAtIndex:0];
        NSString *param = [tmpArray objectAtIndex:1];
        
        BOOL checkConfigSettings = false;
        //상상마루 바로학습
        if ([urlString.lowercaseString hasPrefix:@"sangsangdrmplayer://"]) {
            checkConfigSettings = [app moviePlayCheck];
        } //상상마루 다운로드 플레이
        else {
            checkConfigSettings = [app isWiFi];
        }
        if (checkConfigSettings) {
            
            _playerCallFun = [[PlayerCallFun alloc] init];
            _playerCallFun.delegateViewController = self;


            //self.currentUrl = webView stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
//            if ([urlString.lowercaseString hasPrefix:@"sangsangdownload://"]) {
//                [self dismissViewControllerAnimated:YES completion:nil];
//            }
            [_playerCallFun sangsangmaruInit:[NSArray arrayWithObjects:act, param, nil]];
            
        }
        
        return NO;
    }

    //pc버전 링크
    if ([urlString isEqualToString:[NSString stringWithFormat:@"%@/", app.urlCenter]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:app.urlCenter]];
        return NO;
    }
    
    // 외부 사이트로 이동
    if ([urlString.lowercaseString hasPrefix:@"escape://"]) {
        NSString *escapeUrl = [urlString stringByReplacingOccurrencesOfString:@"escape://" withString:@"http://"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:escapeUrl]];
        return NO;
    }
    
    if ([urlString.lowercaseString hasPrefix:@"appstore://"]) {
        NSString *escapeUrl = [urlString stringByReplacingOccurrencesOfString:@"appstore://" withString:@""];
        escapeUrl = [escapeUrl stringByReplacingOccurrencesOfString:@"https//" withString:@"https://"];
        escapeUrl = [escapeUrl stringByReplacingOccurrencesOfString:@"http//" withString:@"http://"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:escapeUrl]];
        return NO;
    }
    
    if([[[request URL] absoluteString] hasPrefix:@"jscall"])
    {
        NSString *requestString = [[request URL] absoluteString];
        NSArray *components = [requestString componentsSeparatedByString:@"://"];
        NSString *functionName = [components objectAtIndex:1];
        
        SEL selector = NSSelectorFromString(functionName);
        IMP imp = [self methodForSelector:selector];
        void (*func)(id, SEL) = (void *)imp;
        func(self, selector);
        
        return NO;
    }
    
    return YES;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.navigationController.navigationBar.
    //self.title = @"eBook";
//    UIBarButtonItem *flipButton = [[UIBarButtonItem alloc]
//                                   initWithTitle:@"Flip"
//                                   style:UIBarButtonItemStyleBordered
//                                   target:self
//                                   action:@selector(flipView:)];
//    self.navigationItem.rightBarButtonItem = flipButton;
    
    self.LabelNaviTitle.text = self.title;
    
    if(IS_DEVICE_RUNNING_IOS_7_AND_ABOVE()) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        [self setNeedsStatusBarAppearanceUpdate];
    }

    if (_webViewUrl != nil) {
     
        NSString *urlUppercaseString = [_webViewUrl uppercaseString];
        if (//[urlUppercaseString rangeOfString:@".PDF"].location != NSNotFound ||
            [urlUppercaseString rangeOfString:@".PNG"].location != NSNotFound ||
            [urlUppercaseString rangeOfString:@".JPG"].location != NSNotFound ||
            [urlUppercaseString rangeOfString:@".GIF"].location != NSNotFound ||
            [urlUppercaseString rangeOfString:@".JPEG"].location != NSNotFound)
        {
            
            NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><head><meta charset=""utf-8""><meta name=""viewport"" content=""width=device-width, initial-scale=1.0""></head><body><img src=""%@"" style=""max-width:100%%; height:auto;""></body></html>", _webViewUrl];
            [self.WebView loadHTMLString:html baseURL:nil];
        }
        else {
            [[NSURLCache sharedURLCache] removeAllCachedResponses];
            NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:_webViewUrl]
                                                        cachePolicy: NSURLRequestReloadIgnoringCacheData
                                                    timeoutInterval:60.0
                                        ];
            [self.WebView loadRequest:requestObj];
        }
    }
    
    if (self.IsHiddenNaviBar) {
        [self setHiddenNaviBar:self.IsHiddenNaviBar];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload {
    [self setLabelNaviTitle:nil];
    [self setImageViewNavi:nil];
    [self setWebView:nil];
    [self setButtonClose:nil];
    [self setIndicator:nil];
    [self setWebViewUrl:nil];
    [super viewDidUnload];
}

- (NSUInteger)supportedInterfaceOrientations
{
    self.Indicator.center = self.WebView.center;
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (IBAction)ButtonCloseClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)processAction {
    //[super processAction];
}

- (void)openerCallFun {
    NSLog(@"player close4");
    NSString *currentUrl = [self.WebView stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
    
    if (currentUrl != nil) {
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:currentUrl]
                                                    cachePolicy: NSURLRequestReloadIgnoringCacheData
                                                timeoutInterval:60.0
                                    ];
        [self.WebView loadRequest:requestObj];
    }
}

- (void)setHiddenNaviBar:(BOOL) hidden {
    self.IsHiddenNaviBar = hidden;
    if (hidden == [self.ViewNavi isHidden]) {
        return;
    }
    [self.ViewNavi setHidden:hidden];
    CGRect frame = self.WebView.frame;
    frame.size.height += 40;
    frame.origin.y -= 40;
    self.WebView.frame = frame;
}

@end
