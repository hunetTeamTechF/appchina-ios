//
//  MoreWebViewController.h
//  MLCenterForBusiness
//
//  Created by hunet-mac-npc92 on 1/30/15.
//
//

#import <UIKit/UIKit.h>

@interface MoreWebViewController : UIViewController <UIWebViewDelegate> {
    
}

@property (retain, nonatomic) IBOutlet UIWebView *webView;
@end
