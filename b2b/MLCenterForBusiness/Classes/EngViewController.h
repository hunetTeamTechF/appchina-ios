//
//  EngViewController.h
//  Learning
//
//  Created by Joey on 2013. 12. 18..
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVPlayer.h>


@interface EngViewController : UIViewController <UIWebViewDelegate> {
    
    IBOutlet UIWebView *viewWeb;
    IBOutlet UIActivityIndicatorView *indicator;
    
    NSString *engUrl;
    
}
@property (nonatomic, strong) NSString *engUrl;
@property (nonatomic, strong) NSString *contentType;

- (void)viewLoad;
@end