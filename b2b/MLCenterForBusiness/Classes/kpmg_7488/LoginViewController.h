

#import <UIKit/UIKit.h>
#import "DownPicker.h"

@interface LoginViewController : UIViewController <UITextFieldDelegate> {
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UIImageView *viewImageCover;
    UIView *mask;
    CGPoint pointViewControllBox;
    BOOL *userIdPrefixVisiblity;
    NSString *userIdPrefixString;
}

@property (nonatomic, strong) UIImageView *viewImageCover;
@property (strong, nonatomic) IBOutlet UIButton *offlineButton;
@property (strong, nonatomic) IBOutlet UIImageView *viewImageBackground;
@property (strong, nonatomic) IBOutlet UIImageView *viewImageLoginBox;
@property (strong, nonatomic) IBOutlet UITextField *textID;
@property (strong, nonatomic) IBOutlet UITextField *textUnit;
@property (strong, nonatomic) IBOutlet UITextField *textPassword;
@property (strong, nonatomic) IBOutlet UIButton *buttonLogin;
@property (strong, nonatomic) IBOutlet UIButton *buttonSaveUserid;
@property (strong, nonatomic) IBOutlet UIButton *buttonAutoLogin;
@property (weak, nonatomic) IBOutlet UIView *viewControllBox;

- (IBAction) onLogin;
- (void) onWebLogin: (NSString *) userId : (NSString *) userPwd;
- (IBAction)backgroundTab:(id)sender;
- (IBAction)offlineClick:(id)sender;
- (IBAction)clickedSaveUserId:(id)sender;
- (IBAction)clickedAutoLogin:(id)sender;

@end