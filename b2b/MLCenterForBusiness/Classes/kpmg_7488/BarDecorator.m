//
//  BarDecorator.m
//  MLCenterForBusiness
//
//  Created by Park SeungKyun on 2015. 7. 20..
//
//

#import "BarDecorator.h"
#import "BaseViewController.h"
#import "MyViewController.h"
#import "LectureViewController.h"
#import "MoreWebViewController.h"
#import "DownloadViewController.h"
#import "Util.h"
#import "MoreViewController.h"
#import "HomeViewController.h"
#import "SelfStudyViewController.h"
#import "StudyStateViewController.h"
#import "Header.h"
#import "GraphicsUtile.h"

@implementation BarDecorator

+ (void)setting:(MLCenterForBusinessAppDelegate *) app
{
    // 색상 설정
//    UIColor *selectedIconColor = [MLCenterForBusinessAppDelegate colorFromHexString:@"#5b91f5"];    // 선택아이콘
//    UIColor *unselectedIconColor = [MLCenterForBusinessAppDelegate colorFromHexString:@"#78869d"];  // 디폴트아이콘
//    UIColor *tabbarColor = [UIColor colorWithRed:50.0/255.0 green:78.0/255.0 blue:165.0/255.0 alpha:1.0];          // 디폴트배경(탭바)
    UIColor *navibarColor = [UIColor colorWithRed:50.0/255.0 green:78.0/255.0 blue:165.0/255.0 alpha:1.0];         // 디폴트배경(네비게이션바)
    
    [app.window.rootViewController removeFromParentViewController];
    if (app.tabBarController != nil) {
        app.tabBarController = nil;
    }
    app.tabBarController = [[UITabBarController alloc] init];
    app.tabBarController.delegate = app;
    app.window.rootViewController = app.tabBarController;
    
    ////////////////////////////////////////////////
    // tabBar 버튼 갯수와 종류 셋팅
    
    HomeViewController *homeViewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    LectureViewController *lectureViewController = [[LectureViewController alloc] initWithNibName:@"LectureViewController" bundle:nil];
    MyViewController *myViewController = [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil];
    SelfStudyViewController *selfStudyViewController = [[SelfStudyViewController alloc] initWithNibName:@"BaseViewController" bundle:nil];
//    UINavigationController *downloadNavigationController = [Util getNavigationController:[DownloadViewController class] initWithStyle:UITableViewStylePlain];
    MoreViewController *moreViewController = [[MoreViewController alloc] initWithNibName:@"MoreViewController" bundle:nil];
    UINavigationController *moreNavigationController = [[UINavigationController alloc] initWithRootViewController:moreViewController];
    
    // 메뉴 제목 설정
//    [downloadNavigationController.tabBarItem setTitle:@"다운로드"];
//    [downloadNavigationController.tabBarItem setTitle:@"다운로드"];
    [selfStudyViewController.tabBarItem setTitle:@"상상마루"];
    selfStudyViewController.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_free_study"];
    
    app.tabBarController.viewControllers = @[
                                              homeViewController,
                                              lectureViewController,
                                              selfStudyViewController,
                                              myViewController,
                                              moreNavigationController
                                            ];
    //
    ////////////////////////////////////////////////
    
//    [GraphicsUtile setTabBarColor:app.tabBarController.tabBar barColor:tabbarColor selectedIconColor:selectedIconColor unselectedIconColor:unselectedIconColor];
    [GraphicsUtile setNavigationBarColor:navibarColor];
    
    [app performSelector:@selector(pushServiceURL) withObject:nil afterDelay:0.5];
}

@end
