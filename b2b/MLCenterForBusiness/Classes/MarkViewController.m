//
//  MarkViewController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 6. 7..
//  Copyright 2011 hunet. All rights reserved.
//

#import "MarkViewController.h"
#import "MLCenterForBusinessAppDelegate.h"

@implementation MarkViewController
@synthesize listData, lastMarkNo, cellSelected;

- (id) initWithDictionary:(NSDictionary *)playerInfo {
    
	if (self = [super init]) {
        appDelegate = (MLCenterForBusinessAppDelegate*)[[UIApplication sharedApplication] delegate];
		self.lastMarkNo = [playerInfo objectForKey:@"LastMarkNo"];
		self.listData = [NSMutableArray arrayWithArray:[playerInfo objectForKey:@"Result"]];
	}
	return self;
}


#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
}


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.listData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 25;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) 
		//cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	
	NSString *markNm = [[self.listData objectAtIndex:indexPath.row] objectForKey:@"MarkNm"];
	NSString *markNo = [[self.listData objectAtIndex:indexPath.row] objectForKey:@"MarkNo"];
	NSString *markYn = [[self.listData objectAtIndex:indexPath.row] objectForKey:@"MarkYn"];
	
	cell.textLabel.font = [UIFont systemFontOfSize:10];
	cell.textLabel.textColor = [UIColor grayColor];
	cell.textLabel.text = markNm;

	cell.accessoryType = UITableViewCellAccessoryNone;
	
	if ([self.lastMarkNo intValue] == [markNo intValue]) {
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	}
	
	if ([markYn isEqualToString:@"Y"]) {
		cell.textLabel.font = [UIFont boldSystemFontOfSize:11];
		cell.textLabel.textColor = [UIColor blackColor];
	}
	
    cell.userInteractionEnabled = self.cellSelected;
	return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int markValue = [[[self.listData objectAtIndex:indexPath.row] objectForKey:@"MarkValue"] intValue];
	
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	cell.textLabel.font = [UIFont boldSystemFontOfSize:11];
	cell.textLabel.textColor = [UIColor blackColor];
	cell.accessoryType = UITableViewCellAccessoryCheckmark;
	
	[self replaceObjectAtIndex:indexPath.row];
	appDelegate.moviePlayer.currentPlaybackTime = (markValue > 0) ? markValue + 6 : 0;
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}



- (void)replaceObjectAtIndex:(NSUInteger)index {
	
	NSDictionary *dic = (NSDictionary *)[self.listData objectAtIndex:index];
	NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:
																	[dic objectForKey:@"MarkNo"],
																	[dic objectForKey:@"MarkValue"],
																	[dic objectForKey:@"PPTUrl"], 
																	[dic objectForKey:@"MarkNm"],
																	@"Y", 
																	[dic objectForKey:@"RealFrameNo"], nil] forKeys:[NSArray arrayWithObjects:
																						@"MarkNo", @"MarkValue", @"PPTUrl", @"MarkNm", @"MarkYn", @"RealFrameNo",
																						//Very long list of keys
																						nil]];
	
	[self.listData replaceObjectAtIndex:index withObject:dictionary];
	[self.tableView reloadData];
	
}


- (void)reloadList:(NSString *)markNo {
	
	self.lastMarkNo = [NSString stringWithFormat:@"%@", markNo];
	
	for (int i = 0; i < [self.listData count]; i++) {
		NSDictionary *dic = (NSDictionary *)[self.listData objectAtIndex:i];
		
		if ([[dic objectForKey:@"MarkNo"] intValue] == [markNo intValue]) {
			[self replaceObjectAtIndex:i];
			break;
		}
		
	}
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
	self.listData = nil;
	self.lastMarkNo = nil;
    [super viewDidUnload];
}





@end

