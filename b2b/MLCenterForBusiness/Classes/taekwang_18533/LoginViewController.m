
#import "LoginViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "MoreViewController.h"
#import "DownloadViewController.h"
#import "ModalAlert.h"
#import "Header.h"
#import "DownPicker.h"
#import "LectureViewController.h"
#import "BarDecorator.h"
#import "GraphicsUtile.h"
#import "Util.h"
#import "Global.h"
#import "CJSONDeserializer.h"

@implementation LoginViewController
@synthesize viewImageCover;

- (void)initDownPickerForTextUnit {
    self.downPicker = [[DownPicker alloc] initWithTextField:self.textUnit withData:[[NSMutableArray alloc] initWithArray:affiliateKeyArray]];
    [self.downPicker setPlaceholderWhileSelecting:@"회사선택"];
    [self.downPicker getTextField].attributedPlaceholder = [self setAttributedPlaceholderWithString:@"회사선택"];

    UIImage *rightArrowImage = [UIImage imageNamed:@"select-arrow.png"];
    [self.downPicker setArrowImage:rightArrowImage width:10 height:7];

    [self.downPicker addTarget:self
                        action:@selector(downPickerSelected)
              forControlEvents:UIControlEventValueChanged];
    [self.downPicker addTarget:self
                        action:@selector(downPickerSelected)
              forControlEvents:UIControlEventTouchUpInside];
}

- (NSMutableAttributedString *)setAttributedPlaceholderWithString:(NSString *)str {
    UIColor *color = [MLCenterForBusinessAppDelegate colorFromHexString:@"#000000"];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:str];
    [attrStr addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, str.length)];
    [attrStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:NSMakeRange(0, str.length)];
    
    return attrStr;
}

- (void)viewDidAppear:(BOOL)animated {
    [self initDownPickerForTextUnit];
    
    UIView *padding0 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    [self.downPicker getTextField].leftView = padding0;
    [self.downPicker getTextField].leftViewMode = UITextFieldViewModeAlways;
    
    UIView *padding1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.textID.leftView = padding1;
    self.textID.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *padding2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.textPassword.leftView = padding2;
    self.textPassword.leftViewMode = UITextFieldViewModeAlways;
    
    
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    [self.buttonSaveUserid setSelected:[[app.profileUser valueForKey:@"saveid"] boolValue]];
    [self.buttonAutoLogin setSelected:[[app.profileUser valueForKey:@"autologin"] boolValue]];
    
    if (self.buttonSaveUserid.isSelected) {
        _idPrefixString = [app.profileUser valueForKey:@"useridPrefix"];
        _idPrefixString = _idPrefixString ? _idPrefixString : @"";
        [self.textID setText:[[app.profileUser objectForKey:@"userid"] stringByReplacingOccurrencesOfString:_idPrefixString withString:@""]];
        
        [self.textUnit setText:[affiliateDictionary valueForKey:_idPrefixString]];
    }
    
    CGPoint centerPoint = self.viewControllBox.center;
    pointViewControllBox = self.viewControllBox.center = CGPointMake(centerPoint.x, centerPoint.y + (IS_4_INCH ? 0 : 0));
    
    //self.textID.attributedPlaceholder = [self setAttributedPlaceholderWithString:@"아이디"];
    //self.textPassword.attributedPlaceholder = [self setAttributedPlaceholderWithString:@"비밀번호"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [paths objectAtIndex:0];
    NSString *drmFolder = [NSString stringWithFormat:@"%@/%@", documentFolder, @"hunet_docroot"];
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSDirectoryEnumerator *fileEnumerator = [fileManager enumeratorAtPath:drmFolder];
    
    int filecount = 0;
    for (NSString *filename in fileEnumerator) {
        if ([filename isEqualToString:@".DS_Store"]) {
            continue;
        }
        filecount = 1;
        break;
    }

    self.offlineButton.hidden = filecount == 0;
    affiliateKeyArray = @[@"태광산업", @"대한화섬", @"세광패션", @"서한물산",
                          @"흥국생명", @"흥국화재", @"흥국증권", @"흥국자산운용", @"고려저축은행", @"예가람저축은행",
                          @"티브로드", @"티캐스트", @"kct", @"kdmc",
                          @"티시스", @"태광CC", @"동림건설", @"한국도서보급", @"에스티임", @"메르뱅"];
      // 태광산업 tk- / 흥국생명 hk- / 티브로드 tb- / 티시스 ts-
      affiliateValueArray = @[@"tk-",@"tk-",@"tk-",@"tk-",
                          @"hk-",@"hk-",@"hk-",@"hk-",@"hk-",@"hk-",
                          @"tb-",@"tb-",@"tb-",@"tb-",
                          @"ts-",@"ts-",@"ts-",@"ts-",@"ts-",@"ts-"];
    affiliateDictionary = [[NSMutableDictionary alloc] initWithObjects:affiliateValueArray forKeys:affiliateKeyArray];
    
}

-(void)downPickerSelected {
    
    NSString *selectedValue = [self.downPicker getTextField].text;
    if ([selectedValue length] > 0) {
        _idPrefixString = affiliateDictionary[selectedValue];
    }
    else if ([_idPrefixString length] == 0) {
        _idPrefixString = @"";
        [self.downPicker setPlaceholderWhileSelecting:@"회사선택"];
        [self.downPicker getTextField].attributedPlaceholder = [self setAttributedPlaceholderWithString:@"회사선택"];
    }
}

- (IBAction) onLogin {
    
    [self downPickerSelected];
    
    if (_idPrefixString.length == 0) {
        [ModalAlert notify:@"회사를 선택해주세요."];
        return;
    }
    
    self.buttonLogin.userInteractionEnabled = NO;
    
    self.textID.text = [self.textID.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.textPassword.text = [self.textPassword.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if ([self.textID isFirstResponder]) {
        [self.textID resignFirstResponder];
    }
    
    if ([self.textPassword isFirstResponder]) {
        [self.textPassword resignFirstResponder];
    }
    
    if ([self.textUnit isFirstResponder]) {
        [self.textUnit resignFirstResponder];
    }
    
    if (self.textID.text.length == 0) {
        [ModalAlert notify:@"아이디를 입력해 주세요"];
        self.buttonLogin.userInteractionEnabled = YES;
        [self toggleControllBoxPosition:NO];
        [self.textID becomeFirstResponder];
        return;
    }

    if (self.textID.text.length == 0 || self.textPassword.text.length == 0) {
        [ModalAlert notify:@"비밀번호를 입력해 주세요"];
        self.buttonLogin.userInteractionEnabled = YES;
        [self toggleControllBoxPosition:NO];
        [self.textPassword becomeFirstResponder];
        return;
    }
    [self toggleControllBoxPosition:NO];
    mask = [[UIView alloc] initWithFrame:[[self.view window] frame]];
    mask.userInteractionEnabled = NO;
    mask.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.50];
    [self.view addSubview:mask];
    
    [indicator startAnimating];
    [NSThread detachNewThreadSelector:@selector(threadLogin:) toTarget:self withObject:nil];
}

- (void)onWebLogin:(NSString *)userId :(NSString *)userPwd{
    BOOL result = YES;
    @autoreleasepool {
        
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        result = [app loginForID:userId withPassword:userPwd];
        [self performSelectorOnMainThread:@selector(callbackAfterLogin:) withObject:[NSNumber numberWithBool:result] waitUntilDone:NO];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([self.textID isFirstResponder]) {
        [self.textPassword becomeFirstResponder];
    }
    else {
        [self toggleControllBoxPosition:NO];
        [self onLogin];
    }
    
    return YES;
}

- (void)toggleControllBoxPosition:(BOOL)textInput {
    [UIView beginAnimations:nil context:NULL];
    //[UIView setAnimationDuration:0.2f];
    [UIView setAnimationDelegate:self];
    
    // 로그인 박스 위치이동
    self.viewControllBox.center = CGPointMake(pointViewControllBox.x, textInput ? pointViewControllBox.y - (IS_4_INCH ? 20 : 0) : pointViewControllBox.y);
    
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [self toggleControllBoxPosition:YES];
    
    return YES;
}

- (void)threadLogin:(id)somedata {
    BOOL result = YES;
    @autoreleasepool {
        //NSString idPrefix = self.downPicker.selected
        //MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        
        NSString *userId = self.textID.text;
        if ([_idPrefixString length] > 0 && ![userId hasPrefix:_idPrefixString]) {
            userId = [NSString stringWithFormat:@"%@%@", _idPrefixString, userId];
        }
        result = [self loginForID:userId withPassword:self.textPassword.text];
        [self performSelectorOnMainThread:@selector(callbackAfterLogin:) withObject:[NSNumber numberWithBool:result] waitUntilDone:NO];
    }
}

- (BOOL)loginForID:(NSString*)userid withPassword:(NSString*)password {
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    NSString *uid       = userid;
    NSString *passwd    = [[Global sharedSingleton] encodePassword:password];
    NSString *tk        = [app.profileUser objectForKey:@"deviceToken"];
    NSString *param     = [NSString stringWithFormat:@"type=1&uid=%@&pw=%@&token=%@", uid, passwd, tk];
    BOOL result         = [self loginForParam:param];
    
    return result;
}


- (BOOL)loginForParam:(NSString *)param {
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    NSError *errorReq = nil;
    BOOL fLoginResult = NO;
    NSLog(@"%@?%@", app.urlBase, param);
    
    NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:app.urlBase] andQuery:param andError:&errorReq];
    
    if (jsonData) {
        NSError *errorJSON = nil;
        NSDictionary *result = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
        
        BOOL checkCompanyID = NO;
        if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
            
            // CompanySeq, AffiliatedCode
            NSString *affiliatedCode = [[result objectForKey:@"AffiliatedCode"] stringValue];
            NSString *companySeq     = [[result objectForKey:@"CompanySeq"] stringValue];
            checkCompanyID = [self checkAppLoginForCompanySeq:companySeq andAffiliatedCode:affiliatedCode];
            
            if (checkCompanyID) {
                
                NSString *uid           = [[result objectForKey:@"UserId"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString *pw            = [[result objectForKey:@"Password"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                app.companySeq         = [result objectForKey:@"CompanySeq"];
                app.urlCenter          = [result objectForKey:@"Url"];
                app.drmUseType         = [result objectForKey:@"DrmUseType"];
                app.eduType            = [result objectForKey:@"EduType"];
                app.hReminderUseType   = [result objectForKey:@"HReminderUseType"];
                app.hStoryUseType      = [result objectForKey:@"HStoryUseType"];
                app.bSSPlayerSpeedBar  = [[result objectForKey:@"EnableSSPlayerSpeedBar"] isEqualToString:@"Y"];
                
                [app.profileUser setObject:uid forKey:@"userid"];
                [app.profileUser setObject:[[Global sharedSingleton] encodePassword:pw] forKey:@"password"];
                [app.profileUser setObject:[NSNumber numberWithBool:app.bSSPlayerSpeedBar] forKey:@"EnableSSPlayerSpeedBar"];
                
                [Util updateDownloadInfo:[app.companySeq intValue] andProcessMenuAlias:[result objectForKey:@"ProcessMenuAlias"] andImagineMenuAlias:[result objectForKey:@"ImagineMenuAlias"]
                        andOnlyImagineYn:[result objectForKey:@"OnlyImagineYn"] andTabBgColr:@""];
                
                
                app.flagLogin = YES;
                fLoginResult = YES;
            }

        }

        if (checkCompanyID == NO) {
            app.workStateType = [result objectForKey:@"WorkStateType"];
            app.companySeq = @"";
        }
    }
    return fLoginResult;
}

- (BOOL)checkAppLoginForCompanySeq:(NSString*)companySeq andAffiliatedCode:(NSString*)affiliatedCode {
    
    NSString *selectedLoginGroup = [self.downPicker getTextField].text;
    
    if ([selectedLoginGroup isEqualToString:@"태광산업"]) {
        return [companySeq isEqualToString:@"18533"] && (
                                                         [affiliatedCode isEqualToString:@"0"] ||
                                                         [affiliatedCode isEqualToString:@"18533"]);
    } else if ([selectedLoginGroup isEqualToString:@"대한화섬"]) {
        return [companySeq isEqualToString:@"18533"] && [affiliatedCode isEqualToString:@"18534"];
    } else if ([selectedLoginGroup isEqualToString:@"세광패션"]) {
        return [companySeq isEqualToString:@"18533"] && (
                                                         [affiliatedCode isEqualToString:@"18536"] ||
                                                         [affiliatedCode isEqualToString:@"18581"]);
    } else if ([selectedLoginGroup isEqualToString:@"서한물산"]) {
        return [companySeq isEqualToString:@"18533"] && [affiliatedCode isEqualToString:@"18577"];
    } else if ([selectedLoginGroup isEqualToString:@"흥국생명"]) {
        return [companySeq isEqualToString:@"18537"] && (
                                                         [affiliatedCode isEqualToString:@"0"] ||
                                                         [affiliatedCode isEqualToString:@"18537"]);
    } else if ([selectedLoginGroup isEqualToString:@"흥국화재"]) {
        return [companySeq isEqualToString:@"18537"] && [affiliatedCode isEqualToString:@"18538"];
    } else if ([selectedLoginGroup isEqualToString:@"흥국자산운용"]) {
        return [companySeq isEqualToString:@"18537"] && [affiliatedCode isEqualToString:@"18539"];
    } else if ([selectedLoginGroup isEqualToString:@"흥국증권"]) {
        return [companySeq isEqualToString:@"18537"] && [affiliatedCode isEqualToString:@"18540"];
    } else if ([selectedLoginGroup isEqualToString:@"고려저축은행"]) {
        return [companySeq isEqualToString:@"18537"] && [affiliatedCode isEqualToString:@"18541"];
    } else if ([selectedLoginGroup isEqualToString:@"예가람저축은행"]) {
        return [companySeq isEqualToString:@"18537"] && [affiliatedCode isEqualToString:@"18542"];
    } else if ([selectedLoginGroup isEqualToString:@"티브로드"]) {
        return [companySeq isEqualToString:@"18543"] && (
                                                         [affiliatedCode isEqualToString:@"0"] ||
                                                         [affiliatedCode isEqualToString:@"18543"] ||
                                                         [affiliatedCode isEqualToString:@"18583"] ||
                                                         [affiliatedCode isEqualToString:@"18584"] ||
                                                         [affiliatedCode isEqualToString:@"18585"] ||[affiliatedCode isEqualToString:@"18586"] ||
                                                         [affiliatedCode isEqualToString:@"18587"] ||
                                                         [affiliatedCode isEqualToString:@"18588"]);
    } else if ([selectedLoginGroup isEqualToString:@"kdmc"]) {
        return [companySeq isEqualToString:@"18543"] && [affiliatedCode isEqualToString:@"18547"];
    } else if ([selectedLoginGroup isEqualToString:@"티캐스트"]) {
        return [companySeq isEqualToString:@"18543"] && (
                                                         [affiliatedCode isEqualToString:@"18545"] ||
                                                         [affiliatedCode isEqualToString:@"18589"] ||
                                                         [affiliatedCode isEqualToString:@"18590"]);
    } else if ([selectedLoginGroup isEqualToString:@"kct"]) {
        return [companySeq isEqualToString:@"18543"] && [affiliatedCode isEqualToString:@"18546"];
    } else if ([selectedLoginGroup isEqualToString:@"tsis"]) {
        return [companySeq isEqualToString:@"18548"] && (
                                                         [affiliatedCode isEqualToString:@"0"] ||
                                                         [affiliatedCode isEqualToString:@"18548"] ||
                                                         [affiliatedCode isEqualToString:@"18591"] ||
                                                         [affiliatedCode isEqualToString:@"18592"]);
    } else if ([selectedLoginGroup isEqualToString:@"동림건설"]) {
        return [companySeq isEqualToString:@"18548"] && [affiliatedCode isEqualToString:@"18578"];
    } else if ([selectedLoginGroup isEqualToString:@"메르뱅"]) {
        return [companySeq isEqualToString:@"18548"] && [affiliatedCode isEqualToString:@"18552"];
    } else if ([selectedLoginGroup isEqualToString:@"에스티임"]) {
        return [companySeq isEqualToString:@"18548"] && [affiliatedCode isEqualToString:@"18551"];
    } else if ([selectedLoginGroup isEqualToString:@"태광CC"]) {
        return [companySeq isEqualToString:@"18548"] && [affiliatedCode isEqualToString:@"18549"];
    } else if ([selectedLoginGroup isEqualToString:@"한국도서보급"]) {
        return [companySeq isEqualToString:@"18548"] && [affiliatedCode isEqualToString:@"18550"];
    }
    
    return NO;
}

// callback for the login result
- (void) callbackAfterLogin:(NSNumber*)resultLogin {
    BOOL result = [resultLogin boolValue];
    
    [mask removeFromSuperview];
    [indicator stopAnimating];
    
    self.buttonLogin.userInteractionEnabled = YES;
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];

    if (result) {
        [app.profileUser setValue:_idPrefixString forKey:@"useridPrefix"];
        [app profileUserInfoSave];
        self.textPassword.text = @"";
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration: 1.f];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(setBarController)];
        [UIView commitAnimations];
        
    }
    else {
        if ([app isNotInternet]) {
            [ModalAlert notify:@"인터넷에 연결되어 있지 않습니다.\n인터넷 연결을 확인해 주세요."];
        }
        else {
            [ModalAlert notify:@"아이디나 비번 또는 회사를 확인하세요!"];
        }
    }
}

- (void)setBarController {
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    if ([[_idPrefixString lowercaseString] isEqualToString:@"tk-"]) {
        [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#e0e0e0" selectedTintColorRGBHex:@"#a07c40" unSelectedTintColorRGBHex:@"#a6a6a6" naviBarColorRGBHex:@"#b49859"];
    }
    else if ([[_idPrefixString lowercaseString] isEqualToString:@"hk-"]) {
        [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#e0dedf" selectedTintColorRGBHex:@"#351b72" unSelectedTintColorRGBHex:@"#a5a5a5" naviBarColorRGBHex:@"#ed008c"];
    }
    else if ([[_idPrefixString lowercaseString] isEqualToString:@"tb-"]) {
        [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#e0e0e0" selectedTintColorRGBHex:@"#f9b041" unSelectedTintColorRGBHex:@"#a6a6a6" naviBarColorRGBHex:@"#de0175"];
    }
    else if ([[_idPrefixString lowercaseString] isEqualToString:@"ts-"]) {
        [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#e0e0e0" selectedTintColorRGBHex:@"#20348b" unSelectedTintColorRGBHex:@"#a6a6a6" naviBarColorRGBHex:@"#01aef0"];
    }
    else {
        [app settingtabBarControllerDefault:NO isImagine:NO useDefaultNaviStyle:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backgroundTab:(id)sender{
    [_textID resignFirstResponder];
    [_textPassword resignFirstResponder];
    [self toggleControllBoxPosition:NO];
}

- (void)viewDidUnload {
    [self setOfflineButton:nil];
    [self setViewImageBackground:nil];
    [self setViewImageLoginBox:nil];
    [self setTextID:nil];
    [self setTextPassword:nil];
    [self setButtonLogin:nil];
    [self setButtonSaveUserid:nil];
    [self setButtonAutoLogin:nil];
    
    indicator = nil;
    viewImageCover = nil;
    
    [super viewDidUnload];
}


- (BOOL) shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)offlineClick:(id)sender {
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] offlineStudy];
}

- (IBAction)clickedSaveUserId:(id)sender {
    [self.buttonSaveUserid setSelected:!self.buttonSaveUserid.isSelected];
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    [app setNoticeOption:self.buttonSaveUserid.isSelected withKey:@"saveid"];
}

- (IBAction)clickedAutoLogin:(id)sender {
    [self.buttonAutoLogin setSelected:!self.buttonAutoLogin.isSelected];
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    [app setNoticeOption:self.buttonAutoLogin.isSelected withKey:@"autologin"];
}

@end
