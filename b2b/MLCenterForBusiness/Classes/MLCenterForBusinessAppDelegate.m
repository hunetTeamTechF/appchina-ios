#import "MLCenterForBusinessAppDelegate.h"
#import "LoginViewController.h"
#import	"Reachability.h"
#import "ModalAlert.h"
#import "CJSONDeserializer.h"
#import "Util.h"
#import "MoviePlayerController.h"
#import "DownloadViewController.h"
#import "DownloadIndexViewController.h"
#import "DownloadIndexViewSangController.h"
#import "HomeViewController.h"
#import "LectureViewController.h"
#import "SelfStudyViewController.h"
#import "StudyStateViewController.h"
#import "MyViewController.h"
#import "CounselViewController.h"
#import "MoreViewController.h"
#import "SSViewController.h"
#import "HStoryViewController.h"
#import "HStoryDetailViewController.h"
#import "HReminderViewController.h"
#import "Header.h"
#import "HLeaderController.h"
#import "Global.h"
#import "NoticeViewController.h"
#import "MoreWebViewController.h"
#import "Util.h"
#import "PasswordViewController.h"
#import "BarDecorator.h"
#import "GraphicsUtile.h"
#import "VersionChecher.h"
#import "BPush.h"
//#import <Fabric/Fabric.h>
//#import <Crashlytics/Crashlytics.h>

#define ENCODE(X) [(X) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]


@implementation UINavigationController (Rotation_IOS6)


-(BOOL)shouldAutorotate
{
    return [[self.viewControllers firstObject] shouldAutorotate];
}

-(NSUInteger)supportedInterfaceOrientations
{
    return [[self.viewControllers firstObject] supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return [[self.viewControllers firstObject] preferredInterfaceOrientationForPresentation];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return [[self.viewControllers firstObject] preferredStatusBarStyle];
}

@end

@implementation UITabBarController (autoRotate)

- (BOOL)shouldAutorotate {
    return [self.selectedViewController shouldAutorotate];
}
- (NSUInteger)supportedInterfaceOrientations {
    return [self.selectedViewController supportedInterfaceOrientations];
}

@end

@implementation MLCenterForBusinessAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


@synthesize managedObjectContext2 = _managedObjectContext2;
@synthesize managedObjectModel2 = _managedObjectModel2;
@synthesize persistentStoreCoordinator2 = _persistentStoreCoordinator2;
@synthesize flagAutoGo = _flagAutoGo;

@synthesize window;
@synthesize profileUser, companySeq, urlBase, urlSite, urlCenter, flagLogin, lectureUrlType, eduType, workStateType;
@synthesize moviePlayer;
@synthesize etc1, etc2, etc3, gotoUrlAfterLogin;
@synthesize pushService;
@synthesize tagPrevious;
@synthesize tagCurrent;

@synthesize courseTab;
@synthesize imagineTab;
@synthesize CheckPushOx;

- (void)dealloc {
    self.lectureUrlType = nil;
    internetReach = nil;
    //[moviePlayer release];
    //[_tabBarController release];
}

/////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Network Reachability callback implementation
// callback used by Reachability whenever status changes
- (void) reachabilityChanged: (NSNotification*)note {
    
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    // Block for 1 seconds
    [NSThread sleepForTimeInterval:1];
    if (netStatus != [curReach currentReachabilityStatus]) {
        return;
    }
    
    UIViewController *topViewController = [ModalAlert topMostViewController];
    
    switch (netStatus) {
        case NotReachable: {
            // show alert, "You are not connected to the Internet"
            [ModalAlert notify:@"网络状态不佳！.\n请先确认网络状态."];
            break;
        }
        case ReachableViaWWAN: {
            // check the wifionly
            if ([[self.profileUser objectForKey:@"threegnoti"] boolValue] == YES) {
                [ModalAlert toast:@"3G/4G链接中"];
            }
            break;
        }
        case ReachableViaWiFi:
            // ok, it's clear. nothing to do, just keep going
            if ([topViewController isKindOfClass:[DownloadViewController class]]
                || [topViewController isKindOfClass:[DownloadIndexViewController class]]
                || [topViewController isKindOfClass:[DownloadIndexViewSangController class]]) {
                
                // 서버로 전송되지 못한 학습진도 전송.
                [ModalAlert toast:@"因网络环境有变动 \n正在确认离线学习进度"];
                int submitCount = [self syncAllFaileSubmitProgress];
                // 동기화메시징
                if (submitCount > 0) {
                    [ModalAlert toast:@"离线学习进度已保存."];
                }
                else {
                    [ModalAlert toast:@"离线学习进度已保存."];
                }
            }
            break;
            
    }
}

- (CGSize)currentSize {
    CGSize viewSize = [[UIScreen mainScreen] bounds].size;
    UIInterfaceOrientation CURRENT_ORIENTATION = [[UIApplication sharedApplication] statusBarOrientation];
    if(UIInterfaceOrientationIsLandscape(CURRENT_ORIENTATION)){
        viewSize = CGSizeMake(viewSize.height, viewSize.width);
    } else {
        viewSize = CGSizeMake(viewSize.width, viewSize.height);
    }
    return viewSize;
}


- (void)maskLoading {
    self.mask = [[UIView alloc] init];
    self.mask.frame = CGRectMake(0, 0, [self currentSize].width, [self currentSize].height);
    self.mask.userInteractionEnabled = YES;
    self.mask.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.50];
    self.mask.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    [self.window.rootViewController.view addSubview:self.mask];
}

- (void)removemaskLoading
{
    [self.mask removeFromSuperview];
}

-(void) drmloadingView:(id)delegate {
    self.loading = [[DownloadingView alloc] initWithNibName:@"DownloadingView" bundle:nil];
    self.loading.delegate = delegate;
    
    //    if ([delegate isKindOfClass:[SSViewController class]]) {
    //        SSViewController *s = (SSViewController*)delegate;
    //        [s.view addSubview:self.loading.view];
    //    }
    //    else if ([delegate isKindOfClass:[HLeaderController class]]) {
    //        HLeaderController *h = (HLeaderController*)delegate;
    //        [h.view addSubview:self.loading.view];
    //    }
    
    if ([delegate isKindOfClass:[UIViewController class]]) {
        UIViewController *viewController = (UIViewController*)delegate;
        [viewController.view addSubview:self.loading.view];
    }
    else
        [self.window.rootViewController.view addSubview:self.loading.view];
    
    self.loading.view.center = self.window.rootViewController.view.center;
}

-(void)drmremoveLodingView {
    [self.loading.view removeFromSuperview];
}

+ (MLCenterForBusinessAppDelegate *)sharedAppDelegate
{
    return (MLCenterForBusinessAppDelegate *) [UIApplication sharedApplication].delegate;
}


- (void) threadAutoLogin:(id)somedata {
    NSString *param = @"";
    NSString *some = [NSString stringWithFormat:@"%@", somedata];
    
    if ([some isEqualToString:@"sso_login"]) {
        param = [NSString stringWithFormat:@"type=24&etc1=%@&etc2=%@&etc3=%@", self.etc1, self.etc2, self.etc3];
        //result = [self loginSSOForEtc1:self.etc1 withEtc2:self.etc2 withEtc3:self.etc3];
    }
    else
    {
        NSString *uid = [self.profileUser objectForKey:@"userid"];
        NSString *passwd = [[Global sharedSingleton] encodePassword:[self.profileUser objectForKey:@"password"]];
        NSString *tk = [self.profileUser objectForKey:@"deviceToken"];
        param = [NSString stringWithFormat:@"type=1&uid=%@&pw=%@&token=%@", uid, passwd, tk];
    }
    BOOL result = [self loginForParam:param];
    
    [self performSelectorOnMainThread:@selector(callbackAfterAutoLogin:) withObject:[NSNumber numberWithBool:result] waitUntilDone:NO];
    
    
}

// callback for the autologin result
- (void) callbackAfterAutoLogin:(NSNumber*)resultLogin {
    BOOL result = [resultLogin boolValue];
    if (result) {
        // login success
        [self initializeMainView];
    }
    else {
        // auto login failure
        if ([self isNotInternet]) {
            [ModalAlert notify:@"网络不稳定！请先确认网络状态."];
        }
        else {
            [ModalAlert notify:@"登陆失败. 请输入正确的用户信息！"];
        }
        [self initializeMainView];
    }
}

- (BOOL)loginForParam:(NSString *)param
{
#if dunkinschool_16060 || bossi_16794
    param = [param stringByAppendingString:@"&domain=1"];
#elif baskinschool_16060
    param = [param stringByAppendingString:@"&domain=2"];
#endif
    
    NSError *errorReq = nil;
    BOOL fLoginResult = NO;
    NSLog(@"AppDelegate loginForParam: %@?%@",self.urlBase, param);
    
    NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:self.urlBase] andQuery:param andError:&errorReq];
    if (jsonData)
    {
        NSError *errorJSON = nil;
        NSDictionary *result = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
        
        NSString *PushUserId = [BPush getUserId];
        NSString *PushChannelId = [BPush getChannelId];
        NSString *PushAppId = [BPush getAppId];
        
        if (PushUserId == nil) {
            [BPush bindChannelWithCompleteHandler:^(id results, NSError *error) {
                // 绑定返回值
                NSLog(@"%@",error);
                NSLog(@"%@",[NSString stringWithFormat:@"Bind Message: %@\n%@",BPushRequestMethodUnbind,result]);
            }];
            PushUserId = [BPush getUserId];
            PushChannelId = [BPush getChannelId];
            PushAppId = [BPush getAppId];
        }
        
        if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
            NSString *uid           = [[result objectForKey:@"UserId"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            NSString *pw            = [[result objectForKey:@"Password"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            self.companySeq         = [result objectForKey:@"CompanySeq"];
            self.urlCenter          = [result objectForKey:@"Url"];
            self.drmUseType         = [result objectForKey:@"DrmUseType"];
            self.eduType            = [result objectForKey:@"EduType"];
            self.hReminderUseType   = [result objectForKey:@"HReminderUseType"];
            self.hStoryUseType      = [result objectForKey:@"HStoryUseType"];
            self.bSSPlayerSpeedBar  = [[result objectForKey:@"EnableSSPlayerSpeedBar"] isEqualToString:@"Y"];
            fLoginResult            = YES;
            self.flagLogin          = YES;
            
            [self.profileUser setObject:uid forKey:@"userid"];
            [self.profileUser setObject:[[Global sharedSingleton] encodePassword:pw] forKey:@"password"];
            [self.profileUser setObject:[NSNumber numberWithBool:self.bSSPlayerSpeedBar] forKey:@"EnableSSPlayerSpeedBar"];
            
            
            
            NSString *tagsString =  [NSString stringWithFormat:@"tag%@%@",[result objectForKey:@"CompanySeq"],[result objectForKey:@"WorkAreaCode"]];
            
            [self.profileUser setObject:tagsString forKey:@"WorkAreaCode"];
            
            NSString *PushParam = [NSString stringWithFormat:@"language_cd=zh-cn&os=apple&uid=%@&appId=%@&PushUser_id=%@&ChannelId=%@&comSeq=%@", uid, PushAppId, PushUserId,PushChannelId,[result objectForKey:@"CompanySeq"]];
            NSString *PushClientSaveUrl = [NSString stringWithFormat:@"%@/SavePushClientInfo",self.urlBase];
            NSData *PushjsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:PushClientSaveUrl] andQuery:PushParam andError:&errorReq];
            
            
            [Util updateDownloadInfo:[self.companySeq intValue] andProcessMenuAlias:[result objectForKey:@"ProcessMenuAlias"] andImagineMenuAlias:[result objectForKey:@"ImagineMenuAlias"]
                    andOnlyImagineYn:[result objectForKey:@"OnlyImagineYn"] andTabBgColr:@""];
        } else {
            fLoginResult = NO;
            self.workStateType = [result objectForKey:@"WorkStateType"];
            self.companySeq = @"";
        }
    }
    return fLoginResult;
}



//ect1=companySeq, etc2=uid, ect3=공통ID

- (BOOL) loginSSOForEtc1:(NSString*)etcVal1 withEtc2:(NSString*)etcVal2 withEtc3:(NSString*)etcVal3
{
    [self.profileUser setObject:@"" forKey:@"userid"];
    [self.profileUser setObject:@"" forKey:@"password"];
    NSString *param = [NSString stringWithFormat:@"type=24&etc1=%@&etc2=%@&etc3=%@", etcVal1, etcVal2, etcVal3];
    BOOL result = [self loginForParam:param];
    return result;
}


- (BOOL)loginForID:(NSString*)userid withPassword:(NSString*)password
{
    NSString *uid       = userid;
    NSString *passwd    = [[Global sharedSingleton] encodePassword:password];
    NSString *tk        = [self.profileUser objectForKey:@"deviceToken"];
    NSString *param     = [NSString stringWithFormat:@"type=1&uid=%@&pw=%@&token=%@", uid, passwd, tk];
    BOOL result         = [self loginForParam:param];
    return result;
}

- (void) initializeMainView {
#ifndef emart_13037
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:20.f];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(setRootViewController)];
    [UIView commitAnimations];
#endif
    
    [self checkVersion];
}

- (void) openTrainingInstitute {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 1.f];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(setRootViewController)];
    [UIView commitAnimations];
}

- (void)setRootViewController {
    
    if (self.flagLogin) {
        [self settingtabBarController];
        [viewLogin removeFromParentViewController];
        viewLogin = nil;
    } else {
        [viewLogin.viewImageCover removeFromSuperview];
#ifdef PRO
        NSString *uid = [self.profileUser objectForKey:@"userid"];
        viewLogin.textID.text = [NSString stringWithFormat:@"%@", uid];
#endif
    }
    
    if ([self.etc1 length] > 0) {
#if gg_9242 || khnp_4413
        [self setSelectedTabIndex:1];
#else
        [self setSelectedTabIndex:2];
#endif
        
    }
}

- (void)setTabBarAndNavigationBarWihIsFiveTabes:(BOOL)isFiveTabs isImagine:(BOOL)hasImagineTab tabBarColorRGBHex:(NSString*)tabBarColorRGBHex selectedTintColorRGBHex:(NSString*)selectedTintColorRGBHex unSelectedTintColorRGBHex:(NSString*)unSelectedTintColorRGBHex naviBarColorRGBHex:(NSString*)naviBarColorRGBHex {
    
    UIColor *navBarColor = [MLCenterForBusinessAppDelegate colorFromHexString:naviBarColorRGBHex];                  // 네비게이션 바탕 색상
    UIColor *barTintColor = [MLCenterForBusinessAppDelegate colorFromHexString:tabBarColorRGBHex];                  // 탭바 바탕 색상
    UIColor *selectedTintColor = [MLCenterForBusinessAppDelegate colorFromHexString:selectedTintColorRGBHex];       // 선택된 아이콘 색상
    UIColor *unSelectedTintColor = [MLCenterForBusinessAppDelegate colorFromHexString:unSelectedTintColorRGBHex];   // 선택안된 아이콘 색상
    
    [self settingtabBarControllerDefault:isFiveTabs isImagine:hasImagineTab useDefaultNaviStyle:NO];
    
    [GraphicsUtile setTabBarColor:self.tabBarController.tabBar barColor:barTintColor selectedIconColor:selectedTintColor unselectedIconColor:unSelectedTintColor];
    [GraphicsUtile setNavigationBarColor:navBarColor];
    
}

- (void)setRootViewSangDownloadCenter:(NSString*)goodsId
                        andContractNo:(int)contractNo
                       andContentsSeq:(int)contentsSeq
                         andSeGoodsId:(NSString*)seGoodsId
                     andExpirationDay:(int)expirationDay {
    
    if (self.flagLogin) {
        //[self settingtabBarController];
        
        UINavigationController *navi = nil;
        
#ifdef emart_13037
        if (NO)
#else
            if ([self.eduType isEqualToString:@"LearningAndImagine"])
#endif
            {
                self.tabBarController.selectedIndex = 4;
                navi = (UINavigationController*)self.tabBarController.selectedViewController;
                MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
                DownloadViewController *second = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
                DownloadIndexViewSangController *third = [[DownloadIndexViewSangController alloc] initWithNibName:@"DownloadIndexViewSangController" bundle:nil];
                third.goodsId = goodsId;
                third.contractNo = contractNo;
                third.contentsSeq = contentsSeq;
                third.seGoodsId = seGoodsId;
                third.userId = [self.profileUser objectForKey:@"userid"];
                third.expirationDay = expirationDay;
                
                NSArray *controllersArray = [NSArray arrayWithObjects: first, second, third, nil];
                [navi setViewControllers:controllersArray animated:YES];
            }
            else
            {
                self.tabBarController.selectedIndex = 3;
                
                navi = (UINavigationController*)self.tabBarController.selectedViewController;
                DownloadViewController *first = [[navi viewControllers] objectAtIndex:0];
                DownloadIndexViewSangController *second = [[DownloadIndexViewSangController alloc] initWithNibName:@"DownloadIndexViewSangController" bundle:nil];
                second.goodsId = goodsId;
                second.contractNo = contractNo;
                second.contentsSeq = contentsSeq;
                second.seGoodsId = seGoodsId;
                second.userId = [self.profileUser objectForKey:@"userid"];
                second.expirationDay = expirationDay;
                
                NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
                [navi setViewControllers:controllersArray animated:YES];
            }
    }
}

- (void)offlineStudy {
    self.flagLogin = NO;
    self.drmUseType = @"Y";
    
    [self.window.rootViewController removeFromParentViewController];
    UITableViewController *viewController = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"닫기" style:UIBarButtonItemStyleDone target:viewController action:@selector(closeViewController)];
    viewController.navigationItem.hidesBackButton = YES;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    [[self topMostController] presentViewController:nav animated:YES completion:nil];
}

- (void)onlineStudyDownload:(NSString*)courseCd andTakeCourseSeq:(int)takeCourseSeq{
    self.flagLogin = YES;
    self.drmUseType = @"Y";
    
    [self.window.rootViewController removeFromParentViewController];
    DownloadViewController *viewController = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
    viewController.courseCd = courseCd;
    viewController.takeCourseSeq = [@(takeCourseSeq) stringValue];
    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"닫기" style:UIBarButtonItemStyleDone target:viewController action:@selector(closeViewController)];
    viewController.navigationItem.hidesBackButton = YES;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    [[self topMostController] presentViewController:nav animated:YES completion:nil];
}

- (void)sangStudyDownload:(NSString*)goodsId
            andContractNo:(int)contractNo
           andContentsSeq:(int)contentsSeq
             andSeGoodsId:(NSString*)seGoodsId
         andExpirationDay:(int)expirationDay {
    self.flagLogin = YES;
    self.drmUseType = @"Y";
    
    [self.window.rootViewController removeFromParentViewController];
    DownloadViewController *viewController = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
    viewController.goodsId = goodsId;
    viewController.contractNo = contractNo;
    viewController.contentsSeq = contentsSeq;
    viewController.seGoodsId = seGoodsId;
    viewController.userId = [self.profileUser objectForKey:@"userid"];
    viewController.expirationDay = expirationDay;
    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"닫기" style:UIBarButtonItemStyleDone target:viewController action:@selector(closeViewController)];
    viewController.navigationItem.hidesBackButton = YES;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    [[self topMostController] presentViewController:nav animated:YES completion:nil];
}

- (void)loginViewControllerDisplay
{
    [self.window.rootViewController.view removeFromSuperview];
    if (!viewLogin) {
        viewLogin = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        viewLogin.view.frame = [[UIScreen mainScreen] applicationFrame];
        
        viewLogin.viewImageCover.hidden = YES;
#ifdef PRO
        NSString *uid = [self.profileUser objectForKey:@"userid"];
        viewLogin.textID.text = [NSString stringWithFormat:@"%@", uid];
        //viewLogin.viewImageBackground.image =[UIImage imageNamed:@"Default"];
#endif
    }
    
    window.rootViewController = viewLogin;
}

- (void) setNoticeOption:(BOOL)option withKey:(NSString*)key
{
    [self.profileUser setObject:[NSNumber numberWithBool:option] forKey:key];
    [self profileUserInfoSave];
}

- (void) setUserProfile:(BOOL)option withKey:(NSString*)key
{
    [self.profileUser setObject:[NSNumber numberWithBool:option] forKey:key];
    [self profileUserInfoSave];
}

- (void) setSelectedTabIndex:(NSUInteger)indexSelected {
    [self setSelectedTabIndex:indexSelected indexOrUrl:@""];
}

- (void)profileUserInfoSave {
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentDirectory = [paths objectAtIndex:0];
    NSString* myPath = [documentDirectory stringByAppendingPathComponent:@"b2b.plist"];
    
    [profileUser writeToFile:myPath atomically:YES];
}


- (void) setSelectedTabIndex:(NSUInteger)indexSelected indexOrUrl:(NSString*)indexOrUrl {
    // key값이 NO이면 비밀번호변경페이지로 이동
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString * keyFindPw = @"hunet_user_changed_password";
    if ([defaults objectForKey:keyFindPw] && [[defaults objectForKey:keyFindPw] isEqualToString:@"NO"]) {
        self.tabBarController.selectedIndex = [self getTabIndexOfClass:[MoreViewController class]];
        
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
        PasswordViewController *second = [[PasswordViewController alloc] initWithNibName:@"PasswordViewController" bundle:nil];
        [navi.navigationBar setHidden:YES];
        navi.navigationBar.hidden = YES;
        first.navigationController.navigationBar.hidden = YES;
        [second.navigationController.navigationBar setHidden:YES];
        NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
        [navi setViewControllers:controllersArray animated:YES];
        
        return;
    }
    
    self.tabBarController.selectedIndex = indexSelected;
    if ([indexOrUrl isEqualToString:@""]) {
#ifdef PRO
        if (indexSelected == 1) {
            LectureViewController *view = (LectureViewController*)self.tabBarController.selectedViewController;
            view._currentUrl = @"";
        }
#endif
        [self.tabBarController.selectedViewController viewWillAppear:YES];
        return;
    }
    
    if ([self.tabBarController.selectedViewController isKindOfClass:[LectureViewController class]]) {
        LectureViewController *view = (LectureViewController*)self.tabBarController.selectedViewController;
        view._currentUrl = indexOrUrl;
        [view viewLoad];
    }
    else if ([self.tabBarController.selectedViewController isKindOfClass:[MyViewController class]]) {
        MyViewController *view = (MyViewController*)self.tabBarController.selectedViewController;
        view.urlMark = indexOrUrl;
        [view viewLoad];
    }
    return;
}

- (int)getTabIndexOfClass:(Class)class {
    int tabIndex = -1;
    NSArray *arrVC = [MLCenterForBusinessAppDelegate sharedAppDelegate].tabBarController.viewControllers;
    for (int i = 0; i < [arrVC count]; i++) {
        if ([[arrVC objectAtIndex:i] isKindOfClass:[UINavigationController class]]) {
            if ([[((UINavigationController*)[arrVC objectAtIndex:i]).viewControllers objectAtIndex:0] isKindOfClass:class]) {
                tabIndex = i;
                break;
            }
        }
        else {
            if ([[arrVC objectAtIndex:i] isKindOfClass:class]) {
                tabIndex = i;
                break;
            }
        }
    }
    return tabIndex;
}

- (void) setLectureUrlType:(NSString *)type {
    lectureUrlType = type;
}

- (void)settingtabBarController
{
    [BarDecorator setting:self];
}

- (void)settingtabBarControllerDefault:(BOOL)isFiveTabs isImagine:(BOOL)hasImagineTab useDefaultNaviStyle:(BOOL)useDefaultNaviStyle
{
    if (useDefaultNaviStyle) {
        [GraphicsUtile setNavigationBarColor:[UIColor colorWithRed:48.0/255.0 green:70.0/255.0 blue:143.0/255.0 alpha:1.0]];
    }
    
    [self.window.rootViewController removeFromParentViewController];
    if (_tabBarController != nil) {
        _tabBarController = nil;
    }
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.delegate = self;
    self.window.rootViewController = self.tabBarController;
    
    UINavigationController *navDownload = [Util getNavigationController:[DownloadViewController class] initWithStyle:UITableViewStylePlain];
    //navDownload.tabBarItem.titlePositionAdjustment = UIOffsetMake(2, -16);
    //tabBarItem.titlePositionAdjustment
    
    
    UINavigationController *navMore = [Util getNavigationController:[MoreViewController class] withNibName:@"MoreViewController"];
    
#ifdef avene
    navDownload.tabBarItem.titlePositionAdjustment = UIOffsetMake(2, -16);
    navMore.tabBarItem.titlePositionAdjustment = UIOffsetMake(2, -16);
#endif
    if (hasImagineTab) {
        if (isFiveTabs) {
            self.tabBarController.viewControllers = @[
                                                      [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                                      [[LectureViewController alloc] initWithNibName:@"LectureViewController" bundle:nil],
                                                      [[SelfStudyViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                                      [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                                      navMore
                                                      ];
        }
        else {
            self.tabBarController.viewControllers = @[
                                                      [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                                      [[SelfStudyViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                                      [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                                      navMore
                                                      ];
        }
    }
    else {
        if (isFiveTabs) {
            self.tabBarController.viewControllers = @[
                                                      [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                                      [[LectureViewController alloc] initWithNibName:@"LectureViewController" bundle:nil],
                                                      [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                                      navDownload,
                                                      navMore
                                                      ];
        }
        else {
            self.tabBarController.viewControllers = @[
                                                      [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                                      [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                                      navDownload,
                                                      navMore
                                                      ];
        }
    }
    
    [self performSelector:@selector(pushServiceURL) withObject:nil afterDelay:0.5];
}

//바로학습 클릭시
- (BOOL)moviePlayCheck {
    
    Reachability *isConnect = [Reachability reachabilityForInternetConnection];
    
    //셀룰러 데이터 (3G,4G) 접속
    if ([isConnect currentReachabilityStatus] == ReachableViaWWAN)
    {
        //설정 > 동영상 강의 시청 == NO
        if ([[self.profileUser objectForKey:@"threegvideo"] boolValue] == NO)
        {
            [ModalAlert notify:NSLocalizedString(@"wifiSettingMessage", @"因您限制使用3G/4G网络学习，不能观看视频 .\n(要继续学习请在”设置“－>”环境设置“菜单内允许3G/4G网络的使用.")];
            return NO;
        }
        
        //셀룰러 데이터 동영상 강의 시청 == YES && 셀룰러 데이터 접속알림 == YES
        if ([[self.profileUser objectForKey:@"threegnoti"] boolValue] == YES)
        {
            return [ModalAlert ask:@"您正在使用的网络是3G/4G网络。利用该网络观看视频会使用您的流量。（可在“设置”－》“环境设置”菜单内限制使用3G/4G网络）"];
        }
        
    }
    
    return YES;
}

-(BOOL)isNotInternet {
    Reachability *isConnect = [Reachability reachabilityForInternetConnection];
    if ([isConnect currentReachabilityStatus] == NotReachable) {
        return YES;
    }
    return NO;
}

-(BOOL)isWiFi {
    Reachability *isConnect = [Reachability reachabilityForInternetConnection];
    //	if ([isConnect currentReachabilityStatus] != ReachableViaWiFi) {
    //#if nhlife_14384
    //        return [ModalAlert ask:@"현재 3G/4G망에서 접속중으로 별도의 무선데이터 이용료가 발생할 수 있습니다.\n진행 하시겠습니까?"];
    //#else
    //        return [ModalAlert ask:@"현재 3G/4G망에 접속중으로 별도의 무선데이터 이용료가 발생할 수 있습니다.\n(본 알림은 더보기(설정, 고객센터) > 환경설정에서 취소가능합니다."];
    //#endif
    //    }
    
    //셀룰러 데이터 (3G,4G) 접속
    if ([isConnect currentReachabilityStatus] == ReachableViaWWAN)
    {
        //설정 > 동영상 강의 시청 == NO
        if ([[self.profileUser objectForKey:@"threegvideo"] boolValue] == NO)
        {
            [ModalAlert notify:NSLocalizedString(@"wifiSettingMessage", @"因您限制使用3G/4G网络学习，不能观看视频 .\n(要继续学习请在”设置“－>”环境设置“菜单内允许3G/4G网络的使用.")];
            return NO;
        }
        
        //셀룰러 데이터 동영상 강의 시청 == YES && 셀룰러 데이터 접속알림 == YES
        if ([[self.profileUser objectForKey:@"threegnoti"] boolValue] == YES)
        {
            return [ModalAlert ask:@"您正在使用的网络是3G/4G网络。利用该网络观看视频会使用您的流量。（可在“设置”－》“环境设置”菜单内限制使用3G/4G网络）"];
        }
        
    }
    
    return YES;
    
}

-(void)initAndPlayMovie:(NSURL *)movieURL
{
    MPMoviePlayerController *mp = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
    if (mp)
    {
        self.moviePlayer = mp;
    }
}

- (void) clearNotifications {
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

#pragma mark -
#pragma mark Push
- (void)registerPushNotification {
    // iOS 8.0 이상
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        // use registerUserNotificationSettings
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    } else {
        // use registerForRemoteNotificationTypes:
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
}

//push : APNS 에 장치 등록 성공시 자동실행
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //NSLog(@"deviceToken : %@", deviceToken);
    //    NSString *dt = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    //    [self.profileUser setObject:dt forKey:@"deviceToken"];
    //
    //    NSLog(@"token : %@", dt);
    [BPush registerDeviceToken:deviceToken];
    [BPush bindChannelWithCompleteHandler:^(id result, NSError *error) {
        //        [self.viewController addLogString:[NSString stringWithFormat:@"Method: %@\n%@",BPushRequestMethodBind,result]];
        // 需要在绑定成功后进行 settag listtag deletetag unbind 操作否则会失败
        if (error) {
            NSLog(@"网络错误");
            return;
        }
        
        // 其他错误
        if ([result[@"error_code"]intValue]!=0) {
            int errorCode = [result[@"error_code"]intValue];
            NSLog(@"%d",errorCode);
            return;
        }
        
        if (result) {
            [BPush setTag:@"Mytag" withCompleteHandler:^(id result, NSError *error) {
                //NSString *ChannelId = result
                if (result) {
                    NSLog(@"设置tag成功");
                }
            }];
        }
    }];
}

//push : APNS 에 장치 등록 오류시 자동실행
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"DeviceToken 获取失败，原因：%@",error);
}


//push : 어플 실행중에 알림도착
//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
//    [self application:application badge:userInfo];
//
//    [UIApplication sharedApplication].applicationIconBadgeNumber=1;
//    if (userInfo != nil)
//        self.pushService = userInfo;
//
//    if (self.flagLogin)
//    {
//        NSString *_alert        = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
//        UIAlertView *pushAlert  = [[UIAlertView alloc] initWithTitle:@"消息"
//                                                             message:_alert
//                                                            delegate:self
//                                                   cancelButtonTitle:nil
//                                                   otherButtonTitles:@"关闭", @"确认", nil];
//        pushAlert.tag = 2;
//        [pushAlert show];
//    }
//}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    [self application:application badge:userInfo];
    [UIApplication sharedApplication].applicationIconBadgeNumber=1;
    if (userInfo != nil)
        self.pushService = userInfo;
    
    if (self.flagLogin)
    {
        NSString *_alert        = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        UIAlertView *pushAlert  = [[UIAlertView alloc] initWithTitle:@"消息"
                                                             message:_alert
                                                            delegate:self
                                                   cancelButtonTitle:nil
                                                   otherButtonTitles:NSLocalizedString(@"close", @""), NSLocalizedString(@"confirm", @""), nil];
        pushAlert.tag = 2;
        [pushAlert show];
    }
    
    if(application.applicationState == UIApplicationStateInactive) {
        
        NSLog(@"Inactive");
        
        //Show the view with the content of the push
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    } else if (application.applicationState == UIApplicationStateBackground) {
        
        NSLog(@"Background");
        
        //Refresh the local model
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    } else {
        
        NSLog(@"Active");
        
        //Show an in-app banner
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    }
    
    
    
    
}



#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //[Fabric with:@[[Crashlytics class]]];
    
    
#if avene
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Helvetica" size:13.0], UITextAttributeFont,nil] forState:UIControlStateNormal];
#endif
    
    
    if (launchOptions != nil)
    {
        NSDictionary* dictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (dictionary != nil)
        {
            NSLog(@"Launched from push notification: %@", dictionary);
            
            [self clearNotifications];
        }
    }
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;  //핸드폰이 잠들지 않게
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    [Util createDirecotryBy:@"hunet_docroot"];                  //다운로드 될 폴더 지정
    self.isRuningMovieController = NO;
    [self AddAttributeToAllFolder];
    
    NSDictionary *userInfo = [launchOptions objectForKey:
                              UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if(userInfo != nil) {
        [self application:application badge:userInfo];
        self.pushService = userInfo;
    }
    
    [self registerPushNotification];
    
    
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    self.companySeq = @"";
    
    //self.urlBase = @"http://mlc.hunet.co.kr/App/JLog.aspx";
    //self.urlSite = @"http://mlc.hunet.co.kr/";
    
    self.urlBase = @"http://b2bapp.xiunaichina.com/JLog";
    self.urlSite = @"http://b2bapp.xiunaichina.com/";
    
    // Deve서버 접속
    //    self.urlBase = @"http://172.20.80.100:5555/JLog";
    //    self.urlSite = @"http://172.20.80.100:5555/";
    
    
    self.profileUser = nil;
    
    viewLogin = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    viewLogin.view.frame = [[UIScreen mainScreen] applicationFrame];
    
    window.rootViewController = viewLogin;
    [window makeKeyAndVisible];
    
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentDirectory = [paths objectAtIndex:0];
    NSString* myPath = [documentDirectory stringByAppendingPathComponent:@"b2b.plist"];
    
    //self.profileUser = [NSDictionary dictionaryWithContentsOfFile:myPath];
    self.profileUser = [NSMutableDictionary dictionaryWithContentsOfFile:myPath];
    
    if (self.profileUser == nil) {		// if there is no profile file, create one
        // create the user profile file
        self.profileUser = [[NSMutableDictionary alloc]
                            initWithDictionary:[NSDictionary dictionaryWithObjectsAndKeys:
                                                @"", @"userid",
                                                @"", @"useridPrefix",
                                                @"", @"password",
                                                @"", @"deviceToken",
                                                [NSNumber numberWithBool:YES], @"autologin",
                                                [NSNumber numberWithBool:YES], @"saveid",
#if nhlife_14384 || sisul_family_18009 || seoulmilk_6928
                                                [NSNumber numberWithBool:YES], @"threegnoti",
                                                [NSNumber numberWithBool:YES], @"threegvideo",
#else
                                                [NSNumber numberWithBool:NO], @"threegnoti",
                                                [NSNumber numberWithBool:NO], @"threegvideo",
#endif
                                                [NSNumber numberWithBool:NO], @"IsStaging",
                                                [NSNumber numberWithBool:NO], @"schedulenoti",
                                                [NSNumber numberWithBool:NO], @"schedule_monday",
                                                [NSNumber numberWithBool:NO], @"schedule_tuesday",
                                                [NSNumber numberWithBool:NO], @"schedule_wednesday",
                                                [NSNumber numberWithBool:NO], @"schedule_thursday",
                                                [NSNumber numberWithBool:NO], @"schedule_friday",
                                                [NSNumber numberWithBool:NO], @"schedule_saturday",
                                                [NSNumber numberWithBool:NO], @"schedule_sunday",
                                                [NSNumber numberWithBool:YES], @"EnableSSPlayerSpeedBar", nil]];
        
    }
    
    [profileUser writeToFile:myPath atomically:YES];
    
    NSString *_urlBase = self.urlBase;
    NSString *_urlSite = self.urlSite;
    
    self.urlBase = [[Global sharedSingleton] testPrimaryDomainUrl:_urlBase];
    self.urlSite = [[Global sharedSingleton] testPrimaryDomainUrl:_urlSite];
    
    //	[[NSNotificationCenter defaultCenter] addObserver: self
    //											 selector: @selector(reachabilityChanged:)
    //												 name: kReachabilityChangedNotification
    //											   object: nil];
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    
    //application.applicationIconBadgeNumber = 0;
    
    // Handle launching from a notification
    //	UILocalNotification *localNotif =
    //	[launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    //    if (localNotif) {
    //		//NSLog(@"Recieved Notification %@",localNotif);
    //	}
    // Baidu Push 시작
    // iOS8 下需要使用新的 API
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        UIUserNotificationType myTypes = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:myTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }else {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
    }
    
    [BPush registerChannel:launchOptions apiKey:@"bCMxkIGmXwFgg1i8lnOXSGDs" pushMode:BPushModeDevelopment withFirstAction:nil withSecondAction:nil withCategory:nil isDebug:YES];
    //[BPush registerChannel:launchOptions apiKey:@"YE3HlDNzlsml6GPHoTb6tEOw" pushMode:BPushModeProduction withFirstAction:nil withSecondAction:nil withCategory:nil isDebug:YES];
    // App 是用户点击推送消息启动
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    // Baidu Push 종료
    
    
    NSURL *url = [launchOptions objectForKey:UIApplicationLaunchOptionsURLKey];
    if(url) {
        [self application:application handleOpenURL:url];
    }
    
    if (self.etc1 != nil && self.etc2 != nil && self.etc3 != nil) {
        [NSThread detachNewThreadSelector:@selector(threadAutoLogin:) toTarget:self withObject:@"sso_login"];
        return NO;
    }
    
    if ([[self.profileUser objectForKey:@"autologin"] boolValue] && [[self.profileUser objectForKey:@"userid"] length] > 0 && [[self.profileUser objectForKey:@"password"] length] > 0) {
        //flagLogin = true;
        [NSThread detachNewThreadSelector:@selector(threadAutoLogin:) toTarget:self withObject:@"login"];
    }
    else {
        [self initializeMainView];
    }
    
    courseTab = @"직무교육과정";
    imagineTab = @"자율수강과정";
    
    
    //    UITabBarController *tabBarContr = (UITabBarController *)self.window.rootViewController;
    //
    //    CGFloat tabBarItemWidth = self.window.rootViewController.view.bounds.size.width / tabBarContr.tabBar.items.count;
    //    CGFloat tabBarItemHeight = CGRectGetHeight(tabBarContr.tabBar.frame);
    //
    //
    //    UIImage *selTab = [[UIImage imageNamed:@"selected_tab_bg.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    //
    //    UIGraphicsBeginImageContext(self.window.rootViewController.view.bounds.size);
    //    [selTab drawInRect:CGRectMake(0, 0, tabBarItemWidth, tabBarItemHeight)];
    //    UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
    //    UIGraphicsEndImageContext();
    //    self.tabBarController.tabBar.selectionIndicatorImage = [UIImage imageNamed:@"selected_tab_bg.png"];
    //    [self.tabBarController.tabBar setSelectionIndicatorImage:reSizeImage];
    
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    if (moviePlayer.playbackState == MPMoviePlaybackStatePlaying || moviePlayer.playbackState == MPMoviePlaybackStateInterrupted) {
        [moviePlayer pause];
    }
    
    //drm player
    if (self.moviePlayerController.playBool) {
        [self.moviePlayerController.player pause];
        self.moviePlayerController.playBool = FALSE;
        [self.moviePlayerController.playBtn setImage:[UIImage imageNamed:@"playBtn.png"] forState:UIControlStateNormal];
    }
    
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    
    [self profileUserInfoSave];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    NSLog(@"applicationWillEnterForeground");
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    //application.applicationIconBadgeNumber = 0;
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     => 어플리케이션이 활성화 될 때,
     즉 didFinishLaunchingWithOption 호출 직후, 어플리케이션이 백그라운드로 돌아갔다가 다시 불러질 때 호출
     */
    
    if (moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
        [moviePlayer pause];
    }
    
    if (self.moviePlayerController.playBool) {
        [self.moviePlayerController.player pause];
        self.moviePlayerController.playBool = FALSE;
        [self.moviePlayerController.playBtn setImage:[UIImage imageNamed:@"playBtn.png"] forState:UIControlStateNormal];
    }
    
    [self clearNotifications];
    
    // 서버로 전송되지 못한 학습진도 전송.
    if (![self isNotInternet]) {
        int submitCount = [self syncAllFaileSubmitProgress];
        // 동기화메시징
        if (submitCount > 0) {
            [ModalAlert toast:@"离线学习进度保存成功！"];
        }
    }
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate. See also applicationDidEnterBackground:.
     */
    [self saveContext];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    //tcenter://etc1=214&etc2=uid&etc3=사번
    if (!url) {  return NO; }
    
    NSString *strURL = [url absoluteString];
    if ([strURL.lowercaseString hasPrefix:@"tcenter://"]) {
        
        NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:url resolvingAgainstBaseURL:NO];
        NSArray *queryItems = urlComponents.queryItems;
        
        self.etc1 = [Util valueForKey:@"etc1" fromQueryItems:queryItems];
        self.etc2 = [Util valueForKey:@"etc2" fromQueryItems:queryItems];
        self.etc3 = [Util valueForKey:@"etc3" fromQueryItems:queryItems];
        
        if (self.etc1 == nil) {
            //녹십자 앱투앱 로그인을 위한 custom
            self.etc1 = [Util valueForKey:@"cSeq" fromQueryItems:queryItems];
            
            if ([self.etc1 isEqual:@"13830"]) {
                self.etc2 = [NSString stringWithFormat:@"gc%@", [Util valueForKey:@"userId" fromQueryItems:queryItems]];
                self.etc3 = [Util valueForKey:@"userId" fromQueryItems:queryItems];
                
                //과정상세 URL로 Redirect
                
                //http://mlc.hunet.co.kr/SSO/StudyGate.aspx?processCd=HLSP10845&processYear=2015&processTerm=1&cSeq=2162&userId=magic7k
                //processcd=hlsp03975&processyear=2012&processterm=813
                //tcenter://?cSeq=13830&processCd=HLSP03975&processYear=2015&processTerm=2200&userId=1206005
                self.gotoUrlAfterLogin = [NSString stringWithFormat:@"%@/SSO/StudyGate.aspx?processCd=%@&processYear=%@&processTerm=%i&cSeq=%@&userId=%@",
                                          self.urlSite,
                                          [Util valueForKey:@"processCd" fromQueryItems:queryItems],
                                          [Util valueForKey:@"processYear" fromQueryItems:queryItems],
                                          [[Util valueForKey:@"processTerm" fromQueryItems:queryItems] intValue],
                                          [Util valueForKey:@"cSeq" fromQueryItems:queryItems],
                                          [NSString stringWithFormat:@"gc%@", [Util valueForKey:@"userId" fromQueryItems:queryItems]]
                                          ];
            }
            else if ([self.etc1 isEqual:@"9242"])
            {
                self.etc2 = [NSString stringWithFormat:@"gg-%@", [Util valueForKey:@"userId" fromQueryItems:queryItems]];
                self.etc3 = [Util valueForKey:@"userId" fromQueryItems:queryItems];
                
                //과정상세 URL로 Redirect
                //http://mlc.hunet.co.kr/SSO/StudyGate.aspx?processCd=HLSP10845&processYear=2015&processTerm=1&cSeq=2162&userId=magic7k
                //processcd=hlsp03975&processyear=2012&processterm=813
                self.gotoUrlAfterLogin = [NSString stringWithFormat:@"%@/SSO/StudyGate.aspx?processCd=%@&processYear=%@&processTerm=%i&cSeq=%@&userId=%@",
                                          self.urlSite,
                                          [Util valueForKey:@"processCd" fromQueryItems:queryItems],
                                          [Util valueForKey:@"processYear" fromQueryItems:queryItems],
                                          [[Util valueForKey:@"processTerm" fromQueryItems:queryItems] intValue],
                                          [Util valueForKey:@"cSeq" fromQueryItems:queryItems],
                                          [NSString stringWithFormat:@"gg-%@", [Util valueForKey:@"userId" fromQueryItems:queryItems]]
                                          ];
            }
            else {
                self.etc2 = [NSString stringWithFormat:@"%@", [Util valueForKey:@"userId" fromQueryItems:queryItems]];
                self.etc3 = [Util valueForKey:@"userId" fromQueryItems:queryItems];
                
                //과정상세 URL로 Redirect
                //http://mlc.hunet.co.kr/SSO/StudyGate.aspx?processCd=HLSP10845&processYear=2015&processTerm=1&cSeq=2162&userId=magic7k
                //tcenter://processCd%3dHLSP13955%26processYear%3d2015%26processTerm%3d907%26courseCd%3dHLSC13823%26companySeq%3d4413%26userId%3dkhnp-test%26takeCourseSeq%3d
                
                //tcenter://?processCd=HLSP13955&processYear=2015&processTerm=907&cSeq=4413&userId=khnp-test
                self.gotoUrlAfterLogin = [NSString stringWithFormat:@"%@/SSO/StudyGate.aspx?processCd=%@&processYear=%@&processTerm=%i&cSeq=%@&userId=%@",
                                          self.urlSite,
                                          [Util valueForKey:@"processCd" fromQueryItems:queryItems],
                                          [Util valueForKey:@"processYear" fromQueryItems:queryItems],
                                          [[Util valueForKey:@"processTerm" fromQueryItems:queryItems] intValue],
                                          [Util valueForKey:@"cSeq" fromQueryItems:queryItems],
                                          [Util valueForKey:@"userId" fromQueryItems:queryItems]
                                          //[Util valueForKey:@"courseCd" fromQueryItems:queryItems]
                                          ];
            }
        }
        
        return YES;
    }
    return NO;
}




/**
 
 * @brief <#Description#>
 URL 스키마 호출시, 이 delegate 메서드를 호출함
 Default > tcenter://?etc1=13830&etc2={uid}&etc3={공통아이디를 제외한 ID}
 녹십자-13830 > tcenter://cSeq=13830&processCd=HLSP03975&processYear=2015&processTerm=0002&userId=1206005
 
 * @param <#Parameter#>
 
 * @return <#Return#>
 
 * @remark <#Remark#>
 http://test.mlc.hunet.co.kr/App/JLog.aspx?type=24&etc1=13830&etc2=&etc3=1206005
 {"IsSuccess":"YES","CompanySeq":13830,"Url":"http://test.visioncenter.hunet.co.kr","UserId":"gc1206005","Password":"1206005","HReminderUseType":"0","HStoryUseType":"0","EduType":"OnlyLearning"}
 
 * @see <#See#>
 
 * @author magic7k
 
 */
-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if (!url) {  return NO; }
    
    NSString *strURL = [url absoluteString];
    if ([strURL.lowercaseString hasPrefix:@"tcenter://"]) {
        
        [self application:application handleOpenURL:url];
        
        if (self.etc1 != nil && self.etc2 != nil && self.etc3 != nil) {
            [NSThread detachNewThreadSelector:@selector(threadAutoLogin:) toTarget:self withObject:@"sso_login"];
        }
        
        return YES;
    }
    return NO;
}

#pragma mark -
#pragma mark UITabBarControllerDelegate methods


// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    if (!self.flagLogin) {
        [self loginViewControllerDisplay];
    }
    
    NSUInteger indexOfTab = [tabBarController.viewControllers indexOfObject:viewController];
    
    tagPrevious = tagCurrent;
    tagCurrent = indexOfTab;
    
    UINavigationController *navController = (UINavigationController *)[tabBarController.viewControllers objectAtIndex:tabBarController.selectedIndex];
    if ([navController isKindOfClass:[UINavigationController class]])
    {
        [navController popToRootViewControllerAnimated:YES];
        NSArray *viewControllers = navController.viewControllers;
        UIViewController *rootViewController = [viewControllers objectAtIndex:0];
        if ([rootViewController isKindOfClass:[UIViewController class]])
        {
            [rootViewController viewDidLoad];
        }
    }
    else
    {
        UIViewController *view = (UIViewController*)[tabBarController.viewControllers objectAtIndex:tabBarController.selectedIndex];
        [view viewDidLoad];
    }
}

/*
 // Optional UITabBarControllerDelegate method.
 - (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed {
 }
 */

#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notif {
    // Handle the notificaton when the app is running
    
    //application.applicationIconBadgeNumber = 0;
    //NSLog(@"Recieved Notification %@",notif);
    
}

- (void)checkVersion {
    [VersionChecher checkWithUpdate];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1)
        [self performSelector:@selector(pushServiceURL) withObject:nil afterDelay:0.2];
    else
        self.pushService = nil;
}

- (void)application:(UIApplication *)application badge:(NSDictionary *)userInfo {
    NSString *badge = [[userInfo valueForKey:@"aps"] valueForKey:@"badge"];
    application.applicationIconBadgeNumber = [badge intValue];
}



- (void)pushServiceURL
{
    if (self.pushService == nil)
        return;
    
    long long userSeq                   = [[self.pushService valueForKey:@"userSeq"] unsignedLongLongValue];
    NSString *selectedTabIndex          = [self.pushService valueForKey:@"selectedTabIndex"];
    NSString *selectedTabIndexUrl       = [self.pushService valueForKey:@"selectedTabIndexUrl"];
    NSString *selectedTabIndexDetail    = [self.pushService valueForKey:@"selectedTabIndexDetail"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSError *errorReq = nil;
        NSString *url = [NSString stringWithFormat:@"%@?type=1004&userSeq=%llu&eventType=click", self.urlBase, userSeq];
        [Util getNSDataByRequestUrl:[NSURL URLWithString:url]
                           andQuery:nil
                           andError:&errorReq];
        
    });
    
    
    self.tabBarController.selectedIndex = [selectedTabIndex integerValue];
    UIViewController *view = self.tabBarController.selectedViewController;
    
    if ([view isKindOfClass:[UINavigationController class]]) {
        
        NSMutableArray *controllers                     = [[NSMutableArray alloc] init];
        UINavigationController *navigationController    = (UINavigationController*)view;
        
        if (selectedTabIndexDetail && selectedTabIndexDetail.length)
        {
            MoreViewController *moreViewController = [[navigationController viewControllers] objectAtIndex:0];
            [controllers addObject:moreViewController];
            
            if ([selectedTabIndexDetail isEqualToString:@"学员公告"])
            {
                NoticeViewController *noticeViewController = [[NoticeViewController alloc] initWithNibName:@"NoticeViewController" bundle:nil];
                noticeViewController.title  = selectedTabIndexDetail;
                noticeViewController.url    = selectedTabIndexUrl;
                [controllers addObject:noticeViewController];
            }
            
            if ([selectedTabIndexDetail isEqualToString:@"H-스토리"])
            {
                BaseViewController *hStoryViewController = [[HStoryViewController alloc]
                                                            initWithNibName:@"BaseViewController" bundle:nil];
                if (selectedTabIndexUrl && selectedTabIndexUrl.length)
                {
                    hStoryViewController.currentUrl = selectedTabIndexUrl;
                    [hStoryViewController openerCallFun];
                }
                [controllers addObject:hStoryViewController];
            }
            
            if ([selectedTabIndexDetail isEqualToString:@"H-리마인더"])
            {
                BaseViewController *hReminderViewController = [[HReminderViewController alloc]
                                                               initWithNibName:@"BaseViewController" bundle:nil];
                if (selectedTabIndexUrl && selectedTabIndexUrl.length)
                {
                    hReminderViewController.currentUrl = selectedTabIndexUrl;
                    [hReminderViewController openerCallFun];
                }
                [controllers addObject:hReminderViewController];
            }
            
            [navigationController setViewControllers:controllers animated:YES];
        }
        
    }
    else
    {
        if (selectedTabIndexUrl && selectedTabIndexUrl.length)
        {
            if ([view isKindOfClass:[HomeViewController class]]) {
                [((HomeViewController*)view) webViewpushUrl:selectedTabIndexUrl];
            }
            
            if ([view isKindOfClass:[BaseViewController class]]) {
                [((BaseViewController*)view) webViewpushUrl:selectedTabIndexUrl];
            }
            
            if ([view isKindOfClass:[LectureViewController class]]) {
                [((LectureViewController*)view) webViewpushUrl:selectedTabIndexUrl];
            }
        }
    }
    
    self.pushService = nil;
    
    /*
     NSString *_url  = [self.pushService valueForKey:@"URL"];
     NSString *_type = [self.pushService valueForKey:@"TYPE"];
     
     //s : 스토리,  r : 리마인더
     if ([_type.lowercaseString isEqualToString:@"s"]
     || [_type.lowercaseString isEqualToString:@"r"])
     {
     NSArray *controllersArray   = nil;
     self.tabBarController.selectedIndex = 4;
     UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
     navi.navigationBar.hidden = YES;
     MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
     
     //s : 스토리
     if ([_type.lowercaseString isEqualToString:@"s"])
     {
     HStoryViewController *second = [[HStoryViewController alloc] initWithNibName:@"BaseViewController" bundle:nil];
     HStoryDetailViewController *third = [[HStoryDetailViewController alloc] initWithNibName:@"BaseViewController" bundle:nil];
     third.webviewUrl = _url;
     controllersArray = [NSArray arrayWithObjects: first, second, third, nil];
     [second release]; [third release];
     }
     else if ([_type.lowercaseString isEqualToString:@"r"])
     {
     //리마인드
     HReminderViewController *second = [[HReminderViewController alloc] initWithNibName:@"BaseViewController" bundle:nil];
     controllersArray = [NSArray arrayWithObjects: first, second, nil];
     [second release];
     }
     
     [navi setViewControllers:controllersArray animated:YES];
     }
     
     if ([_type.lowercaseString isEqualToString:@"approval"]
     //|| [_type.lowercaseString isEqualToString:@"hclass"]
     )
     {
     self.tabBarController.selectedIndex = 0;
     
     UIViewController *view = self.tabBarController.selectedViewController;
     //UIViewController *view = [[BaseViewController alloc] initWithNibName:@"BaseViewController" bundle:nil];
     
     if ([view isKindOfClass:[BaseViewController class]]) {
     [((BaseViewController*)view) webViewpushUrl:_url];
     }
     }
     
     if ([_type.lowercaseString isEqualToString:	@"hclass"])
     {
     self.tabBarController.selectedIndex = 0;
     
     UIViewController *view = self.tabBarController.selectedViewController;
     
     if ([view isKindOfClass:[HomeViewController class]]) {
     [((HomeViewController*)view) webViewpushUrl:_url];
     }
     }
     
     if ([_type.lowercaseString isEqualToString:@"lecture"])
     {
     self.tabBarController.selectedIndex = 1;
     
     UIViewController *view = self.tabBarController.selectedViewController;
     
     if ([view isKindOfClass:[SelfStudyViewController class]]) {
     [((SelfStudyViewController*)view) webViewpushUrl:_url];
     }
     
     if ([view isKindOfClass:[LectureViewController class]]) {
     [((LectureViewController*)view) webViewpushUrl:_url];
     }
     }
     
     self.pushService = nil;
     */
}

//https://developer.apple.com/library/ios/qa/qa1719/_index.html
//How do I prevent files from being backed up to iCloud and iTunes?
- (void)AddAttributeToAllFolder{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsPath error:nil];
    
    for (int i =0; i < [dirContents count]; i++) {
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@",documentsPath,[dirContents objectAtIndex:i]]];
        if ([self addSkipBackupAttributeToItemAtURL:url]) {
            NSLog(@"success! could add do not backup attribute to folder");
        }
    }
}

- (void)AddAttributeToFolder:(NSString*)folderName{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@",documentsPath,folderName]];
    if ([self addSkipBackupAttributeToItemAtURL:url]) {
        NSLog(@"success! could add do not backup attribute to folder");
    }
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL{
    int result = 0;
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version < 5.1) {
        assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
        const char* filePath = [[URL path] fileSystemRepresentation];
        const char* attrName = "com.apple.MobileBackup";
        u_int8_t attrValue = 1;
        result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
        return result == 0;
    } else {
        assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
        NSError *error = nil;
        BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                      forKey: NSURLIsExcludedFromBackupKey error: &error];
        
        if(!success){
            NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
        }
        return success;
    }
}


- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}


#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"training" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"training.sqlite"];
    
    // handle db upgrade
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        // Handle error
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext2
{
    if (_managedObjectContext2 != nil) {
        return _managedObjectContext2;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator2];
    if (coordinator != nil) {
        _managedObjectContext2 = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext2 setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext2;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel2
{
    if (_managedObjectModel2 != nil) {
        return _managedObjectModel2;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"addinfo" withExtension:@"momd"];
    _managedObjectModel2 = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel2;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator2
{
    if (_persistentStoreCoordinator2 != nil) {
        return _persistentStoreCoordinator2;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"addinfo.sqlite"];
    
    // handle db upgrade
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    NSError *error = nil;
    _persistentStoreCoordinator2 = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel2]];
    if (![_persistentStoreCoordinator2 addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        // Handle error
    }
    
    return _persistentStoreCoordinator2;
}


#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

+ (NSDictionary *)getProtocalParams:(NSURL*)url
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSString *query = url.absoluteString;
    if ([query rangeOfString:@"://?"].location != NSNotFound) {
        query = [[query componentsSeparatedByString:@"://?"] objectAtIndex:1];
        NSArray *urlComponents = [query componentsSeparatedByString:@"&"];
        for (NSString *keyValuePair in urlComponents)
        {
            if ([keyValuePair rangeOfString:@"="].location != NSNotFound) {
                int equalIndex = (int)[keyValuePair rangeOfString:@"="].location;
                NSString *key = [[keyValuePair substringToIndex:equalIndex] stringByRemovingPercentEncoding].lowercaseString;
                NSString *value = [[keyValuePair substringFromIndex:equalIndex + 1] stringByRemovingPercentEncoding];
                
                [params setObject:value ? value : @"" forKey:key];
            }
        }
    }
    
    return params;
}

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

// 서버로 전송되지 못한 학습진도 전송.
- (int)syncAllFaileSubmitProgress {
    int submitCount = 0;
    submitCount += [Util syncAllFaileSubmitProgress]; //정규과정
    submitCount += [Util syncAllFaileSubmitProgressSang]; //상상마루
    
    return submitCount;
}

// Assumes input like "#00FF00" (#RRGGBB).
+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

/*** for WebScriptBridge Begin ***/
- (void)goToLogin
{
}
/*
 - (void)DoLogin:(NSString *)userId :(NSString *)userPwd
 {
 }
 */
- (void)DoLogin
{
}
/*** for WebScriptBridge End ***/

@end


/*** for WebScriptBridge Begin ***/
static WebScriptBridge *gWebScriptBridge = nil;

@implementation WebScriptBridge
- (void)goToLogin:(NSString *)returnUrl
{
    NSLog(@"call goToLogin from WebScriptBridge");
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    [app loginViewControllerDisplay];
}

- (void)DoLogin:(NSString *)userId :(NSString *)userPwd
{
    NSLog(@"call DoLogin from WebScriptBridge");
    if (!viewLogin) {
        viewLogin = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    }
    [viewLogin onWebLogin:userId :userPwd];
}

- (void)changePassword:(NSString *)userPwd
{
    NSLog(@"call ChangePassword from WebScriptBridge");
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    [app.profileUser setObject:userPwd forKey:@"password"];
    
    //[ModalAlert notify:@"정상적으로 수정되었습니다."];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString * keyFindPw = @"hunet_user_changed_password";
    if ([defaults objectForKey:keyFindPw] && [[defaults objectForKey:keyFindPw] isEqualToString:@"NO"]) {
        [defaults setObject:@"OK" forKey:keyFindPw];
        [app setSelectedTabIndex:0];
        app.tabBarController.tabBar.hidden = NO;
    }
    
    [app profileUserInfoSave];
    
    [app.window.rootViewController.navigationController popToRootViewControllerAnimated:YES];
    NSObject *obj = [app.tabBarController.viewControllers objectAtIndex:[app.tabBarController.viewControllers count] - 1];
    UINavigationController *nc = (UINavigationController*)obj;
    if ([nc isKindOfClass:[UINavigationController class]])
    {
        [nc popToRootViewControllerAnimated:YES];
    }
    else
    {
        [nc.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (BOOL)existsInstalledApp:(NSString *)urlScheme
{
    BOOL isInstalled = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:urlScheme]];
    
    return isInstalled;
}



/* BaiduPush Start */
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
}


- (BOOL)isStaging
{
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    return [[app.profileUser objectForKey:@"IsStaging"] boolValue];
}

+ (BOOL)isKeyExcludedFromWebScript:(const char *)name;
{
    return NO;
}

+ (BOOL)isSelectorExcludedFromWebScript:(SEL)aSelector;
{
    return NO;
}

+ (NSString *)webScriptNameForSelector:(SEL)sel
{
    if (sel == @selector(goToLogin:)) return @"goToLogin";
    if (sel == @selector(DoLogin::)) return @"DoLogin";
    if (sel == @selector(changePassword:)) return @"changePassword";
    if (sel == @selector(existsInstalledApp:)) return @"existsInstalledApp";
    if (sel == @selector(isStaging)) return @"isStaging";
    
    return nil;
}

+ (WebScriptBridge*)getWebScriptBridge {
    if (gWebScriptBridge == nil)
        gWebScriptBridge = [WebScriptBridge new];
    
    return gWebScriptBridge;
}
@end
/*** for WebScriptBridge End ***/

