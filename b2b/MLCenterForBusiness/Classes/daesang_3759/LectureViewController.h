//
//  LectureViewController.h
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 5. 31..
//  Copyright 2011 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LectureViewController : UIViewController <UIWebViewDelegate> {
	IBOutlet UIWebView *viewWeb;
	IBOutlet UIActivityIndicatorView *indicator;
	NSString *_currentUrl;
}

@property (nonatomic, strong) NSString *_currentUrl;
@property (nonatomic, strong) NSString *_param;

- (void)viewLoad;

- (void)webViewpushUrl:(NSString *)webViewpushUrl;

@end
