//
//  HomeViewController.h
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 5. 17..
//  Copyright 2011 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ELCImagePickerController.h"


@interface HomeViewController : UIViewController <ELCImagePickerControllerDelegate, UIWebViewDelegate> {
	
    NSString *imgFileName;
    UIActivityIndicatorView *uploadIndicator;
    UIView *mask;
    
	IBOutlet UIWebView *viewWeb;
	IBOutlet UIActivityIndicatorView *indicator;
}

- (void)viewLoad;

- (void)webViewpushUrl:(NSString *)webViewpushUrl;

@end
