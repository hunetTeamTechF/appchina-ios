

#import "MoreViewController.h"
#import "ModalAlert.h"
#import "CJSONDeserializer.h"
#import "MoreCell.h"
#import "LocalNotifierViewController.h"
#import "SetupViewController.h"
#import "UserViewController.h"
#import "VersionViewController.h"
#import "DownloadViewController.h"
//#import "DownloadTabViewController.h"
#import "NoticeViewController.h"
#import "QnaViewController.h"
#import "FaqViewController.h"
#import "HStoryViewController.h"
#import "HReminderViewController.h"
#import "DevOptionViewController.h"
#import "Header.h"
#import "GraphicsUtile.h"

#define ENCODE(X) [(X) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]

@implementation MoreViewController
@synthesize listMenu, linkUrl, array;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        self.title = NSLocalizedString(@"설정", @"설정");
        self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_setting"];
        devOptionClickCount = 0;
    }
    return self;
}

#pragma mark -
#pragma mark Initialization
/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    self = [super initWithStyle:style];
    if (self != nil) {
        // Custom initialization.
    }
    return self;
}
*/

#pragma mark -
#pragma mark View lifecycle

/*
- (void)cellSelected {
	
	if (![self.linkUrl isEqualToString:@""]) {
		
		int row = -1;
		if ([self.listMenu count] > 0) {
			
			for (NSDictionary *dc in self.listMenu) {
				row++;
				if ([self.linkUrl isEqualToString:[dc objectForKey:@"Url"]]) {
					
					[self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:row + 4 inSection:0]];
					break;
				}
			}
		}
	}	
}
*/


- (BOOL)isDownloadCell {
#ifdef emart_13037
    return NO;
#endif
    return [[self appDelegate].drmUseType isEqualToString:@"Y"] && [[self appDelegate].eduType isEqualToString:@"LearningAndImagine"];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithTitle:@"       "
                                                                  style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:@selector(devOptionClick:)];
    self.navigationItem.rightBarButtonItem = barButton;
    
    

    NSDictionary *a = [NSDictionary dictionaryWithObject:@"공지사항" forKey:@"0"];
    NSDictionary *b = [NSDictionary dictionaryWithObject:@"자주묻는 질문" forKey:@"1"];
    NSDictionary *c = [NSDictionary dictionaryWithObject:@"1:1 친절상담" forKey:@"2"];
    NSDictionary *d = [NSDictionary dictionaryWithObject:@"회원정보 관리" forKey:@"3"];
    NSDictionary *e = [NSDictionary dictionaryWithObject:@"스케줄 알리미" forKey:@"4"];
    NSDictionary *f = [NSDictionary dictionaryWithObject:@"환경설정" forKey:@"5"];
    //NSDictionary *g = [NSDictionary dictionaryWithObject:@"버전정보" forKey:@"6"];
    NSDictionary *h = [NSDictionary dictionaryWithObject:@"다운로드 센터" forKey:@"7"];
    NSDictionary *i = [NSDictionary dictionaryWithObject:@"H-리마인더" forKey:@"8"];
    NSDictionary *j = [NSDictionary dictionaryWithObject:@"H-스토리" forKey:@"9"];
    
    

    self.array = [[NSMutableArray alloc] init];
    [self.array addObject:a];
    [self.array addObject:b];
    [self.array addObject:c];
    [self.array addObject:d];
    [self.array addObject:e];
    [self.array addObject:f];
    
    
    if ([self isDownloadCell])
        [self.array addObject:h];
    
    if ([[self appDelegate].hReminderUseType isEqualToString:@"1"])
        [self.array addObject:i];
    
    if ([[self appDelegate].hStoryUseType isEqualToString:@"1"])
        [self.array addObject:j];
    
    [self performSelector:@selector(devOption) withObject:nil afterDelay:0.2];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self devOption];
    [self.tableView reloadData];
    self.navigationController.navigationBar.hidden = NO;    
}


#pragma mark -
#pragma mark Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return [self.array count];
    } else {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 5;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"MoreCell";
    MoreCell* cell = (MoreCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray* arr = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
        cell = [arr objectAtIndex:0];
    }
    cell.textLabel.font = [UIFont systemFontOfSize:17];

    if (indexPath.section == 0)
    {
        switch (indexPath.row) {
            case 2:
            {
                cell.labelInfo.hidden = NO;
                cell.labelInfo.text = [[[self appDelegate].profileUser objectForKey:@"schedulenoti"] boolValue] ? @"사용" : @"사용안함";
            }
                break;
            case 5:
            {
                NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
                cell.labelInfo.text = version;
                cell.labelInfo.hidden = NO;
            }
                break;
            case 0:
            case 1:
            case 3:
            case 4:
            case 6:
            case 7:
            case 8:
            case 9:
            {
                cell.labelInfo.hidden = YES;
            }
                break;
        }

        NSDictionary *dic = [self.array objectAtIndex:indexPath.row];
        NSString *key = [[dic allKeys] objectAtIndex:0];
        cell.labelMenuNm.text = [dic objectForKey:key];
        cell.tag = [key integerValue];
        
        return cell;
    }
    else if (indexPath.section == 1)
    {
        cell.labelMenuNm.text = @"로그 아웃";
        cell.labelInfo.hidden = YES;
        cell.tag = 20;
        return cell;
    }
    
	return nil;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    UITableViewCell *myCell = [tableView cellForRowAtIndexPath:indexPath];

    if (myCell.tag == 0)
    {
        NSString *_noticeUrl = [NSString stringWithFormat:@"%@?type=13&uid=%@&pw=%@",
                          [self appDelegate].urlBase,
                          [[self appDelegate].profileUser objectForKey:@"userid"],
                          [[self appDelegate].profileUser objectForKey:@"password"]];
        
        NoticeViewController *viewToPush = [[NoticeViewController alloc] initWithNibName:@"NoticeViewController" bundle:nil];
        viewToPush.title = @"공지사항";
        viewToPush.url = [self.linkUrl length] > 0 ? self.linkUrl : _noticeUrl;
        [self.navigationController pushViewController:viewToPush animated:YES];
        
    }
    
    if (myCell.tag == 1)
    {
        FaqViewController *viewToPush = [[FaqViewController alloc] initWithNibName:@"FaqViewController" bundle:nil];
        viewToPush.title = @"자주묻는 질문";
        [self.navigationController pushViewController:viewToPush animated:YES];
    }
    
    if (myCell.tag == 2)
    {
        QnaViewController *viewToPush = [[QnaViewController alloc] initWithNibName:@"QnaViewController" bundle:nil];
        viewToPush.title = @"1:1 친절상담";
        [self.navigationController pushViewController:viewToPush animated:YES];
        
    }
    
    if (myCell.tag == 3)
    {
        UserViewController *viewToPush = [[UserViewController alloc] initWithNibName:@"UserViewController" bundle:nil];
        viewToPush.title = @"회원정보 관리";
        [self.navigationController pushViewController:viewToPush animated:YES];

    }
    
    if (myCell.tag == 4)
    {
        LocalNotifierViewController *viewToPush = [[LocalNotifierViewController alloc] initWithNibName:@"LocalNotifierViewController" bundle:nil];
        viewToPush.title = @"스케줄 알리미";
        [self.navigationController pushViewController:viewToPush animated:YES];
    }
    
    if (myCell.tag == 5)
    {
        SetupViewController *viewToPush = [[SetupViewController alloc] initWithNibName:@"SetupViewController" bundle:nil];
        viewToPush.title = @"환경설정";
        [self.navigationController pushViewController:viewToPush animated:YES];
    }
    
    if (myCell.tag == 6)
    {
        VersionViewController *viewToPush = [[VersionViewController alloc] initWithNibName:@"VersionViewController" bundle:nil];
        viewToPush.title = @"버전정보";
        [self.navigationController pushViewController:viewToPush animated:YES];
    }
    
    if (myCell.tag == 7)
    {
        DownloadViewController *viewToPush = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
        [self.navigationController pushViewController:viewToPush animated:YES];
    }
    
    //리마인더
    if (myCell.tag == 8) {
        self.navigationController.navigationBar.hidden = YES;
        HReminderViewController *view = [[HReminderViewController alloc]
                                      initWithNibName:@"BaseViewController" bundle:nil];
        [self.navigationController pushViewController:view animated:YES];
    }
    
    //스토리
    if (myCell.tag == 9) {
        HStoryViewController *view = [[HStoryViewController alloc]
                                       initWithNibName:@"BaseViewController" bundle:nil];
        [self.navigationController pushViewController:view animated:YES];
    }
    
    
    //개발자 옵션
    if (myCell.tag == 19) {
        DevOptionViewController *view = [[DevOptionViewController alloc] initWithStyle:UITableViewStyleGrouped];
        [self.navigationController pushViewController:view animated:YES];
    }
    
    

    if (myCell.tag == 20)
    {
        [self appDelegate].flagLogin = NO;
        [[MLCenterForBusinessAppDelegate sharedAppDelegate] loginViewControllerDisplay];
    }

}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {

    self.listMenu = nil;
	self.linkUrl = nil;
    self.array = nil;
    
    [super viewDidUnload];
}



- (MLCenterForBusinessAppDelegate*)appDelegate
{
    return [MLCenterForBusinessAppDelegate sharedAppDelegate];
}

- (void)devOptionClick:(id)sender
{
    devOptionClickCount++;
    if (devOptionClickCount == 7)
    {
        NSDictionary *j = [NSDictionary dictionaryWithObject:@"개발자 옵션" forKey:@"19"];
        [self.array addObject:j];
        [self.tableView reloadData];
    }
}

- (void)devOption
{
    devOptionClickCount = 0;
    NSDictionary *lastArray = [self.array lastObject];
    if ([lastArray objectForKey:@"19"])
    {
        [self.array removeLastObject];
        [self.tableView reloadData];
    }
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    if ([[app.profileUser objectForKey:@"IsStaging"] boolValue])
    {
        self.navigationItem.rightBarButtonItem.title = @"개발자 옵션";
    }
    else
    {
        self.navigationItem.rightBarButtonItem.title = @"        ";
    }
}

@end

