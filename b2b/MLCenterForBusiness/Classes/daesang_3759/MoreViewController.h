
#import <UIKit/UIKit.h>
#import "MLCenterForBusinessAppDelegate.h"


@interface MoreViewController : UITableViewController {
	NSArray *listMenu;
    NSMutableArray *array;
	NSString *linkUrl;
    int devOptionClickCount;

}

@property (nonatomic, strong) NSArray *listMenu;
@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) NSString *linkUrl;

//- (void)cellSelected;
- (MLCenterForBusinessAppDelegate*)appDelegate;

@end
