//
//  LectureViewController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 5. 31..
//  Copyright 2011 hunet. All rights reserved.
//

#import "LectureViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "ModalAlert.h"
#import "SSViewController.h"
#import "Header.h"
#import "GraphicsUtile.h"

@implementation LectureViewController
@synthesize _currentUrl, _param;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        self.title = NSLocalizedString(@"수강신청", @"수강신청");
        self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_lecturelist"];
    }
    return self;
}


- (void) webViewDidFinishLoad:(UIWebView *)webView {
    if ([viewWeb.subviews count] > 0) {
        UIView *scrollView = viewWeb.subviews[0];
        
        for (UIView *childView in scrollView.subviews) {
            if ([childView isKindOfClass:[UIWebDocumentView class]]) {
                UIWebDocumentView *documentView = (UIWebDocumentView *)childView;
                WebScriptObject *wso = documentView.webView.windowScriptObject;
                
                [wso setValue:[WebScriptBridge getWebScriptBridge] forKey:@"ScriptBridge"];
            }
        }
    }
	[indicator stopAnimating];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
	[indicator startAnimating];
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
	[indicator stopAnimating];	
	//[ModalAlert notify:@"네트워크 연결에 문제가 생겼습니다."];	
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	
	NSURL *url = request.URL;
	NSString *urlString = url.absoluteString.lowercaseString;

    
    /*
	if ([urlString rangeOfString:@"/Lecture/"].location != NSNotFound) {
        self._currentUrl = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
        if ([self._currentUrl isEqualToString:@"about:blank"]) {
            self._currentUrl = @"";
        }
	}
	*/
    
	//Home
	if ([urlString rangeOfString:@"/home/home.aspx"].location != NSNotFound) {
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
		[app setSelectedTabIndex:0];
		return NO;
	}	
		
	//상상마루
	if ([urlString rangeOfString:@"/submain.aspx?type=app"].location != NSNotFound) {
		MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
		NSString *ssUrl = [NSString stringWithFormat:@"%@/submain.aspx?type=app",
						   [app.urlCenter stringByReplacingOccurrencesOfString:@"http://" withString:@"http://m."]];
		
		if ([urlString isEqualToString:ssUrl]) {
			SSViewController *viewToPush = [[SSViewController alloc] initWithNibName:@"SSViewController" bundle:nil];
			if (viewToPush) {
				viewToPush.url = urlString;
				[self presentViewController:viewToPush animated:YES completion:nil];
			}
			return NO;
		}
		return YES;
	}	
	return YES;
}

- (void)viewWillAppear:(BOOL)animated {
	
	if (animated) {
		[self viewLoad];
	}
    
	if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        if(IS_4_INCH)
        {
            viewWeb.frame = CGRectMake(0, 18, viewWeb.frame.size.width, 500);
        }
        else
        {
            viewWeb.frame = CGRectMake(0, 18, viewWeb.frame.size.width, 413);
        }
    }

	[super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];	
	[self viewLoad];
}


- (void)viewLoad {
	
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
	NSString *type = @"5";
	if (app.lectureUrlType != nil) {
		type = app.lectureUrlType;
	}
	
	NSString *urlText = [[NSString stringWithFormat:@"%@?type=%@&uid=%@&pw=%@", app.urlBase, type, [app.profileUser objectForKey:@"userid"], [app.profileUser objectForKey:@"password"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
    /*
	if ([self._currentUrl length] > 0) {
		urlText = self._currentUrl;
	}
    */
    
    if ([self._param length] > 0) {
        urlText = [NSString stringWithFormat:@"%@&param=%@", urlText, self._param];
    }
    
    [viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlText]]];
	viewWeb.scalesPageToFit = YES;	
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (NSUInteger)supportedInterfaceOrientations
{
    indicator.center = viewWeb.center;
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskAllButUpsideDown;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {

	viewWeb = nil;
	indicator = nil;
	self._currentUrl = nil;
    self._param = nil;
    [super viewDidUnload];
}


- (void)dealloc {
	viewWeb.delegate = nil;
}

- (void)webViewpushUrl:(NSString *)webViewpushUrl {
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    [viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:webViewpushUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0]];
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    app.pushService = nil;
    //self.appDelegate.pushService = nil;
}


@end
