//
//  HomeViewController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 5. 17..
//  Copyright 2011 hunet. All rights reserved.
//

#import "HomeViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "ModalAlert.h"
#import "PlayerViewController.h"
#import "SSViewController.h"
#import "ModalViewController.h"
#import "MoreViewController.h"
#import "NoticeViewController.h"
#import "DownloadViewController.h"
#import "SelfStudyViewController.h"
#import "UserViewController.h"
#import "Header.h"
#import "LectureViewController.h"
#import "ELCImagePickerController.h"
#import "ELCAlbumPickerController.h"
#import "ELCAssetTablePicker.h"
#import "WebModalViewController.h"
#import "GraphicsUtile.h"

@implementation HomeViewController


#pragma mark -
#pragma mark UIWebViewDelegate implementation

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        self.title = NSLocalizedString(@"홈", @"홈");
        self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_home"];
    }
    return self;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    if ([viewWeb.subviews count] > 0) {
        UIView *scrollView = viewWeb.subviews[0];
        
        for (UIView *childView in scrollView.subviews) {
            if ([childView isKindOfClass:[UIWebDocumentView class]]) {
                UIWebDocumentView *documentView = (UIWebDocumentView *)childView;
                WebScriptObject *wso = documentView.webView.windowScriptObject;
                
                [wso setValue:[WebScriptBridge getWebScriptBridge] forKey:@"ScriptBridge"];
            }
        }
    }
	[indicator stopAnimating];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
	[indicator startAnimating];
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
	[indicator stopAnimating];	
	//[ModalAlert notify:@"네트워크 연결에 문제가 생겼습니다."];	
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
	NSURL *url = request.URL;
	NSString *urlString = url.absoluteString.lowercaseString;
	
    NSLog(@"%@, %@", urlString, app.eduType);
    
    // 회원정보 페이지로 이동
    if ([urlString.lowercaseString hasPrefix:@"membermodify://"]) {
        self.tabBarController.selectedIndex = 3;
        
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
        UserViewController *second = [[UserViewController alloc] initWithNibName:@"UserViewController" bundle:nil];
        NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
        [navi setViewControllers:controllersArray animated:YES];

        
        return NO;
    }
    
    //상상마루 전용  version = 5, imagine_contract_yn = 'Y', only_imagine_yn = 'Y'
    if ([app.eduType isEqualToString:@"OnlyImagine"]) {
        if ([urlString rangeOfString:@"/imgcategory/contents?"].location != NSNotFound
            || [urlString rangeOfString:@"/imgclassroom/series?"].location != NSNotFound
            || [urlString rangeOfString:@"/imgclassroom/contents?"].location != NSNotFound
            || [urlString rangeOfString:@"/imgclassroom/requiredlecture?headertitle="].location != NSNotFound) {
            self.tabBarController.selectedIndex = 1;
            SelfStudyViewController *selfStudy = (SelfStudyViewController*)self.tabBarController.selectedViewController;
            if ([urlString rangeOfString:@"childcategoryyn=y"].location != NSNotFound) {
                
                NSString *param =[urlString substringFromIndex:[urlString rangeOfString:@"?"].location + 1];
                
                urlString = [NSString stringWithFormat:@"%@/imgCategory/ContentsCategoryListAll?%@&layoutdisplayyn=y", app.urlCenter, param];
                
                //return NO;
            }
            [selfStudy detailView:urlString];
            return NO;
        }
        
        //웹 로그아웃 제어
        if([urlString.lowercaseString hasPrefix:@"logout://"])
        {
            [[MLCenterForBusinessAppDelegate sharedAppDelegate] loginViewControllerDisplay];
        }
        
        return YES;
    }

    //웹 로그아웃 제어
    if([urlString.lowercaseString hasPrefix:@"logout://"])
    {
        [[MLCenterForBusinessAppDelegate sharedAppDelegate] loginViewControllerDisplay];
        return YES;
    }
    
	//행복한 인문학당
	if ([urlString rangeOfString:@"/lecture/category_inmun.aspx"].location != NSNotFound){
		[app setLectureUrlType:@"7"];
		[app setSelectedTabIndex:1];		
		return NO;
	}
    
    
    if ([urlString rangeOfString:@"/lecture/category.aspx?param="].location != NSNotFound){
        //NSArray * param = [urlString componentsSeparatedByString:@"="];
        self.tabBarController.selectedIndex = 1;
        LectureViewController *view = (LectureViewController*)self.tabBarController.selectedViewController;
        view._currentUrl = @"";
        //view._param = [param objectAtIndex:1];
        [self.tabBarController.selectedViewController viewWillAppear:YES];
		return NO;
	}
    
    
	
	//주제별 교육과정
	if ([urlString rangeOfString:@"/lecture/category"].location != NSNotFound){
		[app setLectureUrlType:@"6"];
		[app setSelectedTabIndex:1];		
		return NO;
	}
    
    //주제별 교육과정 상세
	if ([urlString rangeOfString:@"/lecture/depth.aspx"].location != NSNotFound){
		[app setLectureUrlType:@"6"];	
        [app setSelectedTabIndex:1 indexOrUrl:urlString];
		return NO;
	}
    
	
	//나의 강의실, 사용자 즐겨찾기
	if ([urlString rangeOfString:@"/my/my.aspx"].location != NSNotFound
        || [urlString rangeOfString:@"/my/mycourse"].location != NSNotFound){
        int _tabIndex = 2;
        if ([app.eduType isEqualToString:@"LearningAndImagine"]) {
            _tabIndex = 3;
        }

        if ([urlString rangeOfString:@"/my/my.aspx"].location != NSNotFound) {
            [app setSelectedTabIndex:_tabIndex indexOrUrl:nil];
            return NO;
        }
        
        [app setSelectedTabIndex:_tabIndex indexOrUrl:urlString];
		return NO;
	}
	
    //다운로드
    if ([urlString isEqualToString:@"downloadcenter://"]) {
        if ([app.eduType isEqualToString:@"LearningAndImagine"]) {
            self.tabBarController.selectedIndex = 3;
            UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
            MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
            DownloadViewController *second = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
            NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
            [navi setViewControllers:controllersArray animated:YES];
        } else {

            self.tabBarController.selectedIndex = 3;

            UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
            [navi popToRootViewControllerAnimated:YES];
        }
        return NO;
    }
    
	//상담실-공지사항, 공지사항 상세
	if ([urlString rangeOfString:@"/counsel/counsel_noticelist.aspx"].location != NSNotFound
        || [urlString rangeOfString:@"/counsel/counsel_noticedetail.aspx"].location != NSNotFound){

        self.tabBarController.selectedIndex = 3;
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
        NoticeViewController *second = [[NoticeViewController alloc] initWithNibName:@"NoticeViewController" bundle:nil];
        second.title = @"공지사항";
        second.url = urlString;
        NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
        [navi setViewControllers:controllersArray animated:YES];
        
		return NO;
	}
	
	//홍화면 편집
	if ([urlString rangeOfString:@"/home/home_edit.aspx"].location != NSNotFound){
		return YES;
	}	
	
    
	//pc버전 링크
	if ([urlString isEqualToString:[NSString stringWithFormat:@"%@/", app.urlCenter]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:app.urlCenter]];
		return NO;
	}
    
    
	//상상마루
	if ([urlString rangeOfString:@"/sangsang/sangsanglink.aspx"].location != NSNotFound
		|| [urlString rangeOfString:@"/login/mlclogin.aspx"].location != NSNotFound
		|| [urlString rangeOfString:@"/submain.aspx?type=app"].location != NSNotFound
        || [urlString rangeOfString:@"https://ssl.hunet.co.kr"].location != NSNotFound) {
        
        if ([app.eduType isEqualToString:@"LearningAndImagine"]) {
            self.tabBarController.selectedIndex = 1;
            return NO;
        }
		
		NSString *ssUrl = [NSString stringWithFormat:@"%@/submain.aspx?type=app",
						   [app.urlCenter stringByReplacingOccurrencesOfString:@"http://" withString:@"http://m."]];
        
        //NSLog(@"%@", ssUrl);
        
		if ([urlString isEqualToString:ssUrl] || [urlString rangeOfString:@"https://ssl.hunet.co.kr"].location != NSNotFound) {		
			SSViewController *viewToPush = [[SSViewController alloc] initWithNibName:@"SSViewController" bundle:nil];
			if (viewToPush) {
				viewToPush.url = urlString;
				[self presentViewController:viewToPush animated:YES completion:nil];
			}
            
            [self performSelector:@selector(viewLoad) withObject:nil afterDelay:0.3];
            
			return NO;
		}
		return YES;
	}	
    
    //모달 창으로 뛰우기
    if ([urlString.lowercaseString hasPrefix:@"modal://"]) {
        
        NSDictionary *simpleModalUrlDic = [NSDictionary  dictionaryWithObjectsAndKeys:
                                           @"알파코", @"://www.alpaco.co.kr", nil];
        BOOL popupSimpleModal = false;
        
        NSString *simpleModalPopupTitle = nil;
        for (NSString *domainKey in simpleModalUrlDic) {
            
            if ([urlString.lowercaseString rangeOfString:domainKey].location != NSNotFound) {
                popupSimpleModal = true;
                simpleModalPopupTitle = [simpleModalUrlDic objectForKey: domainKey];
                break;
            }
        }
        
        WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
        webModal.webViewUrl = [request.URL.absoluteString stringByReplacingOccurrencesOfString:@"modal://" withString:@"http://"];
        webModal.title = simpleModalPopupTitle;
        [self presentViewController:webModal animated:YES completion:nil];
        
        return NO;
        
    }
	
    
    // 외부 사이트로 이동
    if ([urlString.lowercaseString hasPrefix:@"escape://"]) {
        NSString *escapeUrl = [urlString stringByReplacingOccurrencesOfString:@"escape://" withString:@"http://"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:escapeUrl]];
		return NO;
    }
    
    
    if ([urlString.lowercaseString hasPrefix:@"appstore://"]) {
        NSString *escapeUrl = [urlString stringByReplacingOccurrencesOfString:@"appstore://" withString:@""];
        escapeUrl = [escapeUrl stringByReplacingOccurrencesOfString:@"https//" withString:@"https://"];
        escapeUrl = [escapeUrl stringByReplacingOccurrencesOfString:@"http//" withString:@"http://"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:escapeUrl]];
		return NO;
    }
    
    
	
    if([[[request URL] absoluteString] hasPrefix:@"jscall"])
    {
        NSString *requestString = [[request URL] absoluteString];
        NSArray *components = [requestString componentsSeparatedByString:@"://"];
        NSString *functionName = [components objectAtIndex:1];
        
        SEL selector = NSSelectorFromString(functionName);
        IMP imp = [self methodForSelector:selector];
        void (*func)(id, SEL) = (void *)imp;
        func(self, selector);
        
        return NO;
    }
    
    // 나의강의실
    if ([urlString rangeOfString:@"/classroom/online"].location != NSNotFound){
        self.tabBarController.selectedIndex = 2;
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        [navi popToRootViewControllerAnimated:YES];
        return NO;
    }
    // 상상마루
    if ([urlString rangeOfString:@"/imghome"].location != NSNotFound){
        self.tabBarController.selectedIndex = 1;
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        [navi popToRootViewControllerAnimated:YES];
        return NO;
    }
    // 공지사항
    if ([urlString rangeOfString:@"/counsel/noticelist"].location != NSNotFound){
        self.tabBarController.selectedIndex = 3;
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        [navi popToRootViewControllerAnimated:YES];
        return NO;
    }
    // 학습지원센터
    if ([urlString.lowercaseString hasPrefix:@"more://"]) {
        self.tabBarController.selectedIndex = 3;
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        [navi popToRootViewControllerAnimated:YES];
        return NO;
    }
    
	//그외 더보기 메뉴로 이동, 국민연금 진단
	if ([urlString.lowercaseString rangeOfString:@"/app/jlog.aspx"].location == NSNotFound
		&& [urlString.lowercaseString rangeOfString:@"/home/home.aspx"].location == NSNotFound
        && [urlString.lowercaseString rangeOfString:@"/home"].location == NSNotFound
        && [urlString.lowercaseString rangeOfString:@"/areas/4258/"].location == NSNotFound
        && [urlString.lowercaseString rangeOfString:@"/lecture/detail.aspx"].location == NSNotFound
        && [urlString.lowercaseString rangeOfString:@"/study/study_summary.aspx"].location == NSNotFound
        && [urlString.lowercaseString rangeOfString:@"/study/capacity_diagnosis.aspx"].location == NSNotFound
        && [urlString.lowercaseString rangeOfString:@"study/h_class.aspx"].location == NSNotFound
        && [urlString.lowercaseString rangeOfString:@"study/h_class.aspx"].location == NSNotFound
        && [urlString.lowercaseString rangeOfString:@"/hclass"].location == NSNotFound
        && [urlString.lowercaseString rangeOfString:@"/lecture/humanities"].location == NSNotFound) {
        self.tabBarController.selectedIndex = 3;
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        [navi popToRootViewControllerAnimated:YES];
		return NO;
	}
    
	return YES;
}

- (void)callPhotoView {
    ELCAlbumPickerController *albumController = [[ELCAlbumPickerController alloc] initWithNibName: nil bundle: nil];
	ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initWithRootViewController:albumController];
    [albumController setParent:elcPicker];
	[elcPicker setDelegate:self];
    elcPicker.maximumImagesCount = 4;
    
    [self presentViewController:elcPicker animated:YES completion:nil];
    
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    if ([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]){
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)pickerWebSave:(NSArray *)info
{
    @autoreleasepool {
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        
        NSLog(@"%@", app.urlCenter);
        
        NSString *urlString, *boundary;
        imgFileName = @"";
        NSInteger i = 0;
        urlString = [NSString stringWithFormat:@"%@/Common/MobileUpload?path=Community", app.urlCenter];
        boundary = @"CommunityPicImage";
        
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setCachePolicy:NSURLRequestUseProtocolCachePolicy];
        [request setHTTPMethod:@"POST"];
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        
        /*
         NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
         
         [[NSRunLoop currentRunLoop] run];
         
         
         // Schedule your connection to run on threads runLoop.
         [connection scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
         */
        
        
        /*
         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         NSRunLoop *loop = [NSRunLoop currentRunLoop];
         [connection scheduleInRunLoop:loop forMode:NSRunLoopCommonModes];
         [loop run]; // make sure that you have a running run-loop.
         });
         */
        
        
	for(NSDictionary *dict in info) {
            UIImage *image = [dict objectForKey:UIImagePickerControllerOriginalImage];
            NSData *ImageData = UIImageJPEGRepresentation(image, 1.0);
            NSMutableData *body = [NSMutableData data];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image%d\"; filename=\"Image%d.jpg\"\r\n", i, i] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:ImageData]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [request setHTTPBody:body];
            
            NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
            NSString *returnString = [[NSString alloc] initWithData:urlData encoding:NSUTF8StringEncoding];
            
            NSLog(@"%@", returnString);
            
            imgFileName = [imgFileName stringByAppendingFormat:@"|%@", returnString];
            
            
            i=i+1;
	}
        
        if ([info count] == i) {
            [self performSelectorOnMainThread:@selector(saveEnd:) withObject:imgFileName waitUntilDone:NO];
        }
    }
}


- (void)saveEnd:(NSString*) ImgFileNames
{
	[mask removeFromSuperview];
	[uploadIndicator stopAnimating];
    [viewWeb stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"ReadingPhoto('%@');", ImgFileNames]];
}


- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    if ([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]){
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    uploadIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    uploadIndicator.hidesWhenStopped = YES;
    
    
    mask = [[UIView alloc] initWithFrame:[[self.view window] frame]];
	mask.userInteractionEnabled = NO;
	mask.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.50];
    [mask addSubview:uploadIndicator];
    uploadIndicator.center = mask.center;
	[self.view addSubview:mask];
	
	[uploadIndicator startAnimating];
    
    
    [NSThread detachNewThreadSelector:@selector(pickerWebSave:) toTarget:self withObject:info];
    
}

- (void)viewDidLoad {
    //NSLog(@"viewDidLoad");
    
    [super viewDidLoad];
	[self viewLoad];
	
	
}

- (void)viewLoad {
    NSLog(@"viewLoad");
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
	if (app.flagLogin)
    {
        NSString *urlText = [NSString stringWithFormat:@"%@?type=4&uid=%@&pw=%@", app.urlBase, [app.profileUser objectForKey:@"userid"], [app.profileUser objectForKey:@"password"]];
        
        [viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0]];
        
        viewWeb.scalesPageToFit = YES;
    }
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (NSUInteger)supportedInterfaceOrientations
{
    indicator.center = viewWeb.center;
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskAllButUpsideDown;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {

	viewWeb = nil;
	indicator = nil;
    [super viewDidUnload];
}


- (void)dealloc {
	viewWeb.delegate = nil;
}

- (void)webViewpushUrl:(NSString *)webViewpushUrl {
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    [viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:webViewpushUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0]];
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    app.pushService = nil;
    //self.appDelegate.pushService = nil;
}

@end
