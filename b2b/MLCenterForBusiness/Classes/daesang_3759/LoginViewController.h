

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate> {
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UIImageView *viewImageCover;
    UIView *mask;
}

@property (nonatomic, strong) UIImageView *viewImageCover;
@property (strong, nonatomic) IBOutlet UIButton *offlineButton;
@property (strong, nonatomic) IBOutlet UIImageView *viewImageBackground;
@property (strong, nonatomic) IBOutlet UIImageView *viewImageLoginBox;
@property (strong, nonatomic) IBOutlet UITextField *textID;
@property (strong, nonatomic) IBOutlet UITextField *textPassword;
@property (strong, nonatomic) IBOutlet UIButton *buttonLogin;

- (IBAction) onLogin;
- (void) onWebLogin: (NSString *) userId : (NSString *) userPwd;
//- (IBAction) onSupport;

- (IBAction)offlineClick:(id)sender;

@end