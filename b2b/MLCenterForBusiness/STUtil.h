#import <Foundation/Foundation.h>

@interface STUtil : NSObject {

}
+ (NSString *)formatTime:(int)time;
+ (NSString *)timeMmddss:(int)timeSS;
+ (void) deleteFile :(NSString *) fileName;
+ (NSString *) filePath : (NSString *) filename;
+ (void) deleteDocrootFile :(NSString *) fileName;
@end
