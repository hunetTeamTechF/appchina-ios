
#import "Global.h"
#import "MLCenterForBusinessAppDelegate.h"

@implementation Global
//@synthesize _drmInfo;
@synthesize testPrimaryDomain;
@synthesize testPrimaryDomainFix;


static Global *_global = nil;

- (id)init {
    self = [super init];
    if (self != nil) {
//        self._drmInfo = @"";
        self.testPrimaryDomainFix = @"test.";
    }
    return self;
}

+ (Global *)sharedSingleton {
    @synchronized([Global class]) {
        if (!_global) {
            _global = [[self alloc] init];
        }
        return _global;
    }
    
    return nil;
}

- (NSString *)testPrimaryDomain
{
    BOOL isStaging = [[[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"IsStaging"] boolValue];
    return isStaging ? self.testPrimaryDomainFix : @"";
}

- (NSString *)testPrimaryDomainUrl:(NSString *)url
{
    NSString *returnUrl = url;
    
    if ([self.testPrimaryDomain length] > 0)
    {
        if ([returnUrl hasPrefix:[NSString stringWithFormat:@"http://%@", self.testPrimaryDomainFix]]) {
            return returnUrl;
        }
        
        returnUrl = [returnUrl stringByReplacingOccurrencesOfString:@"http://"
                                                     withString:[NSString stringWithFormat:@"http://%@", self.testPrimaryDomainFix]];
    }
    else
    {
        if ([returnUrl hasPrefix:[NSString stringWithFormat:@"http://%@", self.testPrimaryDomainFix]]) {
            returnUrl = [returnUrl stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"http://%@", self.testPrimaryDomainFix]
                                                             withString:@"http://"];
        }
    }
    return returnUrl;
}

- (NSString *)encodePassword:(NSString *)passwd
{
    NSString *pw  = (__bridge_transfer NSString*)CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)passwd, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8);
    //NSString *pw = [NSString stringWithFormat:@"%@",(NSString *)CFBridgingRelease(stringRef)];
    //CFRelease(stringRef);
    return pw;
}

+ (id)alloc
{
    @synchronized([Global class])
    {
        NSAssert(_global == nil, @"Attempted to allocate a second instance of a singleton.");
        _global = [super alloc];
        return _global;
    }
    
    return nil;
}


@end
