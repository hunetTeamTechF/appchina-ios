#import <UIKit/UIKit.h>
#import "Web.h"


@protocol DownloadingViewDelegate;
@interface DownloadingView : UIViewController
{
    id __weak _delegate;
}

@property (nonatomic,weak) id <DownloadingViewDelegate> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lb_AvTitle;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *ac_Loading;
@property (strong, nonatomic) IBOutlet UIProgressView *pgView;
@property (strong, nonatomic) IBOutlet UILabel *sizePg;
@property (strong, nonatomic) IBOutlet UIProgressView *totalPgView;
@property (strong, nonatomic) IBOutlet UILabel *countPg;
@property (strong, nonatomic) IBOutlet UIButton *btnResult;

@property (nonatomic, strong) Web *webUtil;

- (IBAction)removeWebUtil:(id)sender;


@end


@protocol DownloadingViewDelegate
@optional
- (void)processAction;
@end