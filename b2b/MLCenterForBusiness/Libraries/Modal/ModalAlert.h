/*
 Erica Sadun, http://ericasadun.com
 iPhone Developer's Cookbook, 3.0 Edition
 BSD License, Use at your own risk
 */

#import <UIKit/UIKit.h>

@interface ModalAlert : NSObject
+(NSUInteger) queryWith: (NSString *)question button1: (NSString *)button1 button2: (NSString *)button2;
+(NSUInteger) query3With: (NSString *)question button1: (NSString *)button1 button2: (NSString *)button2 button3: (NSString *)button3;
+ (void) notify: (NSString *) notification;
+ (BOOL) ask: (NSString *) question;
+ (BOOL) confirm:(NSString *) statement;
+ (BOOL) askCancel: (NSString *) statement;
+ (void) message: (NSString *) msg;
+ (void) toast:(NSString *)message;
+ (void) toast:(NSString *)message andDelay:(double)delay;
+ (UIViewController*) topMostController;
+ (UIViewController*) topMostViewController;
@end
