
#import <HDRM/HdrmMultipartMessageHeader.h>

/* 
Part one: HdrmHTTP://tools.ietf.org/html/rfc2045 (Format of Internet Message Bodies)
Part two: HdrmHTTP://tools.ietf.org/html/rfc2046 (Media Types)
Part three: HdrmHTTP://tools.ietf.org/html/rfc2047 (Message Header Extensions for Non-ASCII Text)
Part four: HdrmHTTP://tools.ietf.org/html/rfc4289 (Registration Procedures) 
Part five: HdrmHTTP://tools.ietf.org/html/rfc2049 (Conformance Criteria and Examples) 
 
Internet message format:  HdrmHTTP://tools.ietf.org/html/rfc2822

Multipart/form-data HdrmHTTP://tools.ietf.org/html/rfc2388
*/

@class MultipartFormDataParser;

//-----------------------------------------------------------------
// protocol MultipartFormDataParser
//-----------------------------------------------------------------

@protocol MultipartFormDataParserDelegate <NSObject> 
@optional
- (void) processContent:(NSData*) data WithHeader:(HdrmMultipartMessageHeader*) header;
- (void) processEndOfPartWithHeader:(HdrmMultipartMessageHeader*) header;
- (void) processPreambleData:(NSData*) data;
- (void) processEpilogueData:(NSData*) data;
- (void) processStartOfPartWithHeader:(HdrmMultipartMessageHeader*) header;
@end

//-----------------------------------------------------------------
// interface MultipartFormDataParser
//-----------------------------------------------------------------

@interface HdrmMultipartFormDataParser : NSObject {
NSMutableData*						pendingData;
    NSData*							boundaryData;
    HdrmMultipartMessageHeader*			currentHeader;

	BOOL							waitingForCRLF;
	BOOL							reachedEpilogue;
	BOOL							processedPreamble;
	BOOL							checkForContentEnd;

#if __has_feature(objc_arc_weak)
	__weak id<MultipartFormDataParserDelegate>                  delegate;
#else
	__unsafe_unretained id<MultipartFormDataParserDelegate>     delegate;
#endif	
	int									currentEncoding;
	NSStringEncoding					formEncoding;
}

- (BOOL) appendData:(NSData*) data;

- (id) initWithBoundary:(NSString*) boundary formEncoding:(NSStringEncoding) formEncoding;

#if __has_feature(objc_arc_weak)
    @property(weak, readwrite) id delegate;
#else
    @property(unsafe_unretained, readwrite) id delegate;
#endif
@property(readwrite) NSStringEncoding	formEncoding;

@end
