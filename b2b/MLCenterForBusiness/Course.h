

#import <Foundation/Foundation.h>

@interface Course : NSObject

@property (strong, nonatomic) NSString *course_cd;
@property (strong, nonatomic) NSNumber *take_course_seq;
@property (strong, nonatomic) NSString *course_nm;
@property (strong, nonatomic) NSString *start_end_date;

@end
