#import "STUtil.h"

@implementation STUtil

+ (NSString *)timeMmddss:(int)timeSS {
    
    int totalTimeM = timeSS /3600;
    int totalTimeD = (timeSS %3600)/60;
    int totalTimeS = (timeSS %3600)%60;
    
    return [NSString stringWithFormat:@"%@:%@:%@", 
            [STUtil formatTime:totalTimeM], 
            [STUtil formatTime:totalTimeD],
            [STUtil formatTime:totalTimeS]];
}
+ (NSString *)formatTime:(int)time {
    NSString *strTime = nil;
    if (time < 10) {
        strTime = [NSString stringWithFormat:@"0%d",time];
    } else {
        strTime = [NSString stringWithFormat:@"%d",time];
    }
    return strTime;
}
    
+(NSString *) filePath:(NSString *)filename
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, filename];
    return filePath;
}

+ (void) deleteFile:(NSString *)fileName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:[STUtil filePath:[NSString stringWithFormat:@"%@", fileName]] error:nil];
}

+ (void) deleteDocrootFile :(NSString *) fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/hunet_docroot/%@", documentsDirectory, fileName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:filePath error:nil];
}


@end
