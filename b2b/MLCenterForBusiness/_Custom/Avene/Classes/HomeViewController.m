//
//  HomeViewController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 5. 17..
//  Copyright 2011 hunet. All rights reserved.
//

#import "HomeViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "ModalAlert.h"
#import "PlayerViewController.h"
#import "SSViewController.h"
#import "SangPlayerViewController.h"
//#import "ModalViewController.h"
#import "MoreViewController.h"
#import "NoticeViewController.h"
#import "DownloadViewController.h"
#import "SelfStudyViewController.h"
#import "UserViewController.h"
#import "Header.h"
#import "LectureViewController.h"
#import "ELCImagePickerController.h"
#import "ELCAlbumPickerController.h"
#import "ELCAssetTablePicker.h"
#import "WebModalViewController.h"
#import "PlayerCallFun.h"
#import "Util.h"
#import "CJSONDeserializer.h"
#import "QnaViewController.h"
#import "MyViewController.h"
#import "ViewerViewController.h"
#import "GraphicsUtile.h"
#import "BPush.h"
@implementation HomeViewController
@synthesize urlMark;
@synthesize playerCallFun = _playerCallFun;

#pragma mark -
#pragma mark UIWebViewDelegate implementation

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
#ifdef avene
        self.title = NSLocalizedString(@"home", @"首页");
        self.tabBarItem.titlePositionAdjustment = UIOffsetMake(2, -16);
        
       // self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_home"];
#else
        self.title = NSLocalizedString(@"home", @"首页");
        self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_home"];
#endif
        
        //self.title = nil;
        //self.title.

        
        //self.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);

    }
    return self;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    if ([viewWeb.subviews count] > 0) {
        UIView *scrollView = viewWeb.subviews[0];
        
        for (UIView *childView in scrollView.subviews) {
            if ([childView isKindOfClass:[UIWebDocumentView class]]) {
                UIWebDocumentView *documentView = (UIWebDocumentView *)childView;
                WebScriptObject *wso = documentView.webView.windowScriptObject;
                
                [wso setValue:[WebScriptBridge getWebScriptBridge] forKey:@"ScriptBridge"];
            }
        }
    }
	[indicator stopAnimating];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    NSString *tagsString = [app.profileUser objectForKey:@"WorkAreaCode"];
    //NSString *tagsString = @"hunet";
    NSArray *tags = [tagsString componentsSeparatedByString:@","];
    if (tags) {
        [BPush setTags:tags withCompleteHandler:^(id rst, NSError *error) {
            // 设置多个标签组的返回值
            //  [self addLogString:[NSString stringWithFormat:@"Method: %@\n%@",BPushRequestMethodSetTag,result]];
        }];
    }
	[indicator startAnimating];
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
	[indicator stopAnimating];	
	//[ModalAlert notify:@"네트워크 연결에 문제가 생겼습니다."];	
}


- (int) getTabIndexFromRequestUrl:(NSURL *)url {
    
    // http://app.hunet.co.kr/api.aspx?cmd=tab&cseq={company_seq}&url={encode_url}
    //NSString *apiUrl = @"http://app.hunet.co.kr/api.aspx?cmd=tab&cseq={company_seq}&url={encode_url}";
    
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    NSString *param = [NSString stringWithFormat:@"cmd=tab&cseq=%@&url=%@", app.companySeq, url.absoluteString];
    NSError *errorReq = nil;
    
    NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:@"http://app.hunet.co.kr/api.aspx"] andQuery:param andError:&errorReq];
    if (jsonData)
    {
        NSError *errorJSON = nil;
        NSDictionary *result = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
        return [[result objectForKey:@"tab_index"] integerValue];
    }
    
    return 0;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    //http://app.hunet.co.kr/api.aspx?cmd=tab&cseq={company_seq}&url={encode_url}

    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
	NSURL *url = request.URL;
	NSString *urlString = url.absoluteString;
    
    NSLog(@"%@, %@", urlString, app.eduType);
    
    //int tab_index = [self getTabIndexFromRequestUrl:request.URL];
    //self.tabBarController.selectedIndex = tab_index;
    
    // 외부 사이트로 이동
    if ([urlString.lowercaseString hasPrefix:@"escape://"]) {
        NSString *escapeUrl = [urlString stringByReplacingOccurrencesOfString:@"escape://" withString:@"http://"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:escapeUrl]];
        return NO;
    }
    
    if ([urlString.lowercaseString hasSuffix:@".pdf"] || [urlString.lowercaseString hasSuffix:@".ppt"] || [urlString.lowercaseString hasSuffix:@".pptx"] || [urlString.lowercaseString hasSuffix:@".doc"] || [urlString.lowercaseString hasSuffix:@".docx"] || [urlString.lowercaseString hasSuffix:@".xlsx"] || [urlString.lowercaseString hasSuffix:@".xls"] ) {
        _backBtn.hidden = false;
    } else {
        _backBtn.hidden = true;
    }
    
    if ([urlString.lowercaseString hasPrefix:@"httpweb"]) {
        NSString *Nurls = [urlString stringByReplacingOccurrencesOfString:@"httpweb://" withString:@"http://"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:Nurls]];
        return NO;
    }
    
    
    if ([urlString.lowercaseString hasPrefix:@"appstore://"]) {
        NSString *escapeUrl = [urlString stringByReplacingOccurrencesOfString:@"appstore://" withString:@""];
        escapeUrl = [escapeUrl stringByReplacingOccurrencesOfString:@"https//" withString:@"https://"];
        escapeUrl = [escapeUrl stringByReplacingOccurrencesOfString:@"http//" withString:@"http://"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:escapeUrl]];
        return NO;
    }
    
    if ([urlString.lowercaseString hasPrefix:@"sangsangdrmplayer://"] ||
        [urlString.lowercaseString hasPrefix:@"sangsangdownload://"] ||
        [urlString.lowercaseString hasPrefix:@"sangsangdownloadplay://"]) {
        
        NSLog(@"sangsangdrmp");
        
        NSArray *tmpArray = [urlString componentsSeparatedByString:@"://"];
        NSString *act = [tmpArray objectAtIndex:0];
        NSString *param = [tmpArray objectAtIndex:1];
        NSArray *cTemp = [param componentsSeparatedByString:@"/"];
        
        if ([cTemp count]>3) {
        //    NSString *test = [cTemp objectAtIndex:3];
            self.QuizYn =[cTemp objectAtIndex:3];
        }
        
        if ([app isWiFi]) {
            
            _playerCallFun = [[PlayerCallFun alloc] init];
            _playerCallFun.delegateViewController = self;
            
            self.urlMark = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
            [_playerCallFun sangsangmaruInit:[NSArray arrayWithObjects:act, param, nil]];
        }
        
        return NO;
    }
    
    //상상마루(기존플레이어)
    if ([urlString.lowercaseString rangeOfString:@"sangsangplayer://"].location != NSNotFound) {
        NSArray *tmpArray = [urlString componentsSeparatedByString:@"://"];
        NSString *act = @"sangsangdrmplayer";
        NSString *param = [tmpArray objectAtIndex:1];
        
        if ([app isWiFi]) {
            
            _playerCallFun = [[PlayerCallFun alloc] init];
            _playerCallFun.delegateViewController = self;
            
            self.urlMark = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
            [_playerCallFun sangsangmaruInit:[NSArray arrayWithObjects:act, param, nil]];
        }

        return NO;
    }

    //모달 팝업
    if ([request.URL.absoluteString.lowercaseString hasPrefix:@"modal://"]
        /*|| ([urlString.lowercaseString hasPrefix:@"http://"] && [urlString.lowercaseString hasSuffix:@".mp4"])*/)
    {
        NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:request.URL resolvingAgainstBaseURL:NO];
        NSArray *queryItems = urlComponents.queryItems;
        
        //NSString *urlParam = [Util valueForKey:@"url" fromQueryItems:queryItems];
        NSString *urlParam = [Util valueForKey:@"url" fromQuery:request.URL.query];
        NSString *urlParamDecoded = (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (CFStringRef)urlParam, CFSTR(""), kCFStringEncodingUTF8));
        
        //?url query parameter가 없는 경우,
        if (urlParam == nil) {
            WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
            webModal.webViewUrl = [request.URL.absoluteString stringByReplacingOccurrencesOfString:@"modal://" withString:@"http://"];
            //webModal.title = @"";
            [self presentViewController:webModal animated:YES completion:nil];
        }
        //query에 url parameter가 있으면 해당 url로 modal 팝업
        else {
            
            NSURL *urlParsed = [NSURL URLWithString:[[urlParamDecoded componentsSeparatedByString:@"?"] firstObject]];
            NSString *method = [Util valueForKey:@"method" fromQueryItems:queryItems];
            NSString *title = [Util valueForKey:@"title" fromQueryItems:queryItems];
            //post parameter가 있으면 url을 파싱해서, queryString은 post로 전송
            if ([method.lowercaseString isEqualToString:@"post"])
            {
                WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
                
                //webViewRequestMethod
                NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:urlParsed
                                                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                   timeoutInterval:20.0];
                [request setHTTPMethod:@"POST"];
                NSString *bodyString = (NSString *)[[urlParamDecoded componentsSeparatedByString:@"?"] lastObject];
                [request setHTTPBody:[bodyString dataUsingEncoding:NSUTF8StringEncoding]];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                webModal.title = title;
                [self presentViewController:webModal animated:YES completion:nil];
                [webModal.WebView loadRequest:request];
                
                
            }
            //query에 url parameter가 없으면
            else {
                WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
                webModal.webViewUrl = urlParamDecoded;
                webModal.title = title;
                [self presentViewController:webModal animated:YES completion:nil];
            }
            
        }
        
        return NO;
        
    }
    
    // 회원정보 페이지로 이동
    if ([urlString.lowercaseString hasPrefix:@"membermodify://"]) {
        self.tabBarController.selectedIndex = 4;
        
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
        UserViewController *second = [[UserViewController alloc] initWithNibName:@"UserViewController" bundle:nil];
        NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
        [navi setViewControllers:controllersArray animated:YES];

        
        return NO;
    }
    
    if ([urlString.lowercaseString hasPrefix:@"customercenter://"]) {
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Customer" ofType:@"html"]];
        NSLog(@"url : %@", url);
        [viewWeb loadRequest:[NSURLRequest requestWithURL:url]];
        return NO;
    }
 
    //웹 로그아웃 제어
    if([urlString.lowercaseString hasPrefix:@"logout://"])
    {
        app.flagLogin = NO;
        [app loginViewControllerDisplay];
        return NO;
    }
  
    
    //다운로드
    if ([urlString.lowercaseString isEqualToString:@"downloadcenter://"]) {
        int selectedTabIndex = [self getTabIndexOfClass:[DownloadViewController class]];
        if (selectedTabIndex == -1) {
            selectedTabIndex = [self getTabIndexOfClass:[MoreViewController class]];
        }
        self.tabBarController.selectedIndex = selectedTabIndex;
        
        if ([app.eduType isEqualToString:@"LearningAndImagine"]) {
            UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
            MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
            DownloadViewController *second = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
            NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
            [navi setViewControllers:controllersArray animated:YES];
        } else {
            UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
            [navi popToRootViewControllerAnimated:YES];
        }
        return NO;
    }
    
    //나의 강의실, 사용자 즐겨찾기
    if ([urlString.lowercaseString rangeOfString:@"/my/my.aspx"].location != NSNotFound
        || [urlString.lowercaseString rangeOfString:@"/my/mycourse"].location != NSNotFound
        || [urlString.lowercaseString rangeOfString:@"/classroom/online"].location != NSNotFound
        || [urlString.lowercaseString rangeOfString:@"/imgclassroom/studysummary"].location != NSNotFound){

        int _tabIndex = [self getTabIndexOfClass:[MyViewController class]];
        
        if ([urlString.lowercaseString rangeOfString:@"/my/my.aspx"].location != NSNotFound) {
            [app setSelectedTabIndex:_tabIndex indexOrUrl:nil];
            return NO;
        }
        
        [app setSelectedTabIndex:_tabIndex indexOrUrl:urlString];
        return NO;
    }
    
    //상상마루 전용  version = 5, imagine_contract_yn = 'Y', only_imagine_yn = 'Y'
    if ([app.eduType isEqualToString:@"OnlyImagine"]) {
        if ([urlString.lowercaseString rangeOfString:@"/imgcategory/contents?"].location != NSNotFound
            || [urlString.lowercaseString rangeOfString:@"/imgclassroom/series?"].location != NSNotFound
            || [urlString.lowercaseString rangeOfString:@"/imgclassroom/contents?"].location != NSNotFound
            || [urlString.lowercaseString rangeOfString:@"/imgclassroom/requiredlecture?headertitle="].location != NSNotFound
            || [urlString.lowercaseString rangeOfString:@"/imgclassroom/mystudy?"].location != NSNotFound)
        {
            self.tabBarController.selectedIndex = 1;
            SelfStudyViewController *selfStudy = (SelfStudyViewController*)self.tabBarController.selectedViewController;
            if ([urlString.lowercaseString rangeOfString:@"childcategoryyn=y"].location != NSNotFound) {
                
                NSString *param =[urlString substringFromIndex:[urlString rangeOfString:@"?"].location + 1];
                
                urlString = [NSString stringWithFormat:@"%@/imgCategory/ContentsCategoryListAll?%@&layoutdisplayyn=y", app.urlCenter, param];
                
                //return NO;
            }
            [selfStudy detailView:urlString];
            return NO;
        }
        
        //웹 로그아웃 제어
        if([urlString.lowercaseString hasPrefix:@"logout://"])
        {
            [[MLCenterForBusinessAppDelegate sharedAppDelegate] loginViewControllerDisplay];
        }
        
        return YES;
    } else if ([app.eduType isEqualToString:@"LearningAndImagine"]) {
        if ([urlString.lowercaseString rangeOfString:@"/imgclassroom/mystudy?"].location != NSNotFound)
        {
            self.tabBarController.selectedIndex = 1;
            return YES;
        }
    }
    
	//행복한 인문학당 2.0
	if ([urlString.lowercaseString rangeOfString:@"/lecture/category_inmun.aspx"].location != NSNotFound){
		[app setLectureUrlType:@"7"];
		[app setSelectedTabIndex:1];		
		return NO;
	}
    
    //행복한 인문학당 4.0
    if ([urlString.lowercaseString rangeOfString:@"/knowledgelive"].location != NSNotFound){
        return YES;
    }
    
    if ([[app.tabBarController.viewControllers objectAtIndex:1] isKindOfClass:[LectureViewController class]]) {
        if ([urlString.lowercaseString rangeOfString:@"/lecture/category.aspx?param="].location != NSNotFound){
            NSArray * param = [urlString componentsSeparatedByString:@"="];
            self.tabBarController.selectedIndex = 1;
            LectureViewController *view = (LectureViewController*)self.tabBarController.selectedViewController;
            view._currentUrl = @"";
            view._param = [param objectAtIndex:1];
            [self.tabBarController.selectedViewController viewWillAppear:YES];
            return NO;
        }
    #ifdef nps_4258
    #else
        //주제별 교육과정
        if ([urlString.lowercaseString rangeOfString:@"/lecture/category"].location != NSNotFound){
            [app setLectureUrlType:@"6"];
            [app setSelectedTabIndex:1];

            return NO;
        }
    #endif   
        
        //주제별 교육과정 상세
        if ([urlString.lowercaseString rangeOfString:@"/lecture/depth.aspx"].location != NSNotFound){
            [app setLectureUrlType:@"6"];	
            [app setSelectedTabIndex:1 indexOrUrl:urlString];
            return NO;
        }
    }
    
	//상담실-공지사항, 공지사항 상세
	if ([urlString.lowercaseString rangeOfString:@"/counsel/counsel_noticelist.aspx"].location != NSNotFound
        || [urlString.lowercaseString rangeOfString:@"/counsel/counsel_noticedetail.aspx"].location != NSNotFound
        || [urlString.lowercaseString rangeOfString:@"/counsel/notice"].location != NSNotFound){
        
        self.tabBarController.selectedIndex = [self getTabIndexOfClass:[MoreViewController class]];
        NSString *viewTypeNum = @"13";
        if ([urlString.lowercaseString rangeOfString:@"/counsel/counsel_noticedetail.aspx"].location != NSNotFound
            || [urlString.lowercaseString rangeOfString:@"/counsel/notice?noticeseq"].location != NSNotFound) {
            
            NSString *noticeParam = [urlString.lowercaseString substringFromIndex:[urlString.lowercaseString rangeOfString:@"noticeseq"].location];
            viewTypeNum = [NSString stringWithFormat:@"20&%@", noticeParam];
        }
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        NSString *noticeUrl = [NSString stringWithFormat:@"%@?type=%@&uid=%@&pw=%@",
                                app.urlBase,
                                viewTypeNum,
                                [app.profileUser objectForKey:@"userid"],
                                [app.profileUser objectForKey:@"password"]];
        
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
        NoticeViewController *second = [[NoticeViewController alloc] initWithNibName:@"NoticeViewController" bundle:nil];
        second.title = @"공지사항";
        second.url = noticeUrl;
        NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
        [navi setViewControllers:controllersArray animated:YES];
        
		return NO;
	}
    
    if ([urlString.lowercaseString rangeOfString:@"more://"].location != NSNotFound ||
        [urlString.lowercaseString rangeOfString:@"/more/more.aspx"].location != NSNotFound){
        self.tabBarController.selectedIndex = [self getTabIndexOfClass:[MoreViewController class]];
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        [navi popToRootViewControllerAnimated:YES];
        return NO;
    }
    
    if ([urlString.lowercaseString rangeOfString:@"/counsel/myrequest"].location != NSNotFound
        || [urlString.lowercaseString rangeOfString:@"/counsel/counsel.aspx"].location != NSNotFound) {
        self.tabBarController.selectedIndex = [self getTabIndexOfClass:[MoreViewController class]];
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
        QnaViewController *second = [[QnaViewController alloc] initWithNibName:@"QnaViewController" bundle:nil];
        second.title = NSLocalizedString(@"1d1", @"1:1咨询");
        NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
        [navi setViewControllers:controllersArray animated:YES];
        
        return NO;
    }
	
	//홍화면 편집
	if ([urlString.lowercaseString rangeOfString:@"/home/home_edit.aspx"].location != NSNotFound){
		return YES;
	}	
	
    
	//pc버전 링크
	if ([urlString.lowercaseString isEqualToString:[NSString stringWithFormat:@"%@/", app.urlCenter]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:app.urlCenter]];
		return NO;
	}
    
    //상상마루
    if ([urlString.lowercaseString rangeOfString:@"/sangsang/sangsanglink.aspx"].location != NSNotFound
        || [urlString.lowercaseString rangeOfString:@"/login/mlclogin.aspx"].location != NSNotFound
        || [urlString.lowercaseString rangeOfString:@"/submain.aspx?type=app"].location != NSNotFound
        || [urlString.lowercaseString rangeOfString:@"https://ssl.hunet.co.kr"].location != NSNotFound
        || [urlString.lowercaseString rangeOfString:@"/imghome"].location != NSNotFound) {
        
        if ([app.eduType isEqualToString:@"LearningAndImagine"]) {
            int selectedTabIndex = [self getTabIndexOfClass:[SSViewController class]];
            if (selectedTabIndex == -1) {
                selectedTabIndex = [self getTabIndexOfClass:[SelfStudyViewController class]];
            }
            if (selectedTabIndex > -1) {
                self.tabBarController.selectedIndex = selectedTabIndex;
                return NO;
            }
        }
        
        //NSString *ssUrl = [NSString stringWithFormat:@"%@/submain.aspx?type=app", [app.urlCenter stringByReplacingOccurrencesOfString:@"http://" withString:@"http://m."]];
        
        //NSLog(@"%@", ssUrl);
        
//		if ([urlString.lowercaseString isEqualToString:ssUrl.lowercaseString] || [urlString.lowercaseString rangeOfString:@"https://ssl.hunet.co.kr"].location != NSNotFound) {
//			SSViewController *viewToPush = [[SSViewController alloc] initWithNibName:@"SSViewController" bundle:nil];
//			if (viewToPush) {
//				viewToPush.url = urlString;
//				[self presentViewController:viewToPush animated:YES completion:nil];
//			}
//            
//            [self performSelector:@selector(viewLoad) withObject:nil afterDelay:0.3];
//            
//			return NO;
//		}
        return YES;
    }
    
    if ([urlString hasSuffix:@".mp4"]) {
        //    if ([urlString.lowercaseString rangeOfString:@"htmlplayer://"].location != NSNotFound) {
        NSLog(@"htmlplayer");
        
        //urlString = @"http://w.hunet.hscdn.com/hunet/files/knowledge/High_20150209034650_08_%EC%A7%81%EC%9E%A5%EC%9D%B8%EC%9D%98%EA%B8%B0%EB%B3%B8%EC%83%81%EC%8B%9D_540.mp4";
        
        NSLog(@"urlString = %@", urlString);
        
        //NSString *currentURL = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
        //self.urlMark = currentURL;
        
        NSString *mp4Url = [urlString stringByReplacingOccurrencesOfString:@"htmlplayer://" withString:@"http://"];
        
        NSUserDefaults *userdeDefaults = [NSUserDefaults standardUserDefaults];
        [userdeDefaults setObject:mp4Url forKey:FILENAME];
        //NSLog(@"Streaming filename = %@", [userdeDefaults objectForKey:FILENAME]);
        
        
        MoviePlayerController * movieCon = [[MoviePlayerController alloc] initWithNibName:@"MoviePlayerController" bundle:nil];
        movieCon.isStreamingPlay = TRUE;
        movieCon.currentSecond = 0;
        movieCon.scrollType =  1;   // progress touch 여부 0 : 진도바 이동 불가, 1 : 진도바 이동 가능
        [MLCenterForBusinessAppDelegate sharedAppDelegate].moviePlayerController = movieCon;
        [self presentViewController:movieCon animated:YES completion:nil];
        
        return NO;
    }
    
    //apptoapp://?schemeurl=&storeurl=
    //apptoapp://?schemeurl=hunetceo%3A%2F%2F%3Fuserid%3Daaa%26userpw%3Dbbb&storeurl=http%3A%2F%2Fapps.hunet.co.kr%2Fdown%2Fhunetceo.htm
    if ([urlString hasPrefix:@"apptoapp://?"]) {
        NSDictionary *protocalParams = [MLCenterForBusinessAppDelegate getProtocalParams:request.URL];
        NSString *schemeurl = [protocalParams objectForKey:@"schemeurl"];
        NSString *storeurl = [protocalParams objectForKey:@"storeurl"];
        BOOL isInstalled = [[UIApplication sharedApplication] openURL:[NSURL URLWithString:schemeurl]];
        if (!isInstalled) {
            // 설치 되어 있지 않습니다! 앱스토어로 안내...
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:storeurl]];
        }
        return NO;
    }
    
    if([[[request URL] absoluteString] hasPrefix:@"jscall://"])
    {
        NSString *requestString = [[request URL] absoluteString];
        NSArray *components = [requestString componentsSeparatedByString:@"://"];
        NSString *functionName = [[[components objectAtIndex:1] componentsSeparatedByString:@"?"] objectAtIndex:0];
        NSNumber *fileSelectCount = [NSNumber numberWithInt:[[Util valueForKey:@"count" fromQuery:[request URL].query] integerValue]];
        
        if ([functionName.lowercaseString isEqualToString:@"callphotoview"]) {
            [self performSelector:@selector(callPhotoView:) withObject:fileSelectCount];
            
        } else {
            SEL selector = NSSelectorFromString(functionName);
            IMP imp = [self methodForSelector:selector];
            void (*func)(id, SEL) = (void *)imp;
            func(self, selector);
        }
        
        
        return NO;
    }
    
	//그외 더보기 메뉴로 이동, 국민연금 진단
#ifdef PRO
//	if ([urlString.lowercaseString rangeOfString:@"/app/jlog.aspx"].location == NSNotFound
//		&& [urlString.lowercaseString rangeOfString:@"/home/home.aspx"].location == NSNotFound
//        && [urlString.lowercaseString rangeOfString:@"/home"].location == NSNotFound
//        && [urlString.lowercaseString rangeOfString:@"/areas/4258/"].location == NSNotFound
//        && [urlString.lowercaseString rangeOfString:@"/lecture/detail.aspx"].location == NSNotFound
//        && [urlString.lowercaseString rangeOfString:@"/study/study_summary.aspx"].location == NSNotFound
//        && [urlString.lowercaseString rangeOfString:@"/study/capacity_diagnosis.aspx"].location == NSNotFound
//        && [urlString.lowercaseString rangeOfString:@"study/h_class.aspx"].location == NSNotFound
//        && [urlString.lowercaseString rangeOfString:@"study/h_class.aspx"].location == NSNotFound
//        && [urlString.lowercaseString rangeOfString:@"/hclass"].location == NSNotFound
//        && [urlString.lowercaseString rangeOfString:@"/lecture/humanities"].location == NSNotFound
//        && [urlString.lowercaseString rangeOfString:@"/submain"].location == NSNotFound) {
//        self.tabBarController.selectedIndex = 4;
//        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
//        [navi popToRootViewControllerAnimated:YES];
//		return NO;
//	}
    if ([urlString.lowercaseString rangeOfString:@"/counsel"].location != NSNotFound) {
        self.tabBarController.selectedIndex = 4;
        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        [navi popToRootViewControllerAnimated:YES];
        return NO;
    }
#endif
    
	return YES;
}

- (void)callPhotoView:(NSNumber*)selectCount {
    ELCAlbumPickerController *albumController = [[ELCAlbumPickerController alloc] initWithNibName: nil bundle: nil];
    ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initWithRootViewController:albumController];
    [albumController setParent:elcPicker];
    [elcPicker setDelegate:self];
    elcPicker.maximumImagesCount = [selectCount intValue] == 0 ? 4 : [selectCount intValue];
    
    [self presentViewController:elcPicker animated:YES completion:nil];
}

- (int)getTabIndexOfClass:(Class)class {
    int tabIndex = -1;
    NSArray *arrVC = [MLCenterForBusinessAppDelegate sharedAppDelegate].tabBarController.viewControllers;
    for (int i = 0; i < [arrVC count]; i++) {
        if ([[arrVC objectAtIndex:i] isKindOfClass:[UINavigationController class]]) {
            if ([[((UINavigationController*)[arrVC objectAtIndex:i]).viewControllers objectAtIndex:0] isKindOfClass:class]) {
                tabIndex = i;
                break;
            }
        }
        else {
            if ([[arrVC objectAtIndex:i] isKindOfClass:class]) {
                tabIndex = i;
                break;
            }
        }
    }
    return tabIndex;
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    if ([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]){
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)pickerWebSave:(NSArray *)info
{
    @autoreleasepool {
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        
        NSLog(@"%@", app.urlCenter);
        
        NSString *urlString, *boundary;
        imgFileName = @"";
        NSInteger i = 0;
        urlString = [NSString stringWithFormat:@"%@/Common/MobileUpload?path=Community", app.urlCenter];
        boundary = @"CommunityPicImage";
        
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setCachePolicy:NSURLRequestUseProtocolCachePolicy];
        [request setHTTPMethod:@"POST"];
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        
        /*
         NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
         
         [[NSRunLoop currentRunLoop] run];
         
         
         // Schedule your connection to run on threads runLoop.
         [connection scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
         */
        
        
        /*
         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         NSRunLoop *loop = [NSRunLoop currentRunLoop];
         [connection scheduleInRunLoop:loop forMode:NSRunLoopCommonModes];
         [loop run]; // make sure that you have a running run-loop.
         });
         */
        
        
	for(NSDictionary *dict in info) {
            UIImage *image = [dict objectForKey:UIImagePickerControllerOriginalImage];
            NSData *ImageData = UIImageJPEGRepresentation(image, 1.0);
            NSMutableData *body = [NSMutableData data];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image%d\"; filename=\"Image%d.jpg\"\r\n", i, i] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:ImageData]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [request setHTTPBody:body];
            
            NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
            NSString *returnString = [[NSString alloc] initWithData:urlData encoding:NSUTF8StringEncoding];
            
            NSLog(@"%@", returnString);
            
            imgFileName = [imgFileName stringByAppendingFormat:@"|%@", returnString];
            
            
            i=i+1;
	}
        
        if ([info count] == i) {
            [self performSelectorOnMainThread:@selector(saveEnd:) withObject:imgFileName waitUntilDone:NO];
        }
    }
}


- (void)saveEnd:(NSString*) ImgFileNames
{
	[mask removeFromSuperview];
	[uploadIndicator stopAnimating];
    [viewWeb stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"ReadingPhoto('%@');", ImgFileNames]];
}


- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    if ([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]){
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    uploadIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    uploadIndicator.hidesWhenStopped = YES;
    
    
    mask = [[UIView alloc] initWithFrame:[[self.view window] frame]];
	mask.userInteractionEnabled = NO;
	mask.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.50];
    [mask addSubview:uploadIndicator];
    uploadIndicator.center = mask.center;
	[self.view addSubview:mask];
	
	[uploadIndicator startAnimating];
    
    
    [NSThread detachNewThreadSelector:@selector(pickerWebSave:) toTarget:self withObject:info];
    
}

- (void)viewDidLoad {
    //NSLog(@"viewDidLoad");
    _backBtn.layer.cornerRadius = 5;
    _backBtn.layer.masksToBounds = YES;
    [super viewDidLoad];
	[self viewLoad];
	
    NSArray *vComp = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[vComp objectAtIndex:0] intValue] >= 7) {
        [self setNeedsStatusBarAppearanceUpdate];
        self.edgesForExtendedLayout = UIRectEdgeNone;
        // iOS 7 or above
        CGRect oldBounds = [self.view bounds];
        CGRect newViewBounds = CGRectMake(0, -10, oldBounds.size.width, oldBounds.size.height - 20);
        CGRect newWebViewBounds = CGRectMake(0, -20, oldBounds.size.width, oldBounds.size.height - 40);
        if (self.view.bounds.origin.y != newViewBounds.origin.y) {
            [self.view setBounds:newViewBounds];
            [viewWeb setBounds:newWebViewBounds];
        }
    }
}

- (IBAction)backClick:(id)sender {
    [viewWeb goBack];
    NSLog(@"abc");
}

- (void)viewLoad {
    NSLog(@"viewLoad");
    _backBtn.hidden = true;
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
	if (app.flagLogin)
    {
        NSString *urlText = [NSString stringWithFormat:@"%@?type=4&uid=%@&pw=%@", app.urlBase, [app.profileUser objectForKey:@"userid"], [app.profileUser objectForKey:@"password"]];
        
        [viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0]];
        
        //viewWeb.scalesPageToFit = YES;
    }
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (NSUInteger)supportedInterfaceOrientations
{
    indicator.center = viewWeb.center;
    //return UIInterfaceOrientationMaskAll;
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)prefersStatusBarHidden
{
    //return UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]);
    return NO;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {

	viewWeb = nil;
	indicator = nil;
    [super viewDidUnload];
}


- (void)dealloc {
	viewWeb.delegate = nil;
}



- (void)webViewpushUrl:(NSString *)webViewpushUrl {
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    [viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:webViewpushUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0]];
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    app.pushService = nil;
    //self.appDelegate.pushService = nil;
}

- (void)openerCallFun {
    NSLog(@"player close");
    
    if (self.urlMark != nil) {
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:self.urlMark]
                                                    cachePolicy: NSURLRequestReloadIgnoringCacheData
                                                timeoutInterval:60.0
                                    ];
        [viewWeb loadRequest:requestObj];
        self.urlMark = @"";
    }
}

- (void)openQuiz {
    NSString *Quizyns = self.QuizYn;
    //NSString *urlMarks = self.urlMark;

    NSLog(@"player close2");
    if (self.urlMark != nil) {
        if ([Quizyns isEqualToString:@"Y"]) {
            self.urlMark = [self.urlMark stringByReplacingOccurrencesOfString:@"ImagineDetail"
                                                                   withString:@"QuizView"];
            self.urlMark = [self.urlMark stringByReplacingOccurrencesOfString:@"contents_seq"
                                                                   withString:@"contentSeq"];
            
        }
        
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:self.urlMark]
                                                    cachePolicy: NSURLRequestReloadIgnoringCacheData
                                                timeoutInterval:60.0
                                    ];
        [viewWeb loadRequest:requestObj];
        self.urlMark = @"";
    }
}

@end
