//
//  SangSang.h
//  MLCenterForBusiness
//
//  Created by Joey on 13. 9. 30..
//
//

#import <Foundation/Foundation.h>

@interface Sangsang : NSObject
@property (strong, nonatomic) NSString *goods_id;
@property (strong, nonatomic) NSString *user_id;
@property (strong, nonatomic) NSString *goods_nm;
@property (strong, nonatomic) NSString *study_end_date;
@property (strong, nonatomic) NSString *se_goods_id;
@end 
