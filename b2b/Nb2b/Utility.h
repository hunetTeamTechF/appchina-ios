#import <Foundation/Foundation.h>

enum {
    MY_FILE = 0,
    MY_DIRECOTRY = 1,
    NOT_FOUND = -1,
} FILETYPE;

@interface Utility : NSObject {
    
}

+(NSString*)getFileFullPath:(NSString*)folder fileName:(NSString*)fileName;
+(int)isFileExists:(NSString*)folder fileName:(NSString*)fileName;
+(NSString*)createDirecotryBy:(NSString*)folderName;
+(NSData*)fileToData:(NSString*)folder fileName:(NSString*)fileName;
+(void)dataToFile:(NSString*)folder data:(NSData*)data fileName:(NSString*)fileName;

+(BOOL)UUIDExists;
+(NSString*)loadUUID;
+(void)saveUUID:(NSString*)uuid;

+(int)loadBitLength;
+(void)saveBitLength:(int)length;

@end
