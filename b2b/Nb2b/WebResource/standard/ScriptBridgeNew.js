/**
 * 
 */
if (navigator.userAgent.indexOf("ScriptBridgeNew") > -1 && !window.ScriptBridge && !window.IsScriptBridgeNew) {
    window.IsScriptBridgeNew = true;
    window.scriptBridgeLoad = function (func) {
        if (window.ScriptBridge) {
            func();
        } else {
            setTimeout(function () {
                window.scriptBridgeLoad(func);
            }, 1000);
        }
    };

    var scriptBridgeCallBacks = {};
    $(function () {
        var ScriptBridge = {
            StatusMembers: {
                isSaveCompanySeq : false,
                isSaveId: false,
                isAutoLogin: false,
                isStaging: false,
                isOnline: false,
                hasDownloadContents: false,
            },
            /////////////////////// void start ///////////////////////
            DoLogin: function (id, pwd, isSaveCompanySeq, isSaveId, isAutoLogin, companySeq) {
                var parameters = {
                      id: id
                    , pwd: pwd
                    , isSaveCompanySeq: isSaveCompanySeq
                    , isSaveId: isSaveId
                    , isAutoLogin: isAutoLogin
                };
                if (companySeq) {
                    parameters.companySeq = companySeq; // 있을수도 있고 없을수도 있다.
                }
                this.NativeCall("DoLogin", parameters);
            },
            changePassword: function (password) {
                var parameters = {
                    password: password
                };

                this.NativeCall("changePassword", parameters);
            },
            goToDownloadCenter: function () {
                this.NativeCall("goToDownloadCenter", null);
            },
            goToLogin: function () {
                this.NativeCall("goToLogin");
            },
            /////////////////////// void end ///////////////////////

            /////////////////////// bool start ///////////////////////
            isSaveCompanySeq: function (successCallback) {
                return this.NativeCall("isSaveCompanySeq", {}, successCallback);
            },
            isSaveId: function (successCallback) {
                return this.NativeCall("isSaveId", {}, successCallback);
            },
            isAutoLogin: function (successCallback) {
                return this.NativeCall("isAutoLogin", {}, successCallback);
            },
            isStaging: function (successCallback) {
                return this.NativeCall("isStaging", {}, successCallback);
            },
            isOnline: function (successCallback) {
                return this.NativeCall("isOnline", {}, successCallback);
            },
            hasDownloadContents: function (successCallback) {
                return this.NativeCall("hasDownloadContents", {}, successCallback);
            },
            existsInstalledApp: function (scheme, successCallback) {
                var parameters = {
                    urlScheme: scheme
                };
                return this.NativeCall("existsInstalledApp", parameters, successCallback);
            },
            //구현필요
            //IsHDRM - BOOL
            //ExistsDownloadFileOfCourse:: - BOOL 
            //existsInstalledApp: - BOOL
            //isOnline - BOOL
            //getAppVersion - String

            /////////////////////// bool end ///////////////////////

            NativeCall: function (command, params, successCallback, errorCallback) {
                if (command.indexOf("a") == 0) {
                    alert(command);
                }
                var call = {
                    type: "scriptbridge" 		// 구분
                    , command: command 		// 동작 아이디
                    , param: {} 			// JSON 오브젝트 형태
                };

                if (params && typeof (params) == "object") {
                    call.param = params;
                }

                if (successCallback == undefined) {
                    if (this.StatusMembers[command] != undefined && typeof (this.StatusMembers[command]) != "undefined") {
                        return this.StatusMembers[command];
                    }
                } else {
                    if (typeof (successCallback) == "function") {
                        scriptBridgeCallBacks[command] = {
                            success: successCallback,
                            error: typeof (errorCallback) == "function" ? errorCallback : function (msg) {
                                alert('Error : ' + JSON.stringify(msg));
                            }
                        };

                        // 성공시 돌려받을 콜백 펑션 명 void 형 펑션의 경우 '
                        call.successCallbackName = "scriptBridgeCallBacks." + command + ".success";

                        // 실패시 돌려받을 콜백 펑션 명 void 형 펑션의 경우 ''
                        call.errorCallbackName = "scriptBridgeCallBacks." + command + ".error";
                    }
                }
                var EncodedStr = encodeURIComponent(JSON.stringify(call));
                window.location = 'app://' + EncodedStr;
            },
            reload: function () {
                //콜백방식은 기존 코드에서는 정적호출이기 때문에, 신규 앱에서 별도의 콜백으로 처리하는 방식으로는 처리불가
                //기존 코드 변경없이 사용을 위해서는 페이지 로드 시, 설정값들은 미리 불러서 매핑하는 방식을 사용(콜백 오는 시점도 알 수 없기 때문)
                //또한, 페이지로드가 완료되기 전에 해당 네이티브 콜을 쏘게 되면, 페이지가 죽어버리기 때문에 $(function(){}) 사용
                //해당 네이티브 콜을 동시 콜 할 경우, 콜 간에 개입으로 인해 오류발생하므로, 순차적 콜이 필요
                var obj = this;
                try{
                    obj.NativeCall("isSaveCompanySeq", null, function (msg0) {
                     obj.StatusMembers["isSaveCompanySeq"] = msg0.result;
                        obj.NativeCall("isSaveId", null, function (msg1) {
                            obj.StatusMembers["isSaveId"] = msg1.result;
                                window.ScriptBridge = obj;                            
                        });
                    });
                } catch (e) {
                    window.ScriptBridge = obj;
                }
            }
        };
        ScriptBridge.reload();
    });
}
