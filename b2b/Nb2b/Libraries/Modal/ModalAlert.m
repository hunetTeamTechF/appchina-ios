/*
 Erica Sadun, http://ericasadun.com
 iPhone Developer's Cookbook, 3.0 Edition
 BSD License, Use at your own risk
 */

/*
 Thanks to Kevin Ballard for suggesting the UITextField as subview approach
 All credit to Kenny TM. Mistakes are mine. 
 To Do: Ensure that only one runs at a time -- is that possible?
 */

#import "ModalAlert.h"
#import "MLCenterForBusinessAppDelegate.h"

@interface ModalAlertDelegate : NSObject <UIAlertViewDelegate>
{
	CFRunLoopRef currentLoop;
	NSUInteger index;
}
@property (readonly) NSUInteger index;
@end

@implementation ModalAlertDelegate
@synthesize index;

// Initialize with the supplied run loop
-(id) initWithRunLoop: (CFRunLoopRef)runLoop 
{
	if (self = [super init]) currentLoop = runLoop;
	return self;
}

// User pressed button. Retrieve results
-(void) alertView: (UIAlertView*)aView clickedButtonAtIndex: (NSInteger)anIndex 
{
	index = anIndex;
	CFRunLoopStop(currentLoop);
}
@end

@implementation ModalAlert
+(NSUInteger) queryWith: (NSString *)question button1: (NSString *)button1 button2: (NSString *)button2
{
	CFRunLoopRef currentLoop = CFRunLoopGetCurrent();
	
	// Create Alert
	ModalAlertDelegate *madelegate = [[ModalAlertDelegate alloc] initWithRunLoop:currentLoop];
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:question message:nil delegate:madelegate cancelButtonTitle:button1 otherButtonTitles:button2, nil];
	[alertView show];
	
	// Wait for response
	CFRunLoopRun();
	
	// Retrieve answer
	NSUInteger answer = madelegate.index;
	return answer;
}

+(NSUInteger) query3With: (NSString *)question button1: (NSString *)button1 button2: (NSString *)button2 button3: (NSString *)button3
{
	CFRunLoopRef currentLoop = CFRunLoopGetCurrent();
	
	// Create Alert
	ModalAlertDelegate *madelegate = [[ModalAlertDelegate alloc] initWithRunLoop:currentLoop];
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:question message:nil delegate:madelegate cancelButtonTitle:button1 
											  otherButtonTitles:button2, button3, nil];
	[alertView show];
	
	// Wait for response
	CFRunLoopRun();
	
	// Retrieve answer
	NSUInteger answer = madelegate.index;
	return answer;
}

// to tell the truth, notify is simple modaless!
+ (void) notify: (NSString *) notification {
//	[ModalAlert queryWith:notification button1:@"예" button2:nil];
	UIAlertView* alert = [[UIAlertView alloc] initWithTitle:notification message:nil delegate:nil cancelButtonTitle:@"确认" otherButtonTitles: nil];
	[alert show];
	
}

+ (BOOL) ask: (NSString *) question
{
	return	([ModalAlert queryWith:question button1: @"是" button2: @"否"] == 0);
}

+ (BOOL) confirm: (NSString *) statement
{
	return	[ModalAlert queryWith:statement button1: @"取消" button2: @"确认"];
}

+ (BOOL) askCancel: (NSString *) statement
{
	return	[ModalAlert queryWith:statement button1: @"继续" button2: @"取消"];
}

+ (void) message: (NSString *) msg
{
    [ModalAlert queryWith:msg button1: @"确认" button2: nil];
}

+ (void) toast:(NSString *)message
{
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    // Queue 초기화
    if (app.toastQueue == nil) {
        app.toastQueue = [[NSMutableArray alloc] init];
        app.toastVisibled = NO;
    }
    // 메시지가 있으면 enqueue
    if (message != nil) {
        [app.toastQueue addObject:message];
    }
    // 토스트가 떠있는 상태이면 return
    if (app.toastVisibled) {
        return;
    }
    
    // 상태값 설정
    app.toastVisibled = YES;
    // 메시지 설정
    message = [app.toastQueue objectAtIndex:0];
    
    // UI요소 처리
    UIView *currentView = [self topMostController].view;
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    //CGRect initialFrame = CGRectMake((screenBounds.size.width - 280) / 2, screenBounds.size.height - 100, 280, 40);
    //UILabel *flashLabel=[[UILabel alloc] initWithFrame:initialFrame];
    UILabel *flashLabel = [[UILabel alloc] initWithFrame:CGRectInset(currentView.frame, 20, 20)];
    //UILabel *flashLabel=[[UILabel alloc] init];
    flashLabel.font=[UIFont systemFontOfSize:15.0f];
    //    flashLabel.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
    flashLabel.backgroundColor = [UIColor blackColor];
    flashLabel.textColor = [UIColor whiteColor];
    flashLabel.layer.cornerRadius=9.0f;
    flashLabel.clipsToBounds = YES;
    flashLabel.numberOfLines=10;
    flashLabel.textAlignment=NSTextAlignmentCenter;
    CGSize maxSize = CGSizeMake(flashLabel.frame.size.width, MAXFLOAT);
    CGRect labelRect = [message boundingRectWithSize:maxSize
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{NSFontAttributeName:flashLabel.font}
                                             context:nil];
    
    //adjust the label the the new height.
    CGRect newFrame = flashLabel.frame;
    newFrame.size.width = 280;
    newFrame.size.height = labelRect.size.height + 40;
    newFrame.origin.x = (screenBounds.size.width - 280) / 2;
    newFrame.origin.y = screenBounds.size.height - newFrame.size.height - 40;
    flashLabel.frame = newFrame;
    flashLabel.text = message;
    [currentView addSubview:flashLabel];
    flashLabel.alpha=0.7;
    //view.userInteractionEnabled=FALSE;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void){
        [UIView animateWithDuration:1.0f animations:^
         {
             flashLabel.alpha=0.0f;
         } completion:^(BOOL finished)
         {
             //view.userInteractionEnabled=TRUE;
             [flashLabel removeFromSuperview];
             
             app.toastVisibled = NO;
             [app.toastQueue removeObjectAtIndex:0];
             if ([app.toastQueue count] > 0) {
                 [ModalAlert toast:nil];
             }
         }];
    });
}

+ (void) toast:(NSString *)message andDelay:(double)delay
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void){
        [ModalAlert toast:message];
    });
}

+ (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController
           && ![topController.presentedViewController isKindOfClass:[UIAlertController class]]) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

+ (UIViewController*) topMostViewController
{
    UIViewController *topViewController = [self topMostController];
    if ([topViewController isKindOfClass:[UITabBarController class]]) {
        topViewController = [(UITabBarController*)topViewController selectedViewController];
    }
    if ([topViewController isKindOfClass:[UINavigationController class]]) {
        topViewController = [[(UINavigationController*)topViewController childViewControllers] lastObject];
    }
    
    return topViewController;
}

@end

