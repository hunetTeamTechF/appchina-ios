#import "Web.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "Util.h"
#import "CJSONDeserializer.h"
#import "DrmManager.h"

@implementation Web

@synthesize receivedData;
@synthesize response;
@synthesize result;
@synthesize fileHandle;
@synthesize bIsComplete;
@synthesize downloadInfo;

-(id) init
{
    if( self=[super init] )
    {
    }
    return self;
}

- (NSString *)requestDownLoad:(NSString *)url{
    NSLog(@"download Start");
    
    //url = [url lowercaseString];
    NSLog(@"Download > moviePath(before) : %@", url);
    if ([url hasPrefix:@"http://hunet2.hscdn.com/hunetvod/_definst_/mp4:"]) {
        url = [url stringByReplacingOccurrencesOfString:@"http://hunet2.hscdn.com/hunetvod/_definst_/mp4:" withString:@"http://m.hunet.hscdn.com/hunet/"];
    }
    url = [url stringByReplacingOccurrencesOfString:@"/playlist.m3u8" withString:@""];
    url = [url stringByReplacingOccurrencesOfString:@"http://m.hunet" withString:@"http://w.hunet"];
    url = [url stringByReplacingOccurrencesOfString:@"http://m2.hunet" withString:@"http://w2.hunet"];
    NSLog(@"Download > moviePath(after) : %@", url);
    
    
    NSMutableURLRequest *httpRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                               cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                           timeoutInterval:10.0];
    
    [httpRequest setHTTPMethod:@"GET"];
    //    [httpRequest setValue:@"Streamingway Sniper HD Player" forHTTPHeaderField:@"User-Agent"];  //필수
    [httpRequest setValue:kDrmPrefix forHTTPHeaderField:@"User-Agent"];  //필수
    
    NSURLConnection *tmpcon = [[NSURLConnection alloc] initWithRequest:httpRequest delegate:self];
    
    if(tmpcon){
        
        receivedData = [NSMutableData data];
        hData = [NSMutableData data];
    }
    
    receivedData = [[NSMutableData alloc] initWithLength:0];
    isHeader = YES;
    
    bytesReceived = 0.0;
    expectedBytes = 0.0;
    
    [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.countPg.text = [NSString stringWithFormat:@"%d/%d",1,1];
    
    bIsComplete = NO;
    while (!bIsComplete)
    {
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    return result;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)aResponse
{
    self.response = aResponse;
    [receivedData setLength:0];
    
    NSHTTPURLResponse *r = (NSHTTPURLResponse*) response;
    NSDictionary *headers = [r allHeaderFields];
    
    int statusCode = [r statusCode];
    NSLog(@"StatusCode:[%d]", statusCode);
    
    if (statusCode >=400)
    {
        [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.lb_AvTitle.hidden = NO;
        [[MLCenterForBusinessAppDelegate sharedAppDelegate].loading.ac_Loading stopAnimating];
        [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.sizePg.hidden = YES;
        [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.lb_AvTitle.text = @"因服务器错误，被强制退出";
        
        bIsComplete = YES;
        [connection cancel];
        result = @"ERROR";
    }else{
        if (headers){
            if ([headers objectForKey: @"Content-Range"]) {
                NSString *contentRange = [headers objectForKey: @"Content-Range"];
                NSRange range = [contentRange rangeOfString: @"/"];
                NSString *totalBytesCount = [contentRange substringFromIndex: range.location + 1];
                expectedBytes = [totalBytesCount floatValue];
                NSLog(@"File size : %f",(float)expectedBytes);
            } else if ([headers objectForKey: @"Content-Length"]) {
                expectedBytes = [[headers objectForKey: @"Content-Length"] floatValue];
                NSLog(@"File size : %f",(float)expectedBytes);
            }
        }
        
        uint64_t freeSize = strtoull([[[Util freeDiskspace] objectAtIndex:1] UTF8String], NULL, 0);
        uint64_t intexpectedBytes = (uint64_t)expectedBytes;
        if (intexpectedBytes > freeSize) {
            
            [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.lb_AvTitle.hidden = NO;
            [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.lb_AvTitle.numberOfLines = 9;
            [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.lb_AvTitle.text = [NSString stringWithFormat:@"现在您下载中的视频容量为 %@, 可使用空间为 %@.\n\n请先确认容量空间后重新尝试！",
                                                                       [Util prettyBytes:intexpectedBytes], [Util prettyBytes:freeSize]];
            [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.pgView.hidden = YES;
            [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.sizePg.hidden = YES;
            [[MLCenterForBusinessAppDelegate sharedAppDelegate].loading.ac_Loading stopAnimating];
            
            bIsComplete = YES;
            [connection cancel];
            result = @"ERROR";
        }
        else
        {
            NSString *saveFilePath = [self getSaveFilePath];
            [self deleteSaveFile];
            NSLog(@"PATH : %@",saveFilePath);
            NSFileManager *fileManager = [NSFileManager defaultManager];
            [fileManager createFileAtPath:saveFilePath contents:nil attributes:nil];
            self.fileHandle = [NSFileHandle fileHandleForUpdatingAtPath:saveFilePath];
            [[MLCenterForBusinessAppDelegate sharedAppDelegate] addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:saveFilePath]];
        }
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    bytesReceived = (bytesReceived + [data length]);
    if(expectedBytes != NSURLResponseUnknownLength) {
        
        [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.pgView.progress = ((bytesReceived/(float)expectedBytes)*100)/100;
        [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.sizePg.text = [NSString stringWithFormat:@"%.1fM/%.1fM",((bytesReceived /1024.0)/1024.0),((expectedBytes /1024.0)/1024.0)];
    }
    if (isHeader) {
        //암호화 시작
        NSLog(@"---HeaderData----");
        [hData appendData:data];
        int headerDataLength = [hData length];
        if (headerDataLength > [kDrmEnctyptBytesSize intValue]) {
            [receivedData appendData:[DrmManager encrypt:hData totalBytes:expectedBytes]];
            isHeader = NO;
        }
        //암호화 종료
    } else {
        if (bIsComplete)
        {
            [connection cancel];
            result =@"cancel";
            [self deleteSaveFile];
        }else{
            [receivedData appendData:data];
            
            if ([receivedData length] > 3000000) {
                [fileHandle seekToEndOfFile];
                [fileHandle writeData:receivedData];
                if (receivedData) {
                    [receivedData setLength:0];
                }
            }
        }
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    bIsComplete = YES;
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if ([receivedData length] != 0) {
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:receivedData];
        if (receivedData) {
            [receivedData setLength:0];
        }
    }
    [fileHandle closeFile];
    
    
    NSString *course_cd = [downloadInfo objectForKey:@"course_cd"];
    NSString *take_course_seq = [downloadInfo objectForKey:@"take_course_seq"];
    NSString *chapter_no = [downloadInfo objectForKey:@"chapter_no"];
    NSString *display_no = [downloadInfo objectForKey:@"display_no"];
    NSString *study_max_mark_sec = [downloadInfo objectForKey:@"max_mark_sec"];
    
    NSString *total_sec = @"0";
    NSString *index_nm = [downloadInfo objectForKey:@"index_nm"];
    NSString *user_id = [downloadInfo objectForKey:@"user_id"];
    NSString *study_end_date = [downloadInfo objectForKey:@"study_end_date"];
    NSString *course_nm = [downloadInfo objectForKey:@"course_nm"];
    NSString *file_nm = [self getSaveFileName];
    
    //상상마루
    NSString *view_sec = [downloadInfo objectForKey:@"view_sec"];
    NSString *view_sec_mobile = [downloadInfo objectForKey:@"view_sec_mobile"];
    NSString *last_view_sec = [downloadInfo objectForKey:@"last_view_sec"];
    //NSString *url = [downloadInfo objectForKey:@"url"];
    //NSString *pass_yn = [downloadInfo objectForKey:@"pass_yn"];
    //NSString *quiz_yn = [downloadInfo objectForKey:@"quiz_yn"];
    //NSString *last_mark_no = [downloadInfo objectForKey:@"last_mark_no"];
    NSString *view_no = [downloadInfo objectForKey:@"view_no"];
    NSString *goods_nm = [downloadInfo objectForKey:@"goods_nm"];
    NSString *contents_seq = [downloadInfo objectForKey:@"contents_seq"];
    NSString *goods_id = [downloadInfo objectForKey:@"goods_id"];
    NSString *se_goods_id = [downloadInfo objectForKey:@"se_goods_id"];
    NSString *contents_nm = [downloadInfo objectForKey:@"contents_nm"];
    //NSString *contents_sec = [downloadInfo objectForKey:@"contents_sec"];
    NSString *contract_no = [downloadInfo objectForKey:@"contract_no"];
    
    bIsComplete = YES;
    [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.totalPgView.progress = (CGFloat)1/(CGFloat)1;
    [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.lb_AvTitle.text = @"下载成功！";
    [[MLCenterForBusinessAppDelegate sharedAppDelegate].loading.ac_Loading stopAnimating];
    [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.sizePg.hidden = YES;
    [[MLCenterForBusinessAppDelegate sharedAppDelegate].loading.btnResult setImage:[UIImage imageNamed:@"btn_ok.png"] forState:UIControlStateNormal];
    [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.btnResult.tag = 99;
    
    if (!course_cd) {
        //상상마루 다운로드
        //NSLog(@"%@", goods_id);
        //NSArray *sangsangDb = [Util getSangsang:goods_id];
        NSArray *sangsangDb = [Util getSangsang:se_goods_id];
        if ([sangsangDb count] <= 0) {
            NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
            NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Sangsang" inManagedObjectContext:context];
            NSManagedObject *newSangsang = [[NSManagedObject alloc] initWithEntity:entitydesc insertIntoManagedObjectContext:context];
            [newSangsang setValue:goods_id forKey:@"goods_id"];//Set with series goods_id
            [newSangsang setValue:goods_nm forKey:@"goods_nm"];
            [newSangsang setValue:user_id forKey:@"user_id"];
            [newSangsang setValue:study_end_date forKey:@"study_end_date"];
            [newSangsang setValue:se_goods_id forKey:@"se_goods_id"];
            NSError *error = nil;
            [context save:&error];
        }
        
        NSArray *studySangDb = [Util getSangStudy:se_goods_id andContentsSeq:contents_seq andUserId:user_id andViewNo:view_no];
        if ([studySangDb count] <= 0)
        {
            NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
            NSEntityDescription *indexentitydesc = [NSEntityDescription entityForName:@"SangsangStudy" inManagedObjectContext:context];
            NSManagedObject *newSangStudy = [[NSManagedObject alloc] initWithEntity:indexentitydesc insertIntoManagedObjectContext:context];
            [newSangStudy setValue:goods_id forKey:@"goods_id"];
            [newSangStudy setValue:[NSNumber numberWithInt:[view_no intValue]] forKey:@"view_no"];
            [newSangStudy setValue:[NSNumber numberWithInt:[contents_seq intValue]] forKey:@"contents_seq"];
            [newSangStudy setValue:contents_nm forKey:@"contents_nm"];
            [newSangStudy setValue:[NSNumber numberWithInt:[view_sec intValue]] forKey:@"view_sec"];
            [newSangStudy setValue:[NSNumber numberWithInt:[view_sec_mobile intValue]] forKey:@"view_sec_mobile"];
            [newSangStudy setValue:[NSNumber numberWithInt:[last_view_sec intValue]] forKey:@"last_view_sec"];
            [newSangStudy setValue:[NSNumber numberWithInt:[contents_seq intValue]] forKey:@"contents_seq"];
            [newSangStudy setValue:[NSNumber numberWithInt:[contract_no intValue]] forKey:@"contract_no"];
            
            [newSangStudy setValue:study_end_date forKey:@"study_end_date"];
            [newSangStudy setValue:user_id forKey:@"user_id"];
            [newSangStudy setValue:file_nm forKey:@"file_nm"];
            [newSangStudy setValue:se_goods_id forKey:@"se_goods_id"];
            
            NSError *error = nil;
            [context save:&error];
        }
        
        [NSThread detachNewThreadSelector:@selector(sangDownCompletion) toTarget:self withObject:nil];
        
    }else{
        //온라인 다운로드
        NSArray *courseDb = [Util getCourse:course_cd andTakeCourseSeq:take_course_seq];
        if (courseDb.count <= 0) {
            NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
            NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Course" inManagedObjectContext:context];
            NSManagedObject *newCourse = [[NSManagedObject alloc] initWithEntity:entitydesc insertIntoManagedObjectContext:context];
            [newCourse setValue:course_cd forKey:@"course_cd"];
            [newCourse setValue:[NSNumber numberWithInteger:[take_course_seq intValue]] forKey:@"take_course_seq"];
            [newCourse setValue:course_nm forKey:@"course_nm"];
            [newCourse setValue:study_end_date forKey:@"study_end_date"];
            NSError *error = nil;
            [context save:&error];
        }
        
        NSArray *studyDb = [Util getStudy:course_cd andTakeCourseSeq:take_course_seq andChapterNo:chapter_no];
        if (studyDb.count <= 0)
        {
            NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
            NSEntityDescription *indexentitydesc = [NSEntityDescription entityForName:@"Study" inManagedObjectContext:context];
            NSManagedObject *newStudy = [[NSManagedObject alloc] initWithEntity:indexentitydesc insertIntoManagedObjectContext:context];
            [newStudy setValue:course_cd forKey:@"course_cd"];
            [newStudy setValue:[NSNumber numberWithInt:[take_course_seq intValue]] forKey:@"take_course_seq"];
            [newStudy setValue:chapter_no forKey:@"chapter_no"];
            [newStudy setValue:[NSNumber numberWithInt:[display_no intValue]] forKey:@"display_no"];
            [newStudy setValue:index_nm forKey:@"index_nm"];
            [newStudy setValue:user_id forKey:@"user_id"];
            [newStudy setValue:file_nm forKey:@"file_nm"];
            
            @try {
                [newStudy setValue:[NSNumber numberWithInt:[study_max_mark_sec intValue]]  forKey:@"max_sec"];
            }
            @catch (NSException *exception) {
                
            }
            @finally {
                
            }
            
            [newStudy setValue:[NSNumber numberWithInt:[total_sec intValue]]  forKey:@"total_sec"];
            
            NSError *error = nil;
            [context save:&error];
        }
        
        [NSThread detachNewThreadSelector:@selector(downCompletion) toTarget:self withObject:nil];
    }
}

- (NSString*)getSaveFileName {
    NSString *user_id = [downloadInfo objectForKey:@"user_id"];
    NSString *take_course_seq = [downloadInfo objectForKey:@"take_course_seq"];
    NSString *chapter_no = [downloadInfo objectForKey:@"chapter_no"];
    NSString *display_no = [downloadInfo objectForKey:@"display_no"];
    NSString *contract_no = [downloadInfo objectForKey:@"contract_no"];
    NSString *contents_seq = [downloadInfo objectForKey:@"contents_seq"];
    NSString *file_nm = [NSString stringWithFormat:@"hdrm_%@_%@_%@_%@_%@_%@.mp4"
                         , user_id ? user_id : @""
                         , take_course_seq ? take_course_seq : @""
                         , chapter_no ? chapter_no : @""
                         , display_no ? display_no : @""
                         , contract_no ? contract_no : @""
                         , contents_seq ? contents_seq : @""];
    
    return file_nm;
}

- (NSString*)getSaveFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *file_nm = [self getSaveFileName];
    NSString *saveFilePath = [NSString stringWithFormat:@"%@/%@/%@", documentsDirectory, kDrmFolderName, file_nm];
    return saveFilePath;
}

- (void)deleteSaveFile {
    [[NSFileManager defaultManager] removeItemAtPath:[self getSaveFilePath] error:NULL];
}

- (void)downCompletion {
    @autoreleasepool {
        NSString *uid = [[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"userid"];
        NSError *errorReq = nil;
        NSString* jsonUrl = [MLCenterForBusinessAppDelegate sharedAppDelegate].urlBase;
        NSString* jsonParam = [NSString stringWithFormat:@"type=30&courseCd=%@&takeCourseSeq=%@&chapterNo=%@&downloadType=2&deviceNm=ios&regId=%@",
                               [downloadInfo objectForKey:@"course_cd"],
                               [downloadInfo objectForKey:@"take_course_seq"],
                               [downloadInfo objectForKey:@"chapter_no"],
                               uid];
        
        //NSLog(@"%@", jsonParam);
        NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:jsonUrl]
                                              andQuery:jsonParam
                                              andError:&errorReq];
        
        NSError *errorJSON = nil;
        NSDictionary *results = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
        
        if ([[results objectForKey:@"result"] isEqualToString:@"OK"]) {
            [Util updateMaxSec:[downloadInfo objectForKey:@"course_cd"]
              andTakeCourseSeq:[downloadInfo objectForKey:@"take_course_seq"]
                  andChapterNo:[downloadInfo objectForKey:@"chapter_no"]
                     andMaxSec:[results objectForKey:@"max_study_sec"]];
        }
        
    }
}

- (void)sangDownCompletion {
    @autoreleasepool {
        NSString *uid = [[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"userid"];
        NSError *errorReq = nil;
        NSString* jsonUrl = @"http://apps.hunet.co.kr/Hunet_Player/Android.aspx";
        NSString* jsonParam = [NSString stringWithFormat:@"action=SangSangDrmDownloadComplete&goodsId=%@&contentsSeq=%@&companySeq=2162&contractNo=%@&downloadType=2&deviceNm=iphone&regId=%@",
                               [downloadInfo objectForKey:@"goods_id"],
                               [downloadInfo objectForKey:@"contents_seq"],
                               [downloadInfo objectForKey:@"contract_no"],
                               uid];
        
        //NSLog(@"%@", jsonParam);
        NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:jsonUrl]
                                              andQuery:jsonParam
                                              andError:&errorReq];
        
        NSError *errorJSON = nil;
        NSDictionary *results = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
        
        if ([[results objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
            //        [Util updateMaxSec:[downloadInfo objectForKey:@"course_cd"]
            //          andTakeCourseSeq:[downloadInfo objectForKey:@"take_course_seq"]
            //              andChapterNo:[downloadInfo objectForKey:@"chapter_no"]
            //                 andMaxSec:[results objectForKey:@"max_study_sec"]];
        }
        
    }
}

@end