

#import <Foundation/Foundation.h>


@interface Global : NSObject {
//    NSString *_drmInfo;
}

//@property (strong, nonatomic) NSString *_drmInfo;

@property (strong, nonatomic) NSString *testPrimaryDomainFix;
@property (strong, nonatomic) NSString *testPrimaryDomain;

@property (strong, nonatomic) NSMutableDictionary *profileUser;

+ (Global *) sharedSingleton;
- (NSString *)testPrimaryDomainUrl:(NSString *)url;
- (NSString *)encodePassword:(NSString *)passwd;

+ (void)setSharedData:(NSString *)text withKey:(NSString *)key;
+ (NSString *)getSharedData:(NSString *)key;

+ (NSString*) getUserId;
+ (void) setUserId:(NSString*)text;
+ (NSString*) getUserPassword;
+ (void) setUserPassword:(NSString*)text;
+ (NSString*) getUserInfo:(NSString*)key;
+ (void) setUserInfo:(NSString*)text forKey:(NSString*)key;
+ (NSString *)checkTestDomain:(NSString *)orgUrl;

+ (NSString *)urlEncode:(NSString *)plainText;
+ (NSString *)urlDecode:(NSString *)url;
+ (NSString *) getAppType;

@end
