#import <Foundation/Foundation.h>

@interface Web : NSObject {
        
	NSMutableData *receivedData;
    NSMutableData *hData;
    NSURLResponse *response;
	NSString *result;
	BOOL bIsComplete;
    BOOL isHeader;
    NSFileHandle *fileHandle;
    
    float	bytesReceived;
	long long   expectedBytes;
    NSDictionary *downloadInfo;
	
}
- (NSString *)requestDownLoad:(NSString *)url;
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)aResponse;
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;

@property (nonatomic, strong) NSDictionary *downloadInfo;
@property (nonatomic, strong) NSFileHandle *fileHandle;
@property (nonatomic, strong) NSMutableData *receivedData;
@property (nonatomic, strong) NSURLResponse *response;
@property (nonatomic, strong) NSString *result;
@property (nonatomic, assign) BOOL bIsComplete;
@property (nonatomic, strong) NSString *contractNo;

@end
