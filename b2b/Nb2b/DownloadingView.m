
#import "DownloadingView.h"
#import "MLCenterForBusinessAppDelegate.h"

@interface DownloadingView ()

@end

@implementation DownloadingView
@synthesize delegate=_delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.ac_Loading startAnimating];
    self.pgView.progress = 0;
    self.totalPgView.progress = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    
    _delegate = nil;
    
}
- (void)viewDidUnload {
    _delegate = nil;
    [self setLb_AvTitle:nil];
    [self setAc_Loading:nil];
    [self setPgView:nil];
    [self setSizePg:nil];
    [self setTotalPgView:nil];
    [self setCountPg:nil];
    [self setBtnResult:nil];
    
    [super viewDidUnload];
}

- (IBAction)removeWebUtil:(id)sender {
    UIButton *button = (UIButton*)sender;
    if (button.tag == 99) {
        if ([_delegate respondsToSelector:@selector(processAction)]) {
            [_delegate processAction];
        }
    }
    
    MLCenterForBusinessAppDelegate *dg = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    self.webUtil.bIsComplete = YES;
    self.webUtil = nil;
    [dg drmremoveLodingView];
}


- (NSUInteger)supportedInterfaceOrientations
{
    NSLog(@"downloadingview supportedInterfaceOrientations");
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskAllButUpsideDown;
}

- (BOOL)shouldAutorotate
{
    NSLog(@"downloadingview shouldAutorotate");
    return YES;
}

@end
