

#import <UIKit/UIKit.h>

@interface DownloadViewController : UITableViewController

@property (strong, nonatomic) NSArray *courseArray;
@property (strong, nonatomic) NSArray *sangsangArray;
@property (strong, nonatomic) UISegmentedControl *segmentedControl;
@property CGRect segmentedControlPortraitOrientationFrame;


//download tab 이름
@property (nonatomic, strong) NSString *courseTab;
@property (nonatomic, strong) NSString *imagineTab;

@property (strong, nonatomic) NSArray *downloadInfoArray;

//과정
@property (nonatomic, strong) NSString *courseCd;
@property (nonatomic, strong) NSString *takeCourseSeq;

//상상마루
@property (nonatomic, strong) NSString *goodsId;
@property (nonatomic, strong) NSString *seGoodsId;
@property (nonatomic, assign) NSInteger contractNo;
@property (nonatomic, assign) NSInteger contentsSeq;
@property (nonatomic, assign) NSInteger viewNo;
@property (nonatomic, strong) NSString *userId;

@property (nonatomic, assign) NSInteger expirationDay;
@end
