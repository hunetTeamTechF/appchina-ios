//
//  PlayerViewController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 6. 3..
//  Copyright 2011 hunet. All rights reserved.
//

#import "PlayerViewController.h"
#import "ModalAlert.h"

#import "CJSONDeserializer.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "MarkViewController.h"
#import "OverlayView.h"

#define FULLSIZE CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)
#define PLAYERSMALLSIZE CGRectMake(0, 0, 200, 150)
#define OVERLAYVIEWFULLSIZE CGRectMake(0, 20, self.view.frame.size.height, self.view.frame.size.width)
#define OVERLAYVIEWSMALLSIZE CGRectMake(0, 20, 200, 150)
#define IMAGEVIEWSMALLSIZE CGRectMake(202, 0, self.view.frame.size.height - 204, 320)
#define TABLEVIEWSIZE CGRectMake(0, 152, 200, 168)

#define ENCODE(X) [(X) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]

@implementation PlayerViewController
@synthesize ccd, tcseq, cno, fno, playerInfo, currentTimer;
- (NSDictionary *)playerInfo {
	
	if (playerInfo == nil) {

		NSURLResponse* response;
		NSError* errorNet;
		NSError* errorJSON;	
	
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
		
		// send it as a POST message
		NSString* body = [NSString stringWithFormat:@"type=11&uid=%@&ccd=%@&tcseq=%@&cno=%@&fno=%@", [Global getUserId], self.ccd, self.tcseq, self.cno, self.fno];
		body = ENCODE(body);
        
        //NSLog(@"%@?%@", app.urlBase, body);
        
		NSString* url = [NSString stringWithFormat:@"%@", app.urlBase];
		NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
															   cachePolicy:NSURLRequestUseProtocolCachePolicy 
														   timeoutInterval:20.0];
        
        //NSLog(@"%@?%@", url, body);
		
		
		[request setHTTPMethod:@"POST"];
		[request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
		[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
		
		UIApplication* appIndicator = [UIApplication sharedApplication];
		appIndicator.networkActivityIndicatorVisible = YES;
		NSData* dataReceived = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorNet];
		appIndicator.networkActivityIndicatorVisible = NO;
		
		if (dataReceived) {		// success
            playerInfo = [[CJSONDeserializer deserializer] deserializeAsDictionary:dataReceived error:&errorJSON];
			if ([[playerInfo objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {  // success... 
			} else {
				//[ModalAlert notify:@"리스트 받아오는데 실패했습니다."];
			}
		} else {
			[ModalAlert notify:@"네트워크 연결에 문제가 생겼습니다."];
		}
	}
	return playerInfo;
	
}


- (BOOL)isVideo {
	
	if ([[self.playerInfo objectForKey:@"PlayType"] isEqualToString:@"M"]) {
		return YES;
	}
	
	return NO;
}

 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	 if (self != nil) {
		 lastMarkIndexNo = -1;
		 [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent];
         appDelegate = (MLCenterForBusinessAppDelegate*)[[UIApplication sharedApplication] delegate];
         sumTimer = 0;
	 }
	 return self;
 }


- (void)viewDidLoad {
    [super viewDidLoad];
	
	[[self view] setFrame:[[UIScreen mainScreen] applicationFrame]];
	self.view.backgroundColor = [UIColor blackColor];
	
	indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	indicator.hidesWhenStopped = YES;
	indicator.center = CGPointMake(FULLSIZE.size.width / 2, FULLSIZE.size.height / 2);
	[self.view addSubview:indicator];
	[indicator startAnimating];
    
	NSURL *movieURL = [NSURL URLWithString:[self.playerInfo objectForKey:@"MobileMovUrl"]];
    //NSURL *movieURL = [NSURL URLWithString:[self.playerInfo objectForKey:@"DrmMovUrl"]];
    if (movieURL)
	{
		if ([movieURL scheme])
			[appDelegate initAndPlayMovie:movieURL];
	}

	if ([appDelegate.moviePlayer respondsToSelector:@selector(loadState)]) {
		[appDelegate.moviePlayer setControlStyle:MPMovieControlStyleFullscreen];
		[appDelegate.moviePlayer setFullscreen:YES];
		appDelegate.moviePlayer.controlStyle = MPMovieControlStyleNone;
		appDelegate.moviePlayer.movieSourceType = MPMovieSourceTypeStreaming;
		appDelegate.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
		appDelegate.moviePlayer.view.userInteractionEnabled = YES;
		[appDelegate.moviePlayer prepareToPlay];
		
		[[NSNotificationCenter defaultCenter] addObserver:self 
												 selector:@selector(moviePlayerLoadStateChanged:) 
													 name:MPMoviePlayerLoadStateDidChangeNotification 
												   object:nil];
		
	} 
	
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(moviePlayBackDidFinish:) 
												 name:MPMoviePlayerPlaybackDidFinishNotification 
											   object:nil];
	
	
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(moviePlayBackStateDidChange:)
												 name:MPMoviePlayerPlaybackStateDidChangeNotification 
											   object:nil];
	
	
    
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {

	if((interfaceOrientation == UIInterfaceOrientationLandscapeLeft)
	   || (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
	{
		return YES;
	}
	
	return NO;
}

- (BOOL) shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    
    if (UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
    return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;
{
    if(toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft 
       || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {   
        [UIApplication sharedApplication].statusBarHidden = YES;
    }
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation { 
    UIInterfaceOrientation toOrientation = self.interfaceOrientation;
    if(toOrientation == UIInterfaceOrientationLandscapeLeft || toOrientation == UIInterfaceOrientationLandscapeRight) {                                     
        if (appDelegate.moviePlayer.view.frame.size.width == PLAYERSMALLSIZE.size.width) {
			[UIApplication sharedApplication].statusBarHidden = YES;
		} else {
			[overlayView statusBarDisplay];
		}
    }
}




- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    

	self.ccd = nil;
	self.tcseq = nil;
	self.cno = nil;
	self.fno = nil;
	self.playerInfo = nil;
	self.currentTimer = nil;
	indicator = nil;
	viewImageppt = nil;
	markView = nil;
	overlayView = nil;
    
    [super viewDidUnload];

}


- (void)dealloc {

	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:MPMoviePlayerPlaybackStateDidChangeNotification
												  object:nil];
}


- (void) moviePlayerLoadStateChanged:(NSNotification*)notification {
    
    
    //NSLog(@"%d", appDelegate.moviePlayer.loadState);
	    
    if (appDelegate.moviePlayer.loadState & MPMovieLoadStateStalled) {
        [appDelegate.moviePlayer pause];
    } else if (appDelegate.moviePlayer.loadState & MPMovieLoadStatePlaythroughOK) {
		
		[indicator stopAnimating];
		
		[[NSNotificationCenter defaultCenter] removeObserver:self
														name:MPMoviePlayerLoadStateDidChangeNotification 
													  object:nil];

        
		[[self view] setBounds:FULLSIZE];
		[[appDelegate.moviePlayer view] setFrame:(self.isVideo) ? FULLSIZE : PLAYERSMALLSIZE];
		[[self view] addSubview:[appDelegate.moviePlayer view]];
		[appDelegate.moviePlayer play];
		appDelegate.moviePlayer.currentPlaybackTime = [self currentSliderPosition];
		
		overlayView = [[OverlayView alloc] init];
		overlayView.tag = 300;
		overlayView.sliderProcess.maximumValue = appDelegate.moviePlayer.duration;
        overlayView.sliderProcess.userInteractionEnabled = [[self.playerInfo objectForKey:@"ProgressRestrictionYn"] isEqualToString:@"N"];
        overlayView.btnBack.userInteractionEnabled = overlayView.sliderProcess.userInteractionEnabled;
        overlayView.btnForward.userInteractionEnabled = overlayView.sliderProcess.userInteractionEnabled;
        
        if ([[self.playerInfo objectForKey:@"MarkShowYn"] isEqualToString:@"Y"]) {
            overlayView.btnMarkShow.hidden = NO;
            overlayView.markList = [NSMutableArray arrayWithArray:[playerInfo objectForKey:@"Result"]];
        }       
		
		if (self.isVideo) {
			[UIApplication sharedApplication].statusBarHidden = NO;
			[appDelegate.moviePlayer.view addSubview:overlayView];
            [self timerStart];
			return;
		}
		
		UIView *viewBlank = [[UIView alloc] initWithFrame:appDelegate.moviePlayer.view.frame];
		if (viewBlank) {
			viewBlank.tag = 100;
			[appDelegate.moviePlayer.view addSubview:viewBlank];
		}		

        
		NSURL *_url = [NSURL URLWithString:ENCODE([self.playerInfo objectForKey:@"LastPPTUrl"])];
		NSData *data = [NSData dataWithContentsOfURL:_url];
		UIImage *image = [UIImage imageWithData:data];
         
		viewImageppt = [[UIImageView alloc] initWithImage:image];
		viewImageppt.frame = IMAGEVIEWSMALLSIZE;
		viewImageppt.userInteractionEnabled = YES;
		viewImageppt.tag = 200;
		[self.view addSubview:viewImageppt];
        
        

        markView = [[MarkViewController alloc] initWithDictionary:self.playerInfo];
		if (markView) {
            markView.cellSelected = overlayView.sliderProcess.userInteractionEnabled;
			markView.view.frame = TABLEVIEWSIZE;
			[self.view addSubview:markView.view];
		}
        
        [self timerStart];
        //NSLog(@"moviePlayerLoadStateChanged");
    }
}


- (void) timerStart {
    currentTimer = [NSTimer scheduledTimerWithTimeInterval:0.5
													target:self
												  selector:@selector(onTimer:)
												  userInfo:nil
												   repeats:YES];
}

- (int)currentSliderPosition {
	
	int lsec = [[self.playerInfo objectForKey:@"LastMarkValue"] intValue];
	if ((lsec + 5) >= appDelegate.moviePlayer.duration)
		return 0;
	
	return lsec;
	
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {    
	[currentTimer invalidate];
    
    if (lastMarkIndexNo > 0 && lastMarkIndexNo != -1) {

        [NSThread detachNewThreadSelector:@selector(markSave:) 
                                 toTarget:self 
                               withObject:[[[self.playerInfo objectForKey:@"Result"] objectAtIndex:lastMarkIndexNo] objectForKey:@"MarkNo"]];
    }
    
    
    

	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:MPMoviePlayerPlaybackDidFinishNotification 
												  object:nil];

    [[UIApplication sharedApplication] setStatusBarHidden:NO];
	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];

	[self dismissViewControllerAnimated:YES completion:nil];
}


- (void)moviePlayBackStateDidChange:(NSNotification*)notification {

    if (appDelegate.moviePlayer.playbackState == MPMoviePlaybackStatePaused) {
		overlayView.btnPause.hidden = YES;
		overlayView.btnPlay.hidden = NO;		
	} else if (appDelegate.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
		overlayView.btnPlay.hidden = YES;
		overlayView.btnPause.hidden = NO;
	}
}



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
	UITouch *touch = [touches anyObject];
	NSUInteger tapCount = [touch tapCount];		
	
	//NSLog(@"%d", touch.view.tag);
	
    switch (tapCount) {
        case 1:
			[self performSelector:@selector(singleTapMethod:) withObject:[NSNumber numberWithInt:touch.view.tag] afterDelay:.4];
            break;
        case 2:
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(singleTapMethod:) object:[NSNumber numberWithInt:touch.view.tag]];
            [self performSelector:@selector(doubleTapMethod:) withObject:[NSNumber numberWithInt:touch.view.tag] afterDelay:.4];
            break;
        default:
            break;
			
    }
}

-(void)singleTapMethod:(NSNumber*)tag {
	
	if ([tag intValue] != 300) {
		return;
	}
	
	[overlayView displayItems];
}


-(void)doubleTapMethod:(NSNumber*)tag {
	
    if (self.isVideo) {
		return;
	}
	
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.75];
	[UIView setAnimationDelegate:self];
	
	if ([tag intValue]  == 200) {
		
		if (viewImageppt.frame.size.width == IMAGEVIEWSMALLSIZE.size.width) {
			viewImageppt.frame = FULLSIZE;
			markView.view.frame = CGRectMake(TABLEVIEWSIZE.origin.x, TABLEVIEWSIZE.origin.y, 0, TABLEVIEWSIZE.size.height);
		} else {
			viewImageppt.frame = IMAGEVIEWSMALLSIZE;
			markView.view.frame = TABLEVIEWSIZE;
		}
		
	} else {
		
		if (appDelegate.moviePlayer.view.frame.size.width == PLAYERSMALLSIZE.size.width) {
			
			[overlayView statusBarDisplay];
			
			appDelegate.moviePlayer.view.frame = FULLSIZE;		
			[appDelegate.moviePlayer.view addSubview:overlayView];
			
            viewImageppt.frame = CGRectMake(FULLSIZE.size.width, 0, 0, 0);
			markView.view.frame = CGRectMake(0, FULLSIZE.size.height, 0, 0);            
		} else {
			[UIApplication sharedApplication].statusBarHidden = YES;
			[overlayView removeFromSuperview];
			appDelegate.moviePlayer.view.frame = PLAYERSMALLSIZE;
			viewImageppt.frame = IMAGEVIEWSMALLSIZE;
			markView.view.frame = TABLEVIEWSIZE;
			
		}		
	}
    
	[UIView commitAnimations];
}


- (void)onTimer:(NSTimer *)timer {
    if (appDelegate.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
		[self updateForPlayerInfo];
	}
}

- (void)updateForPlayerInfo {
	
    //NSLog(@"updateForPlayerInfo : %d", [[self.playerInfo objectForKey:@"Result"] count]);
    
    long currentPlaybackTime = appDelegate.moviePlayer.currentPlaybackTime;
	int currentHours = (currentPlaybackTime / 3600);
	int currentMins = ((currentPlaybackTime / 60) - currentHours * 60);
	int currentSecs = (currentPlaybackTime % 60);
	overlayView.lblCurrent.text = [NSString stringWithFormat:@"%i:%02d:%02d", currentHours, currentMins, currentSecs];
	overlayView.sliderProcess.value = currentPlaybackTime;
	
	long totalPlaybackTime = appDelegate.moviePlayer.duration - appDelegate.moviePlayer.currentPlaybackTime;
	int tHours = (totalPlaybackTime / 3600);
	int tMins = ((totalPlaybackTime / 60) - tHours * 60);
	int tSecs = (totalPlaybackTime % 60);
	overlayView.lblDuration.text = [NSString stringWithFormat:@"-%i:%02d:%02d", tHours, tMins, tSecs];
    
    sumTimer += 1;
    
    //NSLog(@"updateForPlayerInfo : %d", sumTimer);
    
	NSArray *list = [NSArray arrayWithArray:[self.playerInfo objectForKey:@"Result"]];	
	
	int markValue = 0;
	int lastIndexNo = 0;
	for (int i = 0; i < [list count]; i++) {
        
		NSDictionary *dt = [list objectAtIndex:i];		
		markValue = [[dt objectForKey:@"MarkValue"] intValue];
		
		if (markValue < currentPlaybackTime) {
			lastIndexNo = i;
		}			
	}
	
	if ([list count] > 0) {
		[self progressSave:lastIndexNo];
	}
}



- (void)progressSave:(int)index {

	if (lastMarkIndexNo == index) {
		return;
	}
    
    //NSLog(@"progressSave");
	
	lastMarkIndexNo = index;
	
	NSArray *list = [NSArray arrayWithArray:[self.playerInfo objectForKey:@"Result"]];
	
	NSURL *_url = [NSURL URLWithString:ENCODE([[list objectAtIndex:index] objectForKey:@"PPTUrl"])];
	NSData *data = [NSData dataWithContentsOfURL:_url];
	UIImage *image = [UIImage imageWithData:data];
	viewImageppt.image = image;

    
	[markView reloadList:[[list objectAtIndex:index] objectForKey:@"MarkNo"]];
		
	[NSThread detachNewThreadSelector:@selector(markSave:) 
							 toTarget:self 
						   withObject:[[list objectAtIndex:index] objectForKey:@"MarkNo"]];
}

-(BOOL)prefersStatusBarHidden { return YES; }


- (void)markSave:(NSString *)markNo {
	
	@autoreleasepool {
	
		NSURLResponse* response;
		NSError* errorNet;
		NSError* errorJSON;	
		
		// send it as a POST message
		
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    int study_sec = [[self.playerInfo objectForKey:@"StudySec"] intValue];
    int mobile_study_sec = [[self.playerInfo objectForKey:@"MobileStudySec"] intValue];
    int mobile_current_sec = (int)floor(sumTimer / 2.0);
		
		NSString* body = [NSString stringWithFormat:@"type=12&uid=%@&ccd=%@&tcseq=%@&cno=%@&fno=%@&mno=%@&ss=%d&msc=%d", 
						  [Global getUserId],
						  self.ccd,
						  self.tcseq,
						  self.cno,
						  self.fno,
						  markNo,
                      study_sec + mobile_current_sec,
                      mobile_study_sec + mobile_current_sec];
		
    //NSLog(@"markSave : %d, markNo : %@", (int)floor(sumTimer / 2.0), markNo);
		//NSLog(@"%@", body);
		
		
		body = ENCODE(body);
		NSString* url = [NSString stringWithFormat:@"%@", app.urlBase];
    
    
    //NSLog(@"markSave  %@?%@", url, body);
		
		NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
															   cachePolicy:NSURLRequestUseProtocolCachePolicy 
														   timeoutInterval:20.0];
		[request setHTTPMethod:@"POST"];
		[request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
		[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
		
		UIApplication* appIndicator = [UIApplication sharedApplication];
		appIndicator.networkActivityIndicatorVisible = YES;
		NSData* dataReceived = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorNet];
		appIndicator.networkActivityIndicatorVisible = NO;
		
		if (dataReceived) {		// successfully received the result
			NSDictionary* result = [[CJSONDeserializer deserializer] deserializeAsDictionary:dataReceived error:&errorJSON];
			
			if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"NO"]) {  // success... 
				//[ModalAlert notify:@"진도 저장중 오류 발생했습니다."];
			}
		} else {
			//[ModalAlert notify:@"네트워크 연결에 문제가 생겼습니다."];		
		}

	}
}



@end
