//
//  URLAction.h
//  MLCenterForBusiness
//
//  Created by KwonOh june on 2017. 2. 14..
//
//  중구난방 흩어져 있는 URL별 동작 처리를 한 곳에서 하게끔 기초공사만 해둔다.
//  샘플로 BoardViewController에 적용함 (AMC간호부)
//
//  반드시 init 시점에 UIViewController를 넣어줘야함.
//  checkUrlAction을 각기 흩어져있는 뷰컨트롤러 내부의 아래 메서드에 집어넣어야 한다.
//  -(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
//
//  옮겨오는 과정에서 macro로 분기 처리된 것은 안옮김 (해당 UIViewController에 남겨두기로 함)
//

#ifndef URLAction_h
#define URLAction_h
#endif

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import <AssetsLibrary/AssetsLibrary.h>

#import "ELCImagePickerController.h"
#import <WebKit/WebKit.h>
#import "PlayerCallFun.h"
#import "Web.h"
#import <MobileCoreServices/MobileCoreServices.h>

#define URLACTION_EXIST NO

@interface URLAction : NSObject <ELCImagePickerControllerDelegate, UIImagePickerControllerDelegate> {
    
    NSString *imgFileName;
    UIActivityIndicatorView *uploadIndicator;
    UIView *mask;
    UIWebView *parentWebView;
    Web *webUtil;
}

@property (nonatomic, strong) PlayerCallFun *playerCallFun;
@property (nonatomic, strong) UIViewController *attachedViewController;

// 초기화는 !!!반드시!!! 뷰컨트롤러를 넣어주세요 아래와 같이
// URLAction *action = [[URLAction alloc] init:self];
- (id)init:(UIViewController *)parentViewController;

// 실제 커스텀 액션 처리 부분 상세내용은 안에 기능별 주석으로 달려있다.
- (BOOL)checkUrlAction:(UIWebView *)webView andRequest:(NSURLRequest *)request;

- (void)refreshCurrentPage;

@end
