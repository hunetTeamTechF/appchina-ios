//
//  LocalNotifierViewController.h
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 7. 1..
//  Copyright 2011 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LocalNotifierViewController : UIViewController <UITableViewDataSource,UITableViewDelegate> {
	IBOutlet UITableView *tableview;
	IBOutlet UIDatePicker *datePicker;
	UISwitch *switchNoti;
}

@property (nonatomic, strong) IBOutlet UITableView *tableview;
@property (nonatomic, strong) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, strong) UISwitch *switchNoti;

- (IBAction) scheduleAlarm:(id) sender;

@end
