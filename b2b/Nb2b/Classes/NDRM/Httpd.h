#import <Foundation/Foundation.h>
#import "HTTPServer.h"
#import "DDLog.h"
#import "DDTTYLogger.h"

#define DOCROOT_NAME @"docroot"

@class HTTPServer;

@interface Httpd : NSObject {
@private
    HTTPServer *httpServer;
    
@public
    NSString *docroot;
    int port;
}

@property (retain, nonatomic) NSString *docroot;
@property (assign, nonatomic) int port;

-(id)initWithRootFolder:(NSString*)folderName;
-(void)start;
-(void)stop;
-(BOOL)isRunning;
-(NSString*)docRootFullpath;

@end
