

#import <UIKit/UIKit.h>

@interface DownloadIndexViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>


@property (nonatomic, strong) NSArray *courseArray;
@property (nonatomic, strong) NSArray *studyArray;

@property (nonatomic, strong) NSString *coursecd;
@property (nonatomic, strong) NSString *takecourseseq;
@property (nonatomic, assign) NSInteger expirationDay;
@property (strong, nonatomic) NSString *synMessage;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UILabel *offlineMessage;
@end
