

#import "MyViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "ModalAlert.h"
#import "PlayerViewController.h"
#import "SangPlayerViewController.h"
#import "SSViewController.h"
#import "HLeaderController.h"
#import	"Reachability.h"
#import "WebtoonViewController.h"
#import "ModalViewController.h"
#import "CJSONDeserializer.h"
#import "Util.h"
#import "Global.h"
#import "MoviePlayerController.h"
#import "MoreViewController.h"
#import "DownloadViewController.h"
#import "DownloadIndexViewController.h"
#import "HTMLPlayerController.h"
#import "Header.h"
#import "EngViewController.h"
#import "WebModalViewController.h"
#import "MP3PlayerController.h"
#import "GraphicsUtile.h"

@implementation MyViewController
@synthesize urlMark;
@synthesize playerCallFun = _playerCallFun;

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        
        self.title = NSLocalizedString(@"myCourse", @"我的课堂");
        self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_lectureroom"];
#if avene || kcc
        self.tabBarItem.titlePositionAdjustment = UIOffsetMake(2, -16);        
     //   self.tabBarItem.image = nil;
#elif hyundai || kia || hyundaiDymos || hyundaiWia || hyundaiHmtc || Wiajs || Wiash || dymosrz
        self.tabBarItem.titlePositionAdjustment = UIOffsetMake(2, -5);
        self.title = @"学习";
#endif
        
    }
    return self;
}



- (void)dealloc {
    
    viewWeb.delegate = nil;
}


- (void)viewDidUnload {
    
    viewWeb = nil;
    indicator = nil;
    self.urlMark = nil;
    [self setPlayerCallFun:nil];
    
    [super viewDidUnload];
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    if ([viewWeb.subviews count] > 0) {
        UIView *scrollView = viewWeb.subviews[0];
        
        for (UIView *childView in scrollView.subviews) {
            if ([childView isKindOfClass:[UIWebDocumentView class]]) {
                UIWebDocumentView *documentView = (UIWebDocumentView *)childView;
                WebScriptObject *wso = documentView.webView.windowScriptObject;
                
                [wso setValue:[WebScriptBridge getWebScriptBridge] forKey:@"ScriptBridge"];
            }
        }
    }
    [indicator stopAnimating];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
    [indicator startAnimating];
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
    [indicator stopAnimating];
    //[ModalAlert notify:@"네트워크 연결에 문제가 생겼습니다."];
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    
    NSURL *url = request.URL;
    NSString *urlString = url.absoluteString;
    
    NSLog(@"%@", urlString);
    
    if (urlString == nil) {
        return YES;
    }
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    if ([urlString.lowercaseString hasPrefix:@"back://"]) {
        [viewWeb goBack];
    }
    
    
    if ([urlString.lowercaseString hasPrefix:@"httpweb"]) {
        NSString *Nurls = [urlString stringByReplacingOccurrencesOfString:@"httpweb://" withString:@"http://"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:Nurls]];
        return NO;
    }
    
    
    //모달 창으로 뛰우기
    if ([urlString.lowercaseString hasPrefix:@"modal://"]) {
        
        NSDictionary *simpleModalUrlDic = [NSDictionary  dictionaryWithObjectsAndKeys:
                                           @"알파코", @"://www.alpaco.co.kr", nil];
        
        NSString *simpleModalPopupTitle = nil;
        for (NSString *domainKey in simpleModalUrlDic) {
            
            if ([urlString.lowercaseString rangeOfString:domainKey].location != NSNotFound) {
                simpleModalPopupTitle = [simpleModalUrlDic objectForKey: domainKey];
                break;
            }
        }
        
        WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
        webModal.webViewUrl = [request.URL.absoluteString stringByReplacingOccurrencesOfString:@"modal://" withString:@"http://"];
        webModal.title = simpleModalPopupTitle;
        [self presentViewController:webModal animated:YES completion:nil];
        
        return NO;
        
    }
    
//    NSString *ssUrl = [NSString stringWithFormat:@"%@/submain.aspx?type=app",
//                       [app.urlCenter stringByReplacingOccurrencesOfString:@"http://" withString:@"http://m."]];
    
    NSString *ssUrl = urlString;
    
    
    
// 잠시주석처리
//    if ([urlString isEqualToString:ssUrl]) {
//        SSViewController *viewToPush = [[SSViewController alloc] initWithNibName:@"SSViewController" bundle:nil];
//        if (viewToPush) {
//            viewToPush.url = urlString;
//            [self presentViewController:viewToPush animated:YES completion:nil];
//            self.urlMark = nil;
//        }
//        return NO;
//    }
    
// 잠시주석처리 이성
//    if ([urlString rangeOfString:@"othersite://h-leadership.hunet.co.kr"].location != NSNotFound) {
//        HLeaderController *viewToPush = [[HLeaderController alloc] initWithNibName:@"HLeader" bundle:nil];
//        if (viewToPush) {
//            viewToPush.url = [urlString stringByReplacingOccurrencesOfString:@"othersite://" withString:@"http://"];
//            [self presentViewController:viewToPush animated:YES completion:nil];
//            //self.urlMark = nil;
//            
//            NSString *currentURL = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
//            self.urlMark = currentURL;
//            
//        }
//        
//        return NO;
//    }
    
//    if ([urlString rangeOfString:@"sangsangplayer://"].location != NSNotFound) {
//        
//        NSString *currentURL = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
//        self.urlMark = currentURL;
//        
//        if ([app moviePlayCheck]) {
//            SangPlayerViewController *viewToPush = [[SangPlayerViewController alloc] initWithNibName:@"SangPlayerViewController" bundle:nil];
//            urlString = [urlString stringByReplacingOccurrencesOfString:@"sangsangplayer://" withString:@""];
//            NSArray *pairs = [urlString componentsSeparatedByString:@"/"];
//            viewToPush.cno = [pairs objectAtIndex:0];
//            viewToPush.cseq = [pairs objectAtIndex:1];
//            viewToPush.gid = [pairs objectAtIndex:2];
//            [self presentViewController:viewToPush animated:YES completion:nil];
//            
//        }
//        return NO;
//    }
    
    
    if ([urlString.lowercaseString hasPrefix:@"sangsangdrmplayer://"] ||
        [urlString.lowercaseString hasPrefix:@"sangsangdownload://"] ||
        [urlString.lowercaseString hasPrefix:@"sangsangdownloadplay://"] ||
        [urlString.lowercaseString hasPrefix:@"sangsangplayer://"]) {
        
        NSLog(@"sangsangdrmp");
        
        if ([urlString.lowercaseString hasPrefix:@"sangsangplayer://"]) {
            urlString = [urlString.lowercaseString stringByReplacingOccurrencesOfString:@"sangsangplayer://" withString:@"sangsangdrmplayer://"];
        }
        
        NSArray *tmpArray = [urlString componentsSeparatedByString:@"://"];
        NSString *act = [tmpArray objectAtIndex:0];
        NSString *param = [tmpArray objectAtIndex:1];
        
        if ([app isWiFi]) {
            
            _playerCallFun = [[PlayerCallFun alloc] init];
            _playerCallFun.delegateViewController = self;
            
            self.urlMark = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
            [_playerCallFun sangsangmaruInit:[NSArray arrayWithObjects:act, param, nil]];
        }
        
        return NO;
    }
    
    
    if ([urlString rangeOfString:@"webtoon://"].location != NSNotFound) {
        WebtoonViewController *viewToPush = [[WebtoonViewController alloc] initWithNibName:@"WebtoonViewController" bundle:nil];
        if (viewToPush) {
            viewToPush.webtoonUrl = [urlString stringByReplacingOccurrencesOfString:@"webtoon://" withString:@"http://study.hunet.co.kr"];
            
            [self presentViewController:viewToPush animated:YES completion:nil];
            
            NSString *currentURL = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
            self.urlMark = currentURL;
            
        }
        return NO;
    }
    
    //비즈니스영어
    if([urlString.lowercaseString rangeOfString:@"mp3player://"].location != NSNotFound){
        
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        if (![app moviePlayCheck]) {
            return NO;
        }
        
        NSString *currentURL = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
        self.urlMark = currentURL;
        
        
        NSString *mp3Url = [urlString.lowercaseString stringByReplacingOccurrencesOfString:@"mp3player://" withString:@"http://"];
        
        MP3PlayerController * mp3Player = [[MP3PlayerController alloc] initWithNibName:@"MP3PlayerController" bundle:nil];
        
        mp3Player.returnUrl = currentURL;
        mp3Player.mp3Url = mp3Url;
        
        [self presentViewController:mp3Player animated:YES completion:nil];
        
        return NO;
        
    }
//    if ([urlString rangeOfString:@"http://study.hunet.co.kr"].location != NSNotFound) {
//        
//        
//        EngViewController *engView = [[EngViewController alloc] initWithNibName:@"EngViewController" bundle:nil];
//        
//        if(engView){
//            engView.engUrl = [urlString stringByReplacingOccurrencesOfString:@"http://" withString:@"http://"];
//            [self presentViewController:engView animated:YES completion:nil];
//            
//            NSString *currentURL = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
//            self.urlMark = currentURL;
//            
//        }
//        
//        NSLog(@"%@", engView.engUrl);
//        return NO;
//    }
    
    /*
     if ([urlString rangeOfString:@"player://"].location != NSNotFound) {
     NSLog(@"player");
     
     NSString *currentURL = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
     self.urlMark = currentURL;
     
     if ([app moviePlayCheck]) {
     PlayerViewController *viewToPush = [[PlayerViewController alloc] initWithNibName:@"PlayerViewController" bundle:nil];
     urlString = [urlString stringByReplacingOccurrencesOfString:@"player://" withString:@""];
     NSArray *pairs = [urlString componentsSeparatedByString:@"/"];
     viewToPush.ccd = [pairs objectAtIndex:0];
     viewToPush.tcseq = [pairs objectAtIndex:1];
     viewToPush.cno = [pairs objectAtIndex:2];
     viewToPush.fno = [pairs objectAtIndex:3];
     [self presentViewController:viewToPush animated:YES completion:nil];
     }
     return NO;
     }
     */
    
    
    if ([urlString.lowercaseString hasPrefix:@"drmplay://"] ||
        [urlString.lowercaseString hasPrefix:@"download://"] ||
        [urlString.lowercaseString hasPrefix:@"downloadplay://"]
        || [urlString rangeOfString:@"player://"].location != NSNotFound) {
        
        NSLog(@"drmplay");
        self.urlMark = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
        
        NSArray *tmpArray = [urlString componentsSeparatedByString:@"://"];
        NSString *act = [[tmpArray objectAtIndex:0] stringByReplacingOccurrencesOfString:@"player" withString:@"drmplay"];
        NSString *param = [tmpArray objectAtIndex:1];
        
        if ([act isEqualToString:@"drmplay"]) {
            if ([app moviePlayCheck]) {
//                NSString *currentURL = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
//                self.urlMark = currentURL;

                //[[MLCenterForBusinessAppDelegate sharedAppDelegate] maskLoading];
            }
            else
                return NO;
        }
        else if ([act isEqualToString:@"download"]){
            if ([app isWiFi]) {
                [[MLCenterForBusinessAppDelegate sharedAppDelegate] drmloadingView:self];
            }
            else
                return NO;
        }
        else {
            [self downloadPush:param];
            return NO;
        }
        
        [NSThread detachNewThreadSelector:@selector(drm:) toTarget:self withObject:[NSArray arrayWithObjects:act, param, nil]];
        return NO;
    }
    
    
    //HTML모듈
    if([urlString rangeOfString:@"htmlplay://"].location != NSNotFound){
        NSString *htmlUrl = [urlString stringByReplacingOccurrencesOfString:@"htmlplay://" withString:@"http://"];
        
        HTMLPlayerController *htmlView = [[HTMLPlayerController alloc] initWithNibName:@"HTMLPlayerController" bundle:nil];
        
        htmlView.url = [NSString stringWithFormat:@"%@%@", htmlUrl, @"&drmYn=Y"];
        [self presentViewController:htmlView animated:YES completion:nil];
        NSString *currentURL = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
        self.urlMark = [NSString stringWithFormat:@"%@%@", currentURL, @"&drmYn=Y"];
        
        return NO;
    }
    
    //apptoapp://?schemeurl=&storeurl=
    //apptoapp://?schemeurl=hunetceo%3A%2F%2F%3Fuserid%3Daaa%26userpw%3Dbbb&storeurl=http%3A%2F%2Fapps.hunet.co.kr%2Fdown%2Fhunetceo.htm
    if ([urlString hasPrefix:@"apptoapp://?"]) {
        NSDictionary *protocalParams = [MLCenterForBusinessAppDelegate getProtocalParams:request.URL];
        NSString *schemeurl = [protocalParams objectForKey:@"schemeurl"];
        NSString *storeurl = [protocalParams objectForKey:@"storeurl"];
        BOOL isInstalled = [[UIApplication sharedApplication] openURL:[NSURL URLWithString:schemeurl]];
        if (!isInstalled) {
            // 설치 되어 있지 않습니다! 앱스토어로 안내...
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:storeurl]];
        }
        return NO;
    }
    
    //웹 로그아웃 제어
    if([urlString.lowercaseString hasPrefix:@"logout://"])
    {
        app.flagLogin = NO;
        [app loginViewControllerDisplay];
        return NO;
    }
    
#ifdef bossi_16794
    if([urlString rangeOfString:@"logout://"].location != NSNotFound){
        app.flagLogin = NO;
        [[MLCenterForBusinessAppDelegate sharedAppDelegate] loginViewControllerDisplay];
        return NO;
    }
    
    if([urlString rangeOfString:@"/more/more.aspx"].location != NSNotFound){
        self.tabBarController.selectedIndex = 1;
        return NO;
    }
#endif
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    if (animated) {
        //NSLog(@"viewWillAppear : %@", self.urlMark);
        //[self viewLoad];
    }
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    //NSLog(@"viewDidLoad : %@", self.urlMark);
    [super viewDidLoad];
    
    
    [self viewLoad];
    
    NSArray *vComp = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[vComp objectAtIndex:0] intValue] >= 7) {
        [self setNeedsStatusBarAppearanceUpdate];
        self.edgesForExtendedLayout = UIRectEdgeNone;
        // iOS 7 or above
        CGRect oldBounds = [self.view bounds];
        CGRect newViewBounds = CGRectMake(0, -10, oldBounds.size.width, oldBounds.size.height - 20);
        CGRect newWebViewBounds = CGRectMake(0, -20, oldBounds.size.width, oldBounds.size.height - 40);
        if (self.view.bounds.origin.y == newViewBounds.origin.y)
            return;
        
        [self.view setBounds:newViewBounds];
        [viewWeb setBounds:newWebViewBounds];
    }
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewLoad {
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
//    NSString *urlText = [NSString stringWithFormat:@"%@?type=8&uid=%@&pw=%@", app.urlBase, [app.profileUser objectForKey:@"userid"], [Global getUserPassword]];
    
    NSString *urlText = [NSString stringWithFormat:@"%@?type=8&uid=%@&pw=%@", app.urlBase, [Global getUserId],[Global getUserPassword]];
    
    if (app.gotoUrlAfterLogin != nil) {
        urlText = app.gotoUrlAfterLogin;
    }
    
    if (self.urlMark != nil) {
        urlText = [[Global sharedSingleton] testPrimaryDomainUrl:self.urlMark];
    }
    
#ifdef bossi_16794
    urlText = [urlText stringByAppendingString:@"&domain=1"];
#endif
    
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlText] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    
    
    [viewWeb loadRequest:request];
    viewWeb.scalesPageToFit = YES;
    
    //viewWeb 로딩 이후에 nil로 설정해야 함.
    app.gotoUrlAfterLogin = nil;
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (NSUInteger)supportedInterfaceOrientations
{
    indicator.center = viewWeb.center;
    //return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskAllButUpsideDown;
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)drm:(NSArray*)array
{
    //NSLog(@"url : %@", [array objectAtIndex:1]);
    NSString *action = [array objectAtIndex:0];
    NSString *url = [array objectAtIndex:1];
    
    NSArray *pairs = [url componentsSeparatedByString:@"/"];
    NSString *courseCd = [pairs objectAtIndex:0];
    NSString *takeCourseSeq = [pairs objectAtIndex:1];
    NSString *chapterNo = [pairs objectAtIndex:2];
    NSString *frameNo = [pairs objectAtIndex:3];
    
    NSString *displayNo = frameNo;
    @try {
        displayNo = [pairs objectAtIndex:4];
    }
    @catch (NSException *exception) {
        
    }
    
    
    @autoreleasepool {
        
        NSError *errorReq = nil;
        NSString* jsonParam;
        NSString* jsonUrl = [MLCenterForBusinessAppDelegate sharedAppDelegate].urlBase;
        
        if ([action isEqualToString:@"drmplay"]) {
            NSString *uid = [[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"userid"];
            jsonParam = [NSString stringWithFormat:@"type=11&uid=%@&ccd=%@&tcseq=%@&cno=%@&fno=%@", uid, courseCd, takeCourseSeq, chapterNo, frameNo];
        } else {
            jsonParam = [NSString stringWithFormat:@"type=29&courseCd=%@&takeCourseSeq=%@&chapterNo=%@", courseCd, takeCourseSeq, chapterNo];
        }
        
        NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:jsonUrl]
                                              andQuery:jsonParam
                                              andError:&errorReq];
        //NSLog(@"%@?takecourseSeq=%@&indexNo=%@", [[Global sharedSingleton] _downloadInfoUrl], [pairs objectAtIndex:0], [pairs objectAtIndex:1]);
        NSLog(@"MyViewController -drm: %@?%@", jsonUrl, jsonParam);
        //http://mlc.hunet.co.kr/App/JLog.aspx?type=11&uid=sisul-11985092&ccd=HLSC06994&tcseq=4756936&cno=0001&fno=1
        if (jsonData)
        {
            NSError *errorJSON = nil;
            NSMutableDictionary *result = [[[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON] mutableCopy];
            
            if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
                
                if ([action isEqualToString:@"drmplay"]) {
                    [self performSelectorOnMainThread:@selector(drmStreaming:) withObject:[NSArray arrayWithObjects:result,
                                                                                           courseCd,
                                                                                           takeCourseSeq,
                                                                                           chapterNo,
                                                                                           [result objectForKey:@"progress_no"],
                                                                                           frameNo,
                                                                                           [result objectForKey:@"evaluation_progress_ratio"],
                                                                                           [result objectForKey:@"sequence_progress_type"],
                                                                                           nil] waitUntilDone:NO];
                } else {
                    [result setObject:displayNo forKey:@"display_no"];
                    [self performSelectorOnMainThread:@selector(drmDownload:) withObject:result waitUntilDone:NO];
                }
                
            }
            
        }
        
    }
}

- (NSString*)getTitle:(NSString*)courseCd andChapterNo:(NSString*)chapterNo{
    
    NSError *errorReq = nil;
    NSString *indexNm = @"";
    NSString *jsonUrl = @"http://apps.hunet.co.kr/Hunet_Player/Android.aspx";
    NSString *jsonParam = [NSString stringWithFormat:@"action=GetIndexNm&courseCd=%@&chapterNo=%@&frameNo=1", courseCd, chapterNo];
    
    NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:jsonUrl]
                                          andQuery:jsonParam
                                          andError:&errorReq];
    if (jsonData)
    {
        NSError *errorJSON = nil;
        
        NSDictionary *result = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
        indexNm = [result objectForKey:@"Title"];
    }
    return indexNm;
}

- (void)drmStreaming:(NSArray*)array
{
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] removemaskLoading];
    
    NSDictionary *result = [array objectAtIndex:0];
    NSString *courseCd = [array objectAtIndex:1];
    NSString *takeCourseSeq = [array objectAtIndex:2];
    NSString *chapterNo = [array objectAtIndex:3];
    
    NSString *progressNo = @"0";
    NSString *evaluationProgressRatio = @"100";
    NSString *sequenceProgressType = @"0";
    
    NSInteger frameNo = 1;
    if ([array count] > 4) {
        progressNo = [array objectAtIndex:4];
    }
    if ([array count] > 5) {
        frameNo = [[array objectAtIndex:5] intValue];
    }
    
    if ([array count] > 6) {
        evaluationProgressRatio = [array objectAtIndex:6];
    }
    if ([array count] > 7) {
        sequenceProgressType = [array objectAtIndex:7];
    }
    
    NSUserDefaults *userdeDefaults = [NSUserDefaults standardUserDefaults];
    //[userdeDefaults setObject:[result objectForKey:@"MobileMovUrl"] forKey:FILENAME];
    [userdeDefaults setObject:[result objectForKey:@"DrmMovUrl"] forKey:FILENAME];
    NSLog(@"Streaming filename = %@", [userdeDefaults objectForKey:FILENAME]);
    
    MoviePlayerController * movieCon = [[MoviePlayerController alloc] initWithNibName:@"MoviePlayerController" bundle:nil];
    movieCon.isStreamingPlay = TRUE;
    movieCon.currentSecond = [[result objectForKey:@"LastMarkValue"] intValue];
    movieCon.scrollType = [[result objectForKey:@"ProgressRestrictionYn"] isEqualToString:@"Y"] ? 0 : 1;   // progress touch 여부 0 : 진도바 이동 불가, 1 : 진도바 이동 가능
    movieCon.courseCd = courseCd;
    movieCon.takecourseSeq = [takeCourseSeq intValue];
    movieCon.chapterNo = chapterNo;
    movieCon.movieTitle = [self getTitle:courseCd andChapterNo:chapterNo];;
    movieCon.progressNo = progressNo;
    movieCon.frameNo = frameNo;
    movieCon.lastMarkNo = (int)round([[result objectForKey:@"LastMarkNo"] doubleValue]);
    movieCon.evaluationProgressRatio = evaluationProgressRatio;
    movieCon.sequenceProgressType = sequenceProgressType;
    movieCon.movContents = [result objectForKey:@"Result"];
    
    movieCon.delegate = self; //openerCallFunc 호출
    
    [MLCenterForBusinessAppDelegate sharedAppDelegate].moviePlayerController = movieCon;
    [self presentViewController:movieCon animated:YES completion:nil];
}

- (void)drmDownload:(NSDictionary*)result
{
    NSString *mp4Url = @"";
    if (result != nil) {
        mp4Url = [NSString stringWithFormat:@"%@?%@", [result objectForKey:@"mov_url"], [result objectForKey:@"user_id"]];
        NSUserDefaults *userdeDefaults = [NSUserDefaults standardUserDefaults];
        [userdeDefaults setObject:mp4Url forKey:FILENAME];
    }
    if (webUtil != nil) {
        webUtil = nil;
    }
    webUtil = [[Web alloc] init];
    [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.webUtil = webUtil;
    [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.webUtil.downloadInfo = result;
    [[MLCenterForBusinessAppDelegate sharedAppDelegate].loading.webUtil requestDownLoad:mp4Url];
}


/**
 
 * @brief <#Description#>
 
 * @param <#Parameter#>
 
 * @return <#Return#>
 
 * @remark <#Remark#>
 
 * @see <#See#>
 
 * @author unknown.
 
 */
- (void)downloadPush:(NSString*)param {
    NSLog(@"downloadPush : %@", param);
    
    NSArray *pairs = [param componentsSeparatedByString:@"/"];
    NSString *courseCd = [pairs objectAtIndex:0];
    NSString *takeCourseSeq = [pairs objectAtIndex:1];
    NSString *chapterNo = [pairs objectAtIndex:2];
    
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    //NSArray *tmp = [Util getCourse:courseCd andTakeCourseSeq:takeCourseSeq];
    NSArray *tmp = [Util getStudy:courseCd andTakeCourseSeq:takeCourseSeq andChapterNo:chapterNo];
    if (tmp.count <= 0) {
        if ([app isWiFi]) {
            [[MLCenterForBusinessAppDelegate sharedAppDelegate] drmloadingView:self];
        }
        [NSThread detachNewThreadSelector:@selector(drm:) toTarget:self withObject:[NSArray arrayWithObjects:@"download", param, nil]];
    }
    else
    {
        //        NSArray *tmpCourse = [Util getCourse:courseCd andTakeCourseSeq:takeCourseSeq];
        //
        //        NSManagedObject *tempObject = [tmpCourse objectAtIndex:0];
        //        NSInteger expirationDay = [Util expirationDay:[tempObject valueForKey:@"study_end_date"]];
        //
        //        self.tabBarController.selectedIndex = 4;
        //        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        //
        //        MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
        //        DownloadViewController *second = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
        //        //second.title = @"다운로드 센터";
        //        DownloadIndexViewController *third = [[DownloadIndexViewController alloc] initWithStyle:UITableViewStylePlain];
        //        third.coursecd = courseCd;
        //        third.takecourseseq = takeCourseSeq;
        //        third.expirationDay = expirationDay;
        //        NSArray *controllersArray = [NSArray arrayWithObjects: first, second, third, nil];
        //        [second release];
        //        [third release];
        //
        //        [navi setViewControllers:controllersArray animated:YES];
        [[MLCenterForBusinessAppDelegate sharedAppDelegate] onlineStudyDownload:courseCd andTakeCourseSeq:[takeCourseSeq intValue]];
    }
    
}

- (void)processAction {
    [viewWeb reload];
    NSLog(@"processAction");
}

- (void)openerCallFun {
    NSString *urlToRefresh = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
    if (self.urlMark != nil && ![self.urlMark isEqualToString:@""]) {
        urlToRefresh = self.urlMark;
        self.urlMark = nil;
    }
    
    NSLog(@"openerCallFun - url: %@ ", urlToRefresh);
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:urlToRefresh]
                                                cachePolicy: NSURLRequestReloadIgnoringCacheData
                                            timeoutInterval:60.0
                                ];
    [viewWeb loadRequest:requestObj];
}
@end
