
#import "SocialViewController.h"

@interface SocialViewController ()

@end

@implementation SocialViewController

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (void)viewDidUnload {
    [self setSocialUrl:nil];
    [super viewDidUnload];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (isFirstLoaded)
        return;
    
    isFirstLoaded = YES;
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:self.socialUrl]
                                                cachePolicy: NSURLRequestReloadIgnoringCacheData
                                            timeoutInterval:90.0];

    [self.webView loadRequest:requestObj];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        self.statusBarHidden = YES;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
