//
//  HReminderViewController.m
//  MLCenterForBusiness
//
//  Created by Joey on 2014. 1. 28..
//
//

#import "HReminderViewController.h"
#import "Header.h"

@interface HReminderViewController ()

@end

@implementation HReminderViewController
@synthesize addStatusBar = _addStatusBar;

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (isFirstLoaded) {
        return;
    }
    
    [self settingStatusBar];
    isFirstLoaded = YES;
    
    NSString *_url = [NSString stringWithFormat:@"http://study.hunet.co.kr/Study/Htools/Gate.aspx?userId=%@&typeCd=0&isMobile=Y", [[self appDelegate].profileUser objectForKey:@"userid"]];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:_url]
                                                cachePolicy: NSURLRequestReloadIgnoringCacheData
                                            timeoutInterval:60.0];
    [self.webView loadRequest:requestObj];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = YES;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSURL *url = request.URL;
	NSString *urlString = url.absoluteString.lowercaseString;

    if ([urlString isEqualToString:@"close://"])
    {
        [self.navigationController popViewControllerAnimated:YES];
        return NO;
    }
    
    
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
