
#import "DownloadIndexSelfViewSangController.h"
#import "DownloadCourseCell.h"
#import "DownloadStudyCell.h"
#import "ModalAlert.h"
#import "Util.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "MoviePlayerController.h"

@interface DownloadIndexSelfViewSangController ()

@end

@implementation DownloadIndexSelfViewSangController

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (void)coredata
{
     //MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    self.sangsangArray = [Util getSangsang:self.seGoodsId];
    //goodsId(시리즈)와 userId만으로, 다운로드된 contentsSeq, 단과goodsId, viewNo를 가져와야함
    self.sagnsangStudyArray = [Util getSangStudy:self.seGoodsId];
    //NSLog(@"%@, %@", self.goodsId, [Global getUserId]);
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self != nil) {
        self.title = @"内容";
    }
    return self;
}

- (IBAction)goBack:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self coredata];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.sagnsangStudyArray count] > 0) {
        return [self.sagnsangStudyArray count] + 2;;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 60;
    }
    else if (indexPath.row == 1) {
        return 84;
    }
    return 79;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        static NSString *CellIdentifier = @"Cell";
        
        UIColor *bgColor = [UIColor colorWithRed:48.0/255.0 green:70.0/255.0 blue:143.0/255.0 alpha:1.0];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell ==nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        
        UIFont *myFont = [ UIFont fontWithName: @"Arial" size: 20.0 ];
        cell.textLabel.font  = myFont;
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.backgroundColor = bgColor;
        cell.textLabel.text = [NSString stringWithFormat:@"内容"];
        //cell.userInteractionEnabled = NO;
        
        
        UIButton *btnClose = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        
        btnClose.frame = CGRectMake(0.0f, 17.0f, 90.0f, 30.0f);
        [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnClose setTitle:NSLocalizedString(@"download", @"下载") forState:UIControlStateNormal];
        [btnClose.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
        [btnClose.titleLabel setBackgroundColor:bgColor];
        [btnClose.titleLabel setTextAlignment:NSTextAlignmentLeft];
        
        [cell addSubview:btnClose];
        [btnClose addTarget:self
                     action:@selector(goBack:)
           forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else if (indexPath.row == 1)
    {
        static NSString *CellIdentifier = @"DownloadCourseCell";
        DownloadCourseCell *cell = (DownloadCourseCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            NSArray *arr = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil];
            cell = [arr objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //온라인
        NSManagedObject *obj = [self.sangsangArray objectAtIndex:indexPath.row-1];
        cell.courseNm.text = [obj valueForKey:@"goods_nm"];
        cell.studyEndDate.text = [NSString stringWithFormat:@"%@", [obj valueForKey:@"study_end_date"]];
        if ([Util expirationDay:[obj valueForKey:@"study_end_date"]] >= 0) {
            //cell.expirationDay.text = [NSString stringWithFormat:@"(%d일 남음)", [Util expirationDay:[obj valueForKey:@"study_end_date"]]];
            cell.expirationDay.text = [NSString stringWithFormat:@"(剩余%li天)", [Util calculatePeroidOfDay:nil to:[obj valueForKey:@"study_end_date"]]];
        }
        
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"DownloadStudyCell";
        DownloadStudyCell *cell = (DownloadStudyCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            NSArray *arr = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil];
            cell = [arr objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSManagedObject *obj = [self.sagnsangStudyArray objectAtIndex:indexPath.row - 2];
        //cell.displayNoLabel.text = [NSString stringWithFormat:@"%@", [obj valueForKey:@"contents_nm"]];
        cell.displayNoLabel.text = @"";
        cell.indexNmLabel.text = [obj valueForKey:@"contents_nm"];
        
        cell.synButton.tag = indexPath.row - 2;
        cell.studyButton.tag = indexPath.row - 2;
        cell.delButton.tag = indexPath.row - 2;
        [cell.synButton addTarget:self action:@selector(ClickSync:) forControlEvents:UIControlEventTouchUpInside];
        [cell.studyButton addTarget:self action:@selector(ClickStudy:) forControlEvents:UIControlEventTouchUpInside];
        [cell.delButton addTarget:self action:@selector(ClickDel:) forControlEvents:UIControlEventTouchUpInside];
    
        return cell;
    }
    
    return nil;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}


- (void)ClickSync:(id)sender
{
    if (self.expirationDay < 0) {
        [ModalAlert notify: NSLocalizedString(@"perioSynError", @"因学习期间结束不能同步学习进度！")];
        return;
    }
    
#ifdef nhlife_14384
    BOOL syncContinue = [ModalAlert ask:NSLocalizedString(@"offlineSynMessage", @"在下线（无wi-fi或没有链接3G/4G网络）学习的内容不能自动存储进度内容。要进行进度同步吗？")  ];
    if (syncContinue == NO)
        return;
#endif
    
    UIButton *button = (UIButton*)sender;
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] maskLoading];
    [NSThread detachNewThreadSelector:@selector(syncSang:) toTarget:self withObject:[NSArray arrayWithObjects:
                                                                                 [NSString stringWithFormat:@"%d", button.tag], @"sync", nil]];
    
}

- (void)ClickStudy:(id)sender
{
    if (self.expirationDay < 0) {
        [ModalAlert notify:NSLocalizedString(@"perioStudyError", @"因学习期间结束不能进行学习！")];
        return;
    }
    
    UIButton *button = (UIButton*)sender;
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] maskLoading];
    [NSThread detachNewThreadSelector:@selector(syncSang:) toTarget:self withObject:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%d", button.tag], @"play", nil] ];
}

- (void)ClickDel:(id)sender
{
    UIButton *button = (UIButton*)sender;
    
    if ([ModalAlert ask:NSLocalizedString(@"RealDelete", @"真的删除内容?")]) {
        NSManagedObject *obj = [self.sagnsangStudyArray objectAtIndex:button.tag];
        //NSString *userId = [obj valueForKey:@"user_id"];
        NSString *goodsId = [obj valueForKey:@"goods_id"];
        NSString *seGoodsId = [obj valueForKey:@"se_goods_id"];
        NSString *contentsSeq = [obj valueForKey:@"contents_seq"];
        NSString *viewNo = [obj valueForKey:@"view_no"];
        NSString *contractNo = [obj valueForKey:@"contract_no"];
        NSString *fileNm = [obj valueForKey:@"file_nm"];
        
        [Util deleteDocrootFile:fileNm];
        
        [Util deleteSangStudy:goodsId andContentsSeq:contentsSeq andViewNo:viewNo andContractNo:contractNo andSeGoodsId:seGoodsId];
        [self coredata];
        [self.tableView reloadData];
        
    }
}


- (void)syncSang:(NSArray*)array {

    
    @autoreleasepool {
    
        NSManagedObject *obj = [self.sagnsangStudyArray objectAtIndex:[[array objectAtIndex:0] intValue]];
        NSString *contentsSeq = [obj valueForKey:@"contents_seq"];
        NSString *goodsId = [obj valueForKey:@"goods_id"];
        NSString *viewNo = [obj valueForKey:@"view_no"];
        NSString *userId = [obj valueForKey:@"user_id"];
        NSString *studyEndDate = [obj valueForKey:@"study_end_date"];
        NSString *viewSec = [obj valueForKey:@"view_sec"];
        NSString *contentsSec = [obj valueForKey:@"contents_sec"];
        NSString *lastViewSec = [obj valueForKey:@"last_view_sec"];
        NSString *viewSecMobile = [obj valueForKey:@"view_sec_mobile"];
        NSString *contractNo = [obj valueForKey:@"contract_no"];
         NSString *fileNm = [obj valueForKey:@"file_nm"];
        NSString *maxSec = [obj valueForKey:@"last_view_sec"];
        //NSString *seGoodsId = [obj valueForKey:@"se_goods_id"];
        //NSString *contentsNm = [obj valueForKey:@"contents_nm"];
        //NSString *goodsNm = [obj valueForKey:@"goods_nm"];
        if (![[MLCenterForBusinessAppDelegate sharedAppDelegate] isNotInternet])
        {
             MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
            
            //NSDictionary *result = [Util syn:courseCd andTakeCourseSeq:takeCourseSeq andChapterNo:chapterNo andMaxSec:maxSec andTotalSec:totalSec andUserId:userId];
            NSDictionary *result = [Util synSang:userId andCompanySeq:app.companySeq andContractNo:contractNo andContentsSeq:contentsSeq andGoodsId:goodsId andviewNo:viewNo andViewSec:viewSec andViewSecMobile:viewSecMobile andLastViewSec:lastViewSec andMode:@"A"];
            if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
                maxSec = [result objectForKey:@"MaxViewSec"];
               // NSLog(@"%maxSec : %@", maxSec);
                self.synMessage = NSLocalizedString(@"SynOk", @"学习进度同步成功！") ;
            } else {
                self.synMessage = NSLocalizedString(@"SynError", @"在进行同步时出现错误。请稍后从心尝试！");
            }
        }
        else {
            self.synMessage = NSLocalizedString(@"internetCheck", @"请先确认网络状态.");
        }
        
        [self performSelectorOnMainThread:@selector(downloadPlaySang:) withObject:[NSArray arrayWithObjects:
                                                                                   contentsSeq,
                                                                                   goodsId,
                                                                                   viewNo,
                                                                                   userId,
                                                                                   studyEndDate,
                                                                                   viewSec,
                                                                                   contentsSec,
                                                                                   //lastViewSec,
                                                                                   maxSec,
                                                                                   viewSecMobile,
                                                                                   contractNo,
                                                                                   fileNm,
                                                                                   [array objectAtIndex:1],
                                                                                   nil] waitUntilDone:NO];
    }
}

- (void)sangStudy:(NSArray*)array {
    
    @autoreleasepool {
    
        NSManagedObject *obj = [self.sagnsangStudyArray objectAtIndex:[[array objectAtIndex:0] intValue]];
        NSString *contentsSeq = [obj valueForKey:@"contents_seq"];
        NSString *goodsId = [obj valueForKey:@"goods_id"];
        NSString *viewNo = [obj valueForKey:@"view_no"];
        NSString *userId = [obj valueForKey:@"user_id"];
        NSString *studyEndDate = [obj valueForKey:@"study_end_date"];
        NSString *viewSec = [obj valueForKey:@"view_sec"];
        NSString *contentsSec = [obj valueForKey:@"contents_sec"];
        NSString *viewSecMobile = [obj valueForKey:@"view_sec_mobile"];
        NSString *contractNo = [obj valueForKey:@"contract_no"];
        NSString *fileNm = [obj valueForKey:@"file_nm"];
        NSString *maxSec = [obj valueForKey:@"last_view_sec"];
        
        
        [self performSelectorOnMainThread:@selector(downloadPlaySang:) withObject:[NSArray arrayWithObjects:
                                                                                   contentsSeq,
                                                                                   goodsId,
                                                                                   viewNo,
                                                                                   userId,
                                                                                   studyEndDate,
                                                                                   viewSec,
                                                                                   contentsSec,
                                                                                   //lastViewSec,
                                                                                   maxSec,
                                                                                   viewSecMobile,
                                                                                   contractNo,
                                                                                   fileNm,
                                                                                   [array objectAtIndex:1],
                                                                                   nil] waitUntilDone:NO];
    }
}

- (void)downloadPlaySang:(NSArray*)array
{
    NSString *contentsSeq = [array objectAtIndex:0];
    NSString *goodsId = [array objectAtIndex:1];
    NSString *viewNo = [array objectAtIndex:2];
    NSString *userId = [array objectAtIndex:3];
    //NSString *studyEndDate = [array objectAtIndex:4];
    NSString *viewSec = [array objectAtIndex:5];
    //NSString *contentsSec = [array objectAtIndex:6];
    NSString *lastViewSec = [array objectAtIndex:7];
    NSString *viewSecMobile = [array objectAtIndex:8];
    NSString *contractNo = [array objectAtIndex:9];
    NSString *fileNm = [array objectAtIndex:10];
    NSString *act = [array objectAtIndex:11];
    
    
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] removemaskLoading];
    if ([act isEqualToString:@"play"]) {
        NSUserDefaults *userdeDefaults = [NSUserDefaults standardUserDefaults];
        [userdeDefaults setObject:fileNm forKey:FILENAME];
        NSLog(@"download filename = %@", [userdeDefaults objectForKey:FILENAME]);
        MoviePlayerController * movieCon = [[MoviePlayerController alloc] initWithNibName:@"MoviePlayerController" bundle:nil];
        movieCon.isStreamingPlay = FALSE;
        movieCon.currentSecond = [lastViewSec intValue];
        movieCon.lastViewSec = [lastViewSec intValue];
        movieCon.viewSec = [viewSec intValue];
        movieCon.viewSecMobile = [viewSecMobile intValue];
        movieCon.scrollType = 1;
        movieCon.gid = goodsId;
        movieCon.userId=userId;
        movieCon.viewNo = [viewNo intValue];
        movieCon.cseq = [contentsSeq intValue];
        movieCon.cno = [contractNo intValue];
        
        [MLCenterForBusinessAppDelegate sharedAppDelegate].moviePlayerController = movieCon;
        [self presentViewController:movieCon animated:YES completion:nil];
    } else { //sync
        [ModalAlert notify:self.synMessage];
    }
}

@end
