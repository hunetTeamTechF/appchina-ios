
#import <UIKit/UIKit.h>

@interface DevOptionViewController : UITableViewController

@property (nonatomic, strong) UISwitch* stagingServerSwitch;
@property (nonatomic, strong) UISwitch* testServerSwitch;
@property (nonatomic, strong) NSString* lastTestHost;

@end
