//
//  MLCenterForBusinessAppDelegate.h
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 5. 9..
//  Copyright 2011 hunet. All rights reserved.
//

#include <sys/xattr.h>
#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "DownloadingView.h"

@class Reachability;
@class LoginViewController;
@class MoviePlayerController;

@interface MLCenterForBusinessAppDelegate : NSObject <UIApplicationDelegate, UITabBarControllerDelegate> {
    UIWindow *window;
	Reachability* internetReach;
	LoginViewController *viewLogin;
	NSInteger indexPreviousTab;
    MPMoviePlayerController *moviePlayer;
	NSMutableDictionary* profileUser;
	NSString *companySeq;
	NSString *urlBase;
	NSString *urlSite;
	NSString *urlCenter;
    NSString *eduType;
    NSString *workStateType;
	BOOL flagLogin;
	NSString *lectureUrlType;
    NSString *etc1;
    NSString *etc2;
    NSString *etc3;
    NSString *gotoUrlAfterLogin; //자동로그인 후 이동할 URL
    NSInteger tagPrevious;
    NSInteger tagCurrent;
    
    //download center tab 이름
    NSString *courseTab;
    NSString *imagineTab;
    NSString *CheckPushOx;
    
    //모듈화과정 플레이어 종료후 자동 페이지 넘기기플래그
    BOOL flagAutoGo;
}

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext2; //addinfo schema, 기존데이터가 마이그레이션이 안됨
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel2;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator2;

//download center tab 이름
@property (nonatomic, strong) NSString *courseTab;
@property (nonatomic, strong) NSString *imagineTab;

@property (nonatomic, strong) IBOutlet UIWindow *window;
@property (strong, nonatomic) UITabBarController *tabBarController;
@property (strong, nonatomic) LoginViewController *viewLogin;
@property (readwrite, strong) MPMoviePlayerController *moviePlayer;
@property (strong, nonatomic) MoviePlayerController *moviePlayerController;
@property (nonatomic, strong) NSMutableDictionary *profileUser;
@property (nonatomic, strong) NSString *companySeq;
@property (nonatomic, strong) NSString *urlBase;
@property (nonatomic, strong) NSString *urlSite;
@property (nonatomic, strong) NSString *urlCenter;
@property (nonatomic, strong) NSString *eduType;
@property (nonatomic, strong) NSString *workStateType;
@property (nonatomic, strong) NSString *downLoadUrl;
@property (readwrite, assign) BOOL flagLogin;
@property (readwrite, assign) BOOL didFinishLaunchMovie;
@property (nonatomic, strong) NSString *lectureUrlType;
@property (nonatomic, strong) NSString *etc1;
@property (nonatomic, strong) NSString *etc2;
@property (nonatomic, strong) NSString *etc3;
@property (nonatomic, strong) NSString *CheckPushOx;
@property (nonatomic, strong) NSString *gotoUrlAfterLogin;
@property (nonatomic, strong) UIView *mask;
@property (nonatomic, strong) DownloadingView *loading;
@property (nonatomic, strong) NSString *drmUseType;
@property (nonatomic, strong) NSString *hReminderUseType;
@property (nonatomic, strong) NSString *hStoryUseType;
@property (nonatomic, assign) BOOL *bSSPlayerSpeedBar;
@property (nonatomic, strong) NSArray *sagnsangStudyArray;
@property (strong, nonatomic) NSDictionary *pushService;
@property (readwrite, assign) NSInteger tagPrevious;
@property (readwrite, assign) NSInteger tagCurrent;
@property (strong, nonatomic) NSMutableArray *toastQueue;
@property BOOL toastVisibled;
@property BOOL isRuningMovieController;

@property (nonatomic, strong) NSString *info_company_seq;
@property (readwrite, assign) BOOL appNeedUpdate;
@property (nonatomic, strong) NSString *appDownLoadUrl;

//모듈화과정 플레이어 종료후 자동 페이지 넘기기플래그값
@property (readwrite, assign) BOOL flagAutoGo;

+ (MLCenterForBusinessAppDelegate *)sharedAppDelegate;

- (BOOL) loginForID:(NSString*)userid withPassword:(NSString*)password;
- (BOOL) loginForID:(NSString*)userid withPassword:(NSString*)password withCompanySeq:(NSString*)cseq;

- (BOOL) handleAppProtocol:(NSURL*)url withWebView:(UIWebView *)webView;
- (BOOL) closeAllModal:(UIViewController*)modalViewController changeTabUrl:(NSURL*)url;
- (BOOL) intentOpenUrl:(NSString*)urlString;


- (BOOL) loginSSOForEtc1:(NSString*)etc1 withEtc2:(NSString*)etc2 withEtc3:(NSString*)etc3;
- (void) setNoticeOption:(BOOL)option withKey:(NSString*)key;
- (void) setUserProfile:(BOOL)option withKey:(NSString*)key;

- (void) profileUserInfoSave;

- (void) initializeMainView;
- (void) openTrainingInstitute;

- (void) setSelectedTabIndex:(NSUInteger)indexSelected;
- (void) setSelectedTabIndex:(NSUInteger)indexSelected indexOrUrl:(NSString*)indexOrUrl;
- (void) setLectureUrlType:(NSString *)type;
- (BOOL) moviePlayCheck;
- (BOOL) isNotInternet;
- (BOOL) isWiFi;

- (void) initAndPlayMovie:(NSURL *)movieURL;
- (void) checkVersion;
- (void) setRootViewController;
- (void) offlineStudy;
- (void) onlineStudyDownload:(NSString*)courseCd andTakeCourseSeq:(int)takeCourseSeq;
- (void) sangStudyDownload:(NSString*)goodsId
             andContractNo:(int)contractNo
            andContentsSeq:(int)contentsSeq
              andSeGoodsId:(NSString*)seGoodsId
          andExpirationDay:(int)expirationDay;

- (void) loginViewControllerDisplay;
- (void)testPrimaryDomainChange;
- (void)checkWithUpdateFrom:(id)alertDelegate;
- (void)maskLoading;
- (void)removemaskLoading;
- (void)drmloadingView:(id)delegate;
- (void)drmremoveLodingView;

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;
- (void)AddAttributeToAllFolder;
- (void)AddAttributeToFolder:(NSString*)folderName;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (void)setRootViewSangDownloadCenter:(NSString*)goodsId andContractNo:(int)contractNo andContentsSeq:(int)contentsSeq
                         andSeGoodsId:(NSString*)seGoodsId andExpirationDay:(int)expirationDay;
- (void)settingtabBarControllerDefault:(BOOL)isFiveTabs isImagine:(BOOL)hasImagineTab useDefaultNaviStyle:(BOOL)useDefaultNaviStyle;
- (void)setTabBarAndNavigationBarWihIsFiveTabes:(BOOL)isFiveTabs isImagine:(BOOL)isImagine tabBarColorRGBHex:(NSString*)tabBarColorRGBHex selectedTintColorRGBHex:(NSString*)selectedTintColorRGBHex unSelectedTintColorRGBHex:(NSString*)unSelectedTintColorRGBHex naviBarColorRGBHex:(NSString*)naviBarColorRGBHex;

- (BOOL)hasDownloadContents;
+ (NSDictionary *)getProtocalParams:(NSURL*)url;
+ (NSDictionary *)getCommonProtocalParams:(NSURL*)url;
+ (NSDictionary *)getProtocalParamsByUrlString:(NSString*)urlString;
+ (NSDictionary *)getParams:(NSString*)urlString;
+ (NSDictionary *)getParams:(NSString*)query separator:(NSString*)separator;


// Assumes input like "#00FF00" (#RRGGBB).
+ (UIColor *)colorFromHexString:(NSString *)hexString;
@end

/*** for WebScriptBridge Begin ***/
@interface WebScriptObject: NSObject
@end

@interface WebView
- (WebScriptObject *)windowScriptObject;
@end

@interface UIWebDocumentView: UIView
- (WebView *)webView;
@end

@interface WebScriptBridge: NSObject
{
    LoginViewController *viewLogin;
}
- (void)goToLogin: (NSString *)returnUrl;
- (void)DoLogin: (NSString *)userId :(NSString *)userPwd;
- (void)changePassword: (NSString *)userPwd;
- (BOOL)existsInstalledApp:(NSString *)urlScheme;
- (BOOL)isStaging;
+ (BOOL)isKeyExcludedFromWebScript:(const char *)name;
+ (BOOL)isSelectorExcludedFromWebScript:(SEL)aSelector;
+ (WebScriptBridge*)getWebScriptBridge;
@end
/*** for WebScriptBridge End ***/
