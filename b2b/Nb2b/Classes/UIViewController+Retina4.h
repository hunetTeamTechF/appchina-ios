
#import <UIKit/UIKit.h>

@interface UIViewController (Retina4)
- (id)initWith4InchNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;
+ (void)load;
@end
