//
//  EtcViewController.m
//  MLCenterForBusiness
//
//  Created by Park Gi-Bok on 12. 1. 28..
//  Copyright (c) 2012 hunet. All rights reserved.
//

#import "HLeaderController.h"
#import "ModalAlert.h"
#import "PlayerViewController.h"

#import "MLCenterForBusinessAppDelegate.h"
#import "ModalAlert.h"
#import "PlayerViewController.h"
#import "SangPlayerViewController.h"
#import "SSViewController.h"
#import "HLeaderController.h"
#import	"Reachability.h"
#import "WebtoonViewController.h"
#import "ModalViewController.h"
#import "CJSONDeserializer.h"
#import "Util.h"
//#import "Global.h"
#import "MoviePlayerController.h"
#import "MoreViewController.h"
#import "DownloadViewController.h"
#import "DownloadIndexViewController.h"
#import "HTMLPlayerController.h"
#import "Header.h"
#import "DownloadSelfViewController.h"




@implementation HLeaderController
@synthesize url;

- (void) goHome {
	   
    if(![[self presentingViewController] isBeingDismissed]) {
        [[self presentingViewController] dismissViewControllerAnimated:YES completion:^ {
              [self dismissViewControllerAnimated:YES completion:nil];
        }];
        
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
		[app openTrainingInstitute];
    }
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
	[indicator stopAnimating];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
	[indicator startAnimating];
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
	[indicator stopAnimating];	
	//[ModalAlert notify:@"네트워크 연결에 문제가 생겼습니다."];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSURL *webUrl = request.URL;
	NSString *urlString = webUrl.absoluteString;
    
   	
	NSLog(@"%@", urlString);
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];

    if ([urlString rangeOfString:@"player://"].location != NSNotFound) {
		
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        
		if ([app moviePlayCheck]) {
			PlayerViewController *viewToPush = [[PlayerViewController alloc] initWithNibName:@"PlayerViewController" bundle:nil];
            if (viewToPush) {
                
                urlString = [urlString stringByReplacingOccurrencesOfString:@"player://" withString:@""];
                
                NSArray *pairs = [urlString componentsSeparatedByString:@"/"];
                viewToPush.ccd = [pairs objectAtIndex:0];
                viewToPush.tcseq = [pairs objectAtIndex:1];
                viewToPush.cno = [pairs objectAtIndex:2];
                viewToPush.fno = [pairs objectAtIndex:3];
                
                [self presentViewController:viewToPush animated:YES completion:nil];
            }
            
            return NO;
		}
	}
    
    if ([urlString.lowercaseString hasPrefix:@"drmplay://"] ||
        [urlString.lowercaseString hasPrefix:@"download://"] ||
        [urlString.lowercaseString hasPrefix:@"downloadplay://"]) {
        
        NSLog(@"drmplay");
        
        NSArray *tmpArray = [urlString componentsSeparatedByString:@"://"];
        NSString *act = [tmpArray objectAtIndex:0];
        NSString *param = [tmpArray objectAtIndex:1];
        
        if ([act isEqualToString:@"drmplay"]) {
            if ([app moviePlayCheck]) {
                NSString *currentURL = [viewWeb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
                self.url = currentURL;
                [[MLCenterForBusinessAppDelegate sharedAppDelegate] maskLoading];
            }
            else
                return NO;
        }
        else if ([act isEqualToString:@"download"]){
            if ([app isWiFi]) {
                [[MLCenterForBusinessAppDelegate sharedAppDelegate] drmloadingView:self];
            }
            else
                return NO;
        }
        else {
            [self downloadPush:param];
            return NO;
        }
        
        [NSThread detachNewThreadSelector:@selector(drm:) toTarget:self withObject:[NSArray arrayWithObjects:act, param, nil]];
        return NO;
    }


	return YES;
}

//drm
- (void)drm:(NSArray*)array
{
    //NSLog(@"url : %@", [array objectAtIndex:1]);
    NSString *action = [array objectAtIndex:0];
    NSString *url2 = [array objectAtIndex:1];
    
    NSArray *pairs = [url2 componentsSeparatedByString:@"/"];
    NSString *courseCd = [pairs objectAtIndex:0];
    NSString *takeCourseSeq = [pairs objectAtIndex:1];
    NSString *chapterNo = [pairs objectAtIndex:2];
    NSString *frameNo = [pairs objectAtIndex:3];
    NSString *displayNo = [pairs objectAtIndex:4];
    
    @autoreleasepool {
    
        NSError *errorReq = nil;
        NSString* jsonParam;
        NSString* jsonUrl = [MLCenterForBusinessAppDelegate sharedAppDelegate].urlBase;
        
        if ([action isEqualToString:@"drmplay"]) {
            NSString *uid = [[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"userid"];
            jsonParam = [NSString stringWithFormat:@"type=11&uid=%@&ccd=%@&tcseq=%@&cno=%@&fno=%@", uid, courseCd, takeCourseSeq, chapterNo, frameNo];
        } else {
            jsonParam = [NSString stringWithFormat:@"type=29&courseCd=%@&takeCourseSeq=%@&chapterNo=%@", courseCd, takeCourseSeq, chapterNo];
        }
        
        NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:jsonUrl]
                                              andQuery:jsonParam
                                              andError:&errorReq];
        //NSLog(@"%@?takecourseSeq=%@&indexNo=%@", [[Global sharedSingleton] _downloadInfoUrl], [pairs objectAtIndex:0], [pairs objectAtIndex:1]);
        NSLog(@"동영상 데이터->%@", jsonParam);
	if (jsonData)
        {
            NSError *errorJSON = nil;
            NSMutableDictionary *result = [[[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON] mutableCopy];
            
            if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
                
                if ([action isEqualToString:@"drmplay"]) {
                    [self performSelectorOnMainThread:@selector(drmStreaming:) withObject:[NSArray arrayWithObjects:result,
                                                                                           courseCd,
                                                                                           takeCourseSeq,
                                                                                           chapterNo,
                                                                                           [result objectForKey:@"progress_no"],
                                                                                           frameNo,
                                                                                           [result objectForKey:@"evaluation_progress_ratio"],
                                                                                           nil] waitUntilDone:NO];
                } else {
                    [result setObject:displayNo forKey:@"display_no"];
                    [self performSelectorOnMainThread:@selector(drmDownload:) withObject:result waitUntilDone:NO];
                }
                
		}
            
        }
    
    }
}

- (NSString*)getTitle:(NSString*)courseCd andChapterNo:(NSString*)chapterNo{
    
    NSError *errorReq = nil;
    NSString *indexNm = @"";
    NSString *jsonUrl = @"http://apps.hunet.co.kr/Hunet_Player/Android.aspx";
    NSString *jsonParam = [NSString stringWithFormat:@"action=GetIndexNm&courseCd=%@&chapterNo=%@&frameNo=1", courseCd, chapterNo];
    
    NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:jsonUrl]
                                          andQuery:jsonParam
                                          andError:&errorReq];
	if (jsonData)
    {
        NSError *errorJSON = nil;
        
        NSDictionary *result = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
        indexNm = [result objectForKey:@"Title"];
    }
    return indexNm;
}

- (void)drmStreaming:(NSArray*)array
{
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] removemaskLoading];
    NSDictionary *result = [array objectAtIndex:0];
    NSString *courseCd = [array objectAtIndex:1];
    NSString *takeCourseSeq = [array objectAtIndex:2];
    NSString *chapterNo = [array objectAtIndex:3];
    
    NSString *progressNo = @"0";
    NSString *evaluationProgressRatio = @"100";
    
    NSInteger frameNo = 1;
    if ([array count] > 4) {
        progressNo = [array objectAtIndex:4];
    }
    if ([array count] > 5) {
        frameNo= [[array objectAtIndex:5] intValue];
    }
    
    if ([array count] > 6) {
        evaluationProgressRatio= [array objectAtIndex:6];
    }
    NSInteger lastMarkNo =(int)round([[result objectForKey:@"LastMarkNo"] doubleValue]);
    
    NSString *indexNm = @"";
    NSUserDefaults *userdeDefaults = [NSUserDefaults standardUserDefaults];
    //[userdeDefaults setObject:[[[result objectForKey:@"MobileMovUrl"] stringByReplacingOccurrencesOfString:@"http://hunet2.hscdn.com/hunetvod/_definst_/mp4:M_Learning" withString:@"http://m.hunet.hscdn.com/hunet/M_learning"] stringByReplacingOccurrencesOfString:@"/playlist.m3u8" withString:@""] forKey:FILENAME];
    [userdeDefaults setObject:[result objectForKey:@"DrmMovUrl"] forKey:FILENAME];
    NSLog(@"Streaming filename = %@", [userdeDefaults objectForKey:FILENAME]);
    
    indexNm = [self getTitle:courseCd andChapterNo:chapterNo];
    
    MoviePlayerController * movieCon = [[MoviePlayerController alloc] initWithNibName:@"MoviePlayerController" bundle:nil];
    movieCon.isStreamingPlay = TRUE;
    movieCon.currentSecond = [[result objectForKey:@"LastMarkValue"] intValue];
    movieCon.scrollType = [[result objectForKey:@"ProgressRestrictionYn"] isEqualToString:@"Y"] ? 0 : 1;   // progress touch 여부 0 : 진도바 이동 불가, 1 : 진도바 이동 가능
    movieCon.courseCd = courseCd;
    movieCon.takecourseSeq = [takeCourseSeq intValue];
    movieCon.chapterNo = chapterNo;
    movieCon.movieTitle = indexNm;
    movieCon.progressNo = progressNo;
    movieCon.frameNo = frameNo;
    movieCon.lastMarkNo =lastMarkNo;
    movieCon.evaluationProgressRatio = evaluationProgressRatio;
    
    movieCon.movContents = [result objectForKey:@"Result"];
    
    [MLCenterForBusinessAppDelegate sharedAppDelegate].moviePlayerController = movieCon;
    [self presentViewController:movieCon animated:YES completion:nil];
}

- (void)drmDownload:(NSDictionary*)result
{
    NSString *mp4Url = @"";
    if (result != nil) {
        
       // mp4Url = [NSString stringWithFormat:@"%@?%@", [result objectForKey:@"mov_url"], [result objectForKey:@"user_id"]];
        NSString *movUrl = [NSString stringWithFormat:@"%@",[[[result objectForKey:@"mov_url"] stringByReplacingOccurrencesOfString:@"http://hunet2.hscdn.com/hunetvod/_definst_/mp4:M_Learning" withString:@"http://m.hunet.hscdn.com/hunet/M_learning"] stringByReplacingOccurrencesOfString:@"/playlist.m3u8" withString:@""]];
        
        mp4Url = [NSString stringWithFormat:@"%@?%@", movUrl, [result objectForKey:@"user_id"]];
        
        NSUserDefaults *userdeDefaults = [NSUserDefaults standardUserDefaults];
        [userdeDefaults setObject:mp4Url forKey:FILENAME];
    }
    if (webUtil != nil) {
        webUtil = nil;
    }
    webUtil = [[Web alloc] init];
    [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.webUtil = webUtil;
    [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.webUtil.downloadInfo = result;
    [[MLCenterForBusinessAppDelegate sharedAppDelegate].loading.webUtil requestDownLoad:mp4Url];
}

- (void)downloadPush:(NSString*)param {
    NSLog(@"downloadPush : %@", param);
    
    NSArray *pairs = [param componentsSeparatedByString:@"/"];
    NSString *courseCd = [pairs objectAtIndex:0];
    NSString *takeCourseSeq = [pairs objectAtIndex:1];
    NSString *chapterNo = [pairs objectAtIndex:2];
    
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    //NSArray *tmp = [Util getCourse:courseCd andTakeCourseSeq:takeCourseSeq];
    NSArray *tmp = [Util getStudy:courseCd andTakeCourseSeq:takeCourseSeq andChapterNo:chapterNo];
    if (tmp.count <= 0) {
        if ([app isWiFi]) {
            [[MLCenterForBusinessAppDelegate sharedAppDelegate] drmloadingView:self];
        }
        [NSThread detachNewThreadSelector:@selector(drm:) toTarget:self withObject:[NSArray arrayWithObjects:@"download", param, nil]];
    }
    else
    {
        //        NSArray *tmpCourse = [Util getCourse:courseCd andTakeCourseSeq:takeCourseSeq];
        //
        //        NSManagedObject *tempObject = [tmpCourse objectAtIndex:0];
        //        NSInteger expirationDay = [Util expirationDay:[tempObject valueForKey:@"study_end_date"]];
        //
        //        self.tabBarController.selectedIndex = 4;
        //        UINavigationController *navi = (UINavigationController*)self.tabBarController.selectedViewController;
        //
        //        MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
        //        DownloadViewController *second = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
        //        //second.title = @"다운로드 센터";
        //        DownloadIndexViewController *third = [[DownloadIndexViewController alloc] initWithStyle:UITableViewStylePlain];
        //        third.coursecd = courseCd;
        //        third.takecourseseq = takeCourseSeq;
        //        third.expirationDay = expirationDay;
        //        NSArray *controllersArray = [NSArray arrayWithObjects: first, second, third, nil];
        //        [second release];
        //        [third release];
        //
        //        [navi setViewControllers:controllersArray animated:YES];
       
        //[[MLCenterForBusinessAppDelegate sharedAppDelegate] onlineStudyDownloadByUrl:courseCd andTakeCourseSeq:[takeCourseSeq intValue] andUrl:self.url];
        
        DownloadSelfViewController *view = [[DownloadSelfViewController alloc] init];
        DownloadViewController *viewController = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
        viewController.courseCd = courseCd;
        viewController.takeCourseSeq = takeCourseSeq;
        [self presentViewController:view animated:YES completion:nil];
        
       }
    
}

- (void)processAction {
    [viewWeb reload];
    NSLog(@"processAction");
}

- (void)openerCallFun {
    NSLog(@"openerCallFun");
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //NSLog(@"%@", self.url);
    [viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
	viewWeb.scalesPageToFit = YES;
}

- (void)viewDidUnload	
{

	viewWeb = nil;
	indicator = nil;
	self.url = nil;
    tabbar = nil;
    [super viewDidUnload];
}

- (void)dealloc {
	viewWeb.delegate = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {    
    
    //NSLog(@"%d", item.tag);
    
    switch (item.tag) {
        case 0:
        {
            [self goHome];
        }
            break;
        case 1:
        {
            [viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://h-leadership.hunet.co.kr/Study/MClassRoom?SiteName=b2b"]]];
        }
            break;
        case 2:
        {
            [viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://h-leadership.hunet.co.kr/Study/MLecture?SiteName=b2b"]]];
        }
            break;
        case 3:
        {
            [viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://h-leadership.hunet.co.kr/Mentoring/MMentor?SiteName=b2b"]]];
        }
            break;
        case 4:
        {
            DownloadSelfViewController *view = [[DownloadSelfViewController alloc] init];
            [self presentViewController:view animated:YES completion:nil];
        }
            break;            
        default:
            break;
    }
}

@end
