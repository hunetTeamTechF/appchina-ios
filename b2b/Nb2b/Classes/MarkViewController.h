//
//  MarkViewController.h
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 6. 7..
//  Copyright 2011 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MLCenterForBusinessAppDelegate;

@interface MarkViewController : UITableViewController {
	NSMutableArray *listData;
	NSString *lastMarkNo;
    MLCenterForBusinessAppDelegate *appDelegate;
    BOOL cellSelected;
}

@property (nonatomic, strong) NSMutableArray *listData;
@property (nonatomic, strong) NSString *lastMarkNo;
@property (readwrite, assign) BOOL cellSelected;


- (id) initWithDictionary:(NSDictionary *)playerInfo;
- (void) replaceObjectAtIndex:(NSUInteger)index;
- (void) reloadList:(NSString *)markNo;
@end
