

#import "Util.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "CJSONDeserializer.h"
#import "DrmManager.h"

@implementation Util

+(NSData *) getNSDataByRequestUrl:(NSURL *)url andQuery:(NSString *)query andError:(NSError **)error {
    
    NSLog(@"Util +getNSDataByRequestUrl: %@?%@", url.path, query);
    //요청 준비/Users/albert/Desktop/ios_n/Nb2b/_Custom/Hyundai/images
	NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:url
                                    cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
    [request setHTTPMethod:@"POST"];
    //query부분을 따로 설정할 수 있음
    [request setHTTPBody:[query dataUsingEncoding:NSUTF8StringEncoding]];
	[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
	
    //응답 받기
    NSURLResponse *response = nil;
    NSError *errorNet = nil;
	
	UIApplication *appIndicator = [UIApplication sharedApplication];
	appIndicator.networkActivityIndicatorVisible = YES;
	NSData *dataReceived = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorNet];
	appIndicator.networkActivityIndicatorVisible = NO;
    
    if (error != nil) {
        *error = errorNet;
    }
    
    return dataReceived;
}

+ (NSArray*)freeDiskspace
{
    uint64_t totalSpace = 0;
    uint64_t totalFreeSpace = 0;
    
    __autoreleasing NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalSpace = [fileSystemSizeInBytes unsignedLongLongValue];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
        NSLog(@"Memory Capacity of %llu MiB with %llu MiB Free memory available.", ((totalSpace/1024ll)/1024ll), ((totalFreeSpace/1024ll)/1024ll));
        //NSLog(@"Memory Capacity of %llu MiB with %llu MiB Free memory available.", totalSpace, totalFreeSpace);
    } else {
        NSLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %d", [error domain], [error code]);
    }
    
    NSArray *array = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%llu", totalSpace], [NSString stringWithFormat:@"%llu", totalFreeSpace], nil];
    return array;
}


+ (NSString*)prettyBytes:(uint64_t)numBytes
{
    uint64_t const scale = 1024;
    char const * abbrevs[] = { "EB", "PB", "TB", "GB", "MB", "KB", "Bytes" };
    size_t numAbbrevs = sizeof(abbrevs) / sizeof(abbrevs[0]);
    uint64_t maximum = powl(scale, numAbbrevs-1);
    for (size_t i = 0; i < numAbbrevs-1; ++i) {
        if (numBytes > maximum) {
            return [NSString stringWithFormat:@"%.2f %s", numBytes / (double)maximum, abbrevs[i]];
        }
        maximum /= scale;
    }
    return [NSString stringWithFormat:@"%u Bytes", (unsigned)numBytes];
}

+ (NSInteger)expirationDay:(NSString*)date
{
    NSString *dateString = date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // 포맷에 주의!
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *dateFromString = [dateFormatter dateFromString:dateString];
    
    NSTimeInterval timerValue = [dateFromString timeIntervalSinceDate:[NSDate date]]; // Time in seconds from startDate to now
    NSInteger days = timerValue / 86400;
    return days;
}

/**
 
 * @brief 입력 받은 시작일과 종료일 사이의 일 수를 반환
 
 * @param fromYYYY_MM_DD @"yyyy-MM-dd" toYYYY_MM_DD @"yyyy-MM-dd"
 
 * @return from일과 to일 까지의 날짜 기간 반환
 
 * @remark nil인 경우 오늘날짜로 설정
 
 * @author Byung Yun Kim.
 
 */
+ (long) calculatePeroidOfDay:(NSString *)fromYYYY_MM_DD to:(NSString *)toYYYY_MM_DD
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    
    if (fromYYYY_MM_DD == nil || [fromYYYY_MM_DD isEqualToString:@""]) {
        fromYYYY_MM_DD = [dateFormatter stringFromDate:[NSDate date]];
    }
    
    if (toYYYY_MM_DD == nil || [toYYYY_MM_DD isEqualToString:@""]) {
        toYYYY_MM_DD = [dateFormatter stringFromDate:[NSDate date]];
    }
    
    NSDate *dateFrom = [dateFormatter dateFromString:fromYYYY_MM_DD];
    NSDate *dateTo = [dateFormatter dateFromString:toYYYY_MM_DD];
    
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *components = [cal components:NSCalendarUnitDay
                                          fromDate:dateFrom
                                            toDate:dateTo
                                           options:0];
    return components.day;
}

+ (NSArray *)getCourse
{
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Course" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entitydesc];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"study_end_date" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sort, nil];
    [request setSortDescriptors:sortDescriptors];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    return matchingData;
}

//상상마루 다운로드 가져오기
+ (NSArray *)getSangsang
{
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Sangsang" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entitydesc];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"study_end_date" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sort, nil];
    [request setSortDescriptors:sortDescriptors];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    return matchingData;
}


+ (NSArray *)getSangsang:(NSString*)goodsId {
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Sangsang" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"se_goods_id==%@", goodsId];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    return matchingData;
}


+ (NSArray *)getSangStudy:(NSString*)goodsId andUserId:(NSString*)userId {
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"SangsangStudy" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"se_goods_id == %@ and user_id == %@", goodsId, userId];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    return matchingData;
}

+ (NSArray *)getSangStudy:(NSString*)seGoodsId{
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"SangsangStudy" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"se_goods_id == %@", seGoodsId];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    return matchingData;
}

+ (NSArray *)getSangStudy:(NSString*)goodsId andContentsSeq:(NSString*)contentsSeq andUserId:(NSString*)userId andViewNo:(NSString*)viewNo {
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"SangsangStudy" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"goods_id == %@ and contents_seq == %@ and user_id == %@ and view_no== %@",
                              goodsId, [NSNumber numberWithInteger:[contentsSeq intValue]], userId, viewNo];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    return matchingData;
}

+ (NSArray *)getSangStudy:(NSString*)goodsId andContentsSeq:(NSString*)contentsSeq andUserId:(NSString*)userId {
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"SangsangStudy" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"goods_id == %@ and contents_seq == %@ and user_id == %@",
                              goodsId, [NSNumber numberWithInteger:[contentsSeq intValue]], userId];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    return matchingData;
}


+ (NSArray *)getCourse:(NSString*)cd andTakeCourseSeq:(NSString*)seq {
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Course" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"course_cd == %@ and take_course_seq == %@",
                              cd.uppercaseString,
                              [NSNumber numberWithInteger:[seq intValue]]];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    return matchingData;
}

+ (NSArray *)getStudy:(NSString*)cd andTakeCourseSeq:(NSString*)seq {
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Study" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"course_cd == %@ and take_course_seq == %@",
                              cd.uppercaseString, [NSNumber numberWithInteger:[seq intValue]]];
    [request setPredicate:predicate];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"display_no" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sort, nil];
    [request setSortDescriptors:sortDescriptors];
    
    NSError *error = nil;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    return matchingData;
}

+ (NSArray *)getStudy:(NSString*)cd andTakeCourseSeq:(NSString*)seq andChapterNo:(NSString*)no {
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Study" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"course_cd == %@ and take_course_seq == %@ and chapter_no == %@",
                              cd.uppercaseString, [NSNumber numberWithInteger:[seq intValue]], no];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    return matchingData;
}


+ (void)updateMaxSec:(NSString*)cd andTakeCourseSeq:(NSString*)seq andChapterNo:(NSString*)no andMaxSec:(NSString*)sec
{
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Study" inManagedObjectContext:context];
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"course_cd == %@ and take_course_seq == %@ and chapter_no == %@ and max_sec < %@",
                              cd, [NSNumber numberWithInteger:[seq intValue]],
                              no, [NSNumber numberWithInteger:[sec intValue]]];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *array = [context executeFetchRequest:request error:&error];
    if (array == nil) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    if([array count] > 0)
    {
        NSManagedObject *tempObject = [[context executeFetchRequest:request error:&error] objectAtIndex:0];
        [tempObject setValue:[NSNumber numberWithInteger:[sec intValue]] forKey:@"max_sec"];
        [context save:&error];
    }
}



+ (void)updateTotalSec:(NSString*)cd andTakeCourseSeq:(NSString*)seq andChapterNo:(NSString*)no andTotalSec:(int)sec
{
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Study" inManagedObjectContext:context];
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"course_cd == %@ and take_course_seq == %@ and chapter_no == %@", cd, [NSNumber numberWithInteger:[seq intValue]], no];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *array = [context executeFetchRequest:request error:&error];
    if (array == nil) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    if([array count] > 0)
    {
        NSManagedObject *tempObject = [[context executeFetchRequest:request error:&error] objectAtIndex:0];
        int totalsec = [[tempObject valueForKey:@"total_sec"] intValue];
        totalsec += sec;
        if (sec < 0) {
            totalsec = 0;
        }
        
        [tempObject setValue:[NSNumber numberWithInteger:totalsec] forKey:@"total_sec"];
        [tempObject setValue:[NSDate date] forKey:@"latest_study_date"];
        
        [context save:&error];
    }
}

+ (void)updateLatestSyncDate:(NSString*)cd andTakeCourseSeq:(NSString*)seq andChapterNo:(NSString*)no
{
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Study" inManagedObjectContext:context];
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"course_cd == %@ and take_course_seq == %@ and chapter_no == %@", cd, [NSNumber numberWithInteger:[seq intValue]], no];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *array = [context executeFetchRequest:request error:&error];
    if (array == nil) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    if ([array count] > 0)
    {
        NSManagedObject *tempObject = [[context executeFetchRequest:request error:&error] objectAtIndex:0];
        
        [tempObject setValue:[NSDate date] forKey:@"latest_sync_date"];
        
        [context save:&error];
    }
}

// 서버로 전송되지 못한 학습진도 전송.
+ (int)syncAllFaileSubmitProgress
{
    int submitCount = 0;
    
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Study" inManagedObjectContext:context];
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"latest_sync_date = null or latest_sync_date < latest_study_date"];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *array = [context executeFetchRequest:request error:&error];
    if (array == nil) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    if([array count] > 0)
    {
        BOOL isSubmited = NO;
        for (NSManagedObject *tempObject in array) {
            isSubmited = [self submitProgress:tempObject];
            if (isSubmited) {
                submitCount++;
            }
        }
        
        [context save:&error];
    }
    
    return submitCount;
}

+ (BOOL)submitProgress:(NSManagedObject *)obj {
    NSString *courseCd = [obj valueForKey:@"course_cd"];
    NSString *getTakeCourseSeq = [obj valueForKey:@"take_course_seq"];
    NSString *chapterNo = [obj valueForKey:@"chapter_no"];
    NSString *maxSec2 = [obj valueForKey:@"max_sec"];
    NSString *totalSec = [obj valueForKey:@"total_sec"];
    NSString *userId = [obj valueForKey:@"user_id"];
    
    if (![[MLCenterForBusinessAppDelegate sharedAppDelegate] isNotInternet])
    {
        NSDictionary *result = [Util synV2:courseCd andTakeCourseSeq:getTakeCourseSeq andChapterNo:chapterNo andStudySec:totalSec andMarkValue:maxSec2 andUserId:userId];
        if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
            [Util updateLatestSyncDate:courseCd andTakeCourseSeq:getTakeCourseSeq andChapterNo:chapterNo];
            return YES;
        } else {
            return NO;
        }
    }
    else {
        return NO;
    }
}

+ (void) deleteCourse:(NSString*)cd andTakeCourseSeq:(NSString*)seq
{
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Course" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entitydesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"course_cd == %@ and take_course_seq == %@", cd, [NSNumber numberWithInteger:[seq intValue]]];
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if (matchingData.count > 0) {
        NSManagedObject *obj = [matchingData objectAtIndex:0];
        [context deleteObject:obj];
        [context save:&error];
    }
}


+ (void) deleteStudy:(NSString*)cd andTakeCourseSeq:(NSString*)seq andChapterNo:(NSString*)no
{
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Study" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entitydesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"course_cd == %@ and take_course_seq == %@ and chapter_no == %@", cd, [NSNumber numberWithInteger:[seq intValue]], no];
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if (matchingData.count > 0) {
        NSManagedObject *obj = [matchingData objectAtIndex:0];
        [context deleteObject:obj];
        [context save:&error];
    }
    
    NSArray *_indexArray = [self getStudy:cd andTakeCourseSeq:seq];
    if (_indexArray.count <= 0) {
        [self deleteCourse:cd andTakeCourseSeq:seq];
    }
}


// 진도 동기화
+ (NSDictionary*)syn:(NSString*)cd andTakeCourseSeq:(NSString*)seq andChapterNo:(NSString*)no andMaxSec:(NSString*)sec andTotalSec:(NSString*)totalsec andUserId:(NSString*)uid
{
    NSError *errorReq = nil;
    NSString* url = [NSString stringWithFormat:@"type=31&courseCd=%@&takeCourseSeq=%@&chapterNo=%@&studySec=%@&totalSec=%@&deviceNm=ios&regId=%@", cd, seq, no, sec, totalsec, uid];
    
    NSLog(@"%@", url);
    NSString* jsonUrl = [MLCenterForBusinessAppDelegate sharedAppDelegate].urlBase;
    NSData *jsonData = [self getNSDataByRequestUrl:[NSURL URLWithString:jsonUrl]
                                          andQuery:url
                                          andError:&errorReq];
    
    NSLog(@"url : %@", url);
    
	if (jsonData)
    {
        NSError *errorJSON = nil;
        NSDictionary *result = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
        
        if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
            [self updateMaxSec:cd andTakeCourseSeq:seq andChapterNo:no andMaxSec:[result objectForKey:@"max_study_sec"]];
            [self updateTotalSec:cd andTakeCourseSeq:seq andChapterNo:no andTotalSec:-1];
            return result;
		}
        return result;
    }
    
    return nil;
}

// 진도 동기화
+ (NSDictionary*)synV2:(NSString*)cd andTakeCourseSeq:(NSString*)seq andChapterNo:(NSString*)no andStudySec:(NSString*)sec andMarkValue:(NSString*)markValue andUserId:(NSString*)uid
{
    NSError *errorReq = nil;
    NSString* url = [NSString stringWithFormat:@"type=42&courseCd=%@&takeCourseSeq=%@&chapterNo=%@&studySec=%@&markValue=%@&deviceNm=ios&regId=%@", cd, seq, no, sec, markValue, uid];
    
    NSLog(@"로컬진도동기화2:%@", url);
    NSString* jsonUrl = [MLCenterForBusinessAppDelegate sharedAppDelegate].urlBase;
    NSData *jsonData = [self getNSDataByRequestUrl:[NSURL URLWithString:jsonUrl]
                                          andQuery:url
                                          andError:&errorReq];
    
    NSLog(@"url : %@", url);
    
	if (jsonData)
    {
        NSError *errorJSON = nil;
        NSDictionary *result = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
        
        if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
          //  [self updateMaxSec:cd andTakeCourseSeq:seq andChapterNo:no andMaxSec:[result objectForKey:@"max_study_sec"]];
            [self updateTotalSec:cd andTakeCourseSeq:seq andChapterNo:no andTotalSec:-1];
            return result;
		}
        return result;
    }
    
    return nil;
}


//상상마루
//상상마루 삭제
+ (void) deleteSangSang:(NSString*)goodsId
{
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Sangsang" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entitydesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"se_goods_id == %@", goodsId];
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if (matchingData.count > 0) {
        NSManagedObject *obj = [matchingData objectAtIndex:0];
        [context deleteObject:obj];
        [context save:&error];
    }
}


+ (void) deleteSangStudy:(NSString*)goodsId andContentsSeq:(NSString*)contentsSeq andViewNo:(NSString*)viewNo andContractNo:(NSString*) contractNo andSeGoodsId:(NSString*) seGoodsId
{

    NSString* seGoodsIdCopy = [seGoodsId copy];
    
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"SangsangStudy" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entitydesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"contents_seq == %d and view_no == %d and contract_no == %d and goods_id = %@", [contentsSeq intValue],
                              [viewNo intValue], [contractNo intValue], goodsId];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if (matchingData.count > 0) {
        NSManagedObject *obj = [matchingData objectAtIndex:0];
        [context deleteObject:obj];
        [context save:&error];
    }
    
    NSArray *_indexArray = [self getSangStudy:seGoodsIdCopy];
    if (_indexArray.count == 0) {
        [self deleteSangSang:seGoodsIdCopy];
    }
    
}

+ (NSDictionary*)synSang:(NSString*)userId andCompanySeq:(NSString*)companySeq andContractNo:(NSString*)contractNo andContentsSeq:(NSString*)contentsSeq  andGoodsId:(NSString*)goodsId andviewNo:(NSString*)viewNo andViewSec:(NSString*)viewSec andViewSecMobile:(NSString*)viewSecMobile andLastViewSec:(NSString*)lastViewSec andMode:(NSString*)mode
{
    
    NSError *errorReq = nil;
    //NSString* jsonUrl = @"http://apps.hunet.co.kr/Hunet_Player/Android.aspx";
    //NSString* jsonUrl = @"http://172.20.80.100:5555/Jlog/SavePlayer";
    
    NSString* jsonUrl = [NSString stringWithFormat:@"%@%@",  [MLCenterForBusinessAppDelegate sharedAppDelegate].urlSite,@"JLog/SavePlayer"];
//    @"http://b2bapp.xiunaichina.com/Jlog/SavePlayer";
    NSString* url = [NSString stringWithFormat:@"action=SangSangProgressUpdateV2&userId=%@&goodsId=%@&contentsSeq=%d&viewSec=%d&viewSecMobile=%d&lastViewSec=%d&viewNo=%d&mode=%@&contractNo=%d",
                         userId,
                         goodsId,
                         [contentsSeq intValue],
                         [viewSec intValue],
                         [viewSecMobile intValue],
                         [lastViewSec intValue],
                         //sec + [second intValue],
                         [viewNo intValue],
                         mode,
                         [contractNo intValue]];
    NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:jsonUrl]
                                          andQuery:url
                                          andError:&errorReq];
    
    NSLog(@"syncSang:%@", url);
    if (jsonData)
    {
        NSError *errorJSON = nil;
        NSDictionary *result = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
        
        if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
            
            //[self updateTotalSecSang:userId andContractNo:contractNo andContentsSeq:contentsSeq andGoodsId:goodsId andviewNo:viewNo andTotalSec:[viewSec intValue] andLastViewSec:[[result objectForKey:@"MaxViewSec"] intValue]];
            [self updateTotalSecSangV2:userId andContractNo:contractNo andContentsSeq:contentsSeq andGoodsId:goodsId andviewNo:viewNo andViewSec:[viewSec intValue] andViewSecMobile:[viewSecMobile intValue] andLastViewSec:[lastViewSec intValue]];

            return result;
		}
        return result;
    }
    return nil;
}

+ (void)updateTotalSecSang:(NSString*)userId andContractNo:(int)contractNo andContentsSeq:(int)contentsSeq  andGoodsId:(NSString*)goodsId andviewNo:(int)viewNo andTotalSec:(int)sec andLastViewSec:(int)lastViewSec
{
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"SangsangStudy" inManagedObjectContext:context];
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user_id == %@ and contract_no == %d and contents_seq == %d and view_no == %d",
                              userId, contractNo, contentsSeq, viewNo];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *array = [context executeFetchRequest:request error:&error];
    if (array == nil) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    if([array count] > 0)
    {
        NSManagedObject *tempObject = [[context executeFetchRequest:request error:&error] objectAtIndex:0];
        int viewSec = [[tempObject valueForKey:@"view_sec"] intValue];
        int viewSecMobile = [[tempObject valueForKey:@"view_sec_mobile"] intValue];
        
        NSLog(@"updateTotalSecSang:view_sec:%d, view_sec_mobile:%d, sec:%d", [[tempObject valueForKey:@"view_sec"] intValue], [[tempObject valueForKey:@"view_sec_mobile"] intValue], sec);
        
        //viewSec += sec;
        //viewSecMobile += sec;
        if (sec < 0) {
            viewSec = 0;
        }
        
        [tempObject setValue:[NSNumber numberWithInteger:viewSec] forKey:@"view_sec"];
        [tempObject setValue:[NSNumber numberWithInteger:viewSecMobile] forKey:@"view_sec_mobile"];
        [tempObject setValue:[NSNumber numberWithInteger:lastViewSec] forKey:@"last_view_sec"];
        [context save:&error];
    }
}

+ (void)updateTotalSecSangV2:(NSString*)userId andContractNo:(int)contractNo andContentsSeq:(int)contentsSeq  andGoodsId:(NSString*)goodsId andviewNo:(int)viewNo andViewSec:(int)viewSec andViewSecMobile:(int)viewSecMobile andLastViewSec:(int)lastViewSec
{
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"SangsangStudy" inManagedObjectContext:context];
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user_id == %@ and contract_no == %d and contents_seq == %d and view_no == %d",
                              userId, contractNo, contentsSeq, viewNo];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *array = [context executeFetchRequest:request error:&error];
    if (array == nil) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    if([array count] > 0)
    {
        NSManagedObject *tempObject = [[context executeFetchRequest:request error:&error] objectAtIndex:0];
        
        [tempObject setValue:[NSNumber numberWithInteger:viewSec] forKey:@"view_sec"];
        [tempObject setValue:[NSNumber numberWithInteger:viewSecMobile] forKey:@"view_sec_mobile"];
        [tempObject setValue:[NSNumber numberWithInteger:lastViewSec] forKey:@"last_view_sec"];
        [tempObject setValue:[NSDate date] forKey:@"latest_study_date"];
        
        [context save:&error];
    }
}

+ (void)updateLatestSyncDateSang:(NSString*)userId andContractNo:(int)contractNo andContentsSeq:(int)contentsSeq  andviewNo:(int)viewNo
{
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"SangsangStudy" inManagedObjectContext:context];
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user_id == %@ and contract_no == %d and contents_seq == %d and view_no == %d",
                              userId, contractNo, contentsSeq, viewNo];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *array = [context executeFetchRequest:request error:&error];
    if (array == nil) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    if([array count] > 0)
    {
        NSManagedObject *tempObject = [[context executeFetchRequest:request error:&error] objectAtIndex:0];

        [tempObject setValue:[NSDate date] forKey:@"latest_sync_date"];
        
        [context save:&error];
    }
}

// 서버로 전송되지 못한 학습진도 전송(상상마루).
+ (int)syncAllFaileSubmitProgressSang
{
    int submitCount = 0;
    
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"SangsangStudy" inManagedObjectContext:context];
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"latest_sync_date = null or latest_sync_date < latest_study_date"];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *array = [context executeFetchRequest:request error:&error];
    if (array == nil) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    if([array count] > 0)
    {
        BOOL isSubmited = NO;
        for (NSManagedObject *tempObject in array) {
            isSubmited = [self submitProgressSang:tempObject];
            if (isSubmited) {
                submitCount++;
            }
        }
        
        [context save:&error];
    }
    
    return submitCount;
}

+ (BOOL)submitProgressSang:(NSManagedObject *)obj {
    NSString *contentsSeq = [obj valueForKey:@"contents_seq"];
    NSString *goodsId = [obj valueForKey:@"goods_id"];
    NSString *viewNo = [obj valueForKey:@"view_no"];
    NSString *userId = [obj valueForKey:@"user_id"];
    NSString *viewSec = [obj valueForKey:@"view_sec"];
    NSString *lastViewSec = [obj valueForKey:@"last_view_sec"];
    NSString *viewSecMobile = [obj valueForKey:@"view_sec_mobile"];
    NSString *contractNo = [obj valueForKey:@"contract_no"];
    if (![[MLCenterForBusinessAppDelegate sharedAppDelegate] isNotInternet])
    {
        MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        NSDictionary *result = [Util synSang:userId andCompanySeq:app.companySeq andContractNo:contractNo andContentsSeq:contentsSeq andGoodsId:goodsId andviewNo:viewNo andViewSec:viewSec andViewSecMobile:viewSecMobile andLastViewSec:lastViewSec andMode:@"A"];
        if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
            [self updateLatestSyncDateSang:userId andContractNo:[contractNo intValue] andContentsSeq:[contentsSeq intValue] andviewNo:[viewNo intValue]];
            return YES;
        } else {
            return NO;
        }
    }
    else {
        return NO;
    }
}

+ (void)updateDownloadInfo:(NSInteger)companySeq andProcessMenuAlias:(NSString*)ProcessMenuAlias andImagineMenuAlias:(NSString*)ImagineMenuAlias andOnlyImagineYn:(NSString*)OnlyImagineYn andTabBgColr:(NSString*)tabBgColor
{
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext2];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"DownloadInfo" inManagedObjectContext:context];
    [request setEntity:entitydesc];
    
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"company_seq == %d", companySeq];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"1 == 1"];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *array = [context executeFetchRequest:request error:&error];
    if (array == nil) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    if([array count] > 0)
    {
        NSManagedObject *tempObject = [[context executeFetchRequest:request error:&error] objectAtIndex:0];
        [context deleteObject:tempObject];
        [context save:&error];
    }
    
    NSManagedObject *updateObj = [[NSManagedObject alloc] initWithEntity:entitydesc insertIntoManagedObjectContext:context];
    [updateObj setValue:[NSNumber numberWithInteger:companySeq] forKey:@"company_seq"];
    [updateObj setValue:ProcessMenuAlias forKey:@"process_menu_alias"];
    [updateObj setValue:ImagineMenuAlias forKey:@"imagine_menu_alias"];
    [updateObj setValue:OnlyImagineYn forKey:@"only_imagine_yn"];
    [updateObj setValue:tabBgColor forKey:@"tab_bg_color"];
    [context save:&error];
  
}

+ (NSArray *)getDownloadInfo
{
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate] managedObjectContext2];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"DownloadInfo" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entitydesc];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"company_seq" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sort, nil];
    [request setSortDescriptors:sortDescriptors];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    return matchingData;
}

+ (NSString *)valueForKey:(NSString *)key
           fromQueryItems:(NSArray *)queryItems
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name=%@", key];
    NSURLQueryItem *queryItem = [[queryItems
                                  filteredArrayUsingPredicate:predicate]
                                 firstObject];
    return queryItem.value;
}


+ (NSString *)valueForKey:(NSString *)key fromQuery:(NSString *)queryString
{

    NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
    NSArray *urlComponents = [queryString componentsSeparatedByString:@"&"];


    for (NSString *keyValuePair in urlComponents)
    {
        NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
        NSString *eachKey = [[pairComponents firstObject] stringByRemovingPercentEncoding];
        NSString *eachValue = [[pairComponents lastObject] stringByRemovingPercentEncoding];
        
        [queryStringDictionary setObject:eachValue forKey:eachKey];
    }
    
    return [queryStringDictionary objectForKey:key];
}

+ (UINavigationController *) getNavigationController: (Class)viewControllerClass withNibName:(NSString *)nibName {
    
    UIViewController *viewController = [[viewControllerClass alloc] initWithNibName:nibName bundle:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    return navController;
}

+ (UINavigationController *) getNavigationController: (Class)viewControllerClass initWithStyle:(UITableViewStyle)style {
    
    UIViewController *viewController = [[viewControllerClass alloc] initWithStyle:style];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    return navController;
}

+ (NSString *)timeMmddss:(int)timeSS {
    
    int totalTimeM = timeSS /3600;
    int totalTimeD = (timeSS %3600)/60;
    int totalTimeS = (timeSS %3600)%60;
    
    return [NSString stringWithFormat:@"%@:%@:%@",
            [Util formatTime:totalTimeM],
            [Util formatTime:totalTimeD],
            [Util formatTime:totalTimeS]];
}
+ (NSString *)formatTime:(int)time {
    NSString *strTime = nil;
    if (time < 10) {
        strTime = [NSString stringWithFormat:@"0%d",time];
    } else {
        strTime = [NSString stringWithFormat:@"%d",time];
    }
    return strTime;
}
+(NSString *) filePath:(NSString *)filename
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, filename];
    return filePath;
}

+ (void) deleteDocrootFile :(NSString *) fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fileFullPath = [NSString stringWithFormat:@"%@/%@/%@", documentsDirectory, kDrmFolderName, fileName];
    
    [self deleteFile:fileFullPath];
}

+ (void) deleteFile:(NSString *)fileFullPath
{
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:fileFullPath];
    if (fileExists)
    {
        NSError *err = nil;
        [[NSFileManager defaultManager] removeItemAtPath:fileFullPath error:&err];
        NSLog(@"remove prev file : %@", fileFullPath);
        if(err != nil) {
            int nZeroReturn = unlink([fileFullPath fileSystemRepresentation]);
            if(nZeroReturn != 0)
                NSLog(@"error:%@", fileFullPath);
        }
    }
}

+ (NSMutableArray *)checkExistsDownloadFileOfCourse:(NSArray *)listItem
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for(NSDictionary *item in listItem){
        
        NSMutableDictionary *resultItem = [item mutableCopy];
        BOOL exists = [Util ExistsDownloadFileOfCourse:[[resultItem objectForKey:@"takeCourseSeq"] intValue] :[resultItem objectForKey:@"chapterNo"]];
        [resultItem setObject:[NSNumber numberWithBool:exists] forKey:@"exist"];
        [array addObject:resultItem];
        
    }
    
    return array;
}

+ (BOOL)ExistsDownloadFileOfCourse:(int)take_course_seq :(NSString*)chapter_no
{
    @try{
        
        NSManagedObject *courseInfo = [Util downloadCourseExists:[NSString stringWithFormat:@"%d", take_course_seq] andChapterNo:chapter_no];
        if(courseInfo == nil)
            return NO;
        
        NSString *assemblyType = [courseInfo valueForKey:@"assembly_type"];
        
        // 모듈화 과정
        if([assemblyType isEqualToString:@"2"]){
            // studymodule 에서
            
            BOOL allExists = YES;
            
            NSArray *moduleArray = [Util downloadModuleCourseExists:[NSString stringWithFormat:@"%d", take_course_seq] andChapterNo:chapter_no];
            for(NSManagedObject *object in moduleArray){
                if([object valueForKey:@"mov_path"] == nil)
                    continue;
                
                NSString *fileNm = [object valueForKey:@"mov_path"];
                allExists = allExists && [self checkFileExists:fileNm];
                
                if(allExists == NO)
                    break;
                
            }
            NSLog(@"ExistsDownloadFileOfCourse chapter_no == %@, isExists %@", chapter_no, allExists ? @"YES" : @"NO");
            return allExists;
        }
        // 일반과정
        else {
            
            NSString *fileNm = [courseInfo valueForKey:@"file_nm"];
            BOOL isExist = [Util checkFileExists:fileNm];
            NSLog(@"ExistsDownloadFileOfCourse chapter_no == %@, isExists %@", chapter_no, isExist ? @"YES" : @"NO");
            return isExist;
        }
        
        return NO;
        
    }
    @catch(NSException *exception){
        return NO;
    }
    
}

+ (NSManagedObject *)downloadCourseExists:(NSString *)takeCourseSeq andChapterNo:(NSString *)chapterNo {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"take_course_seq == %@ and chapter_no == %@",
                              takeCourseSeq, chapterNo];
    
    NSManagedObject *result = [Util selectOne:@"Study" andPredicate:predicate];
    
    return result;
}

+ (NSArray *)downloadModuleCourseExists:(NSString *)takeCourseSeq andChapterNo:(NSString *)chapterNo {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"take_course_seq == %@ and chapter_no == %@",
                              takeCourseSeq, chapterNo];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"chapter_no" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sort, nil];
    
    NSArray *result = [Util selectList:@"StudyModule" andPredicate:predicate andSortDescriptor:sortDescriptors];
    
    return result;
}


+ (BOOL)checkFileExists:(NSString *)fileNm {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/hunet_docroot/%@", documentsDirectory, fileNm];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    return [fileManager fileExistsAtPath:filePath];
}

+(NSString*)getDocumentDirectoryPath {
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    return path;
}

+(NSString*)getFileFullPath:(NSString*)folder {
    NSString *documentdir = [self getDocumentDirectoryPath];
    NSString *fullPath = [documentdir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", folder]];
    return fullPath;
}

+(NSString*)getFileFullPath:(NSString*)folder fileName:(NSString*)fileName {
    NSString *documentdir = [self getDocumentDirectoryPath];
    NSString *fullPath = [documentdir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@", folder, fileName]];
    return fullPath;
}

+(int)isFileExists:(NSString*)folder fileName:(NSString*)fileName {
    NSString *fullPath = [self getFileFullPath:folder fileName:fileName];
    BOOL isExists = [[NSFileManager defaultManager] fileExistsAtPath:fullPath];
    return isExists;
}

+(int)isFileExists:(NSString*)filePath {
    BOOL isExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    return isExists;
}

+(NSString*)createDirecotryBy:(NSString*)folder {
    NSString *folderPath = [self getFileFullPath:folder];
    BOOL isExists = [[NSFileManager defaultManager] fileExistsAtPath:folderPath];
    
    
    if (isExists == TRUE) {
        NSLog(@"already exists : %@", folderPath);
        return nil;
    }
    
    NSError *error;
    if (![[NSFileManager defaultManager] createDirectoryAtPath:folderPath
                                   withIntermediateDirectories:NO
                                                    attributes:nil
                                                         error:&error])
    {
        NSLog(@"Create directory error: %@\n%@", folderPath, error);
        return nil;
    }
    
    return folderPath;
}

+(NSData*)fileToData:(NSString*)filePath {
    NSData *data = [[NSFileManager defaultManager] contentsAtPath:filePath];
    return data;
}

+(void)dataToFile:(NSData*)data saveFilePath:(NSString*)saveFilePath {
    [data writeToFile:saveFilePath atomically:YES];
}

+(NSString*)fileToString:(NSString*)filePath {
    NSData *data = [[NSFileManager defaultManager] contentsAtPath:filePath];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

+(void)stringToFile:(NSString*)text saveFilePath:(NSString*)saveFilePath {
    NSData *data = [text dataUsingEncoding:NSUTF8StringEncoding];
    [data writeToFile:saveFilePath atomically:YES];
}

+(void)moveFile:(NSString*)filePath toPath:(NSString*)toPath {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager copyItemAtPath:filePath toPath:toPath error:nil];
    [fileManager removeItemAtPath:filePath error:nil];
}

+(void)copyFile:(NSString*)filePath toPath:(NSString*)toPath {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager copyItemAtPath:filePath toPath:toPath error:nil];
}


// NSManagedObjectContext * 복붙하기 귀찮아서 아래 메소드 구현함 .. 어떻게 개발된지 4년넘는 코드가 이런 공통모듈 하나 없나....

// 목록조회
+(NSArray *)selectList:(NSString *)entityName andPredicate:(NSPredicate *)predicate {
    return [Util selectList:entityName andPredicate:predicate andSortDescriptor:nil];
}

+(NSArray *)selectList:(NSString *)entityName andPredicate:(NSPredicate *)predicate andSortDescriptor:(NSArray *)sortDescriptors {
    return [Util selectList:entityName andPredicate:predicate andSortDescriptor:sortDescriptors andLimit:-1];
}

/**
 목록조회
 ORDER BY 가 필요 시 sortDescriptors 에 정의해서 넘기면 됨.
 */
+(NSArray *)selectList:(NSString *)entityName andPredicate:(NSPredicate *)predicate andSortDescriptor:(NSArray *)sortDescriptors andLimit:(int)limitCnt {
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    // Top / Limit의 효과
    if(limitCnt != -1){
        request.fetchLimit = limitCnt;
    }
    
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    [request setEntity:entitydesc];
    [request setPredicate:predicate];
    if(sortDescriptors != nil)
        [request setSortDescriptors:sortDescriptors];
    
    NSError *error = nil;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    return matchingData;
}


// 하나만 조회
+(NSManagedObject *)selectOne:(NSString *)entityName andPredicate:(NSPredicate *)predicate {
    return [self selectOne:entityName andPredicate:predicate andSortDescriptor:nil];
}

// 하나만 조회
+(NSManagedObject *)selectOne:(NSString *)entityName andPredicate:(NSPredicate *)predicate andSortDescriptor:(NSArray *)sortDescriptors {
    NSArray *array = [Util selectList:entityName andPredicate:predicate andSortDescriptor:sortDescriptors andLimit:1];
    if([array count] > 1 || [array count] == 0)
        return nil;
    else
        return [array objectAtIndex:0];
}

// 업데이트 -- return affected row count; .. 하 씨... 트랜잭션 ㅠㅠ
+(NSNumber*)update:(NSString *)entityName andPredicate:(NSPredicate *)predicate andUpdatedic:(NSMutableDictionary *)dataDic{
    NSManagedObjectContext* context = [[MLCenterForBusinessAppDelegate sharedAppDelegate]managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    [request setEntity:entitydesc];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    
    NSNumber *affectedRowCount = [NSNumber numberWithInteger:[matchingData count]];
    
    // 조건에 해당하는 내용 없음
    if([matchingData count] <= 0)
        return affectedRowCount;
    
    for(NSManagedObject *rowObj in matchingData){
        NSArray *moduleKeys = [dataDic allKeys];
        for(NSString *keyStr in moduleKeys){
            [rowObj setValue:[dataDic objectForKey:keyStr] forKey:keyStr];
        }
    }
    [context save:&error];
    
    if(error != nil){
        affectedRowCount = [NSNumber numberWithInt:0];
        [context rollback];
    }
    return affectedRowCount;
}




/*
 -- StudyModule
 chapter_no        String
 course_cd        String
 frame_no        Integer 16
 index_nm        String
 last_view_sec    Integer 32
 mov_path        String
 */
+ (NSMutableDictionary *)modifyToStudyModuleDic:(NSDictionary *)result
{
    
    NSMutableDictionary *moduleDrm = [[result objectForKey:@"module_drm"] mutableCopy];
    NSArray *videos = [moduleDrm objectForKey:@"videos"];
    
    NSString* chapter_no            = [result objectForKey:@"chapter_no"];
    NSString* course_cd                = [result objectForKey:@"course_cd"];
    NSString* index_nm                = [result objectForKey:@"index_nm"];
    
    NSMutableArray *studyModuleArray = [[NSMutableArray alloc] init];
    
    int fileExistsCount = 0;
    
    for(NSDictionary *orgDic in videos){
        NSMutableDictionary *copyDic = orgDic.mutableCopy;
        [copyDic setObject:chapter_no forKey:@"chapter_no"];
        [copyDic setObject:course_cd forKey:@"course_cd"];
        [copyDic setObject:index_nm forKey:@"index_nm"];
        
        NSString *movPath = [copyDic objectForKey:@"mov_path"];
        if(movPath != nil && [movPath length] != 0){
            fileExistsCount++;
            [copyDic setObject:[NSNumber numberWithInt:fileExistsCount] forKey:@"file_down_index"];
        }
        
        [studyModuleArray addObject:copyDic];
    }
    
    NSMutableDictionary *copyResult = result.mutableCopy;
    [copyResult setObject:[NSNumber numberWithInt:fileExistsCount] forKey:@"file_count"];
    [copyResult setObject:studyModuleArray forKey:@"studyModuleArray"];
    
    return copyResult;
}

+ (NSMutableDictionary *)getQueryParam:(NSString *)urlString {
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSString *queryParams = url.query;
    
    if(queryParams == nil){
        NSUInteger qLocation = [urlString rangeOfString:@"?"].location;
        
        if(qLocation > [urlString length]){
            // :// 뒤에 것을 찾도록 한다.
            qLocation = [urlString rangeOfString:@"://"].location + 2;
            queryParams = [urlString substringFromIndex:qLocation + 1];
            
        }else{
            queryParams = [urlString substringFromIndex:qLocation + 1];
        }
        
    }
    
    NSArray *queryArray = [queryParams componentsSeparatedByString:@"&"];
    for (NSString *item in queryArray) {
        NSArray *commands = [item componentsSeparatedByString:@"="];
        [paramDic addEntriesFromDictionary:[[NSDictionary alloc]initWithObjectsAndKeys:
                                            [[commands objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                                            , [commands objectAtIndex:0]
                                            , nil]];
    }
    
    return paramDic;
}

@end
