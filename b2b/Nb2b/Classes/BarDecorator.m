//
//  BarDecorator.m
//  MLCenterForBusiness
//
//  Created by Park SeungKyun on 2015. 7. 20..
//
//

#import "BarDecorator.h"
#import "BaseViewController.h"
#import "MyViewController.h"
#import "LectureViewController.h"
#import "DownloadViewController.h"
#import "Util.h"
#import "MoreViewController.h"
#import "HomeViewController.h"
#import "SelfStudyViewController.h"
#import "StudyStateViewController.h"
#import "Header.h"
#import "GraphicsUtile.h"

@implementation BarDecorator

/*** 메뉴타이틀바 & 탭바색상 커스텀 예제 : /Classes/Custom/kangwonland_3856/BarDecorator.m ***/
+ (void)setting:(MLCenterForBusinessAppDelegate *) app
{
#ifdef spc_8907
    NSString *prefixString = [app.profileUser valueForKey:@"useridPrefix"];
    if ([[prefixString lowercaseString] isEqualToString:@"pc_"]) {
        [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#2C1D22" selectedTintColorRGBHex:@"#ffffff" unSelectedTintColorRGBHex:@"#AAAAAA" naviBarColorRGBHex:@"#2C1D22"];
    }
    else if ([[prefixString lowercaseString] isEqualToString:@"sl"]) {
        [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#DD4713" selectedTintColorRGBHex:@"#ffffff" unSelectedTintColorRGBHex:@"#BBBBBB" naviBarColorRGBHex:@"#DD4713"];
    }
    else if ([[prefixString lowercaseString] isEqualToString:@"brk"]) {
        [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#114C93" selectedTintColorRGBHex:@"#ffffff" unSelectedTintColorRGBHex:@"#AAAAAA" naviBarColorRGBHex:@"#114C93"];
    }
    else if ([[prefixString lowercaseString] isEqualToString:@"spc"]) {
        [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#1A9ECC" selectedTintColorRGBHex:@"#ffffff" unSelectedTintColorRGBHex:@"#AAAAAA" naviBarColorRGBHex:@"#1A9ECC"];
    }
    else {
        [app settingtabBarControllerDefault:NO isImagine:NO useDefaultNaviStyle:YES];
    }
    return;
#elif edaekyo_76 || dkl_18508
    [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#BBBBBB" selectedTintColorRGBHex:@"#536C74" unSelectedTintColorRGBHex:@"#FFFFFF" naviBarColorRGBHex:@"#536C74"];
    return;
#elif smrateugenes_18401
    [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:YES tabBarColorRGBHex:@"#F1F1F1" selectedTintColorRGBHex:@"#1B52CE" unSelectedTintColorRGBHex:@"#BBBBBB" naviBarColorRGBHex:@"#1B52CE"];
    return;
#elif avene
    [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#4A4B4D" selectedTintColorRGBHex:@"#ffffff" unSelectedTintColorRGBHex:@"#7F7F80" naviBarColorRGBHex:@"#4A4B4D"];
    return;

#elif furterer
    [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#2E6432" selectedTintColorRGBHex:@"#ffffff" unSelectedTintColorRGBHex:@"#7F7F80" naviBarColorRGBHex:@"#2E6432"];
    return;

#elif hyundai || hyundaiDymos || hyundaiWia || hyundaiHmtc || Wiajs || Wiash || dymosrz
    [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#ffffff" selectedTintColorRGBHex:@"#ffffff" unSelectedTintColorRGBHex:@"#6C8CB2" naviBarColorRGBHex:@"#003b83"];
    return;
#elif kia
    [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#C70026" selectedTintColorRGBHex:@"#ffffff" unSelectedTintColorRGBHex:@"#6D0005" naviBarColorRGBHex:@"#C70026"];
    return;
#elif kcc
    [app setTabBarAndNavigationBarWihIsFiveTabes:NO isImagine:NO tabBarColorRGBHex:@"#E4E5E6" selectedTintColorRGBHex:@"#ffffff" unSelectedTintColorRGBHex:@"#6D0005" naviBarColorRGBHex:@"#003B83"];
    return;
#endif
    
    
    [app.window.rootViewController removeFromParentViewController];
    if (app.tabBarController != nil) {
        app.tabBarController = nil;
    }
    app.tabBarController = [[UITabBarController alloc] init];
    app.tabBarController.delegate = app;
    app.window.rootViewController = app.tabBarController;
    
    UINavigationController *navDownload = [Util getNavigationController:[DownloadViewController class] initWithStyle:UITableViewStylePlain];
    UINavigationController *navMore = [Util getNavigationController:[MoreViewController class] withNibName:@"MoreViewController"];
    
    
//기본연수원 조건 분기
    
    if ([app.eduType isEqualToString:@"OnlyImagine"])
    {
        app.tabBarController.viewControllers = @[
                                                 [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                                 [[SelfStudyViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                                 [[StudyStateViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                                 navDownload,
                                                 navMore
                                                 ];
    }
    else if ([app.eduType isEqualToString:@"LearningAndImagine"])
    {
        app.tabBarController.viewControllers = @[
                                                 [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                                 [[LectureViewController alloc] initWithNibName:@"LectureViewController" bundle:nil],
                                                 [[SelfStudyViewController alloc] initWithNibName:@"BaseViewController" bundle:nil],
                                                 [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                                 navMore
                                                 ];
    }
    else if ([app.eduType isEqualToString:@"OnlySangsang"])
    {
        app.tabBarController.viewControllers = @[
                                                 [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                                 [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                                 navDownload,
                                                 navMore
                                                 ];
    }
    else
    {
        app.tabBarController.viewControllers = @[
                                                 [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil],
                                                 [[LectureViewController alloc] initWithNibName:@"LectureViewController" bundle:nil],
                                                 [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:nil],
                                                 navDownload,
                                                 navMore
                                                 ];
    }

    //
    ////////////////////////////////////////////////
    
    
    /* 탭바 배경 설정 */
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
#if hyundai_16597 || hyundai_16596
        app.tabBarController.tabBar.tintColor = [UIColor colorWithRed:97.0/255.0 green:169.0/255.0 blue:132.0/255.0 alpha:1.0];
#elif avene
        app.tabBarController.tabBar.tintColor = [UIColor colorWithRed:43.0/255.0 green:103.0/255.0 blue:117.0/255.0 alpha:1.0];
#elif furterer
        app.tabBarController.tabBar.tintColor = [UIColor colorWithRed:46.0/255.0 green:100.0/255.0 blue:50.0/255.0 alpha:1.0];
#elif hyundai
        app.tabBarController.tabBar.tintColor = [UIColor colorWithRed:43.0/255.0 green:103.0/255.0 blue:117.0/255.0 alpha:1.0];
#endif
    }
    
    // 디폴트배경(탭바 & 네비게이션바)
    UIColor *barColor = [UIColor colorWithRed:48.0/255.0 green:70.0/255.0 blue:143.0/255.0 alpha:1.0];
    /* 네비게이션바 배경 설정 */
    [GraphicsUtile setNavigationBarColor:barColor];
    
    [app performSelector:@selector(pushServiceURL) withObject:nil afterDelay:0.5];
}

@end
