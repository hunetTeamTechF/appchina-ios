//
//  LocalNotifierViewController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 7. 1..
//  Copyright 2011 hunet. All rights reserved.
//

#import "LocalNotifierViewController.h"
#import "WeekViewController.h"
#import "MLCenterForBusinessAppDelegate.h"


@implementation LocalNotifierViewController
@synthesize tableview, datePicker, switchNoti;

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (NSDate *)scheduleDate:(int)day {
	
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
	switch (day) {
			
		case 0:
		{
			if (![[app.profileUser objectForKey:@"schedule_sunday"] boolValue]) {
				return nil;
			}				
		}
			break;
		case 1:
		{			
			if (![[app.profileUser objectForKey:@"schedule_monday"] boolValue]) {
				return nil;
			}				
		}
			break;
		case 2:
		{
			if (![[app.profileUser objectForKey:@"schedule_tuesday"] boolValue]) {
				return nil;
			}			
		}
			break;
		case 3:
		{
			if (![[app.profileUser objectForKey:@"schedule_wednesday"] boolValue]) {
				return nil;
			}
		}
			break;
		case 4:
		{
			if (![[app.profileUser objectForKey:@"schedule_thursday"] boolValue]) {
				return nil;
			}				
		}
			break;
		case 5:
		{
			if (![[app.profileUser objectForKey:@"schedule_friday"] boolValue]) {
				return nil;
			}				
		}
			break;
		case 6:
		{
			if (![[app.profileUser objectForKey:@"schedule_saturday"] boolValue]) {
				return nil;
			}					
		}
			break;
		
		default:
			break;
	}
	
	
	
	
	int weekday = [[[NSCalendar currentCalendar] components:NSWeekdayCalendarUnit fromDate:self.datePicker.date] weekday];
	NSDate *sunday = [self.datePicker.date dateByAddingTimeInterval:60*60*24*-(weekday - 1)];
	
	return [sunday dateByAddingTimeInterval:60*60*24*day];	
}


- (IBAction) scheduleAlarm:(id) sender {
	
	//NSLog(@"scheduleAlarm");
	
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
	[app setNoticeOption:self.switchNoti.isOn withKey:@"schedulenoti"];
	
	
	[[UIApplication sharedApplication] cancelAllLocalNotifications];
	
	if (self.switchNoti.isOn) {
		
		for (int i = 0; i < 7; i++) {
			
			NSDate *newDate = [self scheduleDate:i];
			
			if (newDate == nil) {
				continue;
			}
			
			
			NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
			
			
			NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) 
														   fromDate:newDate];
			NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) 
														   fromDate:newDate];
			

			//NSLog(@"year : %i, month : %i, day : %i", [dateComponents year], [dateComponents month], [dateComponents day]);
			
			
			NSDateComponents *dateComps = [[NSDateComponents alloc] init];
			[dateComps setDay:[dateComponents day]];
			[dateComps setMonth:[dateComponents month]];
			[dateComps setYear:[dateComponents year]];
			[dateComps setHour:[timeComponents hour]];
			[dateComps setMinute:[timeComponents minute]];
			[dateComps setSecond:[timeComponents second]];
			NSDate *itemDate = [calendar dateFromComponents:dateComps];
			
			UILocalNotification *localNotif = [[UILocalNotification alloc] init];
			if (localNotif == nil)
				return;
			
			localNotif.fireDate = itemDate;
			localNotif.timeZone = [NSTimeZone defaultTimeZone];
			
			
			localNotif.alertBody = @"모바일 연수원 학습시간 입니다 ^^";
			localNotif.alertAction = @"보기";
			
			localNotif.soundName = UILocalNotificationDefaultSoundName;
			//localNotif.applicationIconBadgeNumber = localNotif.applicationIconBadgeNumber + 1;
			//localNotif.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
            localNotif.applicationIconBadgeNumber = 0;
			
	
			
			localNotif.repeatInterval = NSWeekCalendarUnit;
			
			NSDictionary *infoDict = [NSDictionary dictionaryWithObject:@"someValue" forKey:@"someKey"];
			localNotif.userInfo = infoDict;
			
			[[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
			
		}			
	}
}




- (NSString*)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section {
	switch (section) {
		case 0:
		{
			return @"계획적인 학습을 도와주는 알림 기능";
		}
			break;
		default:
			break;
	}
	return @"";
}


/*
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	
	NSString *sectionTitle = @"계획적인 학습을 도와주는 알림 기능입니다";
	
	UILabel *label = [[[UILabel alloc] init] autorelease];
	
	label.backgroundColor = [UIColor grayColor];
	label.frame = CGRectMake(10, 0, 300, 40);
	
	label.font = [UIFont systemFontOfSize:15];
	label.text = sectionTitle;
	label.textAlignment = NSTextAlignmentCenter;
	label.backgroundColor = [UIColor clearColor];

	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
	[view autorelease];
	[view addSubview:label];
	
	return view;
}
*/

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 40.0f;
}

/*
- (NSString*)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section {
	switch (section) {
		case 0:
		{
			return @"계획적인 학습을 도와주는 알림 기능입니다.";
		}
			break;
		default:
			break;
	}
	return @"";
}
 */


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *CellIdentifier = @"Cell";
	
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
	NSMutableString *scheduleDays = [NSMutableString string];
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
	
	
	cell.accessoryView = nil;
	cell.accessoryType = UITableViewCellAccessoryNone;	
	cell.textLabel.font = [UIFont systemFontOfSize:16];	
    cell.backgroundColor = [UIColor whiteColor];
	
	
	if (indexPath.row == 0) {
		
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		cell.textLabel.text = @"알림 사용";		
		switchNoti = [[UISwitch alloc] initWithFrame:CGRectZero];
		switchNoti.tag = 1;
		[switchNoti addTarget:self action:@selector(scheduleAlarm:) forControlEvents:UIControlEventValueChanged];			
		cell.accessoryView = switchNoti;

		switchNoti.on = [[app.profileUser objectForKey:@"schedulenoti"] boolValue];
	

	} else {
		
		

		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		
		//UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithFrame:CGRectMake(-1.0f, -1.0f, 302.0f, 46.0f)]; 
		//[cell.contentView addSubview:segmentedControl];  
		
		/*
		NSArray * segmentItems= [NSArray arrayWithObjects: @"월", @"화", @"수", @"목", @"금", @"토", @"일", nil];
		UISegmentedControl *segmentedControl= [[[UISegmentedControl alloc] initWithItems: segmentItems] retain];
		segmentedControl.segmentedControlStyle = UISegmentedControlStylePlain;
		//segmentedControl.selectedSegmentIndex = -1;
		segmentedControl.multipleTouchEnabled = YES;
		
		
		//[segmentedControl addTarget: self action: @selector(onSegmentedControlChanged:) forControlEvents: UIControlEventValueChanged];
		segmentedControl.frame  = CGRectMake(10, 10, 280, 40);
		segmentedControl.tintColor= [UIColor grayColor];
		[cell.contentView addSubview:segmentedControl];  		
		*/
		
		if ([cell.contentView subviews]){
			for (UIView *subview in [cell.contentView subviews]) {
				[subview removeFromSuperview];
			}
		}
		
		UILabel *cellTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 40, 30)];
		cellTitle.text = @"반복";
		[cell.contentView addSubview:cellTitle];
		
		for (int i = 0; i < 7; i++) {			
			
			switch (i) {
				case 0:
				{			
					if ([[app.profileUser objectForKey:@"schedule_monday"] boolValue]) {
						[scheduleDays appendFormat:@" %@", @"월"];
					}				
				}
					break;
				case 1:
				{
					if ([[app.profileUser objectForKey:@"schedule_tuesday"] boolValue]) {
						[scheduleDays appendFormat:@" %@", @"화"];
					}			
				}
					break;
				case 2:
				{
					if ([[app.profileUser objectForKey:@"schedule_wednesday"] boolValue]) {
						[scheduleDays appendFormat:@" %@", @"수"];
					}
				}
					break;
				case 3:
				{
					if ([[app.profileUser objectForKey:@"schedule_thursday"] boolValue]) {
						[scheduleDays appendFormat:@" %@", @"목"];
					}				
				}
					break;
				case 4:
				{
					if ([[app.profileUser objectForKey:@"schedule_friday"] boolValue]) {
						[scheduleDays appendFormat:@" %@", @"금"];
					}				
				}
					break;
				case 5:
				{
					if ([[app.profileUser objectForKey:@"schedule_saturday"] boolValue]) {
						[scheduleDays appendFormat:@" %@", @"토"];
					}					
				}
					break;
				case 6:
				{
					if ([[app.profileUser objectForKey:@"schedule_sunday"] boolValue]) {
						[scheduleDays appendFormat:@" %@", @"일"];
					}				
				}
					break;
				default:
					break;
			}
		}
		
		UILabel *cellContents = [[UILabel alloc] initWithFrame:CGRectMake(70, 5, 200, 30)];
		cellContents.text = scheduleDays;
		cellContents.textColor = [UIColor blueColor];
		cellContents.textAlignment = NSTextAlignmentRight;
        cellContents.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleRightMargin;
		[cell.contentView addSubview:cellContents];
	}	
	
	return cell;
	
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	if (indexPath.row == 1) {
		
		WeekViewController *viewToPush = [[WeekViewController alloc] initWithNibName:@"WeekViewController" bundle:nil];
		if (viewToPush) {
			viewToPush.title = @"반복";			
			[self.navigationController pushViewController:viewToPush animated:YES];
		}
		
	}
	
	[tableView deselectRowAtIndexPath:indexPath animated:NO]; 
}



/*
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.row == 0) {
		return 45.;
	}
	
	return 60.f;	
}
*/


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithN	ibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        // Custom initialization.
    }
    return self;
}
*/



- (void)viewDidLoad {
	
	NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
	
	NSDate *sDate;
	if ([notificationArray count] > 0) {
		UILocalNotification *notif = [notificationArray objectAtIndex:0];
		sDate = notif.fireDate;
	} else {
		sDate = [NSDate date];
	}

	self.datePicker.date = sDate;
    [super viewDidLoad];
}




-(void) viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	[self.tableview reloadData];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {

	self.tableview = nil;
	self.datePicker = nil;
	self.switchNoti = nil;
    [super viewDidUnload];
}

@end
