//
//  HTMLPlayerController.m
//  MLCenterForBusiness
//
//  Created by Seungkil Ryu on 2013. 12. 4..
//
//

#import "HTMLPlayerController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "MyViewController.h"
#import "CJSONDeserializer.h"
#import "Util.h"
#import "Global.h"
#import "MoviePlayerController.h"
#import "PlayerCallFun.h"
#import "MBProgressHUD.h"

@implementation HTMLPlayerController
@synthesize url;
@synthesize playerCallFun = _playerCallFun;
@synthesize urlMark;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



- (void)viewWillAppear:(BOOL)animated {
    if (animated) {
        NSLog(@"flagAutogo : %hhd", [MLCenterForBusinessAppDelegate sharedAppDelegate].flagAutoGo);
        
        if([MLCenterForBusinessAppDelegate sharedAppDelegate].flagAutoGo){
            [viewweb stringByEvaluatingJavaScriptFromString:@"window.main.fnMobileNext();"];
            [MLCenterForBusinessAppDelegate sharedAppDelegate].flagAutoGo = false;
            
        }
        
    }
    
    [super viewWillAppear:animated];
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewLoad {
    
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    [viewweb loadRequest:request];
    viewweb.scalesPageToFit = YES;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    viewweb.delegate = self;
    // Do any additional setup after loading the view from its nib.
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    
    [viewweb loadRequest:request];
    //status bar hidden for under iOS7
    [[UIApplication sharedApplication] setStatusBarHidden:YES
                                            withAnimation:UIStatusBarAnimationFade];
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *requestStr = [[request URL] absoluteString];
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    
    
    if([requestStr rangeOfString:@"close://"].location != NSNotFound){
        
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        return NO;
    }
    
    if ([requestStr.lowercaseString hasPrefix:@"sangsangdrmplayer://"] ||
        [requestStr.lowercaseString hasPrefix:@"sangsangdownload://"] ||
        [requestStr.lowercaseString hasPrefix:@"sangsangdownloadplay://"]) {
        
        NSLog(@"sangsangdrmp");
        
        NSArray *tmpArray = [requestStr componentsSeparatedByString:@"://"];
        NSString *act = [tmpArray objectAtIndex:0];
        NSString *param = [tmpArray objectAtIndex:1];
        
        
        if ([app isWiFi]) {
            _playerCallFun = [[PlayerCallFun alloc] init];
            _playerCallFun.delegateViewController = self;
            self.urlMark = [viewweb stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
            [_playerCallFun sangsangmaruInit:[NSArray arrayWithObjects:act, param, nil]];
            //[[MLCenterForBusinessAppDelegate sharedAppDelegate] removemaskLoading];
        }
        
        
        return NO;
    }
    
    
    
    if([requestStr.lowercaseString rangeOfString:@"study/contents/main.aspx"].location != NSNotFound){
        
        if([requestStr.lowercaseString rangeOfString:@"study/contents/main.aspx?"].location != NSNotFound){
            
            NSString *tmpStr = [NSString stringWithFormat:@"%@%@", requestStr, @"&drmYn=Y"];
            requestStr = tmpStr;
            NSLog(@"shouldStartLoadWithRequest : %@", requestStr);
        }
        else{
            NSString *tmpStr = [NSString stringWithFormat:@"%@%@", requestStr, @"?drmYn=Y"];
            requestStr = tmpStr;
            NSLog(@"shouldStartLoadWithRequest : %@", requestStr);
        }
        
        //return NO;
        
    }
    
    
    
    if ([requestStr.lowercaseString rangeOfString:@"w.hunet"].location != NSNotFound) {
        
        if ([[MLCenterForBusinessAppDelegate sharedAppDelegate] moviePlayCheck] == NO) {
            return NO;
        }
        
        NSLog(@"url:%@", self.url);
        NSLog(@"player:%@", requestStr );
        
        
        NSArray *tmpArray = [requestStr componentsSeparatedByString:@"://"];
        NSUserDefaults *userdeDefaults = [NSUserDefaults standardUserDefaults];
        
        NSString *param = [tmpArray objectAtIndex:1];
        NSArray *pairs = [param componentsSeparatedByString:@";;"];
        
        
        [userdeDefaults setObject:[[pairs objectAtIndex:0] stringByReplacingOccurrencesOfString:@"w.hunet" withString:@"http://m.hunet"] forKey:FILENAME];
        NSLog(@"Streaming filename = %@", [userdeDefaults objectForKey:FILENAME]);
        
        //NSInteger cno = [[pairs objectAtIndex:1] intValue];
        // NSString *totalFrameCnt = [pairs objectAtIndex:1];
        NSInteger fno = [[pairs objectAtIndex:3] intValue];
        // NSString *frameType = [pairs objectAtIndex:3];
        
        NSString *ccd = [pairs objectAtIndex:5];
        NSInteger tcseq = [[pairs objectAtIndex:6] intValue];
        NSString *progressNo = [pairs objectAtIndex:7];
        // NSInteger lastViewSec = [[pairs objectAtIndex:8] intValue];
        
        
        MoviePlayerController * movieCon = [[MoviePlayerController alloc] initWithNibName:@"MoviePlayerController" bundle:nil];
        movieCon.isStreamingPlay = TRUE;
        // movieCon.currentSecond = lastViewSec; -> 모듈화과정은 플레이어에서 직접 가져오도록 수정
        movieCon.scrollType = 1;   // progress touch 여부 0 : 진도바 이동 불가, 1 : 진도바 이동 가능
        movieCon.courseCd = ccd;
        movieCon.takecourseSeq = tcseq;
        //movieCon.chapterNo = cno;
        // movieCon.movieTitle = indexNm;
        movieCon.progressNo = progressNo;
        movieCon.frameNo = fno;
        //movieCon.lastMarkNo =lastViewSec;
        movieCon.evaluationProgressRatio = @"0";
        movieCon.sequenceProgressType = @"1";
        
        
        //movieCon.movContents = [result objectForKey:@"Result"];
        movieCon.progressSaveYn = @"N";//진도저장필요없음
        [MLCenterForBusinessAppDelegate sharedAppDelegate].moviePlayerController = movieCon;
        [self presentViewController:movieCon animated:YES completion:nil];
        
        return NO;
    }
    
    return YES;
}

//for status bar hidden - iOS7
-(BOOL)prefersStatusBarHidden{
    return YES;
}

//IOS 6.0 회전 관련 시작
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
    //return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
    //기존 뷰 배치 로직 수행...
    NSLog(@"roatate");
}

//IOS 6.0 회전 관련 끝
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation //IOS 6.0 이하 버전
{
    if((interfaceOrientation == UIInterfaceOrientationLandscapeLeft)
       || (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
    {
        return YES;
    }
    
    return NO;
}

- (void)viewDidUnload
{
    viewweb.delegate = nil;
    self.url = nil;
    [super viewDidUnload];
}

- (void)dealloc {
    viewweb = nil;
}

@end
