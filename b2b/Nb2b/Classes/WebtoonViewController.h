//
//  WebtoonViewController.h
//  MLCenterForBusiness
//
//  Created by Park Gi-Bok on 12. 3. 13..
//  Copyright (c) 2012 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebtoonViewController : UIViewController <UIWebViewDelegate, UIGestureRecognizerDelegate> {
	

	IBOutlet UIActivityIndicatorView *indicator;
    NSString *webtoonUrl;
    
}

@property (strong, nonatomic) NSString *webtoonUrl;
@property (strong, nonatomic) IBOutlet UIWebView *viewWeb;

@property(readwrite, assign) BOOL togle;

- (IBAction)webtoonClose;

@end
