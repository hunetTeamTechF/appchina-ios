

#import <UIKit/UIKit.h>

@interface DownloadIndexSelfViewSangController : UITableViewController
//상상마루
@property (nonatomic, strong) NSArray *sangsangArray;
@property (nonatomic, strong) NSArray *sagnsangStudyArray;
@property (nonatomic, strong) NSString *goodsId;
@property (nonatomic, strong) NSString *seGoodsId;
@property (nonatomic, assign) NSInteger contractNo;
@property (nonatomic, assign) NSInteger contentsSeq;
@property (nonatomic, assign) NSInteger viewNo;
@property (nonatomic, strong) NSString *userId;


@property (nonatomic, assign) NSInteger expirationDay;
@property (strong, nonatomic) NSString *synMessage;
@end
