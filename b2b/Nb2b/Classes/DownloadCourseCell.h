//
//  DownloadCourseCell.h
//  MLCenterForBusiness
//
//  Created by 박 기복 on 13. 4. 4..
//
//

#import <UIKit/UIKit.h>

@interface DownloadCourseCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *courseNm;
@property (strong, nonatomic) IBOutlet UILabel *studyEndDate;
@property (strong, nonatomic) IBOutlet UILabel *expirationDay;
@property (strong, nonatomic) IBOutlet UILabel *studyPerio;

@end
