

#import "MoreViewController.h"
#import "ModalAlert.h"
#import "CJSONDeserializer.h"
#import "MoreCell.h"
#import "LocalNotifierViewController.h"
#import "SetupViewController.h"
#import "UserViewController.h"
#import "VersionViewController.h"
#import "DownloadViewController.h"
//#import "DownloadTabViewController.h"
#import "NoticeViewController.h"
#import "QnaViewController.h"
#import "FaqViewController.h"
#import "HStoryViewController.h"
#import "HReminderViewController.h"
#import "DevOptionViewController.h"
#import "Header.h"
#import "PasswordViewController.h"
#import "GraphicsUtile.h"

#define ENCODE(X) [(X) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
@interface MoreViewController () {
    NSString * _menuTitleForNotice;
    NSString * _menuTitleForOneToOneCouncel;
}
@end

@implementation MoreViewController
@synthesize listMenu, linkUrl, array;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        self.title = NSLocalizedString(@"setting", @"设置");
        self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_setting"];
        
    #if kcc
        self.tabBarItem.titlePositionAdjustment = UIOffsetMake(2, -16);
    #endif
    #if hyundai || kia || hyundaiDymos || hyundaiWia || hyundaiHmtc || Wiajs || Wiash || dymosrz || hmgc
        self.tabBarItem.titlePositionAdjustment = UIOffsetMake(2, -5);
        self.title = @"我的";
    #endif
        devOptionClickCount = 0;
    }
    return self;
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

#pragma mark -
#pragma mark Initialization
/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    self = [super initWithStyle:style];
    if (self != nil) {
        // Custom initialization.
    }
    return self;
}
*/

#pragma mark -
#pragma mark View lifecycle

/*
- (void)cellSelected {
	
	if (![self.linkUrl isEqualToString:@""]) {
		
		int row = -1;
		if ([self.listMenu count] > 0) {
			
			for (NSDictionary *dc in self.listMenu) {
				row++;
				if ([self.linkUrl isEqualToString:[dc objectForKey:@"Url"]]) {
					
					[self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:row + 4 inSection:0]];
					break;
				}
			}
		}
	}	
}
*/


- (BOOL)isDownloadCell {
#ifdef emart_13037
    return NO;
#endif
//    return [[self appDelegate].drmUseType isEqualToString:@"Y"] && [[self appDelegate].eduType isEqualToString:@"LearningAndImagine"];
    return YES;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc]init];

    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithTitle:@"       "
                                                                  style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:@selector(devOptionClick:)];
    self.navigationItem.rightBarButtonItem = barButton;
    
    _menuTitleForNotice = NSLocalizedString(@"appNotice", @"公告");
#ifdef adthrd_9534
    _menuTitleForNotice = @"캡스 라인 News";
#endif
    NSDictionary *aNotice = [NSDictionary dictionaryWithObject:_menuTitleForNotice forKey:@"0"];

    _menuTitleForOneToOneCouncel = NSLocalizedString(@"1d1", @"1:1 咨询");

    NSDictionary *bCounsel = [NSDictionary dictionaryWithObject:_menuTitleForOneToOneCouncel forKey:@"1"];

    NSDictionary *cSchedule = [NSDictionary dictionaryWithObject:NSLocalizedString(@"sendMessage", @"消息推送")  forKey:@"2"];
    NSDictionary *dSettings = [NSDictionary dictionaryWithObject:NSLocalizedString(@"appSetting", @"环境设置") forKey:@"3"];
    NSDictionary *eMemberInfo = [NSDictionary dictionaryWithObject:NSLocalizedString(@"personInfo", @"会员信息管理") forKey:@"4"];
    NSDictionary *fVersion = [NSDictionary dictionaryWithObject:NSLocalizedString(@"versionInfo", @"版本信息") forKey:@"5"];
    NSDictionary *gDownload = [NSDictionary dictionaryWithObject:NSLocalizedString(@"downLoadCenter", @"下载中心") forKey:@"6"];
    NSDictionary *hReminder = [NSDictionary dictionaryWithObject:@"H-리마인더" forKey:@"7"];
#ifdef hitejinro_10148
    NSDictionary *iStory = [NSDictionary dictionaryWithObject:@"지식라이브" forKey:@"8"];
#else
    NSDictionary *iStory = [NSDictionary dictionaryWithObject:@"H-스토리" forKey:@"8"];
#endif
    NSDictionary *jPassword = [NSDictionary dictionaryWithObject:NSLocalizedString(@"resetPw", @"修改密码")   forKey:@"9"];
    
    

    self.array = [[NSMutableArray alloc] init];
    [self.array addObject:aNotice];
#ifndef bossi_16794
    [self.array addObject:bCounsel];
#endif
    //[self.array addObject:jPassword];
    //[self.array addObject:eMemberInfo];
    //[self.array addObject:cSchedule];
    [self.array addObject:dSettings];
    
    [self.array addObject:fVersion];
    
    
    if ([self isDownloadCell])
        [self.array addObject:gDownload];
    
//    if ([[self appDelegate].hReminderUseType isEqualToString:@"1"])
//        [self.array addObject:hReminder];
//    
//    if ([[self appDelegate].hStoryUseType isEqualToString:@"1"])
//        [self.array addObject:iStory];
//    
    [self performSelector:@selector(devOption) withObject:nil afterDelay:0.2];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self devOption];
    [self.tableView reloadData];
    self.navigationController.navigationBar.hidden = NO;
    self.tabBarController.tabBar.hidden = NO;
}


#pragma mark -
#pragma mark Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return [self.array count];
    } else {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 5;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1)
    {
        return 60.0f;
    }
    else {
        return 44.0f;
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"MoreCell";
    MoreCell* cell = (MoreCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray* arr = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
        cell = [arr objectAtIndex:0];
    }
    cell.textLabel.font = [UIFont systemFontOfSize:17];

    if (indexPath.section == 0)
    {
        switch (indexPath.row) {

            case 6:
            {
                NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
                cell.labelInfo.text = version;
                cell.labelInfo.hidden = NO;
            }
                break;

            case 4:
            {
                cell.labelInfo.hidden = NO;
                cell.labelInfo.text = [[[self appDelegate].profileUser objectForKey:@"schedulenoti"] boolValue] ? @"" : @"";
            }
                break;
            case 0:
            case 2:
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 9:
            case 10:
            {
                cell.labelInfo.hidden = YES;
            }
                break;
        }

        NSDictionary *dic = [self.array objectAtIndex:indexPath.row];
        NSString *key = [[dic allKeys] objectAtIndex:0];
        cell.labelMenuNm.text = [dic objectForKey:key];
        cell.tag = [key integerValue];
        
        return cell;
    }
    else if (indexPath.section == 1)
    {
//        cell.labelMenuNm.text = @"로그 아웃";
//        cell.labelInfo.hidden = YES;
//        cell.tag = 20;
//        return cell;
        return self.cellFooter;
    }
    
	return nil;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    UITableViewCell *myCell = [tableView cellForRowAtIndexPath:indexPath];

    if (myCell.tag == 0)
    {
        NSString *_noticeUrl = [NSString stringWithFormat:@"%@?type=13&uid=%@&pw=%@",
                          [self appDelegate].urlBase,
                          [[self appDelegate].profileUser objectForKey:@"userid"],
                          [[self appDelegate].profileUser objectForKey:@"password"]];
        
        NoticeViewController *viewToPush = [[NoticeViewController alloc] initWithNibName:@"NoticeViewController" bundle:nil];
        viewToPush.title = _menuTitleForNotice;
        viewToPush.url = [self.linkUrl length] > 0 ? self.linkUrl : _noticeUrl;
        [self.navigationController pushViewController:viewToPush animated:YES];
        
    }
    
    if (myCell.tag == 1)
    {
#ifdef emart_13037
        FaqViewController *viewToPush = [[FaqViewController alloc] initWithNibName:@"FaqViewController" bundle:nil];
        viewToPush.title = _menuTitleForOneToOneCouncel;
        [self.navigationController pushViewController:viewToPush animated:YES];
#else
        QnaViewController *viewToPush = [[QnaViewController alloc] initWithNibName:@"QnaViewController" bundle:nil];
        viewToPush.title = _menuTitleForOneToOneCouncel;
        [self.navigationController pushViewController:viewToPush animated:YES];
#endif
    }
    
    
    if (myCell.tag == 2)
    {
        LocalNotifierViewController *viewToPush = [[LocalNotifierViewController alloc] initWithNibName:@"LocalNotifierViewController" bundle:nil];
        viewToPush.title = NSLocalizedString(@"sendMessage", @"消息推送");
        [self.navigationController pushViewController:viewToPush animated:YES];

    }
    
    if (myCell.tag == 3)
    {
        SetupViewController *viewToPush = [[SetupViewController alloc] initWithNibName:@"SetupViewController" bundle:nil];
        viewToPush.title = NSLocalizedString(@"appSetting", @"环境设置");
        [self.navigationController pushViewController:viewToPush animated:YES];
    }
    
    if (myCell.tag == 4)
    {
        UserViewController *viewToPush = [[UserViewController alloc] initWithNibName:@"UserViewController" bundle:nil];
        viewToPush.title = @"会员信息管理";
        [self.navigationController pushViewController:viewToPush animated:YES];
    }
    
    if (myCell.tag == 5)
    {
        VersionViewController *viewToPush = [[VersionViewController alloc] initWithNibName:@"VersionViewController" bundle:nil];
        viewToPush.title = NSLocalizedString(@"personInfo", @"版本信息");
        [self.navigationController pushViewController:viewToPush animated:YES];
    }
    
    if (myCell.tag == 6)
    {
        DownloadViewController *viewToPush = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
        [self.navigationController pushViewController:viewToPush animated:YES];
    }
    
    //리마인더
    if (myCell.tag == 7) {
        self.navigationController.navigationBar.hidden = YES;
        HReminderViewController *view = [[HReminderViewController alloc]
                                      initWithNibName:@"BaseViewController" bundle:nil];
        [self.navigationController pushViewController:view animated:YES];
    }
    
    //스토리
    if (myCell.tag == 8) {
        HStoryViewController *view = [[HStoryViewController alloc]
                                       initWithNibName:@"BaseViewController" bundle:nil];
        [self.navigationController pushViewController:view animated:YES];
    }
    
    //비밀번호 변경
    if (myCell.tag == 9) {
        PasswordViewController *viewToPush = [[PasswordViewController alloc] initWithNibName:@"PasswordViewController" bundle:nil];
        //viewToPush.title = @"비밀번호 변경";

        viewToPush.title = NSLocalizedString(@"resetPw", @"修改密码");
        self.navigationController.navigationBar.hidden = NO;
        [self.navigationController pushViewController:viewToPush animated:YES];
    }
    
    //개발자 옵션
    if (myCell.tag == 19) {
        DevOptionViewController *view = [[DevOptionViewController alloc] initWithStyle:UITableViewStyleGrouped];
        [self.navigationController pushViewController:view animated:YES];
    }
    
//    if (myCell.tag == 20)
//    {
//        [self appDelegate].flagLogin = NO;
//        [[MLCenterForBusinessAppDelegate sharedAppDelegate] loginViewControllerDisplay];
//    }

}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {

    self.listMenu = nil;
	self.linkUrl = nil;
    self.array = nil;
    
    [super viewDidUnload];
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (MLCenterForBusinessAppDelegate*)appDelegate
{
    return [MLCenterForBusinessAppDelegate sharedAppDelegate];
}

- (void)devOptionClick:(id)sender
{
    devOptionClickCount++;
    if (devOptionClickCount == 7)
    {
        NSDictionary *j = [NSDictionary dictionaryWithObject:@"开发者选项" forKey:@"19"];
        [self.array addObject:j];
        [self.tableView reloadData];
    }
}

- (void)devOption
{
    devOptionClickCount = 0;
    NSDictionary *lastArray = [self.array lastObject];
    if ([lastArray objectForKey:@"19"])
    {
        [self.array removeLastObject];
        [self.tableView reloadData];
    }
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    if ([[app.profileUser objectForKey:@"IsStaging"] boolValue])
    {
        self.navigationItem.rightBarButtonItem.title = NSLocalizedString(@"developeOption", @"开发者选项");
    }
    else
    {
        self.navigationItem.rightBarButtonItem.title = @"        ";
    }
}

- (IBAction)clickedLogout:(id)sender {
    [self appDelegate].flagLogin = NO;
    [Global setUserPassword:@""];
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] profileUserInfoSave];
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] loginViewControllerDisplay];
}

@end

