//
//  DownloadCourseCell.m
//  MLCenterForBusiness
//
//  Created by 박 기복 on 13. 4. 4..
//
//

#import "DownloadCourseCell.h"

@implementation DownloadCourseCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    _studyPerio.text = NSLocalizedString(@"studyPerio", @"学习期间");
    if (self != nil) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
