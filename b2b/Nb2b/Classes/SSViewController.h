//
//  SSViewController.h
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 7. 25..
//  Copyright 2011 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DownloadingView.h"
#import "web.h"

@interface SSViewController : UIViewController <UIWebViewDelegate, DownloadingViewDelegate> {
	NSString *url;
	IBOutlet UIWebView *viewWeb;
	IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UIBarButtonItem *home;
	IBOutlet UIBarButtonItem *back;
	IBOutlet UIBarButtonItem *forward;
    BOOL isFirstLoaded;
    Web *webUtil;
}

@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *home;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *back;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *forward;
@property (strong, nonatomic) NSMutableData *receivedData;

- (IBAction) goHome;

@end
