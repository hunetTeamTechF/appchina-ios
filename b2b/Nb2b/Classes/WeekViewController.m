//
//  WeekViewController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 7. 4..
//  Copyright 2011 hunet. All rights reserved.
//

#import "WeekViewController.h"
#import "MLCenterForBusinessAppDelegate.h"


@implementation WeekViewController


#pragma mark -
#pragma mark View lifecycle

- (BOOL)prefersStatusBarHidden {
    return NO;
}

/*
- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
*/

/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 7;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
	
	cell.accessoryType = UITableViewCellAccessoryNone;

    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
	switch (indexPath.row) {
		case 0:
		{			
			if ([[app.profileUser objectForKey:@"schedule_monday"] boolValue]) {
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			}
			cell.textLabel.text = @"월요일마다";					
		}
			break;
		case 1:
		{
			if ([[app.profileUser objectForKey:@"schedule_tuesday"] boolValue]) {
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			}
			cell.textLabel.text = @"화요일마다";					
		}
			break;
		case 2:
		{
			if ([[app.profileUser objectForKey:@"schedule_wednesday"] boolValue]) {
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			}
			cell.textLabel.text = @"수요일마다";					
		}
			break;
		case 3:
		{
			if ([[app.profileUser objectForKey:@"schedule_thursday"] boolValue]) {
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			}
			cell.textLabel.text = @"목요일마다";					
		}
			break;
		case 4:
		{
			if ([[app.profileUser objectForKey:@"schedule_friday"] boolValue]) {
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			}
			cell.textLabel.text = @"금요일마다";					
		}
			break;
		case 5:
		{
			if ([[app.profileUser objectForKey:@"schedule_saturday"] boolValue]) {
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			}
			cell.textLabel.text = @"토요일마다";					
		}
			break;
		case 6:
		{
			if ([[app.profileUser objectForKey:@"schedule_sunday"] boolValue]) {
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			}
			cell.textLabel.text = @"일요일마다";					
		}
			break;
		default:
			break;
	}
    
    // Configure the cell...
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	UITableViewCell *checkCell = [tableView cellForRowAtIndexPath:indexPath];
	
	BOOL isInsert;
	if (checkCell.accessoryType == UITableViewCellAccessoryCheckmark) {
		checkCell.accessoryType = UITableViewCellAccessoryNone;
		isInsert = NO;
	} else {
		checkCell.accessoryType = UITableViewCellAccessoryCheckmark;
		isInsert = YES;
	}      	
	
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
	NSString *key = nil;
	switch (indexPath.row) {
        default:
            break;
		case 0:
			key = @"schedule_monday";
			break;
		case 1:
			key = @"schedule_tuesday";
			break;
		case 2:
			key = @"schedule_wednesday";
			break;
		case 3:
			key = @"schedule_thursday";
			break;
		case 4:
			key = @"schedule_friday";
			break;
		case 5:
			key = @"schedule_saturday";
			break;
		case 6:
			key = @"schedule_sunday";
			break;
	}
	
	[app setNoticeOption:isInsert withKey:key];
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
    [super viewDidUnload];
}




@end

