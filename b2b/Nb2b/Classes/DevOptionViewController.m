

#import "DevOptionViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "Global.h"

@interface DevOptionViewController ()

@end

@implementation DevOptionViewController

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self != nil) {
        self.title = NSLocalizedString(@"개발자 옵션", @"개발자 옵션");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.lastTestHost = [[Global sharedSingleton] testPrimaryDomainFix];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryView = nil;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        cell.textLabel.text = @"test.mlc.hunet.co.kr(155)";
        
        self.stagingServerSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
        self.stagingServerSwitch.tag = 0;
        [self.stagingServerSwitch addTarget:self action:@selector(switchValueDidChange:) forControlEvents:UIControlEventValueChanged];

        NSString *testPrimaryDomain = [[Global sharedSingleton] testPrimaryDomain];
        self.stagingServerSwitch.on = [testPrimaryDomain rangeOfString:@"test."].location != NSNotFound;
        self.stagingServerSwitch.tag = @"test.";
        cell.accessoryView = self.stagingServerSwitch;
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryView = nil;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        cell.textLabel.text = @"testmlc.hunet.co.kr(156)";
        
        self.testServerSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
        self.testServerSwitch.tag = 0;
        [self.testServerSwitch addTarget:self action:@selector(switchValueDidChange:) forControlEvents:UIControlEventValueChanged];
        
        NSString *testPrimaryDomain = [[Global sharedSingleton] testPrimaryDomain];
        self.testServerSwitch.on = !self.stagingServerSwitch.on && [testPrimaryDomain rangeOfString:@"test"].location != NSNotFound;
        self.testServerSwitch.tag = @"test";
        cell.accessoryView = self.testServerSwitch;
        return cell;
    }
}


- (void)switchValueDidChange:(UISwitch *)sender
{
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] setNoticeOption:sender.isOn withKey:@"IsStaging"];
    //NSString *testHost = (NSString*)sender.tag;
    NSString *testHost = [NSString stringWithFormat:@"%d", sender.tag];

    if (self.stagingServerSwitch.on && self.testServerSwitch.on) {
        self.stagingServerSwitch.on = [testHost rangeOfString:@"test."].location != NSNotFound;
        self.testServerSwitch.on = !self.stagingServerSwitch.on;
    }
    
    if (self.stagingServerSwitch.on) {
        [Global sharedSingleton].testPrimaryDomainFix = @"test.";
    }
    else if (self.testServerSwitch.on) {
        [Global sharedSingleton].testPrimaryDomainFix = @"test";
    }
    else
    {
        [Global sharedSingleton].testPrimaryDomainFix = @"";
    }
    
    [self testPrimaryDomainChange];
}

- (void)testPrimaryDomainChange
{
    NSString *testPrimaryDomain = [[Global sharedSingleton] testPrimaryDomain];
    NSString *testPrimaryDomainFix = [[Global sharedSingleton] testPrimaryDomainFix];
    NSString *urlBase = [MLCenterForBusinessAppDelegate sharedAppDelegate].urlBase;
    NSString *urlSite = [MLCenterForBusinessAppDelegate sharedAppDelegate].urlSite;
    
    if ([testPrimaryDomain length] > 0)
    {
    
        [MLCenterForBusinessAppDelegate sharedAppDelegate].urlBase = [urlBase stringByReplacingOccurrencesOfString:@"http://mlc."
                                                                                                    withString:[NSString stringWithFormat:@"http://%@mlc.", testPrimaryDomainFix]];
    
        [MLCenterForBusinessAppDelegate sharedAppDelegate].urlSite = [urlSite stringByReplacingOccurrencesOfString:@"http://mlc."
                                                                                                      withString:[NSString stringWithFormat:@"http://%@mlc.", testPrimaryDomainFix]];
    }
    else
    {
        [MLCenterForBusinessAppDelegate sharedAppDelegate].urlBase = [urlBase stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"http://%@mlc.", self.lastTestHost]
                                                                                                        withString:@"http://mlc."];
        
        [MLCenterForBusinessAppDelegate sharedAppDelegate].urlSite = [urlSite stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"http://%@mlc.", self.lastTestHost]
                                                                                                       withString:@"http://mlc."];
    }
    
    self.lastTestHost = testPrimaryDomainFix;
}
@end
