
#import "PlayerCallFun.h"
#import "Util.h"
#import "CJSONDeserializer.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "MoviePlayerController.h"

@implementation PlayerCallFun
@synthesize delegateViewController;



- (void)sangsangmaruInit:(NSArray*)array
{
    NSString *action = [array objectAtIndex:0];
    NSString *url = [array objectAtIndex:1];

    if ([action isEqualToString:@"sangsangdrmplayer"])
    {
        [[MLCenterForBusinessAppDelegate sharedAppDelegate] maskLoading];
    }
    else if ([action isEqualToString:@"sangsangdownload"] ) {
        [[MLCenterForBusinessAppDelegate sharedAppDelegate] drmloadingView:delegateViewController];
    }
    else
    {
        [self sangsangmaruDownloadPush:url];
        return;
    }
    
    [NSThread detachNewThreadSelector:@selector(sangsangmaruDrm:) toTarget:self withObject:array];
}
//상상마루
- (void)sangsangmaruDrm:(NSArray*)array
{
    //NSLog(@"url : %@", [array objectAtIndex:1]);
    NSString *action = [array objectAtIndex:0];
    NSString *url2 = [array objectAtIndex:1];
    NSArray *pairs = [url2 componentsSeparatedByString:@"/"];
    NSString *cno = [pairs objectAtIndex:0];
    NSString *cseq = [pairs objectAtIndex:1];
    NSString *gid = [pairs objectAtIndex:2];
    NSString *seGoodsId;
    if ([pairs count]>3) {
        seGoodsId =[pairs objectAtIndex:3];
    }
    else{
        seGoodsId = gid;
    }
    
    //NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
    
    NSError *errorReq = nil;
    NSString* jsonParam;
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    if ([action isEqualToString:@"sangsangdrmplayer"]) {
        jsonParam = [NSString stringWithFormat:@"type=43&company_seq=%@&contract_no=%@&uid=%@&contents_seq=%@&goods_id=%@", app.companySeq, cno, [Global getUserId], cseq, gid];
    }
    else
        if ([action isEqualToString:@"sangsangdownload"]){
            jsonParam = [NSString stringWithFormat:@"type=43&company_seq=%@&contract_no=%@&uid=%@&contents_seq=%@&goods_id=%@&drm_yn=Y&se_goods_id=%@", app.companySeq, cno, [Global getUserId], cseq, gid, seGoodsId];
        }
        else {
            [self sangsangmaruDownloadPush:url2];
            return;
        }
    
    NSString *urls = [NSString stringWithFormat:@"%@%@",  [MLCenterForBusinessAppDelegate sharedAppDelegate].urlSite,@"JLog"];
    NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:urls]
                                          andQuery:jsonParam
                                          andError:&errorReq];
    
//    NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:@"http://172.20.80.100:5555/Jlog"]
//                                          andQuery:jsonParam
//                                          andError:&errorReq];
    
    NSLog(@"%@?%@",urls,jsonParam);
    //NSLog(@"http://172.20.80.100:5555/JLog?%@",jsonParam);
    //NSLog(@"%@?takecourseSeq=%@&indexNo=%@", [[Global sharedSingleton] _downloadInfoUrl], [pairs objectAtIndex:0], [pairs objectAtIndex:1]);
    //http://mlc.hunet.co.kr/App/JLog.aspx?type=43&company_seq=3796&contract_no=3&uid=dw213276&contents_seq=9278&goods_id=y00012573
    //http://mlc.hunet.co.kr/App/JLog.aspx?type=43&company_seq=3796&contract_no=3&uid=dw213276&contents_seq=9278&goods_id=y00012573
	
    
    
    if (jsonData)
    {
        
        NSError *errorJSON = nil;
        NSMutableDictionary *result = [[[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON] mutableCopy];
        
        NSString *tempOrderId = [NSString stringWithFormat:@"%i", (int)[result objectForKey:@"order_id"]];
        
        if ([tempOrderId isEqual :@"50529045"]) {
            cseq = @"0";
        }
        
        if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {
            
            if ([action isEqualToString:@"sangsangdrmplayer"]) {
                [self performSelectorOnMainThread:@selector(sangsangmaruDrmStreaming:) withObject:[NSArray arrayWithObjects:result,
                                                                                           cno,
                                                                                           cseq,
                                                                                           gid,
                                                                                           [result objectForKey:@"view_no"],
                                                                                           nil] waitUntilDone:YES];
            } else {
                [self performSelectorOnMainThread:@selector(sangsangmaruDrmDownload:) withObject:result waitUntilDone:YES];
            }
		}
    }
    
    //[pool release];
}




- (void)sangsangmaruDrmDownload:(NSDictionary*)result
{
    NSString *mp4Url = @"";
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    if (result != nil) {
        mp4Url = [NSString stringWithFormat:@"%@?%@", [result objectForKey:@"url"], [Global getUserId]];
        NSUserDefaults *userdeDefaults = [NSUserDefaults standardUserDefaults];
        [userdeDefaults setObject:mp4Url forKey:FILENAME];
    }

    if (webUtil != nil) {
        webUtil = nil;
    }
    webUtil = [[Web alloc] init];
    [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.webUtil = webUtil;
    [MLCenterForBusinessAppDelegate sharedAppDelegate].loading.webUtil.downloadInfo = result;
    [[MLCenterForBusinessAppDelegate sharedAppDelegate].loading.webUtil requestDownLoad:mp4Url];
}

//상상마루 다운로드
- (void)sangsangmaruDownloadPush:(NSString*)param {

    NSArray *pairs = [param componentsSeparatedByString:@"/"];
    NSInteger contractNo = [[pairs objectAtIndex:0] intValue];
    NSInteger contentsSeq = [[pairs objectAtIndex:1] intValue];
    NSString *goodsId = [pairs objectAtIndex:2];
    NSString *seGoodsId = [pairs objectAtIndex:3];
    
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];

    
    NSArray *tmp = [Util getSangStudy:goodsId andContentsSeq:[pairs objectAtIndex:1] andUserId:[Global getUserId]];
    if (tmp.count <= 0) {
        if ([app isWiFi]) {
            [[MLCenterForBusinessAppDelegate sharedAppDelegate] drmloadingView:delegateViewController];
        }
        
        [NSThread detachNewThreadSelector:@selector(sangsangmaruDrm:) toTarget:self withObject:[NSArray arrayWithObjects:@"sangsangdownload", param, nil]];
    }
    else
    {
        NSManagedObject *tempObject = [tmp objectAtIndex:0];
        NSInteger expirationDay = [Util expirationDay:[tempObject valueForKey:@"study_end_date"]];
        
        //[app setRootViewSangDownloadCenter:goodsId andContractNo:contractNo andContentsSeq:contentsSeq andSeGoodsId:seGoodsId andExpirationDay:expirationDay];
         [app sangStudyDownload:goodsId andContractNo:contractNo andContentsSeq:contentsSeq andSeGoodsId:seGoodsId andExpirationDay:expirationDay];
    }
    
}

//상상마루
- (void)sangsangmaruDrmStreaming:(NSArray*)array
{
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] removemaskLoading];
    NSDictionary *result = [array objectAtIndex:0];
    NSString *cno = [array objectAtIndex:1];
    NSString *cseq = [array objectAtIndex:2];
    //NSString *gid = [array objectAtIndex:3];
    //NSString *viewNo = [array objectAtIndex:4];
    
    NSUserDefaults *userdeDefaults = [NSUserDefaults standardUserDefaults];
    [userdeDefaults setObject:[result objectForKey:@"url"] forKey:FILENAME];
    NSLog(@"Streaming filename = %@", [userdeDefaults objectForKey:FILENAME]);
    
    MoviePlayerController * movieCon = [[MoviePlayerController alloc] initWithNibName:@"MoviePlayerController" bundle:nil];
    movieCon.delegate = (id)delegateViewController;
    movieCon.isStreamingPlay = TRUE;
    movieCon.currentSecond = [[result objectForKey:@"last_view_sec"] intValue];
    
    NSString *pass_yn = [result objectForKey:@"pass_yn"];
    
    movieCon.scrollType = [pass_yn isEqualToString:@"Y"] ? 1 : 0;   // progress touch 여부 0 : 진도바 이동 불가, 1 : 진도바 이동 가능
    
    movieCon.cno = [cno intValue];
    movieCon.cseq = [cseq intValue];
    movieCon.gid = [result objectForKey:@"goods_id"];
    movieCon.viewSec = [[result objectForKey:@"view_sec"] intValue];
    movieCon.lastViewSec = [[result objectForKey:@"last_view_sec"] intValue];
    movieCon.viewSecMobile = [[result objectForKey:@"view_sec_mobile"] intValue];
    movieCon.viewSec = [[result objectForKey:@"view_sec"] intValue];
    movieCon.passYn = pass_yn;
    movieCon.quizYn = [result objectForKey:@"quiz_yn"];
    movieCon.url = [result objectForKey:@"url"];
    movieCon.pptImageUrl = [result objectForKey:@"ppt_image_url"];
    movieCon.viewNo = [[result objectForKey:@"view_no"] intValue];
    movieCon.userId = [result objectForKey:@"user_id"];
    movieCon.movieTitle = [result objectForKey:@"contents_nm"];
    movieCon.catption_yn =[result objectForKey:@"caption_yn"];
    movieCon.catption_cn =[result objectForKey:@"caption_cn"];
    movieCon.catption_en =[result objectForKey:@"caption_en"];
    movieCon.catption_kr =[result objectForKey:@"caption_kr"];
    
    
    [UIApplication sharedApplication].statusBarHidden = YES;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight animated:NO];
    
    [MLCenterForBusinessAppDelegate sharedAppDelegate].moviePlayerController = movieCon;
    [delegateViewController presentViewController:movieCon animated:YES completion:nil];
    
}


@end
