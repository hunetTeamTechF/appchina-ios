//
//  HLeaderController.h
//  MLCenterForBusiness
//
//  Created by Park Gi-Bok on 12. 1. 28..
//  Copyright (c) 2012 hunet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DownloadingView.h"
#import "PlayerCallFun.h"
#import "MoviePlayerController.h"
#import "web.h"

@interface HLeaderController : UIViewController <UIWebViewDelegate, UITabBarDelegate> {
	NSString *url;
	IBOutlet UIWebView *viewWeb;
	IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UITabBar *tabbar;
    
    Web *webUtil;
}

@property (nonatomic, strong) NSString *url;

- (void) goHome;

@end
