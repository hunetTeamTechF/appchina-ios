//
//  WebRequestUtil.h
//  MLCenterForBusiness
//
//  Created by hunet-mac-npc92 on 2/7/15.
//
//

#import <Foundation/Foundation.h>

@interface UIViewController (RequestProtocol)
@property (atomic, strong) UIWebView *webView;
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;
@end
