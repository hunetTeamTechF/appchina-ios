//
//  GraphicsUtile.m
//  MLCenterForBusiness
//
//  Created by Park SeungKyun on 2015. 7. 24..
//
//

#import "GraphicsUtile.h"
#import "Header.h"

@implementation GraphicsUtile

/* 네비게이션바 색상 & 배경이미지 설정 */
+ (void)setNavigationBarColor:(UIColor *)barColor
{
    if (IS_DEVICE_RUNNING_IOS_7_AND_ABOVE() == NO) {
        return;
    }
    
    [[UINavigationBar appearance] setBackgroundImage:[self imageWithColor:[UIImage imageNamed:@"navigation_bg.png"] color:barColor whiteSpaceHeight:20.0] forBarMetrics:UIBarMetricsDefault];
    [UINavigationBar appearance].tintColor = [UIColor whiteColor];
    [UINavigationBar appearance].titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
}

/* 탭바 색상 설정 */
+ (void)setTabBarColor:(UITabBar *)tabBar barColor:(UIColor *)barColor selectedIconColor:(UIColor *)selectedIconColor unselectedIconColor:(UIColor *)unselectedIconColor
{
    if (IS_DEVICE_RUNNING_IOS_7_AND_ABOVE() == NO) {
        return;
    }
    
    if (IS_DEVICE_RUNNING_IOS_7_AND_ABOVE()) {
        tabBar.barTintColor = barColor;
        
        if (IS_DEVICE_RUNNING_IOS_8_AND_ABOVE()) {
            tabBar.translucent = false;
        }
    }
    else {
        [tabBar setBackgroundImage:[self imageWithColor:[UIImage imageNamed:@"tabbar_bg.png"] color:barColor whiteSpaceHeight:0.0]];
    }
    
    
    
#if avene || hyundai || kcc || hyundaiDymos || hyundaiWia || hyundaiHmtc || Wiajs || Wiash || dymosrz
    UIImage *selTab = [[UIImage imageNamed:@"selected_tab_bg.png"] imageWithRenderingMode:
                       UIImageRenderingModeAlwaysOriginal];
    
    UIGraphicsBeginImageContext(tabBar.frame.size);
    float a =tabBar.frame.size.width;
    float b = tabBar.frame.size.height;
    [selTab drawInRect:CGRectMake(120, 0, tabBar.frame.size.width/4, tabBar.frame.size.height)];
    UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [tabBar setSelectionIndicatorImage:reSizeImage];
#endif
    
    
    
    // set the selected colors
    [tabBar setTintColor:selectedIconColor];
    
    
    
    
#if avene || kcc
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Helvetica" size:13.0], UITextAttributeFont,selectedIconColor, NSForegroundColorAttributeName,nil] forState:UIControlStateSelected];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIFont fontWithName:@"Helvetica" size:13.0], UITextAttributeFont,unselectedIconColor, NSForegroundColorAttributeName, nil]
                                             forState:UIControlStateNormal];
    
#else
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       selectedIconColor, NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       unselectedIconColor, NSForegroundColorAttributeName, nil]
                                             forState:UIControlStateNormal];
#endif
    
    
    
    // set color of unselected text  [UIFont fontWithName:@"Helvetica" size:18.0], UITextAttributeFont, nil]
    
    
    //    [tabBar setTintColor:selectedIconColor];
    //    [[UITabBarItem appearance]setTitleTextAttributes:@{NSFontAttributeName:[UIFont   systemFontOfSize:13],NSForegroundColorAttributeName:unselectedIconColor}   forState:UIControlStateNormal];
    //
    //    [[UITabBarItem appearance]setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13],NSForegroundColorAttributeName:selectedIconColor} forState:UIControlStateSelected];
    //
    // generate a tinted unselected image based on image passed via the storyboard
    for(UITabBarItem *item in tabBar.items) {
        // use the UIImage category code for the imageWithColor: method
        item.image = [[self imageWithColor:item.selectedImage color:unselectedIconColor whiteSpaceHeight:0.0] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
}

/* 아이콘 색상 설정 */
+ (UIImage *)imageWithColor:(UIImage *)image color:(UIColor *)color whiteSpaceHeight:(CGFloat)whiteSpaceHeight
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    CGContextClipToMask(context, rect, image.CGImage);
    [color setFill];
    CGContextFillRect(context, rect);
    
    CGContextSetRGBFillColor(context, 1, 1, 1, 1.0);
    CGContextSetRGBStrokeColor(context, 0, 0, 0, 0.0);
    CGRect ellipse = CGRectMake(0, (image.size.height * image.scale - whiteSpaceHeight), image.size.width, whiteSpaceHeight);
    CGContextAddRect(context, ellipse);
    CGContextFillRect(context, ellipse);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (UIImage *)imageWithScaling:(NSString*)imageName {
    #if hyundai || kia || hyundaiDymos || hyundaiWia || hyundaiHmtc || Wiajs || Wiash || dymosrz || hmgc
        return [self imageWithScaling:imageName scale:2.5];
    #elif kcc
        return [self imageWithScaling:imageName scale:100];
    #else
        return [self imageWithScaling:imageName scale:1.7];
    #endif
    
}

+ (UIImage *)imageWithScaling:(NSString*)imageName scale:(float)scale {
    UIImage *originalImage = [UIImage imageNamed:imageName];
    UIImage *scaledImage =
    [UIImage imageWithCGImage:[originalImage CGImage]
                        scale:(originalImage.scale * scale)
                  orientation:(originalImage.imageOrientation)];
    
    return scaledImage;
}

@end
