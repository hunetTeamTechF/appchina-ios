
#import <UIKit/UIKit.h>
#import "MLCenterForBusinessAppDelegate.h"
#import "URLAction.h"

@interface LoginViewController : UIViewController <UIWebViewDelegate> {
    NSURL *loginUrl;
    UIView *mask;
    BOOL isLoadedHtmlLayout;
    MLCenterForBusinessAppDelegate* app;
    URLAction *urlAction;
}

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (void) onWebLogin: (NSString *) userId : (NSString *) userPwd;
- (void) onWebLogin: (NSString *) userId : (NSString *) userPwd :(NSString*)companySeq;
- (void)setLoadingViewHidden:(BOOL)hidden;

@end
