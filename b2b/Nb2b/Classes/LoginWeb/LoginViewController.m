
#import "LoginViewController.h"
#import "MoreViewController.h"
#import "DownloadViewController.h"
#import "ModalAlert.h"
#import "Header.h"
#import "DownPicker.h"
#import "LectureViewController.h"
#import "WebModalViewController.h"
#import "Reachability.h"
#import "Global.h"

@implementation LoginViewController

// plist 파일에서 설정값을 불러온다.
- (void)checkPlistSetting {
    
    // CompanySequence
    NSString *companySeqInPlist = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CompanySequence"];
    // 기업 설정
    app.info_company_seq = companySeqInPlist;
    
    if(companySeqInPlist == nil || [companySeqInPlist length] == 0){
        UILabel *label = [[UILabel alloc] initWithFrame:(CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height))];
        [label setText:@"info탭의 CompanySequence를 확인해주세요"];
        [self.view addSubview:label];
        return;
    }
    
    // CompanyCheckVersionOnLogin
    NSNumber *versionCheckLogin = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CompanyCheckVersionOnLogin"];
    if([versionCheckLogin boolValue]){
        [[MLCenterForBusinessAppDelegate sharedAppDelegate] checkWithUpdateFrom:self];
    }
    
    // PublishStagingMode
    NSNumber *publishStagingMode = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"PublishStagingMode"];
    if([publishStagingMode boolValue]){
        // IsStagingTestNoDot로 시작함
        [[MLCenterForBusinessAppDelegate sharedAppDelegate] setNoticeOption:YES withKey:@"IsStagingTestNoDot"];
        [[MLCenterForBusinessAppDelegate sharedAppDelegate] testPrimaryDomainChange];
    }
    
    NSString *loginUrlString = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"LoginUrl"];
    if(loginUrlString == nil){
        loginUrlString = [NSString stringWithFormat:@"mlc.hunet.co.kr/areas/%@/login.html", app.info_company_seq];
    }
    
    // 로그인 페이지 URL
    // Staging (testmlc)
    if ([[[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"IsStagingTestNoDot"] boolValue]){
        loginUrlString = [NSString stringWithFormat:@"test%@", loginUrlString];
    }
    // Staging (test.mlc)
    else if ([[[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"IsStaging"] boolValue]){
        loginUrlString = [NSString stringWithFormat:@"test.%@", loginUrlString];
    }
    
    loginUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", loginUrlString]];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    urlAction = [[URLAction alloc] init:self];
    
    [self checkPlistSetting];
    
    loginUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"login" ofType:@"html"]];
    
    // 온/오프 변경시 알림 설정
    
    [self registerNotificationForReachability];
    
    isLoadedHtmlLayout = false;
    self.webView.delegate = self;
    
    NSLog(@"LoginView Loaded.");
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"viewWillAppear");
    
    
    [self loadUrl:loginUrl];
}

- (void)loadUrl:(NSURL *)url {
    [self.webView loadRequest:[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30.0]];
}

#pragma mark - Custom Functions (Reachability)

- (void)checkReachabilityForOffline:(Reachability*)curReach {
    if (curReach == nil) {
        return;
    }
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSLog(@"NetworkStatus : %d" , netStatus);
    
    switch (netStatus) {
        case NotReachable: {
            break;
        }
        // check the wifionly
        case ReachableViaWWAN:
        case ReachableViaWiFi: {
            if (isLoadedHtmlLayout == NO) {
                [self loadUrl:loginUrl];
            }
            break;
        }
    }
}


- (void)requestPushToken{
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:@"pushAlertAlreadyShown"];
    
    // iOS 8.0 이상
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        // use registerUserNotificationSettings
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    } else {
        // use registerForRemoteNotificationTypes:
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
}


- (void)registerNotificationForReachability {
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(reachabilityChangedForOffline:)
                                                 name: kReachabilityChangedNotification
                                               object: nil];
}

- (void)reachabilityChangedForOffline: (NSNotification*)note {
    Reachability* curReach = [note object];
    [self checkReachabilityForOffline:curReach];
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    isLoadedHtmlLayout = true;
    [self.indicator stopAnimating];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
    [self.indicator startAnimating];
}

- (void) webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
    [self.indicator stopAnimating];
    isLoadedHtmlLayout = false;
    if ([[MLCenterForBusinessAppDelegate sharedAppDelegate] isNotInternet]) {
        
    }
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSURL *url = request.URL;
    NSString *urlString = url.absoluteString;

//    NSLog(@"base : %@", urlString);

    BOOL blContainAction = [urlAction checkUrlAction:webView andRequest:request];
    if(blContainAction == NO)
        return NO;

    return YES;

}

- (void)onWebLogin:(NSString *)userId :(NSString *)userPwd
{
    [self onWebLogin:userId :userPwd :@"0"];
}

- (void)onWebLogin:(NSString *)userId :(NSString *)userPwd :(NSString*)companySeq{
    BOOL result = YES;
    @autoreleasepool {
        
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        result = [app loginForID:userId withPassword:userPwd withCompanySeq:companySeq];
        [self performSelectorOnMainThread:@selector(callbackAfterLogin:) withObject:[NSNumber numberWithBool:result] waitUntilDone:NO];
    }
}

// callback for the login result
- (void) callbackAfterLogin:(NSNumber*)resultLogin {
    
    BOOL result = [resultLogin boolValue];
    
    [mask removeFromSuperview];
    [self.indicator stopAnimating];
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    if (result) {
        //[self.view removeFromSuperview];
        [app profileUserInfoSave];
        [app openTrainingInstitute];
    }
    else {
        if ([app isNotInternet]) {
            [ModalAlert notify:@"网络状态不稳定！.请先确认网络连接状态."];
        }
        else {
            [ModalAlert notify:@"登陆失败！\n请输入正确的用户信息！"];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL) shouldAutorotate {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)offlineClick:(id)sender {
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] offlineStudy];
}

- (void)setLoadingViewHidden:(BOOL)hidden {
    if (hidden) {
        [mask removeFromSuperview];
        [self.indicator stopAnimating];
    }
    else {
        mask = [[UIView alloc] initWithFrame:[[self.view window] frame]];
        mask.userInteractionEnabled = NO;
        mask.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.50];
        [self.view addSubview:mask];
        [self.indicator startAnimating];
    }
}

@end
