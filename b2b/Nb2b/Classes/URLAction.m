//
//  URLAction.m
//  MLCenterForBusiness
//
//  Created by KwonOh june on 2017. 2. 14..
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "URLAction.h"
#import "ELCImagePickerController.h"
#import "ELCAlbumPickerController.h"
#import "ELCAssetTablePicker.h"
#import "HLeaderController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "WebModalViewController.h"
#import "MP3PlayerController.h"
#import "UserViewController.h"
#import "MoreViewController.h"
#import "DownloadViewController.h"
#import "MyViewController.h"
#import "SelfStudyViewController.h"
#import "LectureViewController.h"
#import "NoticeViewController.h"
#import "QnaViewController.h"
#import "SSViewController.h"
#import "HomeViewController.h"
#import "WebtoonViewController.h"
#import "HTMLPlayerController.h"
#import "CJSONDeserializer.h"
#import "Util.h"

#define FILE_TYPE_TAKE_PICTURE  0
#define FILE_TYPE_IMAGE         1
#define FILE_TYPE_MEDIA         2

@implementation URLAction
@synthesize playerCallFun = _playerCallFun;

- (id)init:(UIViewController *)parentViewController {
    _attachedViewController = parentViewController;
    parentWebView = nil;
    
    
    
    return [self init];
}


/*
 커스텀 프로토콜 및 탭변경 동작모음
 
 @param webView 동작이 일어난 웹뷰
 @param request 그 웹뷰의 요청 NSURLRequest
 
 @return 웹뷰에서 해당 동작을 실행시킬지 말지 여부
 */
- (BOOL)checkUrlAction:(UIWebView *)webView andRequest:(NSURLRequest *)request {
    
    parentWebView = webView;
    
    NSString *requestStr = [[request URL] absoluteString].lowercaseString;
    NSLog(@"checkUrlAction = %@", requestStr);
    
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    NSURL *url = request.URL;
    
    NSString *urlActionString = url.absoluteString.lowercaseString; // 소문자 변환 URL
    NSString *urlString = url.absoluteString;                       // 원본 URL
    
    NSLog(@"urlString, app.eduType, %@, %@", urlString, app.eduType);
    
    if(urlString == nil)
        return YES;
    
    
    /*
     1) Custom protocol 처리
     탭체인지
     모달화면 클로즈
     앱투앱 전환
     클립보드 복사
     웹뷰 히스토리백
     사이트 이동(외부 브라우저로 - 사파리로)
     앱스토어 이동
     모달 팝업
     로그아웃 처리
     행복센터 이동 등등
     */
    
    /*
     탭 체인지 프로토콜
     app://changetab?type=타입&url=주소    탭 변경. (type=[home | lecture | sangsang | classroom | download | config])
     */
    if([app handleAppProtocol:url withWebView:webView]) {
        return NO;
    }
    
    /*
     모달(팝업) 화면 닫는 프로토콜
     app://closeAllModal?type=타입&url=주소	탭 변경. (type=[home | lecture | sangsang | classroom | download | config])
     */
//    else if ([app closeAllModal:_attachedViewController changeTabUrl:url]) {
//        return NO;
//    }
    
    /*
     app to app 전환
     intent://#intent;scheme=mplus;package=com.neungyule.nelearn;iosscheme=nelearn;iosinstall=https://itunes.apple.com/kr/app/ne-learn/id584190802?mt=8;end
     */
    else if ([app intentOpenUrl:urlString]) {
        return NO;
    }
    
    // app://clipboard만 있는 줄 알았는데 clipboard:// 가 또 생길줄이야 추가함 2018.03.30
    else if([urlActionString hasPrefix:@"clipboard://"]) {
        NSString *clipboardText = [urlActionString stringByReplacingOccurrencesOfString:@"clipboard://" withString:@""];
        clipboardText = [clipboardText stringByRemovingPercentEncoding];
        [[UIPasteboard generalPasteboard] setString:clipboardText];
        return NO;
    }
    
    /*
     웹뷰 히스토리백
     */
    if ([urlActionString hasPrefix:@"back://"]) {
        
        UINavigationController *controller = [_attachedViewController navigationController];
        UITabBarController *tabBarController = [_attachedViewController tabBarController];
        
        if([webView canGoBack])
            [webView goBack];
        else
            if(controller != nil){
                [controller popViewControllerAnimated:YES];
            }
            else{
                [_attachedViewController dismissViewControllerAnimated:YES completion:nil];
            }
    }
    
    /*
     외부 사이트로 이동
     */
    if ([urlActionString hasPrefix:@"escape://"] || [urlActionString hasPrefix:@"escapes://"]) {
        NSString *escapeUrl = [urlString stringByReplacingOccurrencesOfString:@"escape://" withString:@"http://"];
        escapeUrl = [escapeUrl stringByReplacingOccurrencesOfString:@"escapes://" withString:@"https://"];
        //        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:escapeUrl]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:escapeUrl] options:[[NSMutableDictionary alloc] init] completionHandler:nil];
        
        return NO;
    }
    
    /*
     앱스토어 이동
     */
    if ([urlActionString hasPrefix:@"appstore://"]) {
        NSString *escapeUrl = [urlString stringByReplacingOccurrencesOfString:@"appstore://" withString:@""];
        escapeUrl = [escapeUrl stringByReplacingOccurrencesOfString:@"https//" withString:@"https://"];
        escapeUrl = [escapeUrl stringByReplacingOccurrencesOfString:@"http//" withString:@"http://"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:escapeUrl] options:[[NSMutableDictionary alloc] init] completionHandler:nil];
        return NO;
    }
    
    
    /*
     모달 팝업
     */
    if([urlActionString hasPrefix:@"modal://"]){
        if([_attachedViewController isKindOfClass:[MyViewController class]] == NO){
            
            NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:request.URL resolvingAgainstBaseURL:NO];
            NSArray *queryItems = urlComponents.queryItems;
            
            //NSString *urlParam = [Util valueForKey:@"url" fromQueryItems:queryItems];
            NSString *urlParam = [Util valueForKey:@"url" fromQuery:request.URL.query];
            NSString *urlParamDecoded = (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (CFStringRef)urlParam, CFSTR(""), kCFStringEncodingUTF8));
            
            NSURLQueryItem *queryItem = [queryItems objectAtIndex:0];
            NSLog(@"urlParamDecoded : %@", urlParamDecoded);
            
            //?url query parameter가 없는 경우,
            if (urlParam == nil) {
                WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
                webModal.webViewUrl = [urlString stringByReplacingOccurrencesOfString:@"modal://" withString:@"http://"];
                //webModal.title = @"";
                [_attachedViewController presentViewController:webModal animated:YES completion:nil];
            }
            //query에 url parameter가 있으면 해당 url로 modal 팝업
            else {
                
                NSURL *urlParsed = [NSURL URLWithString:[[urlParamDecoded componentsSeparatedByString:@"?"] firstObject]];
                NSString *method = [Util valueForKey:@"method" fromQueryItems:queryItems];
                NSString *title = [Util valueForKey:@"title" fromQueryItems:queryItems];
                
                //post parameter가 있으면 url을 파싱해서, queryString은 post로 전송
                if ([method.lowercaseString isEqualToString:@"post"])
                {
                    WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
                    
                    //webViewRequestMethod
                    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:urlParsed
                                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                       timeoutInterval:20.0];
                    [request setHTTPMethod:@"POST"];
                    NSString *bodyString = (NSString *)[[urlParamDecoded componentsSeparatedByString:@"?"] lastObject];
                    [request setHTTPBody:[bodyString dataUsingEncoding:NSUTF8StringEncoding]];
                    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                    webModal.title = title;
                    [_attachedViewController presentViewController:webModal animated:YES completion:nil];
                    [webModal.WebView loadRequest:request];
                }
                //query에 url parameter가 없으면
                else {
                    WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
                    webModal.webViewUrl = urlParamDecoded;
                    webModal.title = title;
                    [_attachedViewController presentViewController:webModal animated:YES completion:nil];
                }
                
            }
            
            return NO;
            
            
        }
        // MyViewController 에서 모달팝업 띄울 때 처리 .... 사실 이게 왜 필요한진 모르겠지만 어디서 쓰이는지 알 수 없어서 붙여놓는다.
        else {
            
            //모달 창으로 뛰우기
            NSDictionary *simpleModalUrlDic = [NSDictionary  dictionaryWithObjectsAndKeys:@"알파코", @"://www.alpaco.co.kr", nil];
            
            NSString *simpleModalPopupTitle = nil;
            NSString *refreshFg = @"N";
            NSString *studyFg = @"N";
            
            for (NSString *domainKey in simpleModalUrlDic) {
                
                if ([urlString.lowercaseString rangeOfString:domainKey].location != NSNotFound) {
                    simpleModalPopupTitle = [simpleModalUrlDic objectForKey: domainKey];
                    break;
                }
            }
            
            NSString *webModalUrl = request.URL.absoluteString;
            
            if ([webModalUrl hasPrefix:@"modal://?"]) {
                
                // getProtocalParams 로 가져올 때 key값은 전부 lowercase로 변경하더라...
                NSDictionary *params = [MLCenterForBusinessAppDelegate getProtocalParams:request.URL];
                webModalUrl = [params valueForKey:@"url"];
                simpleModalPopupTitle = [params valueForKey:@"title"];
                refreshFg = [params valueForKey:@"refreshyn"] == nil ? @"N" : [params valueForKey:@"refreshyn"];
                studyFg = [params valueForKey:@"studyfg"] == nil ? @"N" : [params valueForKey:@"studyfg"];
                
                // 모달창으로 학습하는 과정인데, Wifi/4G 체크 해보고 아니면 리턴
                if ([[studyFg uppercaseString] isEqualToString:@"y"] == NO && [app moviePlayCheck] == NO) {
                    return NO;
                }
                
            }
            else{
                webModalUrl = [webModalUrl stringByReplacingOccurrencesOfString:@"modal://" withString:@"http://"];
            }
            
            WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
            webModal.webViewUrl = webModalUrl;
            webModal.title = simpleModalPopupTitle;
            [_attachedViewController presentViewController:webModal animated:YES completion:nil];
            
            return NO;
            
        }
    }
    
    
    /*
     웹 로그아웃 처리
     */
    if([urlActionString hasPrefix:@"logout://"])
    {
        app.flagLogin = NO;
        [Global setUserPassword:@""];
        [app profileUserInfoSave];
        [app loginViewControllerDisplay];
        return NO;
    }
    
    // 행복센터 이동
    if ([urlActionString hasPrefix:@"customercenter://"]) {
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Customer" ofType:@"html"]];
        NSLog(@"url : %@", url);
        [webView loadRequest:[NSURLRequest requestWithURL:url]];
        return NO;
    }
    
    //apptoapp://?schemeurl=&storeurl=
    //apptoapp://?schemeurl=hunetceo%3A%2F%2F%3Fuserid%3Daaa%26userpw%3Dbbb&storeurl=http%3A%2F%2Fapps.hunet.co.kr%2Fdown%2Fhunetceo.htm
    if ([urlActionString hasPrefix:@"apptoapp://?"]) {
        NSDictionary *protocalParams = [MLCenterForBusinessAppDelegate getProtocalParams:request.URL];
        NSString *schemeurl = [protocalParams objectForKey:@"schemeurl"];
        NSString *storeurl = [protocalParams objectForKey:@"storeurl"];
        BOOL isInstalled = [[UIApplication sharedApplication] openURL:[NSURL URLWithString:schemeurl]];
        if (!isInstalled) {
            // 설치 되어 있지 않습니다! 앱스토어로 안내...
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:storeurl]];
        }
        return NO;
    }
    
    
    if([urlActionString hasPrefix:@"jscall://"])
    {
        NSString *requestString = [[request URL] absoluteString];
        NSArray *components = [requestString componentsSeparatedByString:@"://"];
        NSString *functionName = [[[components objectAtIndex:1] componentsSeparatedByString:@"?"] objectAtIndex:0];
        NSNumber *fileSelectCount = [NSNumber numberWithInt:[[Util valueForKey:@"count" fromQuery:[request URL].query] intValue]];
        NSString *folderPath = [Util valueForKey:@"pathFolderType" fromQuery:[request URL].query];
        if(folderPath == nil)
            folderPath = @"Community"; // 디폴트는 community
        
        if ([functionName.lowercaseString isEqualToString:@"callphotoview"]) {
            [self performSelector:@selector(callPhotoView:) withObject:@{@"count": fileSelectCount, @"path":folderPath}];
            
        } else {
            SEL selector = NSSelectorFromString(functionName);
            IMP imp = [self methodForSelector:selector];
            void (*func)(id, SEL) = (void *)imp;
            func(self, selector);
        }
        
        
        return NO;
    }
    
    
    /*
     2) 탭 이동
     HomeViewController             - 홈화면 탭
     LectureViewController          - 교육과정 탭
     SelfStudyViewController        - 상상마루 탭
     KnowledgeLiveViewController    - 지식라이브 탭 ...?
     MyViewController               - 나의강의실 탭
     DownloadViewController         - 다운로드센터 탭
     MoreViewController             - 설정탭
     */
    
    /*
     홈 탭으로 이동 - Home/Home일 경우에 0번째 탭으로 이동
     */
    if([urlActionString containsString:@"/home/home"]){
        BOOL retValue = [self.attachedViewController isKindOfClass:[HomeViewController class]] ? YES : NO;
        int lectureTabIndex = [self getTabIndexOfClass:[HomeViewController class]];
        if(lectureTabIndex != -1)
            _attachedViewController.tabBarController.selectedIndex = lectureTabIndex;
        return retValue;
    }
    
    
    /*
     교육과정 탭으로 이동
     /lecture
     + /category_inmun  - 행복한인문학당        type=7
     + /category        - 주제별 교육과정       type=5
     + /depth           - 주제별 교육과정 상세   type=6
     */
    if ([urlActionString rangeOfString:@"/lecture/"].location != NSNotFound){
        
        BOOL retValue = [self.attachedViewController isKindOfClass:[LectureViewController class]] ? NO : YES;
        
        int lectureTabIndex = [self getTabIndexOfClass:[LectureViewController class]];
        
        // 탭이 없는 경우 호출한 화면에서 가게 만든다.
        if(lectureTabIndex == -1)
            return retValue;
        
        //행복한 인문학당 2.0 - type=7
        if ([urlActionString rangeOfString:@"/category_inmun.aspx"].location != NSNotFound){
            [app setLectureUrlType:@"7"];
            [app setSelectedTabIndex:lectureTabIndex];
            return retValue;
        }
        
        // 교육과정 - type=6
        if ([urlActionString rangeOfString:@"/category.aspx"].location != NSNotFound){
            NSArray * param = [urlString componentsSeparatedByString:@"="];
            [app setSelectedTabIndex:lectureTabIndex];
            LectureViewController *view = (LectureViewController*)_attachedViewController.tabBarController.selectedViewController;
            view._currentUrl = @"";
            view._param = [param objectAtIndex:1];
            [_attachedViewController.tabBarController.selectedViewController viewWillAppear:YES];
            return retValue;
        }
        
        //주제별 교육과정 상세
        if ([urlActionString rangeOfString:@"/depth.aspx"].location != NSNotFound){
            if ([[Global getUserId] hasPrefix:@"childfund-"] == NO)
                return NO;
            
            [app setLectureUrlType:@"6"];
            [app setSelectedTabIndex:lectureTabIndex indexOrUrl:urlString];
            return NO;
        }
    }
   
    
    // 지식Live 처리
    if ([urlString.lowercaseString rangeOfString:@"/knowledgelive"].location != NSNotFound){
        BOOL retValue = [self.attachedViewController isKindOfClass:[LectureViewController class]] ? NO : YES;
        if(retValue == YES)
            return YES;
        
        int selectedTabIndex = [self getTabIndexOfClass:[LectureViewController class]];
        if(selectedTabIndex == -1)
            return YES;
        else {
            [app setSelectedTabIndex:selectedTabIndex];
            return NO;
        }
    }
    
    
    /*
     상상마루 탭으로 이동
     상상마루 전용  version = 5, imagine_contract_yn = 'Y', only_imagine_yn = 'Y'
     */
    if ([urlActionString rangeOfString:@"/imgcategory"].location != NSNotFound
        || [urlActionString rangeOfString:@"/imgclassroom"].location != NSNotFound
        || [urlActionString rangeOfString:@"/sangsang/sangsanglink.aspx"].location != NSNotFound
        || [urlActionString rangeOfString:@"/imghome"].location != NSNotFound)
    {
        if([self.attachedViewController isKindOfClass:[MyViewController class]]){
            return YES;
        }
        
        
        if([self.attachedViewController isKindOfClass:[SSViewController class]]
           || [self.attachedViewController isKindOfClass:[SelfStudyViewController class]]){
            return YES;
        }
        
        if ([app.eduType isEqualToString:@"OnlyImagine"]) {
            if ([urlActionString rangeOfString:@"/contents?"].location != NSNotFound
                || [urlActionString rangeOfString:@"/series?"].location != NSNotFound
                || [urlActionString rangeOfString:@"/contents?"].location != NSNotFound
                || [urlActionString rangeOfString:@"/requiredlecture?headertitle="].location != NSNotFound
                || [urlActionString rangeOfString:@"/mystudy?"].location != NSNotFound)
            {
                BOOL retValue = [self.attachedViewController isKindOfClass:[SelfStudyViewController class]] ? NO : YES;
                
                int _tabIndex = [self getTabIndexOfClass:[SelfStudyViewController class]];
                if (_tabIndex != -1) {
                    _attachedViewController.tabBarController.selectedIndex = _tabIndex;
                    SelfStudyViewController *selfStudy = (SelfStudyViewController*)_attachedViewController.tabBarController.selectedViewController;
                    if ([urlActionString rangeOfString:@"childcategoryyn=y"].location != NSNotFound) {
                        
                        NSString *param =[urlString substringFromIndex:[urlString rangeOfString:@"?"].location + 1];
                        urlString = [NSString stringWithFormat:@"%@/imgCategory/ContentsCategoryListAll?%@&layoutdisplayyn=y", app.urlCenter, param];
                        
                    }
                    [selfStudy detailView:urlString];
                    return retValue;
                }
            }
            
        } else if ([app.eduType isEqualToString:@"LearningAndImagine"]) {
            if ([urlActionString rangeOfString:@"/imgclassroom/mystudy?"].location != NSNotFound
                || [urlActionString rangeOfString:@"/sangsang/sangsanglink.aspx"].location != NSNotFound
                || [urlActionString rangeOfString:@"/contents?"].location != NSNotFound)
            {
             
                int selectedTabIndex = [self getTabIndexOfClass:[SSViewController class]];
                if (selectedTabIndex == -1){
                    selectedTabIndex = [self getTabIndexOfClass:[SelfStudyViewController class]];
                }
                if(selectedTabIndex != -1){
                    [app setSelectedTabIndex:selectedTabIndex];
                    return NO;
                }else
                    return YES;
            }
            
            
        }
        
        
        
    }
    
    /*
     나의 강의실 탭으로 이동
     나의 강의실, 사용자 즐겨찾기
     */
    if ([urlActionString rangeOfString:@"/my/my.aspx"].location != NSNotFound
        || [urlActionString rangeOfString:@"/my/mycourse"].location != NSNotFound
        || [urlActionString rangeOfString:@"/classroom/online"].location != NSNotFound
        || [urlActionString rangeOfString:@"/imgclassroom/studysummary"].location != NSNotFound){
        
        BOOL retValue = [self.attachedViewController isKindOfClass:[MyViewController class]] ? YES : NO;
        if(retValue == YES)
            return retValue;
        
        int _tabIndex = [self getTabIndexOfClass:[MyViewController class]];
        if(_tabIndex == -1)
            return YES;
        
        if ([urlActionString rangeOfString:@"/my/my.aspx"].location != NSNotFound) {
            [app setSelectedTabIndex:_tabIndex indexOrUrl:nil];
            return NO;
        }
        
        [app setSelectedTabIndex:_tabIndex indexOrUrl:urlString];
        return NO;
    }
    
    
    /*
     다운로드 탭 혹은 설정 > 다운로드센터로 이동
     */
    if ([urlActionString isEqualToString:@"downloadcenter://"]) {
        
        int selectedTabIndex = [self getTabIndexOfClass:[DownloadViewController class]];
        if (selectedTabIndex != -1) {
            
            // 탭 중에 다운로드센터가 있는 경우
            _attachedViewController.tabBarController.selectedIndex = selectedTabIndex;
        }else{
            
            // 없는경우는 설정(혹은 더보기) > 다운로드센터로 이동해야 한다.
            selectedTabIndex = [self getTabIndexOfClass:[MoreViewController class]];
            _attachedViewController.tabBarController.selectedIndex = selectedTabIndex;
            
            UINavigationController *navi = (UINavigationController*)_attachedViewController.tabBarController.selectedViewController;
            MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
            DownloadViewController *second = [[DownloadViewController alloc] initWithStyle:UITableViewStylePlain];
            NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
            [navi setViewControllers:controllersArray animated:YES];
            
        }
        
    }
    
    /*
     설정 탭으로 이동
     */
    if ([urlActionString hasPrefix:@"membermodify://"]
        || [urlActionString rangeOfString:@"/counsel/"].location != NSNotFound
        || [urlActionString rangeOfString:@"more://"].location != NSNotFound
        || [urlActionString rangeOfString:@"/more/"].location != NSNotFound ) {
        
        int moreViewTabIndex = [self getTabIndexOfClass:[MoreViewController class]];
        
        // 회원정보 수정으로 이동
        if ([urlActionString hasPrefix:@"membermodify://"]) {
            
            // 회원정보 화면에서 이 프로토콜을 호출할리 만무하지만 그래도 혹시 모르니
            if([_attachedViewController isKindOfClass:[UserViewController class]])
                return YES;
            
            // 설마 이게 탭으로 빠져있을리는 만무하지만 그래도 혹시 모르니
            int userViewTabIndex = [self getTabIndexOfClass:[UserViewController class]];
            if(userViewTabIndex != -1){
                _attachedViewController.tabBarController.selectedIndex = userViewTabIndex;
                return NO;
            }
            
            // 기타 케이스는 설정으로 이동하여 회원정보 화면으로 이동하도록 한다.            
            _attachedViewController.tabBarController.selectedIndex = moreViewTabIndex;
            
            UINavigationController *navi = (UINavigationController*)_attachedViewController.tabBarController.selectedViewController;
            MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
            UserViewController *second = [[UserViewController alloc] initWithNibName:@"UserViewController" bundle:nil];
            NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
            [navi setViewControllers:controllersArray animated:YES];
        }
        //상담실-공지사항, 공지사항 상세
        else if ([urlActionString rangeOfString:@"/counsel_noticelist.aspx"].location != NSNotFound
            || [urlActionString rangeOfString:@"/counsel_noticedetail.aspx"].location != NSNotFound
            || [urlActionString rangeOfString:@"/counsel/notice"].location != NSNotFound){
            
            NSString *viewTypeNum = @"13";
            if ([urlActionString rangeOfString:@"/counsel/counsel_noticedetail.aspx"].location != NSNotFound
                || [urlActionString rangeOfString:@"/counsel/notice?noticeseq"].location != NSNotFound
                || [urlActionString rangeOfString:@"/counsel/noticeview?noticeseq"].location != NSNotFound) {
                
                NSString *noticeParam = [urlActionString substringFromIndex:[urlActionString rangeOfString:@"noticeseq"].location];
                viewTypeNum = [NSString stringWithFormat:@"20&%@", noticeParam];
            }
            
            MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
            NSString *noticeUrl = [NSString stringWithFormat:@"%@?type=%@&uid=%@&pw=%@",
                                   app.urlBase,
                                   viewTypeNum,
                                   [Global getUserId],
                                   [Global getUserPassword]];
            
            if ([urlActionString rangeOfString:@"/counsel/noticeview?noticeseq"].location != NSNotFound)
            {
                if([_attachedViewController isKindOfClass:[NoticeViewController class]])
                    return YES;
                
                NoticeViewController *viewController = [[NoticeViewController alloc] initWithNibName:@"NoticeViewController" bundle:nil];
                viewController.title = @"공지사항";
                viewController.url = noticeUrl;
                viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"<홈" style:UIBarButtonItemStyleDone target:viewController action:@selector(closeViewController)];
                viewController.navigationItem.hidesBackButton = NO;
                UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
                
                [_attachedViewController presentViewController:nav animated:YES completion:nil];
            }
            else {
                
                if([_attachedViewController isKindOfClass:[NoticeViewController class]])
                    return YES;
                
                _attachedViewController.tabBarController.selectedIndex = moreViewTabIndex;
                UINavigationController *navi = (UINavigationController*)_attachedViewController.tabBarController.selectedViewController;
                MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
                NoticeViewController *second = [[NoticeViewController alloc] initWithNibName:@"NoticeViewController" bundle:nil];
                second.title = @"공지사항";
                second.url = noticeUrl;
                NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
                
                [navi setViewControllers:controllersArray animated:YES];
                [navi setNavigationBarHidden:NO];
                
            }
        }
        else if ([urlActionString rangeOfString:@"more://"].location != NSNotFound ||
            [urlActionString rangeOfString:@"/more/more.aspx"].location != NSNotFound)
        {
            _attachedViewController.tabBarController.selectedIndex = moreViewTabIndex;
            UINavigationController *navi = (UINavigationController*)_attachedViewController.tabBarController.selectedViewController;
            [navi popToRootViewControllerAnimated:YES];
        }
        else if ([urlActionString rangeOfString:@"/myrequest"].location != NSNotFound
            || [urlActionString rangeOfString:@"/counsel.aspx"].location != NSNotFound
            || [urlActionString rangeOfString:@"/setrequest"].location != NSNotFound)
        {
            BOOL retValue = [self.attachedViewController isKindOfClass:[QnaViewController class]] ? YES : NO;
            if(retValue == YES)
                return retValue;
            
            UIViewController *controller = _attachedViewController.tabBarController.selectedViewController;
            if([controller isKindOfClass:[MoreViewController class]]){
                UINavigationController *navi = (UINavigationController*)controller.navigationController;
                MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
                QnaViewController *second = [[QnaViewController alloc] initWithNibName:@"QnaViewController" bundle:nil];
                NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
                [navi setViewControllers:controllersArray animated:YES];
            }
            // UIViewController
            else {                
                _attachedViewController.tabBarController.selectedIndex = moreViewTabIndex;
                
                UINavigationController *navi = (UINavigationController*)_attachedViewController.tabBarController.selectedViewController;
                MoreViewController *first = [[navi viewControllers] objectAtIndex:0];
                QnaViewController *second = [[QnaViewController alloc] initWithNibName:@"QnaViewController" bundle:nil];
                NSArray *controllersArray = [NSArray arrayWithObjects: first, second, nil];
                [navi setViewControllers:controllersArray animated:YES];
            }
        }
        
        return YES;
        
    }
    
    
    
    
    if ([urlActionString hasSuffix:@".mp4"]) {
        NSLog(@"htmlplayer");
        NSLog(@"urlString = %@", urlString);
        NSString *mp4Url = [urlString stringByReplacingOccurrencesOfString:@"htmlplayer://" withString:@"http://"];
        
        NSUserDefaults *userdeDefaults = [NSUserDefaults standardUserDefaults];
        [userdeDefaults setObject:mp4Url forKey:FILENAME];
        
        MoviePlayerController * movieCon = [[MoviePlayerController alloc] initWithNibName:@"MoviePlayerController" bundle:nil];
        movieCon.isStreamingPlay = TRUE;
        movieCon.currentSecond = 0;
        movieCon.scrollType =  1;   // progress touch 여부 0 : 진도바 이동 불가, 1 : 진도바 이동 가능
        [MLCenterForBusinessAppDelegate sharedAppDelegate].moviePlayerController = movieCon;
        [_attachedViewController presentViewController:movieCon animated:YES completion:nil];
        return NO;
    }
    
    if([urlActionString rangeOfString:@"close://"].location != NSNotFound){
        [_attachedViewController dismissViewControllerAnimated:YES completion:nil];
        return NO;
    }
    
    if ([urlString rangeOfString:@"othersite://h-leadership.hunet.co.kr"].location != NSNotFound) {
        HLeaderController *viewToPush = [[HLeaderController alloc] initWithNibName:@"HLeader" bundle:nil];
        if (viewToPush) {
            viewToPush.url = [urlString stringByReplacingOccurrencesOfString:@"othersite://" withString:@"http://"];
            [_attachedViewController presentViewController:viewToPush animated:YES completion:nil];
            //self.urlMark = nil;
            
            NSString *currentURL = [webView stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
            if([_attachedViewController isKindOfClass:[MyViewController class]]){
                ((MyViewController *)_attachedViewController).urlMark = currentURL;
            }  
            
        }
        
        return NO;
    }
    
    // 웹툰도 됨
    if ([urlString rangeOfString:@"webtoon://"].location != NSNotFound) {
        WebtoonViewController *viewToPush = [[WebtoonViewController alloc] initWithNibName:@"WebtoonViewController" bundle:nil];
        if (viewToPush) {
            viewToPush.webtoonUrl = [urlString stringByReplacingOccurrencesOfString:@"webtoon://" withString:@"http://study.hunet.co.kr"];
            
            [_attachedViewController presentViewController:viewToPush animated:YES completion:nil];
            
            NSString *currentURL = [webView stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
            if([_attachedViewController isKindOfClass:[MyViewController class]]){
                ((MyViewController *)_attachedViewController).urlMark = currentURL;
            }            
        }
        return NO;
    }
    
    //비즈니스영어 이젠 됨
    if([urlString.lowercaseString rangeOfString:@"mp3player://"].location != NSNotFound){
        
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        if (![app moviePlayCheck]) {
            return NO;
        }
        
        NSString *currentURL = [webView stringByEvaluatingJavaScriptFromString:@"window.document.location.href"];
        
        if([_attachedViewController isKindOfClass:[MyViewController class]]){
            ((MyViewController *)_attachedViewController).urlMark = currentURL;
        }
        
        NSString *mp3Url = [urlString.lowercaseString stringByReplacingOccurrencesOfString:@"mp3player://" withString:@"http://"];
        
        MP3PlayerController * mp3Player = [[MP3PlayerController alloc] initWithNibName:@"MP3PlayerController" bundle:nil];
        
        mp3Player.returnUrl = currentURL;
        mp3Player.mp3Url = mp3Url;
        
        [_attachedViewController presentViewController:mp3Player animated:YES completion:nil];
        
        return NO;
        
    }
    
    
    /**
     * 파일 다운로드
     *
     * @param url "downloadfile://?url=" + encoding url 형식으로 들어옴
     */
    if([urlString rangeOfString:@"downloadfile://url="].location != NSNotFound){
        NSString *downloadUrl = urlString;
        downloadUrl = [downloadUrl stringByReplacingOccurrencesOfString:@"downloadfile://url=" withString:@""];
        downloadUrl = [downloadUrl stringByRemovingPercentEncoding];
        
        WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
        webModal.webViewUrl = downloadUrl;
        webModal.title = [[downloadUrl componentsSeparatedByString:@"/"] lastObject];
        [_attachedViewController.tabBarController.selectedViewController presentViewController:webModal animated:YES completion:nil];
        
        return NO;
    }
    
    /*
     * 일반 파일 뷰어 처리
     */
    if ([[urlString uppercaseString] rangeOfString:@".PDF"].location != NSNotFound
        || [[urlString uppercaseString] rangeOfString:@".PPT"].location != NSNotFound
        || [[urlString uppercaseString] rangeOfString:@".PPTX"].location != NSNotFound
        || [[urlString uppercaseString] rangeOfString:@".DOC"].location != NSNotFound
        || [[urlString uppercaseString] rangeOfString:@".DOCX"].location != NSNotFound
        || [[urlString uppercaseString] rangeOfString:@".XLS"].location != NSNotFound
        || [[urlString uppercaseString] rangeOfString:@".XLSX"].location != NSNotFound
        || [[urlString uppercaseString] rangeOfString:@".PNG"].location != NSNotFound
        || [[urlString uppercaseString] rangeOfString:@".JPG"].location != NSNotFound
        || [[urlString uppercaseString] rangeOfString:@".GIF"].location != NSNotFound
        || [[urlString uppercaseString] rangeOfString:@".JPEG"].location != NSNotFound){
        
        WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
        webModal.webViewUrl = urlString;
        webModal.title = [[urlString componentsSeparatedByString:@"/"] lastObject];
        [_attachedViewController.tabBarController.selectedViewController presentViewController:webModal animated:YES completion:nil];
        
        return NO;
    }
    
    return YES;
}

- (int)getTabIndexOfClass:(Class)class {
    int tabIndex = -1;
    NSArray *arrVC = [MLCenterForBusinessAppDelegate sharedAppDelegate].tabBarController.viewControllers;
    for (int i = 0; i < [arrVC count]; i++) {
        if ([[arrVC objectAtIndex:i] isKindOfClass:[UINavigationController class]]) {
            if ([[((UINavigationController*)[arrVC objectAtIndex:i]).viewControllers objectAtIndex:0] isKindOfClass:class]) {
                tabIndex = i;
                break;
            }
        }
        else {
            if ([[arrVC objectAtIndex:i] isKindOfClass:class]) {
                tabIndex = i;
                break;
            }
        }
    }
    return tabIndex;
}


- (BOOL)isModal {
    if([_attachedViewController presentingViewController])
        return YES;
    if([[[_attachedViewController navigationController] presentingViewController] presentedViewController] == [_attachedViewController navigationController])
        return YES;
    if([[[_attachedViewController tabBarController] presentingViewController] isKindOfClass:[UITabBarController class]])
        return YES;
    
    return NO;
}


@end
