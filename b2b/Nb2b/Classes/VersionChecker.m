//
//  VersionChecker.m
//  MLCenterForBusiness
//
//  Created by Park SeungKyun on 2015. 7. 27..
//
//

#import "VersionChecker.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "Util.h"
#import "CJSONDeserializer.h"
#import "ModalAlert.h"

@implementation VersionChecker

+ (NSDictionary *)requestCurrentVersionInfo {
    
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
#if defined(PRO) || defined(StandardMLCenter)
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=tcenter", app.urlBase];
#elif avene
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=Avene", app.urlBase];
#elif lemon
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=LLemon", app.urlBase];
#elif hyundai
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=Hyundai", app.urlBase];
#elif hyundaiDymos
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=dymos", app.urlBase];
#elif hyundaiWia
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=wia", app.urlBase];
#elif Wiash
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=wiash", app.urlBase];
#elif Wiajs
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=wiajs", app.urlBase];
#elif dymosrz
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=dymosrz", app.urlBase];
#elif hyundaiHmtc
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=hmtc", app.urlBase];
#elif kcc
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=Kcc", app.urlBase];
#elif kia
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=Kia", app.urlBase];
#elif hmgc
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=hmgc", app.urlBase];
#elif hmgc1022
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=hmgc1022", app.urlBase];
#elif hmgc1023
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=hmgc1023", app.urlBase];
#elif hmgc1024
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=hmgc1024", app.urlBase];

#elif furterer
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=furterer", app.urlBase];

#elif glovis1025
    NSString* url = [NSString stringWithFormat:@"%@?type=28&device=apple&app=glovis1025", app.urlBase];
    
#endif
    
    NSDictionary *dic = nil;
    NSError *errorReq = nil;
    NSData *jsonData = [Util getNSDataByRequestUrl:[NSURL URLWithString:url]
                                          andQuery:nil
                                          andError:&errorReq];
    if (jsonData)
    {
        NSError *errorJSON = nil;
        dic = [[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&errorJSON];
    }
    
    return dic;
}




+ (void)checkWithUpdate
{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        //Background Thread
        NSDictionary *dic = [self requestCurrentVersionInfo];
        if (dic)
        {
            NSString *latestVersion = [dic objectForKey:@"Version"];
            NSString *downLoadUrl = [dic objectForKey:@"DownloadUrl"];
            NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
            NSArray *arrLatestVersion = nil;
            NSArray *arrCurrentVersion = nil;
            NSUInteger minVersionDepth = 0;
            Boolean needUpdate = NO;
            
            if ([latestVersion rangeOfString: @"."].location != NSNotFound) {
                arrLatestVersion = [latestVersion componentsSeparatedByString:@"."];
            }
            
            if ([currentVersion rangeOfString: @"."].location != NSNotFound) {
                arrCurrentVersion = [currentVersion componentsSeparatedByString:@"."];
            }
            
            minVersionDepth = [arrCurrentVersion count] < [arrLatestVersion count] ? [arrCurrentVersion count] : [arrLatestVersion count];
            
            for (NSUInteger i = 0; i < minVersionDepth; i++) {
                if ([[arrCurrentVersion objectAtIndex:(i)] integerValue] > [[arrLatestVersion objectAtIndex:(i)]  integerValue])
                {
                    break;
                }
                else if ([[arrCurrentVersion objectAtIndex:(i)] integerValue] < [[arrLatestVersion objectAtIndex:(i)]  integerValue])
                {
                    needUpdate = YES;
                    NSLog(@"Need Update");
                    break;
                }
            }
            
            
            [MLCenterForBusinessAppDelegate sharedAppDelegate].appNeedUpdate = needUpdate;
            [MLCenterForBusinessAppDelegate sharedAppDelegate].appDownLoadUrl = downLoadUrl;
            
            if (needUpdate == YES)
            {
                // 수정, 삭제, 순서변경
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"版本更新"
                                                                                         message:@"有新的版本. 要进行更新吗？"
                                                                                  preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* modify = [UIAlertAction actionWithTitle:@"更新" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action)
                                         {
                                             [alertController dismissViewControllerAnimated:YES completion:nil];
                                             NSString *downLoadUrl = [dic objectForKey:@"DownloadUrl"];
                                             if ([downLoadUrl length] > 0) {
                                                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:downLoadUrl]];
                                                 [[UIApplication sharedApplication] performSelector:NSSelectorFromString(@"terminateWithSuccess") withObject:nil afterDelay:0.5f];
                                             }
                                         }];
                
                [alertController addAction:modify];
                [[MLCenterForBusinessAppDelegate sharedAppDelegate].window.rootViewController presentViewController:alertController animated:YES completion:nil];
                
            }
        }
    });
    
}

@end
