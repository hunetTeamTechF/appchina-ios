//
//  CounselViewController.m
//  MLCenterForBusiness
//
//  Created by Gi-Bok Park on 11. 5. 13..
//  Copyright 2011 hunet. All rights reserved.
//

#import "CounselViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "CounselCell.h"
#import "ModalAlert.h"
#import "CJSONDeserializer.h"
#import "GuidebookViewController.h"
#import "NoticeViewController.h"
#import "FaqViewController.h"
#import "QnaViewController.h"
#import "GraphicsUtile.h"

#define ENCODE(X) [(X) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]

@implementation CounselViewController
@synthesize newIcon, linkUrl, row;

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        self.title = NSLocalizedString(@"3번째", @"3번째");
        self.tabBarItem.image = [GraphicsUtile imageWithScaling:@"ti_lectureroom"];
    }
    return self;
}


- (NSString*)newIcon {
	
	if (newIcon == nil) {
		
		NSURLResponse* response;
		NSError* errorNet;
		NSError* errorJSON;
		
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
		
		NSString* param = [NSString stringWithFormat:@"?type=2&company_seq=%@&uid=%@&pw=%@", app.companySeq, [Global getUserId], [Global getUserPassword]];
		param = ENCODE(param);
		NSString* url = [NSString stringWithFormat:@"%@%@", app.urlBase, param];
		
		NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]
												 cachePolicy:NSURLRequestUseProtocolCachePolicy 
											 timeoutInterval:20.0];
		
		// create the connection with request
		UIApplication* appIndicator = [UIApplication sharedApplication];
		appIndicator.networkActivityIndicatorVisible = YES;								
		NSData* dataReceived = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorNet];
		appIndicator.networkActivityIndicatorVisible = NO;								
		
		if (dataReceived) {		// success
			NSDictionary* result = [[CJSONDeserializer deserializer] deserializeAsDictionary:dataReceived error:&errorJSON];
			if ([[result objectForKey:@"IsSuccess"] isEqualToString:@"YES"]) {  // success...
				newIcon = [result objectForKey:@"IsNewIcon"];
			} 
			
		} else {
			// inform the user that the download could not be made
			[ModalAlert notify:@"네트워크 연결에 문제가 생겼습니다."];
		}
	}
	
	return newIcon;
}


#pragma mark -
#pragma mark View lifecycle




- (void)cellSelected {
	if (self.row > 0) {
		[self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:self.row inSection:0]];
	}	
}

/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 4;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 44;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
	static NSString *CellNoticeIdentifier = @"CounselCell";
    	
	if (indexPath.row == 1) {
		
		CounselCell* cell = (CounselCell*) [tableView dequeueReusableCellWithIdentifier:CellNoticeIdentifier];
		if (cell == nil) {
			NSArray* arr = [[NSBundle mainBundle] loadNibNamed:CellNoticeIdentifier owner:nil options:nil];
			cell = [arr objectAtIndex:0];
		}		
		//cell.labelUserID.text = [[listQA objectAtIndex:indexPath.row] objectForKey:@"uid"];
		cell.viewImage.hidden = ![self.newIcon isEqualToString:@"YES"];
		
		return cell;
	} else {
		
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
			//cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_arrow"]];

		}
		
		cell.textLabel.font = [UIFont systemFontOfSize:17];
		
		switch (indexPath.row) {
			case 0:
				cell.textLabel.text = @"모바일 연수원 이용안내";
				break;
			case 2:
				cell.textLabel.text = @"FAQ";
				break;
			case 3:
				cell.textLabel.text = @"1:1 Q&A";
				break;				
			default:
				break;
		}
		
		return cell;
	}	
	
    return nil;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	
	switch (indexPath.row) {
		case 0: //모바일 연수원 이용안내	
		{
			GuidebookViewController *viewToPush = [[GuidebookViewController alloc] initWithNibName:@"GuidebookViewController" bundle:nil];
			if (viewToPush) {
				viewToPush.title = @"모바일 연수원 이용안내";
				
				UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:@"상담실" style:UIBarButtonItemStyleDone target:nil action:nil];
				[[self navigationItem] setBackBarButtonItem: newBackButton];

				[self.navigationController pushViewController:viewToPush animated:YES];
			}
		}
			break;
		case 1: //공지사항
		{	
            MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
			NoticeViewController *viewToPush = [[NoticeViewController alloc] initWithNibName:@"NoticeViewController" bundle:nil];
			if (viewToPush) {
                viewToPush.title = @"공지사항";
				viewToPush.url = [self.linkUrl length] > 0 ? self.linkUrl : [NSString stringWithFormat:@"%@/Counsel/Counsel_NoticeList.aspx", app.urlSite];
				
				
				UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:@"상담실" style:UIBarButtonItemStyleDone target:nil action:nil];
				[[self navigationItem] setBackBarButtonItem: newBackButton];
				
				[self.navigationController pushViewController:viewToPush animated:YES];
			}
			
		}
			break;
		case 2: //faq
		{
			FaqViewController *viewToPush = [[FaqViewController alloc] initWithNibName:@"FaqViewController" bundle:nil];
			if (viewToPush) {
				viewToPush.title = @"FAQ";
				
				UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:@"상담실" style:UIBarButtonItemStyleDone target:nil action:nil];
				[[self navigationItem] setBackBarButtonItem: newBackButton];
				
				[self.navigationController pushViewController:viewToPush animated:YES];
			}
		}
			break;
		case 3: //1:1 faq
		{		
			
			QnaViewController *viewToPush = [[QnaViewController alloc] initWithNibName:@"QnaViewController" bundle:nil];
			if (viewToPush) {
				viewToPush.title = @"1:1 Q&A";
				
				UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:@"상담실" style:UIBarButtonItemStyleDone target:viewToPush action:@selector(back)];
				[[self navigationItem] setBackBarButtonItem: newBackButton];
				
				[self.navigationController pushViewController:viewToPush animated:YES];
			}
			
		}
			break;
			
		default:
			break;
	}
	
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {

    self.newIcon = nil;
	self.linkUrl = nil;
	[super viewDidUnload];
}




@end

