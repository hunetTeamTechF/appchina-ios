//
//  SuperViewController.m
//  MLCenterForBusiness
//
//  Created by hunet-mac-npc92 on 2/10/15.
//
//

#import "SuperViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "Util.h"
#import "WebModalViewController.h"
#import "MoviePlayerController.h"
#import "CustomerViewController.h"

@interface SuperViewController ()

@end

@implementation SuperViewController
@synthesize superWebView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSLog(@"SuperViewController shouldStartLoadWithRequest - %@", request.URL.absoluteString);
    
    MLCenterForBusinessAppDelegate *app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
    
    if ([request.URL.absoluteString.lowercaseString hasPrefix:@"back://"])
    {
        if (self.superWebView.canGoBack) {
            [self.superWebView goBack];
        }
        return NO;
    }
    
    //모달 팝업
    if ([request.URL.absoluteString.lowercaseString hasPrefix:@"modal://"])
    {
        NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:request.URL resolvingAgainstBaseURL:NO];
        NSArray *queryItems = urlComponents.queryItems;
        
        //NSString *urlParam = [Util valueForKey:@"url" fromQueryItems:queryItems];
        NSString *urlParam = [Util valueForKey:@"url" fromQuery:request.URL.query];
        NSString *urlParamDecoded = (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (CFStringRef)urlParam, CFSTR(""), kCFStringEncodingUTF8));
        
        //?url query parameter가 없는 경우,
        if (urlParam == nil) {
            WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
            webModal.webViewUrl = [request.URL.absoluteString stringByReplacingOccurrencesOfString:@"modal://" withString:@"http://"];
            //webModal.title = @"";
            [self presentViewController:webModal animated:YES completion:nil];
        }
        //query에 url parameter가 있으면 해당 url로 modal 팝업
        else {
            
            NSURL *urlParsed = [NSURL URLWithString:[[urlParamDecoded componentsSeparatedByString:@"?"] firstObject]];
            NSString *method = [Util valueForKey:@"method" fromQueryItems:queryItems];
            NSString *title = [Util valueForKey:@"title" fromQueryItems:queryItems];
            //post parameter가 있으면 url을 파싱해서, queryString은 post로 전송
            if ([method.lowercaseString isEqualToString:@"post"])
            {
                WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
                
                //webViewRequestMethod
                NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:urlParsed
                                                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                   timeoutInterval:20.0];
                [request setHTTPMethod:@"POST"];
                NSString *bodyString = (NSString *)[[urlParamDecoded componentsSeparatedByString:@"?"] lastObject];
                [request setHTTPBody:[bodyString dataUsingEncoding:NSUTF8StringEncoding]];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                webModal.title = title;
                [self presentViewController:webModal animated:YES completion:nil];
                [webModal.WebView loadRequest:request];
                
                
            }
            //query에 url parameter가 없으면
            else {
                WebModalViewController *webModal = [[WebModalViewController alloc] initWithNibName:@"WebModalViewController" bundle:nil];
                webModal.webViewUrl = urlParamDecoded;
                webModal.title = title;
                [self presentViewController:webModal animated:YES completion:nil];
            }
            
        }
        
        return NO;
        
    }
    
    BOOL requestDirectMovieUrl =
    [request.URL.absoluteString.lowercaseString rangeOfString:@".hscdn.com/"].location != NSNotFound
    && [request.URL.absoluteString.lowercaseString rangeOfString:@".mp4" ].location != NSNotFound;    //htmlpalyer:// 프로토콜로 호출은 영상url에 대한 단순 스트리밍을 용도로 사용
    if ([request.URL.absoluteString.lowercaseString hasPrefix:@"htmlplayer://"] ||
        requestDirectMovieUrl)
    {
        if (app.moviePlayCheck) {
            
            NSString *mp4Url = [request.URL.absoluteString stringByReplacingOccurrencesOfString:@"htmlplayer://" withString:@"http://"];
            
            NSUserDefaults *userdeDefaults = [NSUserDefaults standardUserDefaults];
            [userdeDefaults setObject:mp4Url forKey:FILENAME];
            NSLog(@"Streaming filename = %@", [userdeDefaults objectForKey:FILENAME]);
            
            MoviePlayerController * movieCon = [[MoviePlayerController alloc] initWithNibName:@"MoviePlayerController" bundle:nil];
            movieCon.isStreamingPlay = TRUE;
            movieCon.currentSecond = 0;
            movieCon.scrollType =  1;   // progress touch 여부 0 : 진도바 이동 불가, 1 : 진도바 이동 가능
            app.moviePlayerController = movieCon;
            [self presentViewController:movieCon animated:YES completion:nil];
            
        }
        
        return NO;
    }

    
    return YES;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
