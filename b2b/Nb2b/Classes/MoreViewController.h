
#import <UIKit/UIKit.h>
#import "MLCenterForBusinessAppDelegate.h"


@interface MoreViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
	NSArray *listMenu;
    NSMutableArray *array;
	NSString *linkUrl;
    int devOptionClickCount;

}

@property (nonatomic, strong) NSArray *listMenu;
@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) NSString *linkUrl;
@property (weak, nonatomic) IBOutlet UINavigationBar *navagationBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellFooter;

//- (void)cellSelected;
- (MLCenterForBusinessAppDelegate*)appDelegate;
- (IBAction)clickedLogout:(id)sender;

@end
