//
//  SuperViewController.h
//  MLCenterForBusiness
//
//  Created by hunet-mac-npc92 on 2/10/15.
//
//

#import <UIKit/UIKit.h>

@interface SuperViewController : UIViewController {

}
@property (nonatomic, strong) UIWebView *superWebView;
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;

@end
