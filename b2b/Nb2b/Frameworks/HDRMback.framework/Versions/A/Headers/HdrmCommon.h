//
//  HdrmCommon.h
//  MLCenterForHDRM
//
//  Created by Park SeungKyun on 2015. 8. 6..
//
//

#import <Foundation/Foundation.h>
#import <HDRM/HdrmHTTPServer.h>

#define kDrmEnctyptBytesSize @"1000"
#define kDrmAESCryptKey @"cbc18cf498b143e28b6581af96bee6b6"
#define kDrmPrefix @"HunetDRM"
#define kDrmPrefixBytesSize @"8"
#define kDrmInfoDataLengthBytesSize @"12"

@interface HdrmCommon : NSObject

// 파일이 HDRM 컨텐츠이면 YES를 반환합니다.
+ (BOOL)isHdrmContent:(NSString*)path;

// HDRM 정보를 반환합니다.
+ (NSDictionary*)getDrmInfo:(NSString*)path;

// 복호화된 첫번째 2Bytes를 반환합니다.
+ (NSData*)getFirstTwoBytes:(NSString*)path;

// HDRM 데이터 블록의 크기를 반환합니다.
+ (UInt64)getHdrmBlockSize:(NSString*)path;

// 암호화된 Bytes를 반환합니다.
+ (NSData*)encrypt:(NSData*)firstData totalBytes:(UInt64)totalBytes;

// 복호화된 Bytes를 반환합니다.
+ (NSData*)decrypt:(NSData*)firstData;

// DRM 기본정보를 생성해 반환합니다.
+ (NSMutableDictionary*)makeDrmInfo;

+ (NSString *)formatTime:(int)time;
+ (NSString *)timeMmddss:(int)timeSS;
+ (void) deleteFile :(NSString *) fileFullPath;
+ (NSString *) filePath : (NSString *) filename;

+ (NSString*)getFileFullPath:(NSString*)folder fileName:(NSString*)fileName;
+ (NSString*)getFileFullPath:(NSString*)folder;
+ (int)isFileExists:(NSString*)folder fileName:(NSString*)fileName;
+ (NSString*)createDirecotryBy:(NSString*)folderName;
+ (NSData*)fileToData:(NSString*)filePath;
+ (void)dataToFile:(NSData*)data saveFilePath:(NSString*)saveFilePath;

+ (void)moveFile:(NSString*)filePath toPath:(NSString*)toPath;
+ (NSString*)fileToString:(NSString*)filePath;
+ (void)stringToFile:(NSString*)text saveFilePath:(NSString*)saveFilePath;
+ (void)copyFile:(NSString*)filePath toPath:(NSString*)toPath;

@end
