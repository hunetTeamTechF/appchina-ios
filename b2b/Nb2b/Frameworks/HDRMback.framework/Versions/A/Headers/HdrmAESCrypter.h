//
//  HdrmAESCrypter.h
//  HunetPlayer
//
//  Created by Park SeungKyun on 2015. 5. 28..
//
//

#import <Foundation/Foundation.h>

@interface HdrmAESCrypter : NSObject 

+ (NSData *) AES128Encrypt:(NSData*)data key: (NSString *) key;
+ (NSData *) AES128Decrypt:(NSData*)data key: (NSString *) key;

+ (NSData *) AES256Encrypt:(NSData*)data key: (NSString *) key;
+ (NSData *) AES256Decrypt:(NSData*)data key: (NSString *) key;

@end
