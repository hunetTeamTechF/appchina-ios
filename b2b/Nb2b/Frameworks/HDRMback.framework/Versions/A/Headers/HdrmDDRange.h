/**
 * HdrmDDRange is the functional equivalent of a 64 bit NSRange.
 * The HdrmHTTP Server is designed to support very large files.
 * On 32 bit architectures (ppc, i386) NSRange uses unsigned 32 bit integers.
 * This only supports a range of up to 4 gigabytes.
 * By defining our own variant, we can support a range up to 16 exabytes.
 * 
 * All effort is given such that HdrmDDRange functions EXACTLY the same as NSRange.
**/

#import <Foundation/NSValue.h>
#import <Foundation/NSObjCRuntime.h>

@class NSString;

typedef struct _DDRange {
    UInt64 location;
    UInt64 length;
} HdrmDDRange;

typedef HdrmDDRange *DDRangePointer;

NS_INLINE HdrmDDRange HdrmDDMakeRange(UInt64 loc, UInt64 len) {
    HdrmDDRange r;
    r.location = loc;
    r.length = len;
    return r;
}

NS_INLINE UInt64 HdrmDDMaxRange(HdrmDDRange range) {
    return (range.location + range.length);
}

NS_INLINE BOOL HdrmDDLocationInRange(UInt64 loc, HdrmDDRange range) {
    return (loc - range.location < range.length);
}

NS_INLINE BOOL HdrmDDEqualRanges(HdrmDDRange range1, HdrmDDRange range2) {
    return ((range1.location == range2.location) && (range1.length == range2.length));
}

FOUNDATION_EXPORT HdrmDDRange HdrmDDUnionRange(HdrmDDRange range1, HdrmDDRange range2);
FOUNDATION_EXPORT HdrmDDRange HdrmDDIntersectionRange(HdrmDDRange range1, HdrmDDRange range2);
FOUNDATION_EXPORT NSString *DDStringFromRange(HdrmDDRange range);
FOUNDATION_EXPORT HdrmDDRange HdrmDDRangeFromString(NSString *aString);

NSInteger ddrangeCompare(DDRangePointer pDDRange1, DDRangePointer pDDRange2);

@interface NSValue (NSValueDDRangeExtensions)

+ (NSValue *)valueWithDDRange:(HdrmDDRange)range;
- (HdrmDDRange)ddrangeValue;

- (NSInteger)ddrangeCompare:(NSValue *)ddrangeValue;

@end
