//
//  HdrmServer.h
//  MLCenterForHDRM
//
//  Created by Park SeungKyun on 2015. 8. 7..
//
//

#import <Foundation/Foundation.h>
#import <HDRM/HdrmHTTPServer.h>

@interface HdrmServer : NSObject

@property (nonatomic, strong) HdrmHTTPServer *httpServer;

- (void)start:(NSString*)wwwroot;
- (void)stop;

@end
