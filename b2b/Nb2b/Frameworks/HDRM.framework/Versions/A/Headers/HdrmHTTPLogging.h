/**
 * In order to provide fast and flexible logging, this project uses Cocoa Lumberjack.
 * 
 * The Google Code page has a wealth of documentation if you have any questions.
 * HdrmHTTPs://github.com/robbiehanson/CocoaLumberjack
 * 
 * Here's what you need to know concerning how logging is setup for CocoaHTTPServer:
 * 
 * There are 4 log levels:
 * - Error
 * - Warning
 * - Info
 * - Verbose
 * 
 * In addition to this, there is a Trace flag that can be enabled.
 * When tracing is enabled, it spits out the methods that are being called.
 * 
 * Please note that tracing is separate from the log levels.
 * For example, one could set the log level to warning, and enable tracing.
 * 
 * All logging is asynchronous, except errors.
 * To use logging within your own custom files, follow the steps below.
 * 
 * Step 1:
 * Import this header in your implementation file:
 * 
 * #import <HDRM/HdrmHTTPLogging.h>
 * 
 * Step 2:
 * Define your logging level in your implementation file:
 * 
 * // Log levels: off, error, warn, info, verbose
 * static const int HdrmHTTPLogLevel = HdrmHTTP_LOG_LEVEL_VERBOSE;
 * 
 * If you wish to enable tracing, you could do something like this:
 * 
 * // Debug levels: off, error, warn, info, verbose
 * static const int HdrmHTTPLogLevel = HdrmHTTP_LOG_LEVEL_INFO | HdrmHTTP_LOG_FLAG_TRACE;
 * 
 * Step 3:
 * Replace your NSLog statements with HdrmHTTPLog statements according to the severity of the message.
 * 
 * NSLog(@"Fatal error, no dohickey found!"); -> HdrmHTTPLogError(@"Fatal error, no dohickey found!");
 * 
 * HdrmHTTPLog works exactly the same as NSLog.
 * This means you can pass it multiple variables just like NSLog.
**/

#import <HDRM/HdrmDDLog.h>

// Define logging context for every log message coming from the HdrmHTTP server.
// The logging context can be extracted from the HdrmDDLogMessage from within the logging framework,
// which gives loggers, formatters, and filters the ability to optionally process them differently.

#define HdrmHTTP_LOG_CONTEXT 80

// Configure log levels.

#define HdrmHTTP_LOG_FLAG_ERROR   (1 << 0) // 0...00001
#define HdrmHTTP_LOG_FLAG_WARN    (1 << 1) // 0...00010
#define HdrmHTTP_LOG_FLAG_INFO    (1 << 2) // 0...00100
#define HdrmHTTP_LOG_FLAG_VERBOSE (1 << 3) // 0...01000

#define HdrmHTTP_LOG_LEVEL_OFF     0                                              // 0...00000
#define HdrmHTTP_LOG_LEVEL_ERROR   (HdrmHTTP_LOG_LEVEL_OFF   | HdrmHTTP_LOG_FLAG_ERROR)   // 0...00001
#define HdrmHTTP_LOG_LEVEL_WARN    (HdrmHTTP_LOG_LEVEL_ERROR | HdrmHTTP_LOG_FLAG_WARN)    // 0...00011
#define HdrmHTTP_LOG_LEVEL_INFO    (HdrmHTTP_LOG_LEVEL_WARN  | HdrmHTTP_LOG_FLAG_INFO)    // 0...00111
#define HdrmHTTP_LOG_LEVEL_VERBOSE (HdrmHTTP_LOG_LEVEL_INFO  | HdrmHTTP_LOG_FLAG_VERBOSE) // 0...01111

// Setup fine grained logging.
// The first 4 bits are being used by the standard log levels (0 - 3)
// 
// We're going to add tracing, but NOT as a log level.
// Tracing can be turned on and off independently of log level.

#define HdrmHTTP_LOG_FLAG_TRACE   (1 << 4) // 0...10000

// Setup the usual boolean macros.

#define HdrmHTTP_LOG_ERROR   (HdrmHTTPLogLevel & HdrmHTTP_LOG_FLAG_ERROR)
#define HdrmHTTP_LOG_WARN    (HdrmHTTPLogLevel & HdrmHTTP_LOG_FLAG_WARN)
#define HdrmHTTP_LOG_INFO    (HdrmHTTPLogLevel & HdrmHTTP_LOG_FLAG_INFO)
#define HdrmHTTP_LOG_VERBOSE (HdrmHTTPLogLevel & HdrmHTTP_LOG_FLAG_VERBOSE)
#define HdrmHTTP_LOG_TRACE   (HdrmHTTPLogLevel & HdrmHTTP_LOG_FLAG_TRACE)

// Configure asynchronous logging.
// We follow the default configuration,
// but we reserve a special macro to easily disable asynchronous logging for debugging purposes.

#define HdrmHTTP_LOG_ASYNC_ENABLED   YES

#define HdrmHTTP_LOG_ASYNC_ERROR   ( NO && HdrmHTTP_LOG_ASYNC_ENABLED)
#define HdrmHTTP_LOG_ASYNC_WARN    (YES && HdrmHTTP_LOG_ASYNC_ENABLED)
#define HdrmHTTP_LOG_ASYNC_INFO    (YES && HdrmHTTP_LOG_ASYNC_ENABLED)
#define HdrmHTTP_LOG_ASYNC_VERBOSE (YES && HdrmHTTP_LOG_ASYNC_ENABLED)
#define HdrmHTTP_LOG_ASYNC_TRACE   (YES && HdrmHTTP_LOG_ASYNC_ENABLED)

// Define logging primitives.

#define HdrmHTTPLogError(frmt, ...)    LOG_OBJC_MAYBE(HdrmHTTP_LOG_ASYNC_ERROR,   HdrmHTTPLogLevel, HdrmHTTP_LOG_FLAG_ERROR,  \
                                                  HdrmHTTP_LOG_CONTEXT, frmt, ##__VA_ARGS__)

#define HdrmHTTPLogWarn(frmt, ...)     LOG_OBJC_MAYBE(HdrmHTTP_LOG_ASYNC_WARN,    HdrmHTTPLogLevel, HdrmHTTP_LOG_FLAG_WARN,   \
                                                  HdrmHTTP_LOG_CONTEXT, frmt, ##__VA_ARGS__)

#define HdrmHTTPLogInfo(frmt, ...)     LOG_OBJC_MAYBE(HdrmHTTP_LOG_ASYNC_INFO,    HdrmHTTPLogLevel, HdrmHTTP_LOG_FLAG_INFO,    \
                                                  HdrmHTTP_LOG_CONTEXT, frmt, ##__VA_ARGS__)

#define HdrmHTTPLogVerbose(frmt, ...)  LOG_OBJC_MAYBE(HdrmHTTP_LOG_ASYNC_VERBOSE, HdrmHTTPLogLevel, HdrmHTTP_LOG_FLAG_VERBOSE, \
                                                  HdrmHTTP_LOG_CONTEXT, frmt, ##__VA_ARGS__)

#define HdrmHTTPLogTrace()             LOG_OBJC_MAYBE(HdrmHTTP_LOG_ASYNC_TRACE,   HdrmHTTPLogLevel, HdrmHTTP_LOG_FLAG_TRACE, \
                                                  HdrmHTTP_LOG_CONTEXT, @"%@[%p]: %@", THIS_FILE, self, THIS_METHOD)

#define HdrmHTTPLogTrace2(frmt, ...)   LOG_OBJC_MAYBE(HdrmHTTP_LOG_ASYNC_TRACE,   HdrmHTTPLogLevel, HdrmHTTP_LOG_FLAG_TRACE, \
                                                  HdrmHTTP_LOG_CONTEXT, frmt, ##__VA_ARGS__)


#define HdrmHTTPLogCError(frmt, ...)      LOG_C_MAYBE(HdrmHTTP_LOG_ASYNC_ERROR,   HdrmHTTPLogLevel, HdrmHTTP_LOG_FLAG_ERROR,   \
                                                  HdrmHTTP_LOG_CONTEXT, frmt, ##__VA_ARGS__)

#define HdrmHTTPLogCWarn(frmt, ...)       LOG_C_MAYBE(HdrmHTTP_LOG_ASYNC_WARN,    HdrmHTTPLogLevel, HdrmHTTP_LOG_FLAG_WARN,    \
                                                  HdrmHTTP_LOG_CONTEXT, frmt, ##__VA_ARGS__)

#define HdrmHTTPLogCInfo(frmt, ...)       LOG_C_MAYBE(HdrmHTTP_LOG_ASYNC_INFO,    HdrmHTTPLogLevel, HdrmHTTP_LOG_FLAG_INFO,    \
                                                  HdrmHTTP_LOG_CONTEXT, frmt, ##__VA_ARGS__)

#define HdrmHTTPLogCVerbose(frmt, ...)    LOG_C_MAYBE(HdrmHTTP_LOG_ASYNC_VERBOSE, HdrmHTTPLogLevel, HdrmHTTP_LOG_FLAG_VERBOSE, \
                                                  HdrmHTTP_LOG_CONTEXT, frmt, ##__VA_ARGS__)

#define HdrmHTTPLogCTrace()               LOG_C_MAYBE(HdrmHTTP_LOG_ASYNC_TRACE,   HdrmHTTPLogLevel, HdrmHTTP_LOG_FLAG_TRACE, \
                                                  HdrmHTTP_LOG_CONTEXT, @"%@[%p]: %@", THIS_FILE, self, __FUNCTION__)

#define HdrmHTTPLogCTrace2(frmt, ...)     LOG_C_MAYBE(HdrmHTTP_LOG_ASYNC_TRACE,   HdrmHTTPLogLevel, HdrmHTTP_LOG_FLAG_TRACE, \
                                                  HdrmHTTP_LOG_CONTEXT, frmt, ##__VA_ARGS__)

