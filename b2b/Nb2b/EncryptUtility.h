#import <Foundation/Foundation.h>

#define E_KEY_NAME @"encrypt_key"
#define E_LENGTH_NAME @"encrypted_length"

@interface EncryptUtility : NSObject {
    
}

+(void)bitTest;
+(NSData*)encrypt:(NSData*)data;
+(NSData*)decrypt:(NSData*)data;

@end
