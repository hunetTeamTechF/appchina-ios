

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate> {
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UIImageView *viewImageCover;
    UIView *mask;
}

@property (nonatomic, strong) UIImageView *viewImageCover;
@property (strong, nonatomic) IBOutlet UIButton *offlineButton;
@property (strong, nonatomic) IBOutlet UIImageView *viewImageBackground;
@property (strong, nonatomic) IBOutlet UIImageView *viewImageLoginBox;
@property (strong, nonatomic) IBOutlet UITextField *textID;
@property (strong, nonatomic) IBOutlet UITextField *textPassword;
@property (strong, nonatomic) IBOutlet UIButton *buttonLogin;
@property (strong, nonatomic) IBOutlet UIImageView *loginBackgoundBox;
@property (strong, nonatomic) IBOutlet UILabel *loginTxt1;
@property (strong, nonatomic) IBOutlet UILabel *loginTxt2;

- (IBAction)textFiledReturnEditing:(id)sender;
- (IBAction) onLogin;
- (void) onWebLogin: (NSString *) userId : (NSString *) userPwd;
//- (IBAction) onSupport;
- (IBAction) backgroundTab:(id)sender;
- (IBAction)offlineClick:(id)sender;
- (IBAction)clickedFindPW:(id)sender;
//- (IBAction)backgroundTouched:(id)sender;

@end