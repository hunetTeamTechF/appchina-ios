
#import "LoginViewController.h"
#import "MLCenterForBusinessAppDelegate.h"
#import "MoreViewController.h"
#import "DownloadViewController.h"
#import "ModalAlert.h"
#import "Header.h"

@interface LoginViewController ()
{
    CGPoint initLoginBoxPoint;
	CGPoint initTextIDPoint;
	CGPoint initTextPasswordPoint;
    CGPoint initButtonLoginPoint;
    CGPoint initLoginBgBoxPoint;
    CGPoint initloginTxt1Point;
    CGPoint initloginTxt2Point;
    CGPoint initLoginLogoPoint;
    CGPoint initOfflineButtonPoint;
    CGPoint initGetPwdPoint;
}
@end
@implementation LoginViewController
@synthesize viewImageCover;

- (int) yPositionEachCompany{
    CGFloat y = 0;
    return y;
}

- (void) setControlsPosition {
    
}


- (void)viewDidLoad {
    [super viewDidLoad];

    // 앱 최초 실행시 무조건 비밀번호 1회 변경해야 하도록 하는 로직.
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString * keyFindPw = @"hunet_user_changed_password";
//    if (![defaults objectForKey:keyFindPw] || [[defaults objectForKey:keyFindPw] isEqualToString:@""]) {
//        [defaults setObject:@"NO" forKey:keyFindPw];
//    }
    
    CGFloat y = [self yPositionEachCompany];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];

    self.viewImageBackground.image = [UIImage imageNamed:@"loginbg"];
    self.viewImageLoginBox.center = CGPointMake(self.viewImageLoginBox.center.x, self.viewImageLoginBox.center.y + y);
    
    self.textID.center = CGPointMake(self.textID.center.x, self.textID.center.y + y);
    self.textID.leftView = paddingView;
    self.textID.leftViewMode = UITextFieldViewModeAlways;
    
    
    self.textPassword.center = CGPointMake(self.textPassword.center.x, self.textPassword.center.y + y);
    self.textPassword.leftView = paddingView2;
    self.textPassword.leftViewMode = UITextFieldViewModeAlways;
    self.buttonLogin.center = CGPointMake(self.buttonLogin.center.x, self.buttonLogin.center.y + y);
    
    CGRect screenBounds = [UIScreen mainScreen].bounds;
    CGRect viewBounds = self.view.bounds;
    CGFloat scaleX = screenBounds.size.width / viewBounds.size.width;
    CGFloat scaleY = screenBounds.size.height / viewBounds.size.height;
    CGPoint scalePoint = CGPointMake(scaleX, scaleY);
    
    initTextIDPoint = [self scaleCenter:self.textID.center scaledPoint:scalePoint];
    initTextPasswordPoint = [self scaleCenter:self.textPassword.center  scaledPoint:scalePoint];
    initButtonLoginPoint = [self scaleCenter:self.buttonLogin.center scaledPoint:scalePoint];
    initOfflineButtonPoint = [self scaleCenter:self.offlineButton.center scaledPoint:scalePoint];
    initLoginLogoPoint = [self scaleCenter:self.LoginTitle.center scaledPoint:scalePoint];
    initGetPwdPoint = [self scaleCenter:self.GetPwd.center scaledPoint:scalePoint];
    
    self.offlineButton.center = CGPointMake(self.offlineButton.center.x, self.offlineButton.center.y + y);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [paths objectAtIndex:0];
    NSString *drmFolder = [NSString stringWithFormat:@"%@/%@", documentFolder, @"hunet_docroot"];
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSDirectoryEnumerator *fileEnumerator = [fileManager enumeratorAtPath:drmFolder];
    
    int filecount = 0;
    for (NSString *filename in fileEnumerator) {
        if ([filename isEqualToString:@".DS_Store"]) {
            continue;
        }
        filecount = 1;
        break;
    }
    
    
    self.offlineButton.hidden = TRUE;
}

- (CGPoint)scaleCenter:(CGPoint)orgPoint scaledPoint:(CGPoint)scaledPoint
{
    return CGPointMake(orgPoint.x * scaledPoint.x, orgPoint.y);
}

- (IBAction)backgroundTab:(id)sender{
    [_textID resignFirstResponder];
    [_textPassword resignFirstResponder];
    self.viewImageLoginBox.center = CGPointMake(initLoginBoxPoint.x, initLoginBoxPoint.y);
    self.textID.center = CGPointMake(initTextIDPoint.x, initTextIDPoint.y);
    self.textPassword.center = CGPointMake(initTextPasswordPoint.x, initTextPasswordPoint.y);
    self.buttonLogin.center = CGPointMake(initButtonLoginPoint.x, initButtonLoginPoint.y );
    self.loginBackgoundBox.center = CGPointMake(initLoginBoxPoint.x, initLoginBoxPoint.y);
    self.loginTxt1.center = CGPointMake(initloginTxt1Point.x, initloginTxt1Point.y);
    self.loginTxt2.center = CGPointMake(initloginTxt2Point.x, initloginTxt2Point.y);
    self.offlineButton.center = CGPointMake(initOfflineButtonPoint.x, initOfflineButtonPoint.y);
    self.LoginTitle.center =CGPointMake(initLoginLogoPoint.x, initLoginLogoPoint.y);
    self.GetPwd.center = CGPointMake(initGetPwdPoint.x, initGetPwdPoint.y);
    [UIView commitAnimations];
}


- (IBAction) onLogin {
	self.buttonLogin.userInteractionEnabled = NO;
    NSString *RuleId = @"hmtc-";
    self.textID.text = [self.textID.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.textPassword.text = [self.textPassword.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    self.textID.text = [NSString stringWithFormat:@"%@%@", RuleId, self.textID.text];
    
    
	if ([self.textID isFirstResponder]) {
		[self.textID resignFirstResponder];
	}
	
	if ([self.textPassword isFirstResponder]) {
		[self.textPassword resignFirstResponder];
	}
	
	if (self.textID.text.length == 0 || self.textPassword.text.length == 0) {
		[ModalAlert notify:@"登陆失败.\n请输入正确的信息！"];
        self.textID.text = [self.textID.text stringByReplacingOccurrencesOfString: RuleId withString:@""];
		self.buttonLogin.userInteractionEnabled = YES;
		return;
	}
	
	mask = [[UIView alloc] initWithFrame:[[self.view window] frame]];
	mask.userInteractionEnabled = NO;
	mask.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.50];
	[self.view addSubview:mask];
	
	[indicator startAnimating];
	[NSThread detachNewThreadSelector:@selector(threadLogin:) toTarget:self withObject:nil];
}

- (void)onWebLogin:(NSString *)userId :(NSString *)userPwd
{
    BOOL result = YES;
    @autoreleasepool {
    
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
        result = [app loginForID:userId withPassword:userPwd];
        [self performSelectorOnMainThread:@selector(callbackAfterLogin:) withObject:[NSNumber numberWithBool:result] waitUntilDone:NO];
    }
}

//웹 16진 색상값으로 RGB색상 가져오기
-(UIColor *) getColor:(NSString *)hexColor
{
    unsigned int red, green, blue;
    NSRange range;
    range.length =2;
    range.location =0;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]]scanHexInt:&red];
    range.location =2;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]]scanHexInt:&green];
    range.location =4;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]]scanHexInt:&blue];
    
    return [UIColor colorWithRed:(float)(red/255.0f)green:(float)(green/255.0f)blue:(float)(blue/255.0f)alpha:1.0f];
}

/*
 - (IBAction) onSupport {
 BOOL flagCall = [ModalAlert confirm:@"기업별 로그인 방법은 연수원 사이트 공지사항을 통해 먼저 확인주세요.\n\n고객센터 1588-6559로 문의 하시겠습니까?"];
 if (flagCall) {
 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:0215886559"]];
 }
 }
 */




- (BOOL) textFieldShouldReturn:(UITextField *)textField {
	if ([self.textID isFirstResponder]) {
		[self.textPassword becomeFirstResponder];
	}
	else {
		[self onLogin];
	}
	return YES;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}



- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.2f];
	//[UIView setAnimationDelegate:self];
    
    CGFloat y = 70;
    
    self.viewImageLoginBox.center = CGPointMake(initLoginBoxPoint.x, initLoginBoxPoint.y - y);
    self.textID.center = CGPointMake(initTextIDPoint.x, initTextIDPoint.y - y);
    self.textPassword.center = CGPointMake(initTextPasswordPoint.x, initTextPasswordPoint.y - y);
    self.buttonLogin.center = CGPointMake(initButtonLoginPoint.x, initButtonLoginPoint.y - y);
    self.loginBackgoundBox.center = CGPointMake(initLoginBoxPoint.x, initLoginBoxPoint.y - y);
    self.loginTxt1.center = CGPointMake(initloginTxt1Point.x, initloginTxt1Point.y - y);
    self.loginTxt2.center = CGPointMake(initloginTxt2Point.x, initloginTxt2Point.y - y);
    self.offlineButton.center = CGPointMake(initOfflineButtonPoint.x, initOfflineButtonPoint.y - y);
    self.LoginTitle.center =CGPointMake(initLoginLogoPoint.x, initLoginLogoPoint.y - y);
    self.GetPwd.center =CGPointMake(initGetPwdPoint.x, initGetPwdPoint.y - y);

    [UIView commitAnimations];

    
    //self.textID.backgroundColor = [self getColor:@"EAE7E8"];
    //self.textPassword.backgroundColor = [self getColor:@"EAE7E8"];
    //textField.backgroundColor = [self getColor:@"CE3574"];
	return YES;
}


// 失去第一响应者时调用
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.layer.borderColor=[[UIColor darkGrayColor] CGColor];
    return YES;
}



- (void) threadLogin:(id)somedata {
	BOOL result = YES;

	
        MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
#if posco_15725
        result = [app loginForID:[NSString stringWithFormat:@"posco-%@", self.textID.text] withPassword:self.textPassword.text];
#elif bossi_16794
        result = [app loginForID:[NSString stringWithFormat:@"bossi-%@", self.textID.text] withPassword:self.textPassword.text];
#else
        result = [app loginForID:self.textID.text withPassword:self.textPassword.text];
#endif
	[self performSelectorOnMainThread:@selector(callbackAfterLogin:) withObject:[NSNumber numberWithBool:result] waitUntilDone:NO];
	
}

// callback for the login result
- (void) callbackAfterLogin:(NSNumber*)resultLogin {
	
	BOOL result = [resultLogin boolValue];
	
	[mask removeFromSuperview];
	[indicator stopAnimating];
	
	self.buttonLogin.userInteractionEnabled = YES;
	
    MLCenterForBusinessAppDelegate* app = [MLCenterForBusinessAppDelegate sharedAppDelegate];
	if (result) {
		//[self.view removeFromSuperview];
		[app openTrainingInstitute];
	}
	else {
        if ([app isNotInternet]) {
            [ModalAlert notify:@"网络状态不稳定！.请先确认网络连接状态."];
        } else {
            self.textID.text = [self.textID.text stringByReplacingOccurrencesOfString: @"hmtc-" withString:@""];
            [ModalAlert notify:@"登陆失败！\n请输入正确的用户信息！"];
        }
	}
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setOfflineButton:nil];
    [self setViewImageBackground:nil];
    [self setViewImageLoginBox:nil];
    [self setTextID:nil];
    [self setTextPassword:nil];
    [self setButtonLogin:nil];

	indicator = nil;
	viewImageCover = nil;
    
    [super viewDidUnload];
}


- (BOOL) shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)offlineClick:(id)sender {
    [[MLCenterForBusinessAppDelegate sharedAppDelegate] offlineStudy];
}

- (IBAction)clickedFindPW:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://hyundaimobile.xiunaichina.com/Login/GetBackPwd?comSeq=1017"]];
}

-(IBAction)textFiledReturnEditing:(id)sender {
    NSLog(@"ok");
  //  [sender resignFirstResponder];
}




@end
