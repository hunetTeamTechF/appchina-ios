
#import "Global.h"
#import "MLCenterForBusinessAppDelegate.h"

@implementation Global
//@synthesize _drmInfo;
@synthesize testPrimaryDomain;
@synthesize testPrimaryDomainFix;


static Global *_global = nil;

- (id)init {
    self = [super init];
    if (self != nil) {
//        self._drmInfo = @"";
        self.testPrimaryDomainFix = @"test.";
    }
    return self;
}

+ (Global *)sharedSingleton {
    @synchronized([Global class]) {
        if (!_global) {
            _global = [[self alloc] init];
        }
        return _global;
    }
    
    return nil;
}

- (NSString *)testPrimaryDomain
{
    BOOL isStaging = [[[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"IsStaging"] boolValue];
    return isStaging ? self.testPrimaryDomainFix : @"";
}

- (NSString *)testPrimaryDomainUrl:(NSString *)url
{
    NSString *returnUrl = url;
    
    if ([self.testPrimaryDomain length] > 0)
    {
        if ([returnUrl hasPrefix:[NSString stringWithFormat:@"http://%@", self.testPrimaryDomainFix]]) {
            return returnUrl;
        }
        
        returnUrl = [returnUrl stringByReplacingOccurrencesOfString:@"http://"
                                                     withString:[NSString stringWithFormat:@"http://%@", self.testPrimaryDomainFix]];
    }
    else
    {
        if ([returnUrl hasPrefix:[NSString stringWithFormat:@"http://%@", self.testPrimaryDomainFix]]) {
            returnUrl = [returnUrl stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"http://%@", self.testPrimaryDomainFix]
                                                             withString:@"http://"];
        }
    }
    return returnUrl;
}

- (NSString *)encodePassword:(NSString *)passwd
{
    NSString *pw  = (__bridge_transfer NSString*)CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)passwd, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8);    
    return pw;
}

+ (id)alloc
{
    @synchronized([Global class])
    {
        NSAssert(_global == nil, @"Attempted to allocate a second instance of a singleton.");
        _global = [super alloc];
        return _global;
    }
    
    return nil;
}

/* 중요 정보 암호화 */
+ (NSString*) getUserId {
    return [self getUserInfo:@"userid"];
}
+ (void) setUserId:(NSString*)text {
    [self setUserInfo:text forKey:@"userid"];
}
+ (NSString*) getUserPassword {
    return [self getUserInfo:@"password"];
}
+ (void) setUserPassword:(NSString*)text {
    [self setUserInfo:text forKey:@"password"];
}
+ (NSString*) getUserInfo:(NSString*)key {
    if ([MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser) {
        NSObject *val = [[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:key];
        if ([[val class] isSubclassOfClass:[NSData class]]) {
            NSData *data = [[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:key];            
            NSString *text = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            text = (__bridge_transfer NSString*)CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)text, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8);
            return text;
        }
        else {
            NSString *text = [[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:key];
            text = (__bridge_transfer NSString*)CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)text, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8);
            return text;
        }
    }
    return @"";
}
+ (void) setUserInfo:(NSString*)text forKey:(NSString*)key {
    if (text && [text length] > 0) {
        NSData *inputData = [text dataUsingEncoding:NSUTF8StringEncoding];
        [[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser setObject:inputData forKey:key];
    }
    else {
        if (!text) {
            text = @"";
        }
        [[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser setObject:text forKey:key];
    }
}


+ (NSString *) getAppType {
    
    // MLearningCenterType 0 또는 nil = 기존연수원, 1 = SAM
    NSString *appType = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppType"];
    if(appType == nil)
        appType = @"0";
    
    return appType;
    
}

+ (NSString *)checkTestDomain:(NSString *)orgUrl {
    NSString *urlPrefix = @"";
    
    BOOL isStaging = [[[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"IsStaging"] boolValue];
    BOOL isStagingTestNoDot = [[[MLCenterForBusinessAppDelegate sharedAppDelegate].profileUser objectForKey:@"IsStagingTestNoDot"] boolValue];
    
    if(isStaging){
        urlPrefix = @"http://test.";
    }else if(isStagingTestNoDot){
        urlPrefix = @"http://test";
    }
    
    // SSL은 test.은 안된다.
    if([orgUrl hasPrefix:@"https://"] && (isStaging || isStagingTestNoDot)){
        urlPrefix = @"https://test";
    }
    
    if([urlPrefix length] != 0){
        orgUrl = [orgUrl stringByReplacingOccurrencesOfString:@"https://" withString:urlPrefix];
        orgUrl = [orgUrl stringByReplacingOccurrencesOfString:@"http://" withString:urlPrefix];
    }
    
    //    return [NSString stringWithFormat:@"%@%@", urlPrefix, orgUrl];
    return orgUrl;
    
}

+ (void)setSharedData:(NSString *)text withKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:text forKey:key];
    [userDefaults synchronize];
}

+ (NSString *)getSharedData:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults objectForKey:key];
}



@end
